(defproject bestway "0.1.0-SNAPSHOT"

  :description "The Lifestyle"
  :url "https://www.bestway.lk/"

  :dependencies [
                 [luminus-log4j "0.1.3"]
                 [cljs-ajax "0.6.0"]
                 [com.cognitect/transit-cljs "0.8.239"]
                 [secretary "1.2.3"]

                 [crypto-random "1.2.0"] ;; random string gen

                 [jarohen/chord "0.8.1"]
                 [bidi "2.0.13" ]  ; cljs <-> clj routes

                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.908"]
                 [org.clojure/tools.cli "0.3.5"]
                 [org.clojure/tools.logging "0.3.1"]

                 [org.omcljs/om "1.0.0-beta1"]
                 [com.andrewmcveigh/cljs-time "0.5.1"]

                 [com.taoensso/encore "2.91.0"] ; cljs utils
                 [com.taoensso/sente "1.11.0"] ; websockets/ajax
                 [com.taoensso/truss "1.5.0"] ;; Assertion Lib

                 [selmer "1.10.7"]
                 [markdown-clj "0.9.89"]
                 [environ "1.0.0"]
                 [ring-middleware-format "0.7.0"]
                 [org.clojure/data.codec "0.1.0"]
                 [metosin/ring-http-response "0.8.2"]
                 [bouncer "1.0.0"]

                 [org.webjars.bower/tether "1.4.0"]
                 [org.webjars/bootstrap "4.0.0-alpha.5"]
                 [org.webjars/font-awesome "4.7.0"]

                 [org.clojure/tools.logging "0.3.1"]

                 [compojure "1.6.0"]
                 [clout "2.1.2"]

                 [re-frame "0.9.2"]
                 [reagent "0.6.1"]
                 [reagent-utils "0.2.1"]
                 [reagent-forms "0.5.29"]

                 [ring-webjars "0.1.1"]
                 [ring/ring-core "1.6.1"]
                 [ring/ring-defaults "0.2.3"]
                 [ring/ring-servlet "1.6.1"]

                 [mount "0.1.11"]
                 [cprop "0.1.10"] ;env variables reader

                 [luminus-nrepl "0.1.4"]
                 [luminus/ring-ttl-session "0.3.2"]

                 [buddy "1.3.0"]
                 [cheshire "5.5.0"]
                 [com.novemberain/monger "3.1.0"]
                 [luminus-http-kit "0.1.3"]
                 [org.clojure/core.async "0.3.442"]
                 [com.draines/postal "2.0.0"]
                 [com.novemberain/pantomime "2.7.0"] ; file MIME
                 [cljsjs/cropper "0.7.0-0"] ; cljs img crop
                 [cljsjs/react-timer-mixin "0.13.2-1"] ; setTimeout/setInterval/setImmediate

                 [com.rpl/specter "0.12.0"]

                 [http-kit "2.2.0"]
                 [ring "1.6.1"]
                 [org.clojure/tools.reader "1.0.0-beta4"]
                 [org.clojure/data.json "0.2.6"]
                 [com.cemerick/url "0.1.1"]
                 [pandect "0.6.1"] ; message digest util/ integrity checksum

                 [alandipert/storage-atom "1.2.4"] ;; to-do mvc
                 [cljsjs/jquery "2.2.4-0"]
                 ;[cljsjs/semantic-ui-react "0.68.2-0"]
                 [soda-ash "0.2.0"] ;; sementic ui

                 [feedme "0.0.3"]
                 [re-com "0.9.0"]
                 [reaver "0.1.2"]
                 [org.clojure/clojure "1.9.0-alpha16"]
                 ]
;  :repositories {"local" ~(str (.toURI (java.io.File. "mvn-repo")))}
  :repositories {"local" "mvn-repo"}
  :min-lein-version "2.0.0"

  :jvm-opts ["-server" "-Dconf=.lein-env"]
  :source-paths ["src/clj" "src/cljc" ]

  :test-paths ["test/clj"]

  :target-path "target/%s/"
  :main  ^:skip-aot bestway.core


  :plugins [[lein-cprop "1.0.1"]
            [lein-environ "1.0.0"]
            [lein-ancient "0.6.5"]
            [lein-cljsbuild "1.1.5"]
;            [lein-immutant "2.1.0"]
;            [org.webjars/webjars-locator-jboss-vfs "0.1.0"]
;            [luminus-immutant "0.2.2"]
            [lein-uberwar "0.2.0"]
            ;[org.clojure/clojurescript "1.9.293"]
            ]

  :resource-paths ["resources/templates/front/includes/" "resources" "resources/templates/front/" "target/cljsbuild"]

:clean-targets ^{:protect false}
[:target-path [:cljsbuild :builds :app :compiler :output-dir] [:cljsbuild :builds :app :compiler :output-to]]

  :figwheel
  {:http-server-root "public"
   :nrepl-port 7002
   :css-dirs ["resources/public/css"]
   :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]
   }

  :profiles
  {:uberjar
                              {:omit-source true
                              :prep-tasks ["compile" ["cljsbuild" "once"]]
                              :cljsbuild {
                               :builds {:app
                                         {:source-paths ["src/cljs" "src/cljc" ]
                                          :compiler {:optimizations :advanced
                                                     ;:output-dir "target/web"
                                                     :externs ["react/externs/react.js" "externs/bestway.js" "externs/jquery-3.2.js" ]
                                                     :elide-asserts true
                                                     :pretty-print false

                                                     :modules       {
                                                                     :common        {:output-to  "../../bestway/public/html/bwland/js/bwinc.js"
                                                                                     :entries    #{"bestway.index.common" "bestway.utils" "bestway.index.websocket"}
                                                                                     }
                                                                     :landing
                                                                                    {:output-to "../../bestway/public/html/bwland/js/landing.js"
                                                                                     :entries   #{"bestway.index.register" "bestway.index.login" }
                                                                                     :depends-on #{:common}
                                                                                     }
                                                                     :settings
                                                                                    {:output-to "../../bestway/public/html/bwland/js/settings.js"
                                                                                     :entries   #{"bestway.index.settings" }
                                                                                     :depends-on #{:common}
                                                                                     }
                                                                     :adpost
                                                                                    {:output-to  "../../bestway/public/html/bwland/js/adpost.js"
                                                                                     :entries   #{"bestway.index.adpost"}
                                                                                     :depends-on #{:common}
                                                                                     }

                                                                     :news        {:output-to  "../../bestway/public/html/bwland/js/news.js"
                                                                                   :entries   #{ "bestway.index.news" }
                                                                                   :depends-on #{:common}
                                                                                   }

                                                                     :search        {:output-to  "../../bestway/public/html/bwland/js/search.js"
                                                                                     :entries   #{ "bestway.index.search" }
                                                                                     :depends-on #{:common}
                                                                                     }

                                                                     :user        {:output-to  "../../bestway/public/html/bwland/js/user.js"
                                                                                   :entries   #{"bestway.index.user"}
                                                                                   :depends-on #{:common}
                                                                                   }

;                                                                     :cljs-base   {:output-to "js/app.js"}
                                                                     }

                                                     }}}}


                              :aot :all
                              :uberjar-name "bestway.jar"
                              :source-paths ["env/prod/clj"]
                              :resource-paths ["env/prod/resources"]}

 ;  :prod {:cljsbuild
 ;         {:builds {:app
 ;                   {:source-paths ["src/cljs" "src/cljc" ]
 ;                    :compiler {:optimizations :advanced
 ;                               ;:output-dir "target/web"
 ;                               :externs ["react/externs/react.js" "externs/bestway.js" "externs/jquery-3.2.js" ]
 ;                               :elide-asserts true
 ;                               :pretty-print false
 ;
 ;                               :modules       {
 ;                                                :common        {:output-to  "../../bestway/public/html/bwland/js/bwinc.js"
 ;                                                               :entries    #{"bestway.index.common" "bestway.utils" "bestway.index.websocket"}
 ;                                                                }
 ;                                               :landing
 ;                                                              {:output-to "../../bestway/public/html/bwland/js/landing.js"
 ;                                                               :entries   #{"bestway.index.register" "bestway.index.login" }
 ;                                                               :depends-on #{:common}
 ;                                                               }
 ;                                               :settings
 ;                                                            {:output-to "../../bestway/public/html/bwland/js/settings.js"
 ;                                                             :entries   #{"bestway.index.settings" }
 ;                                                             :depends-on #{:common}
 ;                                                             }
 ;                                               :adpost
 ;                                                            {:output-to  "../../bestway/public/html/bwland/js/adpost.js"
 ;                                                             :entries   #{"bestway.index.adpost"}
 ;                                                             :depends-on #{:common}
 ;                                                             }
 ;
 ;                                               :news        {:output-to  "../../bestway/public/html/bwland/js/news.js"
 ;                                                             :entries   #{ "bestway.index.news" }
 ;                                                             :depends-on #{:common}
 ;                                                             }
 ;
 ;                                               :search        {:output-to  "../../bestway/public/html/bwland/js/search.js"
 ;                                                             :entries   #{ "bestway.index.search" }
 ;                                                               :depends-on #{:common}
 ;                                                               }
 ;
 ;                                               :user        {:output-to  "../../bestway/public/html/bwland/js/user.js"
 ;                                                               :entries   #{"bestway.index.user"}
 ;                                                               :depends-on #{:common}
 ;                                                               }
 ;
 ;;                                               :cljs-base   {:output-to "js/app.js"}
 ;                                               }
 ;
 ;                               }}}}}
   :dev           [:project/dev :profiles/dev]
   :test          [:project/dev :project/test :profiles/test]

   :project/dev  {:dependencies [
                                 [prone "1.1.4"]
                                 [ring/ring-mock "0.3.0"]
                                 [ring/ring-devel "1.5.1"]
                                 [luminus-http-kit "0.1.4"]
                                 [pjstadig/humane-test-output "0.8.1"]
                                 [binaryage/devtools "0.9.2"]
                                 [com.cemerick/piggieback "0.2.2-SNAPSHOT"]
                                 [directory-naming/naming-java "0.8"]
                                 [doo "0.1.7"]
                                 [figwheel-sidecar "0.5.9"]
                                 [org.clojure/test.check "0.9.0"]
                                 ]
                  :plugins      [
                                 [com.jakemccrary/lein-test-refresh "0.18.1"]
                                 [lein-doo "0.1.7"]
                                 [lein-figwheel "0.5.13"]
                                 [org.clojure/clojurescript "1.9.495"]
                                 ]
                  :cljsbuild
                                {:builds
                                 {:app
                                  {:source-paths ["src/cljs" "src/cljc" "env/dev/cljs"]
                                   ;:figwheel {:on-jsload "bestway.index.common/update-search"}
                                   :compiler
                                                 {:main "bestway.app"
                                                  :asset-path "js/web"
                                                  :output-to "js/web/main.js"
                                                  :output-dir "js/web"
                                                  :source-map true
                                                  :optimizations :none
                                                  :pretty-print true
                                                  :modules       {
                                                                  :common        {:output-to  "js/bwinc.js"
                                                                                  :entries    #{"bestway.index.common" "bestway.utils" "bestway.index.websocket"}
                                                                                  }
                                                                  :landing
                                                                                 {:output-to "js/landing.js"
                                                                                  :entries   #{"bestway.index.register" "bestway.index.login" }
                                                                                  :depends-on #{:common}
                                                                                  }
                                                                  :settings
                                                                                 {:output-to "js/settings.js"
                                                                                  :entries   #{"bestway.index.settings" }
                                                                                  :depends-on #{:common}
                                                                                  }
                                                                  :adpost
                                                                                 {:output-to  "js/adpost.js"
                                                                                  :entries   #{"bestway.index.adpost"}
                                                                                  :depends-on #{:common}
                                                                                  }

                                                                  :news        {:output-to  "js/news.js"
                                                                                :entries   #{ "bestway.index.news" }
                                                                                :depends-on #{:common}
                                                                                }

                                                                  :search        {:output-to  "js/search.js"
                                                                                  :entries   #{ "bestway.index.search" }
                                                                                  :depends-on #{:common}
                                                                                  }

                                                                  :user        {:output-to  "js/user.js"
                                                                                :entries   #{"bestway.index.user" "bestway.bw-chat.core" }
                                                                                :depends-on #{:common}
                                                                                }

                                                                  ; :cljs-base   {:output-to "js/web/main.js"}
                                                                  }

                                                  }}}}


;                  :source-paths ["env/dev/clj" "env/dev/cljs" "src/cljc" "src/cljs" ]

                  :doo {:build "test"}
                  :source-paths ["env/dev/clj"]
                  :resource-paths ["env/dev/resources"]
                  :repl-options {:init-ns user}
                  :injections [(require 'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!)]}
   :project/test {
                  :resource-paths ["env/test/resources"]
                  :cljsbuild
                                  {:builds
                                   {:test
                                    {:source-paths ["src/cljc" "src/cljs" "test/cljs"]
                                     :compiler
                                                   {:output-to "target/test.js"
                                                    :main "bestway.doo-runner"
                                                    :optimizations :whitespace
                                                    :pretty-print true}}}}

                  }
   :profiles/dev {}
   :profiles/test {}})
