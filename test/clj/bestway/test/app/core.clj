(ns bestway.test.app.core

  (:require [clojure.test :refer :all]
            [bestway.layout :as layout]
            [clojure.spec.alpha :as s]
            [ring.mock.request :refer :all]

            [bestway.handler :refer [app]]
            [bestway.db.core :as database]

            [net.cgrand.enlive-html :as html]
            )
  )

(def test-config {:test              true
                  :app-base-location "/home/dan/Projects/best-way/"
                  :app-context       "http://localhost:8088/"
                  :http-url          "http://localhost:8088/"
                  :ws-url            "ws://localhost:8090/"
                  :port              8088
                  ;;websocket port
                  :ws-port           8090
                  ;; when :nrepl-port is set the application starts the nREPL server on load
                  :nrepl-port        7001
                  :public-front-urls [
                                      "/search-results"
                                      "/privacy-policy"
                                      "/terms-service"
                                      "/faq"
                                      "/user"
                                      "/item"]}
)


(database/connect!)





(deftest test-index-page-authentication
  (testing "authenticated response"

    (is (= false (database/get-user "bob" "bob")))
    (is (= true (database/get-user "shandanjay@gmail.com" "123456")))

    )
  )

(deftest ad-post-submission
  (testing "the post ad submission arrived! \"/post-your-ad\""
    (let [ response (app (request :post "/post-your-ad" )) ]
      (is (= (get-in response [:headers "Content-Type"]) "text/html; charset=utf-8"))
      (is (= 200 (:status response) ))
      )
    )

  )


