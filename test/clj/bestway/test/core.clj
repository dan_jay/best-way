(ns bestway.test.core
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [bestway.db.core :as database]
            [bestway.handler :refer :all]))

(database/connect!)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
