(ns bestway.test.handler
  (:require [clojure.test :refer :all]
            [bestway.config :as config]
            [bestway.layout :as layout]
            [ring.mock.request :refer :all]
            [bestway.handler :refer :all]))




(deftest test-app

  (testing "not-found route"
    (let [response (app (request :get "/invalid"))]
      (is (= (:status response) 404))))

  ;(testing "Main URL"
  ;  (let [response (app (request :get "/"))]
  ;        (is (= (layout/*app-context*) ""))
  ;  ))
  ;
  ;
  ;(testing "index"
  ;  (let [response (app (request :get "/"))]
  ;    (is (= (:status response) 200))
  ;    (is (= (get-in response [:headers "Content-Type"]) "application-json"))))
  ;
  ;(testing "lists endpoint"
  ;  (let [response (app (request :get "/lists"))]
  ;    (is (= (:status response) 200))
  ;    (is (= (get-in response [:headers "Content-Type"]) "application-json"))))
  ;
  ;(testing "not-found route"
  ;  (let [response (app (request :get "/bogus-route"))]
  ;    (is (= (:status response) 404))))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
