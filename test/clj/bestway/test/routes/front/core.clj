(ns bestway.test.routes.front.core
  (:require [clojure.test :refer :all]
            [bestway.layout :as layout]
            [clojure.spec.alpha :as s]
            [ring.mock.request :refer :all]
            [bestway.handler :refer [app]]
            [bestway.db.core :as database]

  ;          [bestway.routes.front.core :refer :all]
  )
  )


(def test-config {:test              true
                  :app-base-location "/home/dan/Projects/best-way/"
                  :app-context       "http://localhost:8088/"
                  :http-url          "http://localhost:8088/"
                  :ws-url            "ws://localhost:8090/"
                  :port              8088
                  ;;websocket port
                  :ws-port           8090
                  ;; when :nrepl-port is set the application starts the nREPL server on load
                  :nrepl-port        7001
                  :public-front-urls [
                                      "/search-results"
                                      "/privacy-policy"
                                      "/terms-service"
                                      "/faq"
                                      "/user"
                                      "/item"]}
  )


(deftest front-login-route-test

  (testing "index"
    (let [ response (app (request :get "/")) ]
      (is (= (get-in response [:headers "Content-Type"]) "text/html; charset=utf-8"))
      (is (= (:status response) 200))
      )
  )



  (testing "visitor allowed static urls"
    ;(for [v ["/search-results"
    ;         "/privacy-policy"
    ;         "/terms-service"
    ;         "/faq"
    ;         ]]
    (binding [layout/*app-context* (:app-context test-config)
              layout/*visitor-allowed-urls* (:public-front-urls test-config)]
      (let [response (app (request :get (str layout/*app-context* "search-results") {:headers {:referer "http://localhost:8088/"}}))]

        (is (= (:status response) 302))   ;;;??????
        (is (= (get-in response [:session :identity]) nil))
        )

      ;)
      )
    )
  )