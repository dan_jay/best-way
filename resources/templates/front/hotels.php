<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway Hotels</title>
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->

<style type="text/css">
    body{
    	background-color: #FCFCFC;
    }
    #j-nav-con{
        padding: 5px 0px;
    }
    #cover-con{
        min-height: 250px;
        background-image: url("<?php echo base_url(); ?>assets/img/hotels/ban1.jpg");
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    #s-dic{
        
    }
    #s-dic h1{
        color: #FFFFFF;
        font-size: 40px;
        font-weight: bolder;
        text-align: center;
        text-shadow: 0px 5px 1px rgba(0,0,0,0.3);
    }
    #s-dic h2{
        color: #FFFFFF;
        font-size: 30px;
        font-weight: normal;
        text-align: center;
        text-shadow: 0px 5px 1px rgba(0,0,0,0.3);
    }
	#txt-slider{
        background-color: #3863A0;
        padding: 10px;
    }
    #txt-slider #horvac span{
        color: #ffffff;
        font-weight: bold;
        margin: 0px 10px;
    }
    .foh {
        text-align: center;
        padding: 5px;
    }
    .vacan-con{
        margin: 5px 0px;
        height: 60px;
        background-color: #fff;
        border-radius: 4px;
        border: 1px solid #c3c3c3;
        box-shadow: 2px 2px 5px rgba(0,0,0,1);
        overflow: hidden;
        -webkit-transition: box-shadow .2s;
        transition: box-shadow .2s;
    }
    .vacan-con h3{
        color: #3863a0;
        display: block;
        font-size: 12px;
        font-weight: 600;
        max-width: 100%;
        overflow: hidden;
        text-decoration: none;
        text-align: center;
        /*text-overflow: ellipsis;*/
        /*white-space: nowrap;*/
    }
    .ui.button{
        border-radius: 2px;
        margin: 2px;
    }
</style>
</head>
<body>
<div id="main-con">
    <!-- top bar navigation goes here -->
    <?php $this->load->view('includes/top_nav'); ?>
    <div id="j-nav-con" class="container">
        <span class="ui green button">All Vacancies</span>
        <a href="<?php echo base_url(); ?>hotels/view_hotel"><span class="ui green button">View Hotel</span></a>
        <span class="ui green button">Upload a CV</span>
        <span class="ui green button">Partners</span>
        <span class="ui green button">Education</span>
    </div>
    <header id="cover-con" class="container-fluid">
        <div id="s-dic" class="container">
            <h1>Bestway offers you the Best</h1>
            <h2>Vacanvies - Employees - Freelancers</h2>
            <?php $this->load->view('includes/jobsearch'); ?>
        </div>
    </header>
    <div id="txt-slider" class="container-fluid">
        <div class="container">
            <div class="row ">
                <div class="visible-xs-block">
                    <div class="foh col-xs-12">
                        <div class="foh-con">
                            <button class="button is-green_candy">Find a Job</button>                       
                        </div>
                    </div>
                    <div class="foh col-xs-12">
                        <div class="foh-con">                        
                            <button class="button is-green_candy">Hire an Employee</button>
                        </div>
                    </div>
                </div>
                <div id="horvac" class="hidden-xs">
                    <div class="foh">
                        <div class="foh-con">
                            <button class="button is-green_candy">Need to edit find hotel</button>
                            <span>or</span>
                            <button class="button is-green_candy">Need to edit find a reservation</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">   
        <div class="row">
                
        </div>
    </div> 
</div>
<?php $this->load->view('includes/footer'); ?>

    <script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/semantic.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/app/chart.js"></script>
    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        })
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

        $('.fancybox').fancybox();

            /*
             *  Different effects
             */
        });
    </script>

</body>
</html>