<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <base href="/front/" ></base> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/AdminLTE.css">
    <!-- <link href="css/chosen.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <style type="text/css">
    body{
    	background-color: #F4F4F4;
    }
	#home-con{
		margin-top: 5px;
	}
    #gr_wh{
        width: 97.8%;
        padding: 6px 10px 6px 0px;
        margin-bottom: 10px;
        margin-right: 10px;
        background-color: #FFFFFF;
        border-radius: 4px;
        border: 1px solid #990066;
        height: 90px;
    }
    #gr_wh_nm{
        margin-top: -28px;
        font-weight: bolder;
        color: #5c4c5c;
        font-size: 16px;
    }
    #mainAds h1{
      white-space: nowrap;
      width: 298px;
      overflow: hidden;
      text-overflow: ellipsis;
      color: #3B5998;
    }
    #gr_wh_pro_pic{
        /*width: 50px; */
        height: 80px;
        margin-right: 10px;
         border: 1px solid #ccc;
    }
    #gr_wh .ui.label{
        margin-left: -4px;
    }
    /*.visual-area{
        width: 80px !important;
        height: 80px !important;
        margin: 0 6px !important;
        padding: 0px !important;
    }*/

    #leftCol{
        background-color: #F2EAF0;
    }
    .ui.purple.label{
        background-color: #990066 !important;
        border-color: #330022 !important;
    }
    .ad_img_con{
      background-color: #f7f7f7;
      border: 1px solid #ccc;
      margin: 2px;
      display: inline-block;
      width: 310px;
    }

    @media screen and (max-width: 980px) {
      #mainSearchCon {
        background-color: #3863A0;
        display: none;
      }
    }
    .AnimalsPetSupplies, .OfficeSchoolSupplies, .Properties, .ReligiousCeremonial, .SecurityProtection,
    .Services, .Software, .SportingGoods, .ToysGames, .VehiclesParts,
    .WeddingsEvents, .ApparelAccessories, .Antiques, .BusinessIndustrial, .Finance,
    .FlowersGift, .FoodBeverages, .Furniture, .Hardware, .HealthBeauty,
    .HomeGarden, .Jobs, .ArtsHobbies, .LuggageBags, .Media,
    .MotherKids, .NoveltySpecialUse, .CamerasOptics, .CraftsPaintingsPottery, .Education,
    .Electronics, .Entertainment, .HotelsTravel
    {
      background-size: 550% 650%;
      width: 28px;
      height: 28px;
    }
    .AnimalsPetSupplies{ background-position: -1px 4px;  }
    .ApparelAccessories{ background-position: -30px -49px;  }
    .ArtsHobbies{ background-position: -59px -101px;  }
    .CamerasOptics{ background-position: -59px -127px;  }
    .CraftsPaintingsPottery{ background-position: -89px -128px;  }
    .Education{ background-position: -117px -128px;  }
    .Electronics{ background-position: -1px -153px;  }
    .Entertainment{ background-position: -30px -154px;  }
    .HotelsTravel{ background-position: -57px -154px;  }
    .OfficeSchoolSupplies{ background-position: -30px 4px;  }
    .Properties{ background-position: -59px 5px;  }
    .ReligiousCeremonial{ background-position: -88px 3px;  }
    .SecurityProtection{ background-position: -116px 3px;  }
    .Services{ background-position: -1px -23px;  }
    .Software{ background-position: -30px -25px;  }
    .SportingGoods{ background-position: -59px -23px;  }
    .ToysGames{ background-position: -87px -24.19px;  }
    .VehiclesParts{ background-position: -116px -24px;  }
    .WeddingsEvents{ background-position: -1px -50px;  }
    .Antiques{ background-position: -59px -51px;  }
    .BusinessIndustrial{ background-position: -88px -50px;  }
    .Finance{ background-position: -117px -50px;  }
    .FlowersGift{ background-position: -1px -75px;  }
    .FoodBeverages{ background-position: -30px -76px;  }
    .Furniture{ background-position: -60px -77px;  }
    .Hardware{ background-position: -88px -77px;  }
    .HealthBeauty{ background-position: -116px -76px;  }
    .HomeGarden{ background-position: -1px -103px;  }
    .Jobs{ background-position: -30px -102px;  }
    .LuggageBags{ background-position: -87px -101.7px;  }
    .Media{ background-position: -117px -102px;  }
    .MotherKids{ background-position: -1px -126px;  }
    .NoveltySpecialUse{ background-position: -30px -127px;  }
    a .cat-text{
      vertical-align: middle;
      margin-top: -15px;
      display: inline-block;
    }
    #leftCol{
        padding-left: 0px;
        padding-right: 0px;
    }
    @media (max-width: 980px) {
      #leftCol{
          display: none;
          margin-bottom: 10px;
      }
      #rightCol{
        display: none;
      }
    }
</style>
</head>
<body>
<div id="main-con">
<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}


<!-- promo bar goes here -->
    {% include "front/includes/promo_bar.php" %}

    {% include "front/includes/top-hor-menu.php" %}
     <!-- <div class="global-page"> -->
        {% include "front/includes/main_search.php" %}
    <!-- </div> -->

<div id="main-wrap" class="container">


	<div id="home-con" class="row">

        <div id="leftCol" class="col-md-2">
            <!-- main_cat.php -->
            {% include "front/includes/sidebar.php" %}
        </div>
        <div class="col-md-7">
<!-- /Special AdBox -->
            <div id="contentArea" class="row">
                <!-- Ad layout in home page-->
                <div id="gr_wh" class="col-md-12" >
                  <span class="ui purple right ribbon label">Welcome !</span>
                  <div class="pg_pr_name" >
                    <div id="home_img_container" class="visual-area" style="float: left;">
                        <!--<img id="gr_wh_pro_pic"  height="80px" src="{{ profile-pic }}" style="height: 80px!important;">-->
                    </div>
                    <p id="gr_wh_nm">
                     Hi, {{fnamelname}}
                    </p>
                  </div>
                </div>
                <div id="mainAds_container" >
                    <div id="listing_container" >
                    </div>
                </div>
                <div>

                </div>

            </div>
        </div>

        <div id="rightCol" class="col-md-3">
            {%comment%}
            {% include "front/includes/todo.php" %}
            {%endcomment%}
            <div class="fb-page" data-href="https://www.facebook.com/Bestwaylanka/" data-tabs="timeline" data-small-header="false"
                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div
                    class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Bestwaylanka/"><a
                    href="https://www.facebook.com/Bestwaylanka/">Bestway.lk</a></blockquote></div></div>
            <p id="sp">sponsored</p>
<!-- Special AdBox -->
            <div id="sp_ad_x1" class="sp_ad">
                <img src="img/s1.png" />

                <h1><a href="#">Ad Name</a></h1>

                <p class="ad_desc">Ad description</p>
            </div>
            <div id="sp_ad_x2" class="sp_ad">
                <img src="img/s2.png" />

                <h1><a href="#">Ad Name</a></h1>

                <p class="ad_desc">Ad description</p>
            </div>
        </div>
    </div>
</div>
</div>
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts.php" %}
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/sidebar.js"></script>
<script>
$(document).ready(function(){
    $("#shw-srch").click(function(){
        $("#mainSearchCon").slideToggle("slow");
    });
    //$('select').chosen({width: '100%'});
    $('#sh-cat').click(function(){
        $("#leftCol").slideToggle("slow");
      //  $('#contentArea').removeClass("col-md-7").addClass("col-md-9");
    });
    //$(this).removeClass("classname");
});
</script>
</body>
</html>
