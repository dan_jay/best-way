<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<title>Bestway Search</title>
{% include "front/includes/head-assets.php" %}
<link href="https://fonts.googleapis.com/css?family=Lobster|Quicksand" rel="stylesheet"/>

<style type="text/css">
body{
	background-color: #F9F9F9;
}
#allads{
	margin-top: 5px;
	padding: 0px;
}
#contentArea{
	padding-right: 5px;
}
#ad-con ul {
	list-style: none;
	padding: 0px;
	margin-top: 5px;
}
#ad-con .grid{
	margin-right: 5px;
  margin-left: 4px;
}

#ad-con .buttons {
	right: 10px;
  	position: absolute;
}
#ad-con .list li {
	border: 1px solid #d3e0e9;
	margin-bottom: 5px;
	padding-bottom: 10px;
}
#ad-con .grid li {
	float: left;
	border: 1px solid #d3e0e9;
	width: 332px;
	height: auto;
  margin-top: 1px;
}
#ad-con .grid li:nth-child(even){
  margin-right: 0px !important;
  margin-left: 0.14374999rem;
}
#ad-con .grid li:nth-child(1), #ad-con .grid li:nth-child(2){
  margin-top: 5px;
}
#ad-con .segment{
  margin-left: 4px;
}
#ad-list-con{
	overflow: auto;
	margin: 0px;
}
#pagi-con-top, #pagi-con-bot{
  margin-left: 4px;
}
#pagi-con-top {
  margin-bottom: 10px;
}
#pagi-con-bot{
  margin-top: 10px;
}
#pagi-con-top .ui.menu, #pagi-con-bot .ui.menu {
    border: 1px solid rgba(34, 36, 38, 0.005);
}
/*.grid .ad{
	width: 335px;
	height: auto;
	margin: 0.197321421rem;
}*/
.grid .ad h1{
  white-space: nowrap;
  width: 298px;
  overflow: hidden;
  text-overflow: ellipsis;
}
.ad .itm_img{
	border: 1px solid #d3e0e9;
}
.grid .adprice, .list .adprice{
	font-size: 1.5em;
	font-family: 'Lato', sans-serif;
	font-weight: 600;
	color: #FF4500;
	display: inline;
}
.grid .itm_img_con{
	margin-bottom: 5px;
}
.grid .ad_txt{
	margin-top: 2px;
	font-size: 12px;
}
.list .ad{
	height: 215px;
}
.list img{
	width: 140px;
}
.list .itm_img_con{
	margin-right: 15px;
}
.list .itm_img_con{
	float: left;
}
.list .txt_con{
}
.grid .ad h1, .list .ad h1{
  color: #3B5998;
}
.price .ui.label{
	padding-left: 0.2em !important;
	padding-right: 0.2em !important;
}
.price .field{
	padding-left: 0.25em !important;
  padding-right: 0.25em !important;
}
#price-range{
	font-size: 11px;
  line-height: 36px;
}
#price-refine{
	font-size: 13px;
  padding: 0.45em;
  margin-top: 10px;
}
/******** Overrides *********/
#leftcol_con{
	padding-right: 0px;
}
#content_area_con{
	padding-left: 0px;
}
#leftcol{
	background-color: #FFFFFF;
	border: 1px solid #d3e0e9;
	padding: 5px;
	border-radius: 4px;
}
#leftcol .search-title-txt-main{
  font-family: 'Quicksand', sans-serif;
  margin-top: 5px;
  display: inline-block;
  font-size: 16px;
}
#leftcol .search-title-txt-main, #leftcol .search-title-txt{
  color: #990066;
	font-weight: bold;
}
#leftcol .search-title-txt{
  margin-top: 20px;
	font-size: 14px;
}
#leftcol .fa{
  margin: 5px 10px 10px 10px;
  font-size: 1.2em;
}
#steps-fivepercent-slider {
  visibility: visible;
  opacity: 1;
  top: -30px;
}
.ui-widget-header {
	background: #96BF49;
}
.range_price{
	font-size: 13px !important;
	padding: 5px !important;
	font-weight: bold;
	color: #666 !important;
}
.ui.segment {
	margin: 0.5rem 0rem;
}
.ad .ui.segment{
	margin: -5px -15px -10px;
	height: 100px;
	border-radius: 0px;
}
.list .ad:nth-child(2n) {
	margin-left: 0px;
}
.ui.labeled.input > .label {
	font-size: 0.75rem;
	line-height: 21px;
}
/****************************/
#no-search-itm .ui.segment{
  background-color: rgba(153, 0, 102, 0.07);
}
#no-search-itm{
  text-align: center;
}
#no-search-itm #sr-txt{
  font-family: 'Lobster', cursive;
  font-size: 3.5em;
}
#no-search-itm #no-res-txt{
  font-family: 'Quicksand', sans-serif;
  font-size: 1.55em;
  margin-top: 10px;
}

</style>

</head>
<body>

<div id="main-con">
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}

    {% include "front/includes/promo_bar.php" %}

    <div id="main-wrap" class="container">
        <div id="allads" class="row">
            <div id="leftcol_con" class="col-md-3">
                <div id="leftcol">
                    <div id="search-ref" class="row">
                        <div class="col-xs-12"><i class="fa fa-search" aria-hidden="true"></i><h3 class="search-title-txt-main">Filter your results</h3></div>
                        <div class="col-xs-12">
                            <button id="filter-reset" class="ui button fluid red disabled">Reset</button>
                        </div>
                    </div>


                    <div class="ui divider"></div>
                    <form class="ui form">

                        <div class="ui action input fluid">
                            <input id="txt-query" placeholder="Search..." type="text"/>
                            <button id="btn-query" class="ui icon orange button">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>


                        <h4 class="search-title-txt">Category</h4>
                        <select id="cat-select" name="cat-select" class="ui fluid dropdown">
                            <option value="any">Any Category</option>
                            {% for y in catz %}
                            {{y|safe}}
                            {% endfor %}
                        </select>

                        <h4 class="search-title-txt">Sub Category</h4>
                        <select id="sub-cat-select" class="ui fluid dropdown" name="sub-cat-select">
                            <option value="any">Sub Category</option>
                            {% for y in subcatz %}

                            {% for z in y %}
                            {{z|safe}}
                            {% endfor %}

                            {% endfor %}

                        </select>

                        <h4 class="search-title-txt">Price Range</h4>
                        <div class="field">
                            <div class="two fields price">
                                <div class="field">
                                    <div class="ui left labeled input">
                                        <div class="ui label">Rs.</div>
                                        <input id="min-price" type="number" placeholder="0.00" class="range_price"/>
                                    </div>
                                </div>
                                <span id="price-range">to</span>
                                <div class="field price">
                                    <div class="ui left labeled input">
                                        <div class="ui label">Rs.</div>
                                        <input id="max-price" placeholder="10000.00" type="number" class="range_price"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="search-title-txt">Location</h4>
                        <select class="ui fluid dropdown" id="loc-select">
                            <option value="any">Any District</option>
                            {% for y in locs.districts %}
                            {{y|safe}}
                            {% endfor %}
                        </select>
                        <button id="price-refine" class="ui icon orange button fluid">
                            <i class="fa fa-search" aria-hidden="true"></i>Refine Search
                        </button>
                    </form>
                </div>
            </div>
            <div id="content_area_con" class="col-md-7">
                <div id="contentArea">
                    <article id="class-con">
                        <div id="ad-con">
                            <div class="ui segment">
                                <select class="ui dropdown" id="res-p-page">
                                    <option value="">Results per page</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                </select>
                                <select class="ui dropdown" id="sort-search">
                                    <option value="1">Latest first</option>
                                    <option value="2">Oldest first</option>
                                    <option value="3">Highest price first</option>
                                    <option value="4">Lowest price first</option>
                                    <option value="5">Sort by most viewed</option>
                                    <option value="6">Sort by highest rated</option>
                                </select>

                                <div class="ui right buttons list-selected" id="grid-list-wrapper">
                                    <span id="grid-viewer" class="btn btn-info grid"><i
                                            class="fa fa-th-large"></i></span>
                                    <span id="list-viewer" class="btn btn-info list"><i
                                            class="fa fa-th-list"></i></span>
                                </div>
                            </div>

                            <div id="pagi-con-top"></div>

                            <div id="ad-list-con" class="row"></div>

                            <div id="pagi-con-bot"></div>

                        </div>
                    </article>
                </div>
            </div>

            <div id="rightcol" class="col-md-2">
                <div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/semantic.js" type="text/javascript"></script>

{% script "/js/web/main.js" %}
{% script "/js/bwinc.js" %}
{% script "js/search.js" %}

<script src="js/jquery.fancybox.js" type="text/javascript"></script>
<script src="js/chosen.jquery.js" type="text/javascript"></script>



<script type="text/javascript">

$(document).ready(function() {

                  /*
             *  Categories
             */
            $('#cat-select').chosen().on('change',
                 function(){
                            var chosenId =  $(this).val();
                            var sub = $(this).parent().parent().find('#sub-cat-select');
                            sub.find('option').not('option:first').hide();
                            sub.find('[data-parent-id='+chosenId+']').show();
                            sub.trigger('chosen:updated');
                }
            );
			$('#sub-cat-select').chosen();
            $('#loc-select').dropdown();
            $('#res-p-page').dropdown();
            $('#sort-search').dropdown();



//	$('div.ui.right.buttons span').on('click',function(e) {
//            if ($(this).hasClass('grid')) {
//                $('#ad-list-con ul').removeClass('list').addClass('grid');
//            }
//            else if($(this).hasClass('list')) {
//                $('#ad-list-con ul').removeClass('grid').addClass('list');
//            }
//     });
});

</script>
<!-- page footer goes here -->
{% include "front/includes/footer.php" %}



</body>
</html>
