<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>All Classified</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css" />

<style type="text/css">
	body{
		background-color: #F9F9F9;
	}
	#allads{
		margin-top: 5px;
		padding: 0px;
	}
	#contentArea{
		padding-right: 5px;
	}
	#ad-con ul { 
		list-style: none;
		padding: 0px;
	}
	#ad-con .buttons { 
		margin-bottom: 20px; 
	}
	#ad-con .list li { 
		width: 100%; 
		border: 1px solid #d3e0e9;
		border-right: none;
		border-left: none; 
		margin-bottom: 10px; 
		padding-bottom: 10px; 
	}
	#ad-con .grid li { 
		float: left;
		border: 1px solid #CCC;
	}
	#ad-list-con{
		overflow: auto;
		margin: 0px;
	}
	.grid .ad{
		width: 324px;
		height: auto;
		margin: 0px 0px 5px 3px;
	}
	.ad .itm_img{
		border: 1px solid #d3e0e9;
	}
	.grid .adprice, .list .adprice{
		font-size: 1.7em;
	    font-family: 'Roboto', sans-serif;
	    font-weight: 600;
	    color: #009432;
	    display: inline;
	}
	.grid .itm_img_con{
		margin-bottom: 5px;
	}
	.grid .ad_txt{
		margin-top: 2px;
		font-size: 12px;
	}
	.list .ad{
		height: auto;
	}
	.list img{
		width: 140px;
	}
	.list .itm_img_con{
		margin-right: 25px;
	}
	.list .itm_img_con{
		float: left;
	}
	.list .txt_con{
	}
</style>
</head>
<body>
<div id="main-con">
	<!-- top bar navigation goes here -->
	<?php $this->load->view('includes/top_nav'); ?>

	<?php $this->load->view('includes/promo_bar'); ?>
	<div class="global-page">
        <?php $this->load->view('includes/main_search'); ?>
	</div>
	<div id="main-wrap" class="globalCon">

		<div id="allads">
			<div id="leftCol">
			    <?php $this->load->view('includes/main_cat'); ?>
			</div>
			<div id="contentCol">
				<div id="rightCol"></div>
	            <div id="contentArea">
	            	<article id="class-con">
	            		<div id="ad-con">
						    <div class="buttons">
						        <span class="btn btn-info grid">Grid View</span>
						        <span class="btn btn-info list">List View</span>
						    </div>
						    <div id="ad-list-con" class="row">
							    <ul class="list">
							        <li id="" class="ad col-md-6">
							        	<div class="itm_wrapper">
									        <h1><a href="#">Ad Name</a></h1>

					                        <h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
					                        <div class="itm_img_con">
					                        	<img src="<?php echo base_url(); ?>assets/img/2.jpg" class="itm_img">
					                        </div>
					                        <div class="txt_con">
						                        <p class="adprice">Rs. 12,000.00</p>
						                        <p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
					                        </div>
				                        </div>
							        </li>
							        <li id="" class="ad col-md-6">
								        <div class="itm_wrapper">
									        <h1><a href="#">Ad Name</a></h1>

					                        <h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
					                        <div class="itm_img_con">
					                        	<img src="<?php echo base_url(); ?>assets/img/2.jpg" class="itm_img">
					                        </div>
					                        <div class="txt_con">
						                        <p class="adprice">Rs. 12,000.00</p>
						                        <p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
					                        </div>
					                    </div>
							        </li>
							    </ul>
						    </div>
						</div>  
	            	</article>
	            </div>
	        </div>
		</div>

	</div>
</div>

<!-- page footer goes here -->
<?php $this->load->view('includes/footer'); ?>

	<script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/semantic.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/app/chart.js"></script>
    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        })
        $('span').on('click',function(e) {
		    if ($(this).hasClass('grid')) {
		        $('#ad-list-con ul').removeClass('list').addClass('grid');
		    }
		    else if($(this).hasClass('list')) {
		        $('#ad-list-con ul').removeClass('grid').addClass('list');
		    }
		});
		$(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

        $('.fancybox').fancybox();

            /*
             *  Different effects
             */
        });
    </script>
</body>
</html>