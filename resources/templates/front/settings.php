<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Account Settings</title>
  {% include "front/includes/head-assets.php" %}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link href="https://www.bestway.lk/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="https://www.bestway.lk/css/reagent-forms.css" rel="stylesheet" type="text/css" />

<style type="text/css">
	body{
		background-color: #F9F9F9;
	}
  #main-wrap>h1{
    text-shadow: 2px 3px 2px #dedede;
  }
  #content_area_con{
    padding-left: 0px;
    padding-right: 0px
  }
  #main-wrap{
    padding-top: 20px;
  }
	/******** Overrides *********/
  .segment{
    margin-top: 0px !important;
  }
  .ui.form{
    overflow: auto;
  }
  .ui .menu .item a{
    font-size: 0.95rem !important;
  }
  .fa{
    margin-right: 10px;
  }

  .settings.positive.ui.button{
        float: right;

  }

	/****************************/
    .typeahead-input {
            margin-top: -9px!important;
                }

	.typeahead-list {
	            width: 53%;
	           list-style-type: none;
              left: 0px;
              border-radius: 0em 0em 0.28571429rem 0.28571429rem;
              background: #ffffff;
              margin: 0em 0px 0px;
              box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.08);
              -webkit-box-orient: vertical !important;
              -webkit-box-direction: normal !important;
              -webkit-flex-direction: column !important;
              -ms-flex-direction: column !important;
              flex-direction: column !important;
}
	.typeahead-list li {
              margin: 0;
              text-align: left;
              font-size: 1em !important;
              padding: 0.71428571em 1.14285714em !important;
              background: transparent !important;
              color: rgba(0, 0, 0, 0.87) !important;
              text-transform: none !important;
              font-weight: normal !important;
              box-shadow: none !important;
              -webkit-transition: none !important;
              transition: none !important;
}
    .typeahead-list li:hover{
              background: rgba(0, 0, 0, 0.05) !important;
              color: rgba(0, 0, 0, 0.95) !important;
    }
    .set-err{
            max-width: 150px !important;
    }
    a.set {
        cursor: pointer;
    }
</style>
</head>
<body>
<div id="main-con">
	<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}

	<div id="main-wrap" class="container">
    <h1><i class="fa fa-cogs"></i>Settings</h1>
		<div id="settings" class="row">
			<div id="leftcol_con" class="col-md-10">
				<div id="content_area">
          <div class="ui stackable grid">
              {%csrftok%}
            <div id="settings_menu" class="four wide column">

            </div>

            <div id="settings_con" class="twelve wide column">

            </div>

          </div>
      	</div>
      </div>

			<div id="rightcol" class="col-md-2">
				<div id="right_col">
				</div>
			</div>

		</div>

	</div>

</div>

<!-- page footer goes here -->
{% include "front/includes/footer.php" %}

<script src="https://www.bestway.lk/js/jquery.js" type="text/javascript"></script>
<script src="https://www.bestway.lk/js/bootstrap.min.js"></script>
<script src="https://www.bestway.lk/js/semantic.js" type="text/javascript"></script>
<script src="https://www.bestway.lk/js/chosen.jquery.js" type="text/javascript"></script>


<script>
    window.app_context = "{{app-context}}";
</script>

{% script "https://www.bestway.lk/js/web/main.js" %}
{% script "https://www.bestway.lk/js/bwinc.js" %}
{% script "https://www.bestway.lk/js/settings.js" %}
    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        };
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        })

		$(document).ready(function() {

            /*
             *  Different effects
             */
      $('.ui.checkbox').checkbox();


    });
    </script>

</body>
</html>
