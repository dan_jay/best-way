<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <base href="/front/" ></base> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway | Terms of Service</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway|Crimson+Text|Open+Sans+Condensed:300|Peddana" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/AdminLTE.css">
    <link rel="stylesheet" href="css/home.css">
    <!-- <link href="css/chosen.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <style type="text/css">
    h1{
      font-family: 'Raleway', sans-serif;
      font-size: 35px;
      line-height: 40px;
      color: #990066;
    }
    h2{
      font-family: 'Open Sans Condensed', sans-serif;
      font-size: 30px;
      color: #FF4500;
    }
    p{
      font-family: 'Crimson Text', serif;
      font-size: 16px;
      line-height: 24px;
    }
    p ul li{
      font-family: 'Peddana', serif;
      font-size: 18px;
      line-height: 22px;
    }
    p, li{
      color: #AAAAAA;
    }
    </style>
    </head>
    <body>
    <div id="main-con">
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}


    <!-- promo bar goes here -->
        {% include "front/includes/promo_bar.php" %}

        {% include "front/includes/top-hor-menu.php" %}
         <!-- <div class="global-page"> -->
            {% include "front/includes/main_search.php" %}
        <!-- </div> -->

    <div id="main-wrap" class="container">
<h1>Bestway.lk Terms of Service</h1>

<p>Welcome and thank you for visiting <a href="https://www.bestway.lk">Bestway.lk</a>. Please read and follow our house rules before accessing our services.</p>

<h2>Accepting our terms.</h2>
<p>Our Terms are a legally binding contract between you and Bestway. Please read these Terms of Service carefully before using the <a href="https://www.bestway.lk">Bestway.lk</a> website (the "Service") operated by Bestway. Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service. By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>

<h2>Registration.</h2>
<p>To sign up for the Services, you must create an account (an "Account") by registering for an Account on the Services (an "Account") or logging in through your Facebook or Google account (each a "Third Party Account") using your Facebook ID or Google ID. You agree not to create an Account or use the Services if you have been previously removed by us or banned from any of the Services. We reserve the right to suspend or terminate your Account and refuse any and all current or future use of the Services at any time for any reason. You must provide accurate and complete information and keep your Account information updated. You shall not select or use, as a username, a name of another person with the intent to impersonate that person or use, as a username, a name subject to any rights of a person other than you without appropriate authorization; or  use, as a username, a name that is otherwise offensive, vulgar or obscene. You are solely responsible for the activity that occurs on your Account, and for keeping your Account password secured.</p>

<h2>Links To Other Web Sites.</h2>
<p>Our Service may contain links to third­party web sites or services that are not owned or controlled by Bestway. Bestway has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Bestway shall not be responsible or liable,directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services. We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>

<h2>Our rules for our members.</h2>
<p>Members,</p>
<ul>
  <li>Shall not misrepresent their identities.</li>
  <li>Must always provide valid and complete contact information and must always have a valid email address.</li>
  <li>Shall not publish contact information of other members in an online public area.</li>
  <li>Shall not use website services to send threats or spam.</li>
  <li>Shall always post or upload content or items in appropriate categories in our site.</li>
  <li>Shall not fail to pay for items purchased by you/ fail to deliver items sold by you.</li>
  <li>Shall not post false or misleading content.</li>
  <li>Shall not transfer your bestway account to another party without our consent.</li>
  <li>Shall not distribute viruses or any other technologies that may harm our website or other or the interests or property of users.</li>
  <li>Shall not harvest or otherwise collect information about users without their consent.</li>
</ul>

<h2>Payments.</h2>
<p>We use a third-party payment processor (the "Payment Processor") to allow you to pay for products purchased through the Services. The processing of payments will be subject to the terms, conditions and privacy policies of the Payment Processor in addition to this Agreement. We are not responsible for error by the Payment Processor. By choosing to purchase goods through the Services, you agree to pay through the Payment Processor, all charges at the prices then in effect for your purchase in accordance with the applicable payment terms and you authorize us, via the applicable Payment Processor, to charge your chosen payment provider ("Payment Method"). Such charges for your purchase may include shipping fees and state and local sales tax, the amount of which varies due to factors including the type of item purchased and the shipping destination. You agree to make payment using that selected Payment Method. We reserve the right to correct any errors or mistakes that it makes even if it has already requested or received payment.</p>
<p>You acknowledge and agree that;</p>
<ol>
  <li>your purchases through the Services are transactions between you and the brand of such purchases, and not with us or any of our affiliates and</li> 
  <li>we are not a party to your payment transaction for such purchases and we are not a buyer or a seller in connection with such transactions.</li>
</ol>

<h2>Governing Law.</h2>
<p>These Terms shall be governed and construed in accordance with the laws of Sri Lanka, without regard to its conflict of law provisions.Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>

<h2>Changes.</h2>
<p>We reserve the right to modify or replace these Terms at any time. If a revision is material we will try to provide at least 15 days’ notice prior to any new  terms taking effect. What constitutes a material change will be determined at our sole discretion. By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p>

<h2>Termination.</h2>
<p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

<h2>Contacting Us.</h2>

<p>If there are any questions regarding this terms of services, you may contact us using the information below.</p>
<p>Website : <a href="https://www.bestway.lk">Bestway.lk</a></p>
<p>Email : <a href="contact@bestway.lk">contact@bestway.lk</a></p>

<p>Last Edited on 2016-12-18</p>

</div>
</div>
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts.php" %}
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/sidebar.js"></script>
<script>
$(document).ready(function(){
    $("#shw-srch").click(function(){
        $("#mainSearchCon").slideToggle("slow");
    });
    //$('select').chosen({width: '100%'});
    $('#sh-cat').click(function(){
        $("#leftCol").slideToggle("slow");
      //  $('#contentArea').removeClass("col-md-7").addClass("col-md-9");
    });
    //$(this).removeClass("classname");
});
</script>
</body>
</html>
