<!DOCTYPE html>
<html lang="en">
<head >
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>{{item.title|capitalize}}</title>

    {% include "front/includes/head-assets.php" %}

    <!-- jQuery library -->
    <script src="{{app-context}}js/jquery.js" type="text/javascript"></script>
    <!-- <link rel="stylesheet" href="css/demo.css" /> -->

    <!-- desoSlide styles -->
    <link href="{{app-context}}css/jquery.desoslide.css" rel="stylesheet"/>

    <!-- Animate, used for slides transition (if you set effect.provider to 'animate', it's the default value) -->
    <link href="{{app-context}}css/animate.min.css" rel="stylesheet"/>

    <!-- Magic, used for slides transition (if you set effect.provider to 'magic') -->
    <link href="{{app-context}}css/magic.min.css" rel="stylesheet"/>
    <link href="{{app-context}}css/item.css" rel="stylesheet" type="text/css" />

    <style>
        body{
            background-color: #F9F9F9;
        }
        #map {
            width: 500px;
            height: 400px;
        }
        #price_tag{
            box-shadow: 4px 4px 4px #d7d2d9;
            margin-bottom: 15px;
        }
        /****** overrides *************/
        .ui.orange.label{
            background-color: #FF4500 !important;
        }
        .ui.labeled.icon.buttons > .button > .fa, .ui.labeled.icon.button > .fa {
      position: absolute;
      height: 100%;
      line-height: 2.7;
      border-radius: 0px;
      border-top-left-radius: inherit;
      border-bottom-left-radius: inherit;
      text-align: center;
      margin: 0em;
      width: 2.57142857em;
      background-color: rgba(0, 0, 0, 0.05);
      color: #090000;
      box-shadow: -1px 0px 0px 0px transparent inset;
    }

    /* Left Labeled */

    .ui.labeled.icon.buttons > .button > .fa,
    .ui.labeled.icon.button > .fa {
      top: 0em;
      left: 0em;
    }
    .ui.comments{
        max-width: 100% !important;
    }
    .comment{
        min-height: 70px;
        margin-top: 15px !important;
        background-color: #FFFFFF !important;
        border: 1px solid #eeeeee !important;
        padding: 10px 20px 10px 10px !important;
        box-shadow: 2px 3px 6px rgba(0,0,0,0.25);
    }
    .comment h3{
        color: #026dc5;
        font-weight: bold;
        font-size: 15px;
    }
    .comment p{
        line-height: 22px !important;
        text-align: justify;
    }
    #fb .fa, #tw .fa, #go .fa, #in .fa, #tf .fa{
        margin-bottom: 2px;
    }
    #fb:hover{
        background-color: #304D8A;
        color: #FFFFFF;
    }
    #tw:hover{
        background-color: #00719B;
        color: #FFFFFF;
    }
    #go:hover{
        background-color: #DE321D;
        color: #FFFFFF;
    }
    #in:hover{
        background-color: #147BAF;
        color: #FFFFFF;
    }
    #tf:hover{
        background-color: #17AD3A;
        color: #FFFFFF;
    }

    #edit-pub-con{
        float: right;
        padding-left: 5px;
        padding-right: 5px;
        background-color: #FFFFFF;
    }
    #slider{
        height: 500px;
        background-color: #FFFFFF;
    }
    #slider img{
        max-height: 496px;
        margin: auto;
    }
    #report_ad, #report_ad i, #del-post-btn i{
        color: #FFFFFF !important;
    }
    #report_ad{
        font-weight: bold;
    }
    .rating-icons{
        display: inline-block;
    }
    .rating-icons ul li{
        float: left;
        list-style: none;
        margin-bottom: -5px;
    }
    /*.rating-icons #em-bad img, .rating-icons #em-nok img, .rating-icons #em-ok img, .rating-icons #em-good img{
        width: 20px;
        margin-right: 5px;
    }*/
    .rating-icons #em-bad, .rating-icons #em-nok, .rating-icons #em-ok, .rating-icons #em-good{
        height: 20px;
        width: 20px;
        margin-right: 5px;
        background-size: contain;
    }
    .rating-icons #em-bad{
        background-image: url(https://www.bestway.lk/img/icons/bad_b.png);
    }
    .rating-icons #em-nok{
        background-image: url(https://www.bestway.lk/img/icons/nok_b.png);
    }
    .rating-icons #em-ok{
        background-image: url(https://www.bestway.lk/img/icons/ok_b.png);
    }
    .rating-icons #em-good{
        background-image: url(https://www.bestway.lk/img/icons/good_b.png);
    }

    .rating-icons #em-bad:hover{
        background-image: url(https://www.bestway.lk/img/icons/bad.png);
    }
    .rating-icons #em-nok:hover{
        background-image: url(https://www.bestway.lk/img/icons/nok.png);
    }
    .rating-icons #em-ok:hover{
        background-image: url(https://www.bestway.lk/img/icons/ok.png);
    }
    .rating-icons #em-good:hover{
        background-image: url(https://www.bestway.lk/img/icons/good.png);
    }
    #pub_panel_con{
        padding-left: 0px;
    }
        #no-search-itm .ui.segment{
            background-color: rgba(153, 0, 102, 0.07);
        }
        #no-search-itm{
            text-align: center;
        }
        #no-search-itm #sr-txt{
            font-family: 'Lobster', cursive;
            font-size: 3.5em;
        }
        #no-search-itm #no-res-txt{
            font-family: 'Quicksand', sans-serif;
            font-size: 1.55em;
            margin-top: 10px;
        }
    </style>

</head>
<body>
<div id="main-con">
<!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}
    <!-- promobar goes here -->

    <!-- search bar -->
    <div class="global-page">
        {% include "front/includes/main_search.php" %}
    </div>

    <div id="main-wrap" class="container">
    <!-- promo bar goes here -->
        <div class="row">

        <div id="contentCol" class="">

            <div id="contentArea">
                {%if not any item.status.deleted item|empty? %}
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
                <!-- Ad layout in item page-->
                <div id="itemContent">
                    <div id="itemTitle">
                    {%ifequal identity item.publisher %}
                        <div id="edit-pub-con" class="wb">
                            <button id="del-post-btn" onclick="confirm('Are your sure want to delete this post?',removeAd());" class="ui tiny labeled icon button red">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                Remove this post!
                            </button>

                            <a id="edit-post-btn" class="ui labeled icon button" href="{{app-context}}post/edit/{{item._id}}">
                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                              Edit this post
                            </a>

                            <div id="pub-post" class="ui toggle checkbox">
                              <input type="checkbox" {%if item.status.submitted %} checked="checked" {%endif%} onchange="publishPost(this.value);" name="public"/>
                              <label>Submit to Publish</label>
                            </div>
                        </div>
                   {%endifequal%}
                        <h1 id="it-ttle">{{item.title|capitalize}}</h1>
                        <img src="{{app-context}}img/click5.png" class="ad_view_pub_img hidden"/><span class="ad_views hidden">267 Ad views</span>
                        <div class="rating-icons hidden">
                            <ul>
                                <li id="em-bad" title="Bad"></li>
                                <li id="em-nok" title="Not OK"></li>
                                <li id="em-ok" title="OK"></li>
                                <li id="em-good" title="Good"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <article id="ad_image" class="col-md-7">
                            {% if not item.imgs|empty? %}
                            <div id="slider" class="col-lg-12 wb"></div>
                            {% endif %}
                            <div class="row">
                                {% if not item.imgs|empty? %}
                                <article id="slider_images" class="col-lg-12">
                                    <ul id="slider_thumbs" class="desoslide-thumbs-horizontal list-inline text-center">
                                        {% for img in item.imgs %}
                                        <li>
                                            <a href="{{img.url}}">
                                                <img src="{{img.url}}" alt="city">
                                            </a>
                                        </li>
                                        {% endfor %}
                                    </ul>
                                </article>
                                {% else %}
                                <article class="col-md-12">
                                <img src="https://www.bestway.lk/img/bestway-no-image-l.png" alt="city"/>
                                </article>
                                {% endif %}
                                <article>
                                    <div id="short_disc" class="col-xs-12">
                                        <h4>Description</h4>
                                        <p>{{item.description|safe}}</p>
                                    </div>
                                </article>
                                {%ifunequal identity item.publisher %}
                                <div id="con_box2">
                                    <button id="message_seller" class="ui teal medium button msg_seller"><i class="fa fa-envelope"></i> Message Seller</button>
                                    <button id="call_seller" class="ui purple medium button"><i class="fa fa-phone"></i> Contact Seller</button>
                                </div>
                                {% endifunequal %}
                            </div>

                        </article>

                        <article id="item_det" class="col-md-5">
                            <aside id="pub_view">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a id="price_tag" class="ui orange huge tag label">Rs. {{item.price|ad-price}}</a>
                                    </div>
                                    <div class="ui vertical labeled icon buttons col-md-6">
                                        {%ifunequal identity item.publisher %}
                                      <button id="report_ad" class="ui negative basic button">
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                        Report this
                                      </button>
                                        {%endifunequal%}
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <button id="report_ad" class="ui negative basic button"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Report this</button>
                                    </div> -->
                                    {%ifunequal identity item.publisher %}
                                    <div id="con_box">
                                        <button id="message_seller" class="ui teal medium button col-md-4 msg_seller"><i class="fa fa-envelope"></i> Message Seller</button>
                                        <button id="call_seller" class="ui purple medium button col-md-4"><i class="fa fa-phone"></i> Contact Seller</button>
                                    </div>
                                    {%endifunequal%}
                                </div>

                                <div class="row">

                                    <div id="pub_panel_con" class="col-md-12">
                                        <aside class="panel panel-default shadow">
                                            <div id="pub_panel">
                                                <div class="panel-body">
                                                    <div id="publisher">
                                                        <i class="fa fa-user" aria-hidden="true"></i>
                                                        <span class="pub-txt">Publisher</span>
                                                        <span class="pub-val">
                                                            <a target="_blank" href="{{app-context}}profile/{{item.publisher.id}}">
                                                                {{item.publisher.display-name|capitalize}}
                                                            </a>
                                                        </span>
                                                    </div>
                                                    <div id="pub-city">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        <span class="pub-txt">City</span>
                                                        <span class="pub-val">
                                                            {{item.location.city|capitalize}}
                                                        </span>
                                                    </div>
                                                    <div id="pub-district">
                                                        <i class="fa fa-map" aria-hidden="true"></i>
                                                        <span class="pub-txt">District</span>
                                                        <span class="pub-val">
                                                            {{item.location.district|capitalize}}
                                                        </span>
                                                    </div>
                                                <div id="badges">
                                                </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                            </aside>
                            <div class="row">
                                <div id="prop_panel_con" class="col-md-12">
                                    <aside class="panel panel-default shadow">
                                        <div id="prop_panel">
                                            <div class="panel-body">
                                                {% for prop in item.properties %}
                                                <div class="props">
                                                    <span class="propLab" style="">{{prop.0|name|capitalize|ad-property}}:</span><span class="propVal">{{prop.1|capitalize}}</span>
                                                </div>
                                                {% endfor %}
                                            </div>
                                        </div>
                                    </aside>
                                </div>

                                <div class="col-md-12">
                                    <a class="ui teal huge label"><i class="fa fa-map-marker"></i> Location</a>
                                    <a class="ui teal huge label disabled"><i class="fa fa-globe"></i> Website</a>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div id="my_comments" class="row">

                            <div class="col-md-12" style="padding: 15px 8px 8px 0px;">
                                <article id="comments_td">

                                </article>
                            </div>

                    </div>
                    <div id="comments" class="row">

                            <div class="col-md-12" style="padding: 15px 8px 8px 0px;">
                                <article id="comments_list_td">

                                    <div class="ui comments">
                                        {% for c in item.comments %}
                                        <div class="comment">
                                        <a class="avatar">
                                          <img src="img/app/avatar-dhg.png"/>
                                        </a>
                                        <div class="content">
                                          <a class="author" href="{{app-context}}user/{{c.author.id}}">{{c.author.name}}</a>
                                          <div class="metadata">
                                            <div class="ui star rating" data-rating="3" data-max-rating="5"></div><span class="date">{{c.posted|date:mediumDateTime}}</span>
                                          </div>
                                          <div class="text">
                                            <h3>{{c.title}}</h3>
                                            <p>{{c.text|safe}}</p>
                                          </div>
                                        </div>
                                      </div>
                                        {% endfor %}

                                    </div>

                                </article>
                            </div>

                    </div>
                    {%csrftok%}
                        <!-- <div id="map"></div>
                        <script>
                            function initMap() {
                                var mapDiv = document.getElementById('map');
                                var map = new google.maps.Map(mapDiv, {
                                    center: {lat: 44.540, lng: -78.546},
                                    zoom: 8
                                });
                            }
                        </script> -->


                </div>
                {%else%}
                <div id="no-search-itm"><div class="ui segment"><p id="sr-txt">Sorry...!</p><img src="https://www.bestway.lk/img/search_err.png"><p id="no-res-txt">Item cannot be found. It may have deleted or unpublished.</p></div></div>
                {%endif%}
            </div>

        </div>
    </div>
    </div>
</div>
<div id="bottomBar"></div>

<!-- Send message to seller -->
<div id="send-seller-message" class="ui modal">
  <i class="close fa fa-times-circle"></i>
  <div class="header">Send a Message to Seller</div>
  <div class="content">
    <form id="send-message" class="ui form">
      <div class="field">
        <label>Subject</label>
        <input type="text" name="subject" placeholder="Subject of the Message"/>
        <input type="hidden" name="send-to" value="{{item.publisher}}"/>
      </div>
      <div class="field">
        <label>Message</label>
        <textarea name="message"></textarea>
      </div>
    </form>
  </div>
  <div class="actions">
    <button class="ui button" onclick="sendMessage();">Submit</button>
  </div>
</div>
<!-- Send message to seller -->

<!-- Send message to seller | chat -->
<div id="chat-seller" class="ui modal">
    <i class="close fa fa-times-circle"></i>
    <div class="header">Send a Message to Seller</div>
    <div class="content">
        <div id="bw-chat"></div>
    </div>
    <div class="actions">

        <input type="hidden" id="title" name="title" value="{{item.title|capitalize}}" />
        <input type="hidden" id="id" name="id" value="{{identity}}" />
        <input type="hidden" id="publisher-id" name="publisher-id" value="{{item.publisher.id}}" />
        <input type="hidden" id="publisher-name" name="publisher-name" value="{{item.publisher.display-name|capitalize}}" />
        <input type="hidden" id="item-id" name="item-id" value="{{item._id}}" />
        <button id="send-chat" name="send-chat" class="ui button" onclick="sendChat();">Send</button>
    </div>
</div>
<!-- Send message to seller -->

<!-- Report this ad -->
<div id="report-this-ad" class="ui modal">
  <i class="close fa fa-times-circle"></i>
  <div class="header">I want to report this Ad</div>
  <div class="content">
    <form id="send-report" class="ui form">
      <div class="field">
        <label>Reason</label>
      </div>
      <div class="inline field">
        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="category"/>
          <label>Wrong category</label>
        </div>

        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="fraud">
          <label>Fraud</label>
        </div>

        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="duplicate">
          <label>Duplicate</label>
        </div>

        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="spam">
          <label>Spam</label>
        </div>

        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="unavailable">
          <label>Item sold/unavailable</label>
        </div>

        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="offensive">
          <label>Offensive</label>
        </div>

        <div class="ui checkbox">
          <input type="checkbox" tabindex="0" class="hidden" name="other">
          <label>Other</label>
        </div>
      </div>
      <div class="field">
        <label>Your e-mail</label>
        <input type="email" placeholder="joe@bestway.lk">
      </div>
      <div class="field">
        <label>Message</label>
        <textarea name="message"></textarea>
      </div>
    </form>
  </div>
  <div class="actions">
    <button class="ui button" onclick="reportAd();">Submit</button>
  </div>
</div>
<!-- Report this ad -->


<!--<div id="toolWidget"></div>-->

<script src="{{app-context}}js/bootstrap/bootstrap.min.js"></script>
<script src="{{app-context}}js/semantic.js" type="text/javascript"></script>
<script src="{{app-context}}js/highlight/highlight.pack.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js" async defer></script>-->
<script src="{{app-context}}js/jquery.fancybox.js" type="text/javascript"></script>
<script src="{{app-context}}js/chosen.jquery.js" type="text/javascript"></script>

<!-- desoSlide core -->
<script src="{{app-context}}js/jquery.desoslide.min.js"></script>

<script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

    $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

        //$('.fancybox').fancybox();

        $( ".msg_seller" ).click(function() {
            $('#send-seller-message').modal({inverted: true, blurring: true}).modal('show');
        });

        $( "#report_ad" ).click(function() {
            $('#report-this-ad').modal({inverted: true, blurring: true}).modal('show');
        });
        $('.ui.checkbox').checkbox();
            /*
             *  Different effects
             */

        $('#slider').desoSlide({
            thumbs: $('#slider_thumbs li > a'),
            thumbEvent: 'mouseover',
            imageClass: 'img-responsive',
            auto: {
                load: true,
                start: false
            },
            effect: 'none',
            overlay: 'none',
            first: 0,
            interval: 100
        });

        $('.ui.rating').rating('disable');

        $( "#slider_images" ).click(function() {
            var Imheight = $('.desoslide-wrapper>img').height();
            var TopM = Math.ceil((500-Imheight)/2);
            $('.desoslide-wrapper>img').css( "margin-top", TopM );
        });
    });
</script>
{% script "https://www.bestway.lk/js/web/main.js" %}
{% script "https://www.bestway.lk/js/bwinc.js" %}

{%if not identity|empty? %}
    {% script "https://www.bestway.lk/js/user.js" %}
{%endif%}


<script>
    $(document).ready(function(){
        //bestway.comments.core.run("{{item._id}}");
        {%ifequal identity item.publisher %}
            {%if all item.status.submitted item.status.approved %}
                bestway.index.common.alert("Congratulations Your Ad is been approved and it's Live!","success");
            {%endif%}
            {%if all item.status.submitted not item.status.approved %}
                bestway.index.common.alert("Your Ad is waiting to be approved. Please wait!","info");
            {%endif%}
        {%endifequal%}
    });
    function sendMessage(){
        $.ajax({headers:{"x-csrf-token":$("#__anti-forgery-token").val()},url:"{{app-context}}post/send-message/{{item._id}}",type:"POST",dataType:"json",data:$('form#send-message').serialize() ,success:function(a){console.log(a); $('#send-seller-message').modal({inverted: true, blurring: true }).modal("hide");bestway.index.common.alert("Your message sent successfully!","success");}});
    }
    function removeAd(){
        $.ajax({headers:{"x-csrf-token":$("#__anti-forgery-token").val()},url:"{{app-context}}post/remove/{{item._id}}",type:"POST",dataType:"json",success:function(a){ window.location.href = "{{app-context}}" }});
    }
    function reportAd(){
        $.ajax({headers:{"x-csrf-token":$("#__anti-forgery-token").val()},url:"{{app-context}}post/report/{{item._id}}",type:"POST",dataType:"json",data:$('form#send-report').serialize(), success:function(a){ bestway.index.common.alert("Thank you for your support! We will get back to you as soon as we check your report.","success"); setTimeout(function(){window.location.href = "{{app-context}}"},800); }});
    }
    function publishPost(publish){
        $.ajax({headers:{"x-csrf-token":$("#__anti-forgery-token").val()},url:"{{app-context}}post/publish/{{item._id}}",type:"POST",dataType:"json",data:{publish:publish},success:function(a){ $('#pub-post input[type=checkbox]').prop('checked',a.publish);bestway.index.common.alert(a.msg,"normal");}});
    }
</script>
<!-- footer bar -->
{% include "front/includes/footer.php" %}
{%if not any item.status.deleted item|empty? %}
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5934efcb2e01ff00121c6762&amp;product=custom-share-buttons"></script>
{%endif%}
</body>
</html>
