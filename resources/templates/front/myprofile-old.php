<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <title>Bestway | My Page</title>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

  <link href="css/semantic.css" rel="stylesheet" type="text/css" />
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="css/chosen.css" rel="stylesheet" type="text/css" />
  <link href="css/app/toolkit.css" rel="stylesheet">

  <link rel="stylesheet" href="css/cropper.min.css">
  <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
  <link href="css/main.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <style>
  #topfix{
    margin-top: 55px;
  }
  #main-wrap{
    padding: 5px 0px;
  }
  #leftCon{
    padding-left: 0px;
  }
  #midCon{
    padding-left: 0px;
    padding-right: 0px;
  }
  #rightCon{
    padding-right: 0px;
  }
  #pro_tab{
    margin-top: 60px;
  }
  #pro_pic_con{
    position: relative !important;
  }
  #pro_pic{
/*    position: absolute;
    margin: -99px 16%;
    border: 2px solid #fff;*/
        position: absolute;
        height: 240px;
        margin: -99px 16%;
        border: 2px solid #fff;
  }
    #pro_pic img{
    max-width: 160px !important;
  }
  #pro_pic_con:hover #pro_pic_changer{
    opacity: 1;
  }
  #pro_pic_con:hover #pro_pic_changer i{
    font-size: 15px;
    margin-right: 5px;
  }
  #pro_pic_changer{
    position: absolute;
    bottom: 75px !important;
    width: 100%;
    padding: 15px 20px;
    background-color: rgba(0,0,0,0.8);
    color: #FFFFFF;
    text-align: center;
    opacity: 0;
    transition: opacity .5s ease-out;
    z-index: 5;
    cursor: pointer;
  }
  #pro_pic_changer i {
    margin-right: 8px !important;
    font-size: 5px;
    transition: font-size 0.5s ease-out;
  }
  #upload-file {
    display: none;
  }
  #pro_name{
    margin-top: 70px;
  }
  #pro_border{
    margin-top: -76px;
  }

  .top_btn {
  display:inline-block;
  cursor:pointer;
  font-family:Arial;
  font-size:13px;
  font-weight:bold;
  padding:6px 12px;
  margin-bottom: 5px;
  text-decoration:none;
}
.top_btn:active {
  position:relative;
  top:1px;
}
.gr_btn{
  -moz-box-shadow:inset 0px 1px 0px 0px #9acc85;
  -webkit-box-shadow:inset 0px 1px 0px 0px #9acc85;
  box-shadow:inset 0px 1px 0px 0px #9acc85;
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #74ad5a), color-stop(1, #68a54b));
  background:-moz-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:-webkit-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:-o-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:-ms-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:linear-gradient(to bottom, #74ad5a 5%, #68a54b 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#74ad5a', endColorstr='#68a54b',GradientType=0);
  background-color:#74ad5a;
  border:1px solid #3b6e22;
  color:#ffffff;
}
.gr_btn:hover {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #68a54b), color-stop(1, #74ad5a));
  background:-moz-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:-webkit-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:-o-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:-ms-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:linear-gradient(to bottom, #68a54b 5%, #74ad5a 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#68a54b', endColorstr='#74ad5a',GradientType=0);
  background-color:#68a54b;
  color: #FFFFFF;
  text-decoration: none;
}
.gra_btn{
  -moz-box-shadow:inset 0px 1px 0px 0px #f9f9f9;
  -webkit-box-shadow:inset 0px 1px 0px 0px #f9f9f9;
  box-shadow:inset 0px 1px 0px 0px #f9f9f9;
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9));
  background:-moz-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:-webkit-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:-o-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:-ms-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9',GradientType=0);
  background-color:#f9f9f9;
  border:1px solid #D5D5D5;
  color:#666666;
}
.gra_btn:hover{
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9));
  background:-moz-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:-webkit-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:-o-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:-ms-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9',GradientType=0);
  background-color:#e9e9e9;
  color: #666666;
  text-decoration: none;
}
#msg_btn i{
  margin-right: 5px;
}
#crop-popup {

}
.img-container img {
    width: auto!important;
    height: 450px;
}
.panel {
    margin-bottom: 0px !important;
  }
.panel-body {
    padding: 0px !important;
}
.view-mode{
    width: 100%;
    border: none;
    background: #FFF;
}
.edit-mode{
    width: 100%;
    border: 1px solid #ccc;
    border-radius: 4px;
    background: #EEFEEF;
}
</style>
</head>
<body>


<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}

<!-- <div class="anq global-page" id="app-growl"></div>
 --><!-- search bar -->
<div id="topfix" class="global-page">

  <!-- promo bar goes here -->
  {% include "front/includes/promo_bar.php" %}
  {% include "front/includes/main_search.php" %}
</div>
  <!--
promo bar goes here -->
<div id="main-wrap" class="container amt">
  <div>
    <div id="leftCon" class="gn">
      <div id="pro_tab" class="qv rc aog alu">

        <div class="qw dj">
          <div id="pro_pic_con">
            <svg id="pro_border" height="95" width="170">
              <path id="X1" d="M 0 60 0 0 0" stroke="#d3e0e9" stroke-width="1" fill="none" />
              <path id="X2" d="M 165 00 l 0 60" stroke="#d3e0e9" stroke-width="1" fill="none" />
              <path id="Y1" d="M 0 0 l 165 0" stroke="#d3e0e9" stroke-width="1" fill="none" />
            </svg>
           <div id="pro_pic">
              <a id="pro_pic_a" href="/my-profile">
           <!--<img src="{{ user.profile-pic }}"  />-->
              </a>
              <div id="pro_pic_changer">
                <i class="fa fa-camera"></i>
                <span>Change Image</span>
              </div>
            </div>
          </div>

          <h5 id="pro_name" class="qy">
            <a class="aku" href="profile/index.html">{{fnamelname}}</a>
          </h5>
<input type="hidden" id="token" value="{{csrf-token}}">
          <p class="alu">{{slogan}}</p>


        </div>
      </div>

      <div class="qv rc sm sp">
        <div class="qw">
          <h5 class="ald">About<small>· <a href="#" >Edit</a></small></h5>
          <ul class="eb tb row">
            <li><span class="col-md-4">From</span><span class="col-md-6"><a href="#" style=""><input name="from" value="Oh, Canada" class="view-mode" disabled/></a></span><i class="col-md-2 hidden fa fa-pencil"></i></li>
            <li><span class="col-md-4">Member</span><span class="col-md-6"><a href="#" style=""><input name="from" value="Senior" class="view-mode" disabled/></a></span><i class="col-md-2 hidden fa fa-pencil"></i></li>
          </ul>
        </div>
      </div>


    </div>

    <div id="midCon" class="gz">
    <div id="app"></div>

      <div id="loader__circle"></div>


      <ul class="ca qo anx">



        {%for a in user-ads %}
        <li class="qf b aml">
          <div class="qg">
            <div class="qn">
              <small class="eg dp">{{a.publisheddate|date:mediumDateTime}}</small>

                <a href="{{app-context}}item/{{a._id}}">

                  <h5>{{a.title}}</h5>
                </a>

            </div>

            <p>
              {{a.description.0|safe}}
            </p>

            <div class="any" data-grid="image-s" data-target-height="650">

              <div style="" >
                <img data-action="zoom" data-width="1200" data-height="900" src="{{a.images.medium.url}}" />
              </div>
             </div>

          </div>
        </li>
        {% endfor %}

      </ul>
    </div>
    <div id="rightCon" class="hidden gn">
      <div class="qv rc alu ss">
        <div class="qw">
          <h5 class="ald">Sponsored</h5>
          <div data-grid="images" data-target-height="150">
            <img class="qh" data-width="640" data-height="640" data-action="zoom"  src="">
          </div>
          <p><strong>It might be time to visit Iceland.</strong> Iceland is so chill, and everything looks cool here. Also, we heard the people are pretty nice. What are you waiting for?</p>
          <button class="cg ts fx">Buy a ticket</button>
        </div>
      </div>

      <div class="qv rc alu ss">
        <div class="qw">
          <h5 class="ald">Likes <small>· <a href="#">View All</a></small></h5>
          <ul class="qo anx">
            <li class="qf alm">
              <a class="qj" href="#">
                <img
                        class="qh cu"
                        src="img/app/avatar-fat.jpg">
              </a>
              <div class="qg">
                <strong>Jacob Thornton</strong> @fat
                <div class="aoa">
                  <button class="cg ts fx">
                    <span class="h vc"></span> Follow</button>
                </div>
              </div>
            </li>
            <li class="qf">
              <a class="qj" href="#">
                <img
                        class="qh cu"
                        src="img/app/avatar-mdo.png">
              </a>
              <div class="qg">
                <strong>Mark Otto</strong> @mdo
                <div class="aoa">
                  <button class="cg ts fx">
                    <span class="h vc"></span> Follow</button>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="qz">
          Dave really likes these nerds, no one knows why though.
        </div>
      </div>

      <div class="qv rc aok">
        <div class="qw">
          &copy; {% now yyyy %} Bestway.lk

          <a href="#">About</a>
          <a href="#">Help</a>
          <a href="#">Terms</a>
          <a href="#">Privacy</a>
          <a href="#">Cookies</a>
          <a href="#">Ads </a>

          <a href="#">info</a>
          <a href="#">Brand</a>
          <a href="#">Blog</a>
          <a href="#">Status</a>
          <a href="#">Apps</a>
          <a href="#">Jobs</a>
          <a href="#">Advertise</a>
        </div>
      </div>
    </div>
  </div>
</div>

<!--<div id="prof-overlay" class="overlay"-->
 <!--style="-->
    <!--opacity: 0.7;-->
    <!--position: fixed;-->
    <!--min-height: 100px;-->
    <!--background-color: #333;-->
    <!--min-width: 100px;-->
    <!--top: 0;-->
    <!--height: 100%;-->
    <!--width: 100%;-->
<!--">-->
	      <!--<div class="copy col-md-12"></div>-->
<!--</div>-->


<!-- footer bar -->
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts-my-profile.php" %}

<div id="crop-popup" style="overflow:hidden; display: none;">
  <div id="pro-img-panel" class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Select the best of you</h3>
    </div>
    <div class="panel-body">
      <div id="app-crop-upload-box">
      </div>

      <div class="panel-footer">
        <div class="signin-action-panel">

          <div class="button-panel">
            <input type="button" id="upload-profic" class="btn btn-primary" value="Done">
          </div>
        </div>
      </div>


    </div>
  </div>
</div>

  </body>
</html>
