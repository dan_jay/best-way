<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <base href="/front/" ></base> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oh Snap</title>
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- common assets  -->
    {% include "front/includes/head-assets.php" %}


    <style type="text/css">
    body{
    	background-color: #F4F4F4;
    }
    #con-404{
        font-family: 'Josefin Slab', serif;
        text-align: center;
        margin-top: 25px;
    }
    #con-404 h1{
        padding: 15px;
        font-size: 45px;
        color: #990066;
        text-shadow: 1px 2px rgba(0,0,0,0.1);
    }
    #con-404 p{
        font-size: 22px;
        color: #777;
        text-shadow: 1px 1px rgba(0,0,0,0.1);
    }
    #con-404 img{
        height: 250px;
        margin: 10px 0px;
    }
</style>
</head>
<body>
<div id="main-con">

    {% include "front/includes/top-hor-menu.php" %}
     <!-- <div class="global-page"> -->
        {% include "front/includes/main_search.php" %}
    <!-- </div> -->

    <div class="container">
        <div class="row">
            <div id="con-404" class="col-md-12">
                <h1>Something bad happened</h1>
                <img src="https://www.bestway.lk/img/bw_super_hero.png">
                <p>Our highly talented engineers dedicated to fixing this issue soon as possible</p>
            </div>
        </div>
    </div>
</div>
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts.php" %}

<script src="http://www.bestway.lk/js/jssor.slider-22.0.15.min.js" type="text/javascript" ></script>
<!-- Latest compiled and minified JavaScript -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/sidebar.js"></script>

</body>
</html>
