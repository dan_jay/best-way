<div id="sc-log-sig" class="row">
    <div class="col-xs-4 col-md-4">
      <a class="signin-google-popup img-gp">Sign in with Google </a>
    </div>
    <div class="col-xs-4 col-md-4">
      <a class="signin-facebook-popup img-fb">Sign in with Facebook </a>
    </div>
    <div class="col-xs-4 col-md-4">
      <a class="signin-twitter-popup img-tw">Sign in with Twitter </a>
    </div>
</div>

<script src="https://cdn.auth0.com/w2/auth0-7.1.min.js"></script>
<script src="http://code.jquery.com/jquery.js"></script>
<script>
function authGet(b){$.ajax({headers:{"x-csrf-token":$("#__anti-forgery-token").val()},url:"https://www.bestway.lk/auth0",type:"POST",dataType:"json",data:{code:b.accessToken},success:function(a){window.location.href="https://www.bestway.lk/auth/"+a.authToken}})}var auth=new Auth0({domain:"bestwaylk.eu.auth0.com",clientID:"SUTx9p7MI0gycUaivjSG8Pcv5Z364qC2",callbackURL:"https://www.bestway.lk/auth0",responseType:"token",redirect_uri:"https://www.bestway.lk/auth0"});
$(".signin-google-popup").on("click",function(){auth.signin({popup:!0,connection:"google-oauth2"},function(b,a){authGet(a)})});$(".signin-facebook-popup").on("click",function(){auth.signin({popup:!0,connection:"facebook"},function(b,a){authGet(a)})});$(".signin-twitter-popup").on("click",function(){auth.signin({popup:!0,connection:"twitter",additional_parameters: {include_email:true}},function(b,a){authGet(a)})});
</script>

