<!-- <div class="wrapper" style="height: auto;"> -->

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
      <!-- Sidebar user panel -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <h3 id="cat-head">Categories</h3>
      <ul id="cat_listing" class="sidebar-menu">
        <!-- <li class="header">CATEGORIES</li> -->
        
        {%for x in categories %}
        <li class="icon_con treeview">
            <!-- <a href="/search-results?main-cat={{x.id}}">{{x.label}}</a> -->
            <a href="search-results?main-cat={{x.id}}">
              <span class="{{x.css_class}}"></span> <span class="cat-text">{{x.label}}</span>
              <span class="pull-right-container">
                <i class="fa fa-arrow-circle-left pull-right"></i>
              </span>
            </a>

              <ul class="treeview-menu">
              {%for subcat in x.children %}
                <li><a href="{{app-context}}search-results?main-cat={{x.id}}&amp;subcat={{subcat._id}}">{{subcat.label}}</a></li>
                          {% endfor %}
            </ul>
        </li>
        {% endfor %}
        <!-- <li class="treeview active">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li class="active"><a href="collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a>
            </li>
          </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper" style="min-height: 1745px;"> -->
    <!-- Content Header (Page header) -->


    <!-- Main content -->

    <!-- /.content -->
  <!-- </div> -->
  <!-- /.content-wrapper -->



  <!-- Control Sidebar -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->

<!-- </div> -->
<!-- ./wrapper -->
