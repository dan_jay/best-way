<!--    
<script src="js/jquery.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script src="https://cdn.auth0.com/w2/auth0-7.1.min.js"></script>
-->
<script src="http://code.jquery.com/jquery.js"></script>

<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="https://semantic-ui.com/dist/semantic.min.js" type="text/javascript"></script>

<script src="js/range.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>
<!-- <script src="js/chosen.jquery.js" type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>

        {% script "/js/web/main.js" %}
        {% script "/js/bwinc.js" %}
        {% script "/js/landing.js" %}

<script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }


    $(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

         var fbon = false;
		 $('a.fancybox').fancybox({
			        afterLoad : function(fb) {
			            //console.log(fb);
			            if(fb.href=="#signin"){
			              fbon = true;
			            }else{
			            fbon = false;
			            }

                },
			        afterClose : function(fb) {
                          fbon = false;
                }


		});

                $(document).on("keyup",
                    function(e){

                            if(e.keyCode==13 && fbon ){
                                $("input#login").trigger("click");
                            }
                    }
                );
			/*
			 *  Different effects
			 */
        $('.shape').shape();
        $('#testimo').on('click', function () {
            $('.shape').shape('flip left');
        });

        $('#feedcon').popup();
        $('#prof_pg').dropdown({
            on: 'hover'
        });
        $('#on_state .checkbox').checkbox({
            input  : 'input[type=radio]'
        });

        //contact popup
        $('.coupled.modal').modal({allowMultiple: true});
        $('#show-contact').click(function(){
            $('#email-con').modal('attach events', '#contact-form .button');
            $('#contact-form').modal('show');
          });
	});
	  $(document).ready(function() {
          $('#tc-range').range({
              min: 0,
              max: 10.5,
              start: 5,
              end: 9,
              step: 1.5,
              onChange: function(val) {
                                      var tmp =  $("#acc_term").attr("data-tmp");

                                      $("#acc_term").attr("data-tmp", (parseInt(tmp, 10) + val) );
                                      $("#acc_term").val( $("#acc_term").prop("data-tmp") );
                                          //myRangeValue = val;
                        }
          });
      });

  </script>


        <!-- Bower Depedencies -->
        <script src="js/modernizr.js"></script>
        <!-- Load the plugin -->
        <script src="js/jquery.animateSlider.js"></script>
        <script src="js/homeslider.js"></script>





<script type="text/javascript">


       $(window).load(function() {
           $("#hssignin,#hssignup").attr("href", function () {
               return $(this).attr("id").replace("hs", "#");
           });

             //bestway.index.ajax.login_and_register();

//
//           var clientId = "SUTx9p7MI0gycUaivjSG8Pcv5Z364qC2";
//           var domain = "bestwaylk.eu.auth0.com";
//           var options = {
//               auth: {
//                   redirect: false
//               }
//           };
//           var lock = new Auth0Lock(clientId, domain, options);
//           lock.show();
       } );
</script>
