
<div id="mainSearchCon" class="container-fluid">
  <div id="adSearch" class="container">
      <div id="adSearchCon" class="row">
      <div class="col-xs-12">
            <form class="form-inline" action="/search-results">
                <div class="search-items col-md-6">
                  <input type="text" name="query" id="ad_search" placeholder="Search..." class="mainSearch maxmarg search_control"/>
                </div>
                <div class="search-items col-md-2">
  						<select id="search_cat" name="main-cat" data-placeholder="Choose Category" class="chosen-select search_control" >
  							<option value="any">Category</option>
  							{% for y in catz %}
  							{{y|safe}}
  							{% endfor %}
  						</select>
                </div>
                <div id="root" class="search-items col-md-2">
                  <select id="search_sub_cat" name="loc-district" data-placeholder="Choose a District" class="chosen-select search_control">
                      <option value="any">Any where in SriLanka</option>
                      {% for y in locs.districts %}
                      {{y|safe}}
                      {% endfor %}
                  </select>
                </div>
                <div class="search-items col-md-2">

                    <input id="main_s_b" type="submit" value="Search"/>

                </div>
            </form>
            </div>
      </div>
  </div>
</div>
