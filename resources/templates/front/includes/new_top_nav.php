<span id="app_state" style="display: none;" class="hidden">
    <span id="app_context" >{{servlet-context}}</span>
    <span id="app_pathname" >{{pathname}}</span>
    <span id="user_profile_pic" >{{profile-pic}}</span>
    <span id="user_fnamelname" >{{fnamelname}}</span>
    <span id="user_id" >{{id}}</span>
</span>

<nav id="topBar" class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topBarContent" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{servlet-context}}">
          <img src="img/bestway.png" alt="{{servlet-context}} bestway logo" title="Bestway Logo" />
          </a>
        </div>

        <div id="topBarContent" class="collapse navbar-collapse">
            <!-- <div class="nav navbar-nav"> -->
                <!-- <div id="topLinks" class="col-md-8"> -->
                    <ul class="nav navbar-nav navbar-right">
                        {%ifunequal id nil %}
                        <li><a href="home">Home</a></li>
                        <!-- <li><a href="home/categories">Categories</a></li> -->
                        <li><a href="/my-page">My Store</a></li>
                        <li id="prof_pg" class="ui top pointing dropdown link item">

                                <div id="tp_prf_img_con" class="visual-area-topbar">
                                </div>




                            <div class="verticle menu">
                                <a href="/my-profile" class="item"><i class="fa fa-user"></i>Profile</a>
                                <a class="item"><i class="fa fa-envelope"></i>Messages <div class="ui red small label">10</div></a>
                                <div class="ui divider"></div>
                                <a class="item"><i class="fa fa-bar-chart"></i>Analytics</a>
                                <div class="ui divider"></div>
                                <div class="item">
                                  <span class="text">Online Status</span>
                                  <i class="fa fa-chevron-right right"></i>
                                  <div class="menu">
                                    <div class="ui form">
                                      <div id="on_state" class="grouped fields">
                                        <div class="field">
                                          <div class="ui radio checkbox">
                                            <input name="online" checked="checked" type="radio" />
                                            <label>Online</label>
                                          </div>
                                        </div>
                                        <div class="field">
                                          <div class="ui radio checkbox">
                                            <input name="away" type="radio" />
                                            <label>Away</label>
                                          </div>
                                        </div>
                                        <div class="field">
                                          <div class="ui radio checkbox">
                                            <input name="offline" type="radio" />
                                            <label>Offline</label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="ui divider"></div>
                                <div class="item">Help</div>
                                <a class="item">Report a Problem</a>
                                <div class="ui divider"></div>
                                <a class="item" href="/account-settings"><i class="fa fa-cog"></i>Account Settings</a>
                                <div class="ui divider"></div>
                                <a id="" class="item" href="/logout"><i class="fa fa-power-off"></i> Logout</a>

                            </div>
                            <li id="post_ad_btn">
                              <ul>
                                <li class="post_buy"><a href="/post-your-ad"><span>BUY</span></a></li>
                                <li class="post_sell"><a href="/post-your-ad"><span>SELL</span></a></li>
                              </ul>
                            </li>
                        </li>
                         {%endifunequal%}
                        {% ifequal id nil %}
                        <li><a class="fancybox" id="hssignin"  href="/signin">Log In</a></li>
                        <li><a class="fancybox" id="hssignup" href="/signup">Sign Up</a></li>

                        <li id="postad_btn"><a href="/post-your-ad">
                            <button class="ui orange button">Post an Ad</button></a>
                        </li>
                        {% endifequal %}

                    </ul>
        </div>
    </div>
</nav>

{% ifequal id nil %}
<div id="signup" style="display: none;">

    <div id="signup-panel" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Sign up now for exclusive deals!</h3>
        </div>
        <div class="panel-body">
            <form id="signupfrm" action="signin" method="post" >
                <div class="row row-panel">
                    <div class="col-xs-6">
                        <label for="fname">First Name</label>
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" required>
                    </div>
                    <div class="col-xs-6">
                        <label for="lname">Last Name</label>
                        <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required>
                    </div>
                </div>
                <div class="row row-panel">
                    <div id="email-wrap" class="col-xs-6">
                        <label for="regemail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input type="email" class="form-control" name="regemail" id="regemail" placeholder="yourmail@example.com" required >
                        </div>
                    </div>
                    <div id="pwd-wrap" class="col-xs-6">
                        <label for="newpwd">Password</label>
                        <input type="password" class="form-control" name="newpwd" id="newpwd" placeholder="Password" required>
                    </div>
                </div>
                <div class="row row-panel">
                    <!--<div class="ui toggle checkbox col-xs-6">-->
                      <!--<input id="acc_term" name="acc_term" value="Accept Terms" type="checkbox">-->
                      <!--<label for="acc_term">Accept terms and conditions</label>-->
                    <!--</div>-->

                    <div style="max-width:80px!important; background-color: #F5F5F5!important;" class="col-md-3 ui range" id="tc-range">

                    </div>
                    <label for="acc_term">Accept terms and conditions</label>
                    <input id="acc_term" data-tmp="0" name="acc_term" value="Accept Terms" type="hidden">
                    <!--<iframe style="max-height: 30px; margin-left: 15px;" id="terms_intro" src="http://localhost/"></iframe>-->
                </div>

        <div class="panel-footer">
                <div class="signin-action-panel">
                    <div class="sign-link-panel">
                        <div>
                            <span>Already have an account? <a class="fancybox" href="#signin">Sign In</a></span>
                        </div>
                    </div>
                    <div class="button-panel">
                        <input type="submit" id="join" class="btn btn-primary" value="Sign Up">
                    </div>
                </div>
            </div>
            </form>
            <div class="ui horizontal divider">
              Or
            </div>
        </div>
    </div>

</div>

<div id="signin" style="display: none;">
    <div id="signin-panel" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Sign in to your account</h3>
        </div>
        <div class="panel-body">
            <form id="signinfrm" action="login" method="post">
                <div class="row row-panel">
                    <div class="col-sm-6 col-xs-12">
                        <label for="regemail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input autocomplete="true" name="username" type="email" class="form-control" id="regemail" placeholder="yourmail@example.com" required >
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <label for="password">Password</label>
                        <input autocomplete="true" type="password" name="password" class="form-control" id="newpwd" placeholder="Password" required>
                    </div>
                </div>
                <div class="row row-panel">
                    <div class="ui toggle checkbox col-xs-6">
                      <input id="remme" name="remme" value="Remember Me" type="checkbox">
                      <label for="remme">Remember Me</label>
                    </div>
                </div>

              <div class="panel-footer">
                <div class="signin-action-panel">
                    <div class="sign-link-panel">
                        <div>
                            <span>Forgot password?</span>
                        </div>
                        <div>
                            <span>Create an account? <a class="fancybox" href="#signup">Sign Up</a></span>
                            <input value="{{csrf-token}}" type="hidden" id="token" name="token">
                        </div>
                    </div>
                    <div class="button-panel">
                        <input type="submit" id="login" class="btn btn-primary" value="Sign In">
                    </div>
                </div>
            </div>
            </form>
            <div class="ui horizontal divider">
              Or
            </div>

        </div>
    </div>
</div>
{% endifequal %}

<div id="alert_container"></div>

<div id="loader_container"></div>
