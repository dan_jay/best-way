<span id="app_state" style="display: none;" class="hidden">
    <span id="app_context" class="app_context" >{{app-context}}</span>
    <span id="ws_context" class="ws_context" >{{ws-context}}</span>
    <span id="app_pathname" class="app_pathname">{{pathname}}</span>
    <span id="user_profile_pic" class="user_profile_pic">{{profile-pic}}</span>
    <span id="user_fnamelname" class="user_fnamelname">{{fnamelname}}</span>
    <span id="user_display-name" class="user_display-name">{{display-name}}</span>
    <span id="user_id" class="user_id">{{id}}</span>
</span>

<nav id="topBar" class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topBarContent" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{app-context}}">
          <img src="https://www.bestway.lk/img/bestway.png" alt="bestway.lk logo" title="Bestway.lk - The Lifestyle" />
          </a>
        </div>

        <div id="topBarContent" class="collapse navbar-collapse">
            <!-- <div class="nav navbar-nav"> -->
                <!-- <div id="topLinks" class="col-md-8"> -->
                    <ul class="nav navbar-nav navbar-right">
                        {%ifunequal id nil %}
                        <li><a href="{{app-context}}home">Home</a></li>
                        <!-- <li><a href="home/categories">Categories</a></li> -->
                        <li id="post_ad_btn">
                            <ul>
                                <li class="post_buy"><a href="{{app-context}}buy"><span>BUY</span></a></li>
                                <li class="post_sell"><a href="{{app-context}}sell"><span>SELL</span></a></li>
                            </ul>
                        </li>
                        <li id="prof_pg" class="ui top pointing dropdown link item">

                                <div id="tp_prf_img_con" class="visual-area-topbar">


                                    <a href="{{app-context}}my-profile"  rel="my-profile" title="Go to my-profile">
                                        <img class="tp_prf_img" src="{{profile-pic}}" />
                                        <p class="tp_prf_nm">{{fnamelname}}</p>
                                    </a>
                                </div>



                            <li><a class="item" href="{{app-context}}account-settings"><i class="fa fa-cog w-pad"></i> Settings</a></li>
                            <li><a id="" class="item" href="/logout" title="Log Out"><i class="fa fa-power-off w-pad"></i>Log Out</a></li>
                        </li>
                         {%endifunequal%}
                        {% ifequal id nil %}
                        <li><a class="fancybox" id="hssignin"  href="{{app-context}}signin">Log In</a></li>
                        <li><a class="fancybox" id="hssignup" href="{{app-context}}signup">Sign Up</a></li>

                        <li id="postad_btn"><a href="/post-your-ad">
                            <button class="ui orange button">Post an Ad</button></a>
                        </li>
                        {% endifequal %}

                    </ul>
        </div>
    </div>
</nav>

{% ifequal id nil %}
<div id="signup" style="display: none;">

    <div id="signup-panel" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Sign up now for exclusive deals!</h3>
        </div>
        <div class="panel-body">
            <form id="signupfrm" class="ui form" action="{{app-context}}join" method="post" >
                <div class="row row-panel">
                    <div class="col-xs-6">
                        <label for="fname">First Name</label>
                        <input type="text" class="form-control" tabindex="1" name="fname" id="fname" placeholder="First Name" required>
                    </div>
                    <div class="col-xs-6">
                        <label for="lname">Last Name</label>
                        <input type="text" class="form-control" tabindex="2" name="lname" id="lname" placeholder="Last Name" required>
                    </div>
                </div>
                <div class="row row-panel">
                    <div id="email-wrap" class="col-xs-6">
                        <label for="regemail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input type="email" class="form-control" tabindex="3" name="regemail" id="regmail" placeholder="yourmail@example.com" required >
                        </div>
                    </div>
                    <div id="pwd-wrap" class="col-xs-6">
                        <label for="newpwd">Password</label>
                        <input type="password" class="form-control" tabindex="4" name="newpwd" id="nwpwd" placeholder="Password" required>
                    </div>
                </div>
                <div class="row row-panel">

                    <div class="fitted col-xs-6" style="float: right">
                        <input id="acc_term" name="acc_term" tabindex="5" class="term_acc" type="checkbox">
                        <label>Accept terms and conditions</label>
                    </div>
                </div>

             <div class="panel-footer">
                <div class="signin-action-panel">
                    <div class="sign-link-panel">
                        <div>
                            <span>Already have an account? <a class="fancybox" href="#signin">Sign In</a></span>
                        </div>
                    </div>
                    <div class="button-panel">
                        {%csrftok%}
                        <button type="button" id="join" tabindex="6" class="hidden" >Sign Up</button>
                        <button type="button" id="submit" tabindex="6" class="ui submit btn btn-primary" onclick="if($('form.form').form('validate form')==true){  $('#join').trigger('click');}">Sign Up</button>
                    </div>
                </div>
            </div>
                <div class="ui error message"></div>
            </form>
            <div class="ui horizontal divider">
                OR
            </div>
            {% include "front/includes/custom_lock.php" %}
        </div>
    </div>
    <div class="ui icon success message hidden">
        <div class="content">
            <div class="header">
                Your user registration was successful.
            </div>
            <p>Please find your invitation email in your emailbox</p>
        </div>
        <i class="fa fa-envelope fa-4x"></i>
    </div>
</div>

<div id="signin" style="display: none;">
    <div id="signin-panel" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Sign in to your account</h3>
        </div>
        <div class="panel-body">
            <form action="{{app-context}}" id="signinfrm" method="POST">
                <div class="row row-panel">
                    <div class="col-sm-6 col-xs-12">
                        <label for="regemail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input autocomplete="true" name="username" type="email" class="form-control" id="regemail" placeholder="yourmail@example.com" required >
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <label for="newpwd">Password</label>
                        <input autocomplete="true" type="password" name="password" class="form-control" id="newpwd" placeholder="Password" required>
                    </div>
                </div>
                <div class="row row-panel">
                    <div class="ui toggle checkbox col-xs-6">
                      <input id="remme" name="remme" value="Remember Me" type="checkbox">
                      <label for="remme">Remember Me</label>
                    </div>
                </div>

              <div class="panel-footer">
                <div class="signin-action-panel">
                    <div class="sign-link-panel">
                        <div>
                            <span>Forgot Your Bestway password?</span>
                        </div>
                        <div>
                            <span>Don't have a Bestway account? <a class="fancybox" href="#signup">Sign Up</a></span>

                        </div>
                    </div>
                    <div class="button-panel">

                        {%csrftok%}


                        <input type="submit" id="login" class="btn btn-primary" value="Sign In" />
                    </div>
                </div>
            </div>
            </form>
            <div class="ui horizontal divider">
                OR
	    </div>
            {% include "front/includes/custom_lock.php" %}
        </div>
    </div>
</div>
{% endifequal %}

<div id="alert_container"></div>

<div id="loader_container"></div>
