<div id="bottomBar" class="container-fluid">
	<div class="row">
	<div id="sub_foot" class="copy col-md-12">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xm-4">
					<h4>About Us</h4>
					<ul>
						<li><a href="{{app-context}}about-us">About Us</a></li>
						<li><a href="{{app-context}}privacy-policy">Privacy Policy</a></li>
						<li><a href="{{app-context}}terms-service">Terms of Service</a></li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 col-xm-4">
					<h4>Help &amp; Support</h4>
					<ul>
						<li><a href="{{app-context}}faq">FAQ</a></li>
						<li><a id="show-contact"><i class="fa fa-comment" aria-hidden="true"></i> Contact Us</a></li>
					</ul>			
					
				</div>
				<div class="col-md-4 col-sm-4 col-xm-4">
					<h4>Find us in Social</h4>
					<ul>
						<li>
							<a href="https://www.facebook.com/Bestwaylanka" target="_blank"><img src="https://www.bestway.lk/imgs/fb.png"> Facebook</a>
						</li>
						<li>
							<a href="https://twitter.com/yourbestway" target="_blank"><img src="https://www.bestway.lk/imgs/tw.png"> Twitter</a>
						</li>
						<li>
							<a href="https://plus.google.com/u/0/103393849390183231085" target="_blank"><img src="https://www.bestway.lk/imgs/gp.png"> Google +</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/company/bestway.lk" target="_blank"><img src="https://www.bestway.lk/imgs/in.png"> LinkedIn</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="end_foot" class="copy col-md-12">
		<a href="http://www.bestway.lk" title="Best Way">&copy; {% now yyyy %} Bestway.lk <span>&trade;</span></a>
	</div>
	</div>
</div>

<div class="coupled modal">
<div id="contact-form" class="ui basic modal">
  <div class="ui icon header">
    <h1>Contact Us</h1>
  </div>
  <div class="content">
    
      <div class="row">
        <div id="subs" class="col-xs-12 ">
          <div>
          <form action="subscribe.php" id="subform" name="subform" method="post">
            <div class=" col-md-12"><input class="betextarea sub_name col-xs-12" name="uname" placeholder="Enter name here" type="text"></div>
            <div class=" col-md-12"><input class="betextarea sub_mail col-xs-12" name="email" placeholder="Enter email address here" type="email"></div>
            <div class=" col-md-12"><textarea class="betextarea bt-area sub_mail col-xs-12" name="message" placeholder="Message" ></textarea></div>
            <div class=" col-md-2"><input class="besubmit col-xs-12" name="subscribe" value="Submit" type="submit"></div>
          </form>
          </div>
        </div>
      </div>
  </div>
  <div class="actions">
    <div class="ui green ok inverted button">
    <i class="fa fa-at" aria-hidden="true"></i>
      Show Emails
    </div>
  </div>
</div>

<div id="email-con" class="ui basic modal">
 
      <div class="row">
        <div class="col-xs-12">
          <p>You can contact Bestway.lk Customer Service for all suggestions, objections, questions and queries on any of the following channels.</p>
        </div>
      </div>
      <div id="con-det-con" class="row">
        <div class="col-xs-12 col-md-6">
          <div class="ui piled segment">
            <h4 class="ui header">Emails</h4>
            <p>contact@bestway.lk</p>
            <p>hr@bestway.lk</p>
            <p>sales@bestway.lk</p>
            <p>technical@bestway.lk</p>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
          <div class="ui piled segment">
            <h4 class="ui header">Find Us</h4>
            <p>
              <a href="https://www.facebook.com/Bestwaylanka" target="_blank"><img src="https://www.bestway.lk/imgs/fb.png"> Facebook</a>
            </p>
            <p>
              <a href="https://twitter.com/yourbestway" target="_blank"><img src="https://www.bestway.lk/imgs/tw.png"> Twitter</a>
            </p>
            <p>
              <a href="https://plus.google.com/u/0/103393849390183231085" target="_blank"><img src="https://www.bestway.lk/imgs/gp.png"> Google +</a>
            </p>
            <p>
              <a href="https://www.linkedin.com/company/bestway.lk" target="_blank"><img src="https://www.bestway.lk/imgs/in.png"> LinkedIn</a>
            </p>
          </div>
        </div>
      </div>      
      <div class="actions">
        <div class="ui green ok inverted button">
          <i class="fa fa-check" aria-hidden="true"></i>
          OK
        </div>
      </div>

</div>
</div>