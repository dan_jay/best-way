    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'/>
    <link href="https://www.bestway.lk/css/semantic.css" rel="stylesheet" type="text/css" />

    <link href="https://www.bestway.lk/css/bootstrap.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" />
    <link href="{{app-context}}css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://www.bestway.lk/css/main.css" rel="stylesheet" type="text/css" />
  
  <link rel="shortcut icon" href="{{app-context}}img/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="{{app-context}}img/apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="57x57" href="{{app-context}}img/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="{{app-context}}img/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{app-context}}img/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="{{app-context}}img/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="{{app-context}}img/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="{{app-context}}img/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="{{app-context}}img/apple-touch-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="{{app-context}}img/apple-touch-icon-180x180.png" />
