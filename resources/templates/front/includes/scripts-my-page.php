<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script src="{{app-context}}js/jquery.js" type="text/javascript"></script>
<script src="{{app-context}}js/bootstrap/bootstrap.min.js"></script>
<script src="{{app-context}}js/semantic.js" type="text/javascript"></script>

<script src="{{app-context}}js/jquery.fancybox.js" type="text/javascript"></script>
<!-- <script src="{{app-context}}js/chosen.jquery.js" type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>

<script src="{{app-context}}js/app/chart.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9UzumFWNCH2xhZZFqwTNJee0MgnXZzMI&callback=initMap"
        type="text/javascript"></script>

    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }

        });
        $('span').on('click',function(e) {
		    if ($(this).hasClass('grid')) {
		        $('#ad-list-con ul').removeClass('list').addClass('grid');
		    }
		    else if($(this).hasClass('list')) {
		        $('#ad-list-con ul').removeClass('grid').addClass('list');
		    }
		});
    </script>

