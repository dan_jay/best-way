<div id="sc-log-sig" class="row">
    <div class="col-xs-4 col-md-4">
      <a class="signin-google-popup img-gp">Sign in with Google </a>
    </div>
    <div class="col-xs-4 col-md-4">
      <a class="signin-facebook-popup img-fb">Sign in with Facebook </a>
    </div>
    <div class="col-xs-4 col-md-4">
      <a class="signin-twitter-popup img-tw">Sign in with Twitter </a>
    </div>
</div>

<script src="https://cdn.auth0.com/w2/auth0-7.1.min.js"></script>
<script src="http://code.jquery.com/jquery.js"></script>
<script>

  var auth0 = new Auth0({
    domain:         'bestwaylk.eu.auth0.com',
    clientID:       'SUTx9p7MI0gycUaivjSG8Pcv5Z364qC2',
    //callbackURL:    'https://www.bestway.lk/callback'
    callbackURL:    'http://luminus.bestway.lk/auth0'
  });

  // sign-in with social provider using a popup (window.open)
  $('.signin-google-popup').on('click', function() {
    auth0.signin({popup: true, connection: 'google-oauth2'},
                function(err, profile, id_token, access_token, state) {
                    /*
                      store the profile and id_token in a cookie or local storage
                        $.cookie('profile', profile);
                        $.cookie('id_token', id_token);
                    */
                });
  });

  $('.signin-facebook-popup').on('click', function() {
    auth0.signin({popup: true, connection: 'facebook'},
                function(err, profile, id_token, access_token, state) {
                    /*
                      store the profile and id_token in a cookie or local storage
                        $.cookie('profile', profile);
                        $.cookie('id_token', id_token);
                    */
                });
  });

  $('.signin-twitter-popup').on('click', function() {
    auth0.signin({popup: true, connection: 'twitter'},
                function(err, profile, id_token, access_token, state) {
                    /*
                      store the profile and id_token in a cookie or local storage
                        $.cookie('profile', profile);
                        $.cookie('id_token', id_token);
                    */
                });
  });

  $('.signin-db').on('click', function() {
    auth0.signin({
      connection: 'foo',
      username: 'bar',
      password: 'foobar'
    },
    function (err, profile, id_token, access_token, state) {
      /*
          store the profile and id_token in a cookie or local storage
            $.cookie('profile', profile);
            $.cookie('id_token', id_token);
        */
    });
  });
</script>
