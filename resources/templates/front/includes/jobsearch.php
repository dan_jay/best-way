<div id="adSearch" class="container">
    <div id="adSearchCon" class="row">
    <div class="col-xs-12">
          <form class="form-inline">
              <div class="search-items col-md-4">
                <input type="text" id="ad_search" placeholder="Search..." class="mainSearch maxmarg search_control">
              </div>
              <div class="search-items col-md-2">
                <select id="search_cat" data-placeholder="Choose Category" class="chosen-select search_control">
                    <option value=""></option>
                    <option value="United States">United States</option>
                    <option value="United Kingdom">United Kingdom</option>
                </select>
              </div>
              <div class="search-items col-md-2">
                <select id="search_cat" data-placeholder="Contract Type" class="chosen-select search_control">
                    <option value=""></option>
                    <option value="All">All Types</option>
                    <option value="Full time">Full time</option>
                    <option value="Part time">Part time</option>
                    <option value="Contract">Contract</option>
                    <option value="Internship">Internship</option>
                    <option value="Temporary">Temporary</option>
                    <option value="Traineeship">Traineeship</option>
                    <option value="Other">Other</option>
                </select>
              </div>
              <div class="search-items col-md-2">
                <select id="search_sub_cat" data-placeholder="Choose a District" class="chosen-select search_control">
                    <option></option>
                    <option>Anywhere in Sri Lanka</option>
                    <option>Select District</option>
                </select>
              </div>
              <div class="search-items col-md-2">
                
                  <input id="main_s_b" type="submit" value="Search">
               
              </div>
          </form>
          </div>
    </div>
</div>