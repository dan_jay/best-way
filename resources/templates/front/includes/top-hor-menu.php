<div id="top-v-men" class="container-fluid">
  <div class="container">
    <div class="ui icon menu">
      <a id="sh-cat" class="item" data-content="Ad Categories" data-position="bottom center" data-variation="mini">
        <i class="fa fa-list" aria-hidden="true"></i>
      </a>
      <a id="shw-srch" class="item" data-content="Search" data-position="bottom center" data-variation="mini">
        <i class="fa fa-search" aria-hidden="true"></i>
      </a>
      <a id="shw-home" href="{{app-context}}home" class="item" data-content="Home" data-position="bottom center" data-variation="mini">
        <i class="fa fa-home" aria-hidden="true"></i>
      </a>
      <a id="shw-profile" href="{{app-context}}my-profile" class="item" data-content="Profile" data-position="bottom center" data-variation="mini">
        <i class="fa fa-user" aria-hidden="true"></i>
      </a>
      <a id="shw-settings" href="{{app-context}}account-settings" class="item" data-content="Settings" data-position="bottom center" data-variation="mini">
        <i class="fa fa-cog" aria-hidden="true"></i>
      </a>
      <a id="shw-logout" href="{{app-context}}logout" class="item" data-content="Log Out" data-position="bottom center" data-variation="mini">
        <i class="fa fa-power-off" aria-hidden="true"></i>
      </a>
    </div>
  </div>
</div>
