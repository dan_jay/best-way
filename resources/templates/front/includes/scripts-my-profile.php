<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>-->
<script src="{{app-context}}js/jquery.js" type="text/javascript"></script>
<!--<script src="js/app/chart.js"></script>-->
<script src="{{app-context}}js/bootstrap/bootstrap.min.js"></script>
<script src="{{app-context}}js/semantic.js" type="text/javascript"></script>
<script src="{{app-context}}js/jquery.fancybox.js" type="text/javascript"></script>
<script src="{{app-context}}js/chosen.jquery.js" type="text/javascript"></script>


    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        })
        $(document).ready(function() {


            /*
             *  Simple image gallery. Uses default settings
             */

        $('.fancybox').fancybox();

        $('#prof_pg').dropdown({
            on: 'hover'
        });
            /*
             *  Different effects
             */
        });
    </script>
        {% script "js/web/main.js" %}
        {% script "js/bwinc.js" %}
        {% script "js/user.js" %}

<script type="text/javascript" >
                               $(window).load(function()
                                     {
                                         bestway.index.user.upload_fns();
                                         }
                                      );
 </script>


