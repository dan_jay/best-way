<div id="sc-log-sig" class="row">
    <div class="col-xs-4 col-md-4">
      <a class="signin-google-popup img-gp">Sign in with Google </a>
    </div>
    <div class="col-xs-4 col-md-4">
      <a class="signin-facebook-popup img-fb">Sign in with Facebook </a>
    </div>
    <div class="col-xs-4 col-md-4">
      <a class="signin-twitter-popup img-tw">Sign in with Twitter </a>
    </div>
</div>

<script src="https://cdn.auth0.com/w2/auth0-7.1.min.js"></script>
<script src="http://code.jquery.com/jquery.js"></script>
<script>

function authGet(p){
    var token = "";
    $.ajax({
        headers: {"x-csrf-token": $("#__anti-forgery-token").val()},
        url: "http://luminus.bestway.lk/auth0",
        type: "POST",
        dataType: "json",
        data: {code: p.accessToken},
        success: function (res) {
            console.log(res);
            window.location.href ="http://luminus.bestway.lk/auth/" + res.authToken;
        }

    });


}

var auth = new Auth0({
    domain:         'bestwaylk.eu.auth0.com',
    clientID:       'SUTx9p7MI0gycUaivjSG8Pcv5Z364qC2',
    callbackURL:    'http://luminus.bestway.lk/auth0',
    responseType:   'token',
    redirect_uri:   'http://luminus.bestway.lk/auth0'
    //responseMode:   'form_post'
});

// sign-in with social provider using a popup (window.open)
$('.signin-google-popup').on('click', function() {
    auth.signin({popup: true, connection: 'google-oauth2'},
        function(err, profile) {
            console.log(profile);

            authGet(profile);
        });
});

$('.signin-facebook-popup').on('click', function() {
    auth.signin({popup: true, connection: 'facebook'},
        function(err, profile) {

                authGet(profile);

        });
});

$('.signin-twitter-popup').on('click', function() {
    auth.signin({popup: true, connection: 'twitter'},
        function(err, profile) {

            authGet(profile);

        });
});

</script>

