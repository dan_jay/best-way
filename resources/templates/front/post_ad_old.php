<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway Business Directory</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="css/imageup/jquery.fileupload.css">
    <link rel="stylesheet" href="css/imageup/jquery.fileupload-ui.css">
    <noscript><link rel="stylesheet" href="css/imageup/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="css/imageup/jquery.fileupload-ui-noscript.css"></noscript>

<style type="text/css">
    body{
    	background-color: #E9EBEE;
    }
    #cover-con {
        min-height: 260px;
        background-image: url("img/adpost/mban.jpg");
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    #adv-post{
        margin-top: 15px;
        min-height: 300px;
        background-color: #FFFFFF;
    }
    #adv-post h2{
        margin-top: 15px;
        padding-bottom: 10px;
        color: rgba(0,0,0,.6);
        text-align: center;
    }
    #its-free{
        text-align: center;
    }
    #free-content{
        margin:0px auto;
        width: 40%;        
    }
    #shout-img{
        width: 70px;
        float: left;
    }
    #f-t-p{
        font-size: 22px;
        font-weight: bold;
        color:  #2980b9;
    }
    #i-w-e{
        font-size: 18px;
        font-weight: bold;
        color:  #e74c3c;
    }
    #looking{
        border: 1px solid #95CC6B;
    }
    #offer, #looking{
        min-height: 300px;
        border-radius: 3px;
    }
    /* temp
    #offer-form{
        display: none;
    }
    */
    #offer-form, #looking-form{
        margin-top: 15px;
        padding: 0px 15px;
        display: none;
    }
    .t-content{
        padding: 5px;
    }
    .f-border{        
        border: 1px solid;
        border-color: #e5e6e9 #dfe0e4 #d0d1d5;
        border-radius: 3px;
    }
    .t-select{
        padding: 15px;
        width: 100%;
        display: inline-block;
        background-color: #F6F7F9;
        font-size: 20px;
        font-weight: bolder;
        text-align: center;
        cursor: pointer;
    }
    .t-select:hover{
        background-color: #2BA8E3;
        color: #FFFFFF;
    }
    .colour_purple{
        color: #9E5BA1;
    }
    .colour_green{
        color: #95CC6B;
    }

    /*********************** over rides **********************/
    .inputfile {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
    }
    .ui.icon.buttons > button {
      margin: 2px 1px;
      padding: 10px 22px !important;
    }
    #offer-form .ui.header {
      color:  #d35400;
      margin-bottom: 20px;
      border-bottom: 1px solid #e59866;
    }
    #offer-form .ui.form .field > label , #offer-form .ui.form .inline.fields > label {
      width: 130px;
      font-size: 0.9rem;
    }
    #offer-form .ui.selection.dropdown, #offer-form .ui.form input[type="text"], #offer-form .ui.form textarea {
      border: 1px solid #85c1e9;
    }
    #offer-form .ui.selection.dropdown:focus, #offer-form .ui.form input[type="text"]:focus, #offer-form .ui.form textarea:focus {
      border: 1px solid #28b463;
    }

    
</style>
</head>
<body>
<main id="main-con">
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}
    <header id="cover-con" class="container-fluid">
        
    </header>
    <div id="adpost-con" class="container-fluid">
        <div id="adv-post" class="container f-border">
            <div class="row">
                <h2>Create your Advertisement</h2>
                <div class="ui divider"></div>
                <div class="t-content">
                    <div class="col-md-6">
                        <div id="offer-btn" class="t-select colour_purple f-border">
                            <span class="">Offer Something</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="looking-btn" class="t-select colour_green f-border">
                            <span class="">Looking for Something</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="form-container" class="row">
                <div id="offer-form" class="col-md-8">
                    <form id="offer" class="ui form col-md-12">
                        <h4 class="ui dividing header">Details about your advertisement</h4>
                          <div class="inline fields">
                            <label>Select the type</label>
                            <div class="field">
                              <div class="ui checkbox">
                                <input id="for-sale" class="hidden" name="for-sale" type="checkbox"/>
                                <label for="for-sale">For Sale</label>
                              </div>
                            </div>
                            <div class="field">
                              <div class="ui checkbox">
                                <input id="for-rent" class="hidden" name="for-rent" type="checkbox"/>
                                <label for="for-rent">For Rent</label>
                              </div>
                            </div>
                          </div>

                          <div class="field">
                            <label>Select a Category</label>
                                <select class="ui fluid dropdown" id="cat-select">
                                  <option value="">Category</option>
                              {%for x,y in categories %}
                                    <option class='home-cat-listing' value="{{y}}">{{x|name}}</option>
                              {% endfor %}
                                </select>
                          </div>
                          <div class="field">
                            <label>Select Sub Category</label>
                            <select class="ui fluid dropdown" id="sub-cat-select">
                              <option value="">Sub Category</option>
                              <option value="1">Sub Cat 1</option>
                              <option value="2">Sub Cat 2</option>
                              <option value="3">Sub Cat 3</option>
                              <option value="4">Sub Cat 4</option>
                            </select>
                          </div>

                          <div class="inline fields">
                            <label>Condition</label>
                            <select class="ui fluid dropdown" id="condition">
                              <option value="">Condition</option>
                              <option value="1">Used</option>
                              <option value="2">New</option>
                            </select>
                          </div>

                          <div class="field">
                            <label>Item Type</label>
                            <select class="ui fluid dropdown" id="item-type">
                              <option value="">Item Type</option>
                              <option value="1">Type 1</option>
                              <option value="2">Type 2</option>
                            </select>
                          </div>
                          <div class="field">
                            <label>Title of your Ad</label>
                            <div class="ui input">
                              <input name="ad-title" id="ad-title" placeholder="Ad Title" type="text"/>
                            </div>
                          </div>
                          <div class="field">
                            <label>Description</label>
                            <textarea></textarea>
                          </div>
                          <div class="inline field">
                            <label>Price</label>
                            <div class="ui Left labeled input">                                 
                              <div class="ui teal label">Rs.</div>
                              <input placeholder="Price" type="text">
                            </div>
                           </div>

                           <div class="ui horizontal segments">
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-1" id="file-1" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-2" id="file-2" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-3" id="file-3" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-4" id="file-4" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-5" id="file-5" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                          </div>
                          <div class="ui horizontal segments">
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-1" id="file-1" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-2" id="file-2" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-3" id="file-3" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-4" id="file-4" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-5" id="file-5" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                          </div>

                          <h4 class="ui dividing header">Ad Location</h4>
                          <div class="two fields">
                            <div class="field">
                                <label>District</label>
                                <select class="ui fluid dropdown" id="ad-district">
                                  <option value="">Select District</option>
                                  <option value="1">Colombo</option>
                                  <option value="2">Kaluthara</option>
                                  <option value="3">Gampaha</option>
                                  <option value="4">Galle</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>City</label>
                                <select class="ui fluid dropdown" id="ad-city">
                                  <option value="">Select City</option>
                                  <option value="1">Kadawatha</option>
                                  <option value="2">Mahara</option>
                                  <option value="3">Kiribathgoda</option>
                                  <option value="4">Ganemulla</option>
                                </select>
                            </div>
                          </div>
                          
                          <div class="ui button" tabindex="0">Post Ad</div>

                    </form>
                </div>
                <div id="looking-form" class="col-md-12">
                    <form id="looking" class="col-md-12">
                        
                    </form>
                </div>
            </div>
            <div class="ui divider"></div>
            <div id="its-free">
                <div id="free-content" class="ui compact segment">
                  <img id="shout-img" src="img/adpost/shout.gif"/>
                  <p id="f-t-p">It's free to post ads on Bestway.lk</p>
                  <p id="i-w-e">It will ever!</p>
                </div>
            </div>
        </div>
    </div>
    
</main>
{% include "front/includes/footer.php" %}

    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/semantic.js" type="text/javascript"></script>
    <script src="js/bootstrap/bootstrap.js"></script>
    <script src="js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="js/chosen.jquery.js" type="text/javascript"></script>

        
    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        })
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

                /*
                 *  Different effects
                 */

            $('#offer-btn').click(function(){
                $( "#offer-form" ).slideDown( "slow" );
                $( "#looking-form" ).slideUp( "slow" );
            });
            $('#looking-btn').click(function(){
                $( "#looking-form" ).slideDown( "slow" );
                $( "#offer-form" ).slideUp( "slow" );
            });

            $('#cat-select').dropdown();
            $('#sub-cat-select').dropdown();
            $('#condition').dropdown();
            $('#item-type').dropdown();
            $('#c-type').dropdown();
            $('#ad-district').dropdown();
            $('#ad-city').dropdown();

            //$('.button').popup();

        });

        $(".img-btn").click(function(e){
          e.preventDefault();
        });

        $(".s-btn").click(function(){
          //var elid = 
              $(this).siblings("input").click().next('button').removeClass('disabled').addClass('red');
          //alert(elid);
        });

        // function uploadImage($img){
        //   var formdata = new FormData($img);
        //   var request = new XMLHttpRequest();
        //   request.open('post', 'imageupload');
        //   request.send(formdata);
        // }
    </script>

</body>
</html>