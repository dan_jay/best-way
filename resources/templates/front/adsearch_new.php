<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bestway Search</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'/>
	<link href="css/main.css" rel="stylesheet" type="text/css" />
	<link href="css/semantic.css" rel="stylesheet" type="text/css" />
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="css/chosen.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />

	<style type="text/css">
	body{
		background-color: #F9F9F9;
	}
	#allads{
		margin-top: 5px;
		padding: 0px;
	}
	#contentArea{
		padding-right: 5px;
	}
	#ad-con ul {
		list-style: none;
		padding: 0px;
		margin-top: 1px;
	}
	#ad-con .grid{
		margin-right: 5px;
	}
	#ad-con .buttons {
		right: 10px;
    	position: absolute;
	}
	#ad-con .list li {
		width: 664px;
		border: 1px solid #d3e0e9;
		margin-bottom: 0px;
		padding-bottom: 10px;
	}
	#ad-con .grid li {
		float: left;
		border: 1px solid #d3e0e9;
	}
	#ad-list-con{
		overflow: auto;
		margin: 0px;
	}
	.grid .ad{
		width: 330px;
		height: auto;
		margin: 5px 0px 5px 2px;
	}
	.ad .itm_img{
		border: 1px solid #d3e0e9;
	}
	.grid .adprice, .list .adprice{
		font-size: 1.7em;
	    font-family: 'Roboto', sans-serif;
	    font-weight: 600;
	    color: #009432;
	    display: inline;
	}
	.grid .itm_img_con{
		margin-bottom: 5px;
	}
	.grid .ad_txt{
		margin-top: 2px;
		font-size: 12px;
	}
	.list .ad{
		height: auto;
	}
	.list img{
		width: 140px;
	}
	.list .itm_img_con{
		margin-right: 25px;
	}
	.list .itm_img_con{
		float: left;
	}
	.list .txt_con{
	}
	.price .ui.label{
		padding-left: 0.2em !important;
		padding-right: 0.2em !important;
	}
	.price .field{
		padding-left: 0.25em !important;
    padding-right: 0.25em !important;
	}
	#price-range{
		font-size: 11px;
    line-height: 36px;
	}
	#price-refine{
		padding-left: 0.5em !important;
		padding-right: 0.5em !important;
	}
	/******** Overrides *********/
	#leftcol_con{
		padding-right: 0px;
	}
	#content_area_con{
		padding-left: 0px;
	}
	#leftcol{
		background-color: #FFFFFF;
		border: 1px solid #d3e0e9;
		padding: 5px;
		border-radius: 4px;
	}
	#leftcol h3{
		font-size: 16px;
	}
	#leftcol h4{
		font-size: 14px;
		color: #666;
	}
	#steps-fivepercent-slider {
	    visibility: visible;
	    opacity: 1;
	    top: -30px;
	}
	.ui-widget-header {
		background: #96BF49;
	}
	.range_price{
		font-size: 13px !important;
		padding: 5px !important;
		font-weight: bold;
		color: #666 !important;
	}
	.fa {
    	margin-right: 0px !important;
	}
	.ui.segment {
		margin: 0.5rem 0rem;
	}
	.ad .ui.segment{
		margin: -5px -15px -10px;
		height: 100px;
		border-radius: 0px;
	}
	/****************************/
</style>


</head>


<div id="main-con">
	<!-- top bar navigation goes here -->
	{% include "front/includes/top_nav.php" %}

	{% include "front/includes/promo_bar.php" %}

	<div id="main-wrap" class="container">
		<div id="allads" class="row">
			<div id="leftcol_con" class="col-md-3">
				<div id="leftcol">
					<h3>Filter your results</h3>
					<div class="ui divider"></div>
					<form class="ui form">

						<div class="ui action input fluid">
							<input id="txt-query" placeholder="Search..." type="text">
							<button id="btn-query" class="ui icon orange button">
								<i class="fa fa-search"></i>
							</button>
						</div>



						<h4>Category</h4>
						<select id="cat-select" name="cat-select" class="ui fluid dropdown" >
							<option value="">Category</option>
							{% for y in catz %}
							{{y|safe}}
							{% endfor %}
						</select>

						<h4>Sub Category</h4>
						<select id="sub-cat-select" class="ui fluid dropdown" name="sub-cat-select">
							<option value="">Sub Category</option>
	                        {% for y in subcatz %}

	                            {% for z in y %}
							        {{z|safe}}
							     {% endfor %}

							{% endfor %}

						</select>

						<h4>Price Range</h4>
						<div class="field">
							<div class="two fields price">
								<div class="field">
									<div class="ui left labeled input">
										<div class="ui label">Rs.</div>
										<input id="min-price" placeholder="Minimum" class="range_price">
									</div>
								</div>
								<span id="price-range">to</span>
								<div class="field price">
									<div class="ui left labeled input">
										<div class="ui label">Rs.</div>
										<input id="max-price" placeholder="Maximum" type="text" class="range_price">
									</div>
								</div>
								<button id="price-refine" class="ui icon orange button">
  							  <i class="fa fa-angle-double-right"></i>
								</button>
							</div>
						</div>
						<h4>Location</h4>
						<select class="ui fluid dropdown" id="loc-select">
							<option value="any">Location</option>
							{%for u,t in locations %}
							<option class="sub" >{{u|first|name}}</option>
							{%endfor%}
						</select>
					</form>
				</div>
			</div>
			<div id="content_area_con" class="col-md-7">
				<div id="contentArea">
					<article id="class-con">
						<div id="ad-con">
							<div class="ui segment">
								<select class="ui dropdown" id="res-p-page">
									<option value="">Results per page</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="50">50</option>
								</select>
								<select class="ui dropdown" id="sort-search">
									<option value="1">Latest first</option>
									<option value="2">Oldest first</option>
									<option value="3">Highest price first</option>
									<option value="4">Lowest price first</option>
									<option value="5">Sort by most viewed</option>
									<option value="6">Sort by highest rated</option>
								</select>
								<div class="ui right buttons">
									<span class="btn btn-info grid"><i class="fa fa-th-large"></i></span>
									<span class="btn btn-info list"><i class="fa fa-th-list"></i></span>
								</div>
							</div>

							<div class="ui pagination menu">
							  <a class="active item">
							    1
							  </a>
							  <div class="disabled item">
							    ...
							  </div>
							  <a class="item">
							    10
							  </a>
							  <a class="item">
							    11
							  </a>
							  <a class="item">
							    12
							  </a>
							</div>

							<div id="ad-list-con" class="row">
								<ul id="result-list" class="list">

									<li id="" class="ad col-md-6">
						        <div class="ui segment">
										  <div class="ui active dimmer">
										    <div class="ui mini text loader">Loading</div>
										  </div>
										  <p></p>
										</div>
					        </li>

									<li id="" class="ad col-md-6">
										<div class="itm_wrapper">
											<h1><a href="#">Ad Name</a></h1>

											<h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
											<div class="itm_img_con">
												<img src="img/2.jpg" class="itm_img">
											</div>
											<div class="txt_con">
												<p class="adprice">Rs. 12,000.00</p>
												<p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
											</div>
										</div>
									</li>
									<li id="" class="ad col-md-6">
										<div class="itm_wrapper">
											<h1><a href="#">Ad Name</a></h1>

											<h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
											<div class="itm_img_con">
												<img src="img/2.jpg" class="itm_img">
											</div>
											<div class="txt_con">
												<p class="adprice">Rs. 12,000.00</p>
												<p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
											</div>
										</div>
									</li>
								</ul>
							</div>

							<div class="ui pagination menu">
							  <a class="active item">
							    1
							  </a>
							  <div class="disabled item">
							    ...
							  </div>
							  <a class="item">
							    10
							  </a>
							  <a class="item">
							    11
							  </a>
							  <a class="item">
							    12
							  </a>
							</div>

						</div>
					</article>
				</div>
			</div>

			<div id="rightcol" class="col-md-2">
				<div id="rightcol">
				</div>
			</div>

		</div>

	</div>
</div>

<!-- page footer goes here -->


<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/semantic.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script src="js/app/chart.js" type="text/javascript"></script>


<script type="text/javascript">
      $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        });
        $('span').on('click',function(e) {
		    if ($(this).hasClass('grid')) {
		        $('#ad-list-con ul').removeClass('list').addClass('grid');
		    }
		    else if($(this).hasClass('list')) {
		        $('#ad-list-con ul').removeClass('grid').addClass('list');
		    }
		});
$(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

        $('.fancybox').fancybox();

            /*
             *  Categories
             */
            $('#cat-select').chosen().on('change',
                 function(){
                            var chosenId =  $(this).val();
                            var sub = $(this).parent().parent().find('#sub-cat-select');
                            sub.find('option').not('option:first').hide();
                            sub.find('[data-parent-id='+chosenId+']').show();
                            sub.chosen('updated');
                }
            );

            $('#loc-select').dropdown();
            $('#res-p-page').dropdown();
            $('#sort-search').dropdown();

//$( "#slider-range" ).slider({
//      range: true,
//      min: 0,
//      max: 100000,
//      values: [ 5000, 50000 ],
//      slide: function( event, ui ) {
//        $( "#amount" ).val( "Rs. " + ui.values[cat-select ] + " - Rs. " + ui.values[ 1 ] );
//      }
//    });

//    $( "#amount" ).val( "Rs. " + $( "#slider-range" ).slider( "values", 0 ) +
//      " - Rs. " + $( "#slider-range" ).slider( "values", 1 ) );

	$('div.ui.right.buttons span').on('click',function(e) {
            if ($(this).hasClass('grid')) {
                $('#ad-list-con ul').removeClass('list').addClass('grid');
            }
            else if($(this).hasClass('list')) {
                $('#ad-list-con ul').removeClass('grid').addClass('list');
            }
        });
});
    </script>

{% script "/js/app.js" %}

</body>
</html>
