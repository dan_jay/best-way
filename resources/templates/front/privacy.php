<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <base href="/front/" ></base> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway | Privacy Policy</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway|Crimson+Text|Open+Sans+Condensed:300|Peddana" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/AdminLTE.css">
    <link rel="stylesheet" href="css/home.css">
    <!-- <link href="css/chosen.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <style type="text/css">
    h1{
      font-family: 'Raleway', sans-serif;
      font-size: 35px;
      line-height: 40px;
      color: #990066;
    }
    h2{
      font-family: 'Open Sans Condensed', sans-serif;
      font-size: 30px;
      color: #FF4500;
    }
    p{
      font-family: 'Crimson Text', serif;
      font-size: 16px;
      line-height: 24px;
    }
    p ul li{
      font-family: 'Peddana', serif;
      font-size: 18px;
      line-height: 22px;
    }
    p, li{
      color: #AAAAAA;
    }
    </style>
    </head>
    <body>
    <div id="main-con">
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}


    <!-- promo bar goes here -->
        {% include "front/includes/promo_bar.php" %}

        {% include "front/includes/top-hor-menu.php" %}
         <!-- <div class="global-page"> -->
            {% include "front/includes/main_search.php" %}
        <!-- </div> -->

    <div id="main-wrap" class="container">
<h1>Bestway.lk Privacy Policy</h1>

<p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in Sri Lankan privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>

<h2>What information do we collect?</h2>
<p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, credit card information or other details to help you with your experience.</p>

<h2>When do we collect information?</h2>
<p>We collect information from you when you register on our site, fill out a form or enter information on our site.</p>

<h2>How do we use your information?</h2>
<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
<ul>
  <li>To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
  <li>To improve our website in order to better serve you.</li>
  <li>To allow us to better service you in responding to your customer service requests.</li>
  <li>To administer a contest, promotion, survey or other site feature.</li>
  <li>To quickly process your transactions.</li>
  <li>To ask for ratings and reviews of services or products</li>
  <li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
</ul>

<h2>How do we protect your information?</h2>
<p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.
We use regular Malware Scanning.</p>
<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
<p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p>
<p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>

<h2>Do we use 'cookies'?</h2>
<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

<p>We use cookies to:</p>
<ul>
  <li>Help remember and process the items in the shopping cart.</li>
  <li>Understand and save user's preferences for future visits.</li>
  <li>Keep track of advertisements.</li>
  <li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</li>
</ul>
<p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>

<h2>If users disable cookies in their browser.</h2>
<p>If you turn cookies off, some features will be disabled. Some of the features that make your site experience more efficient and may not function properly.
</p>
<p>However, you will still be able to place orders.</p>

<h2>Third-party disclosure.</h2>
<p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when its release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.</p>

<p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

<h2>Third-party links.</h2>
<p>Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>

<h2>Google.</h2>
<p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">Here</a>
We use Google AdSense Advertising on our website.</p>

<p>Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>

<p>We have implemented the following,</p>
<ul>
  <li>Remarketing with Google AdSense</li>
  <li>Google Display Network Impression Reporting</li>
</ul>

<p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.</p>

<!-- Opting	out:
Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt out Browser add on.
 -->

<h2>Electronic Transactions Act.</h2>

<p>The most relevant legislation for use of ICT in government and establishment of e-government services is the Electronic Transactions Act No. 19 of 2006. The Electronic Transactions Act was brought into operation with effect from 1st October 2007.</p>
<p>The Electronic Transactions Act No. 19 of 2006 is based on the standards established by United Nations Commission on International Trade Law (UNCITRAL) Model Law on Electronic Commerce (1996) and Model Law on Electronic Signatures (2001).</p>
<p>The objectives of the Act as are as follows</p>
<ul>
  <li>to facilitate domestic and international electronic commerce by eliminating legal barriers and establishing legal certainty.</li>
  <li>to encourage the use of reliable forms of electronic commerce.</li>
  <li>to facilitate electronic filing of documents with government and to promote efficient delivery of government services by means of reliable forms of electronic communications and</li>
  <li>to promote public confidence in the authenticity, integrity and reliability of data messages and electronic communications. This has ensured that electronic communication is officially and legally accepted as a proper means of communication (emphasis added).</li>
</ul>
<p>Based on this Act steps could now be taken by government organizations to provide services by electronic means as well as to retain data and information in electronic form.</p>
<p>As a follow-up to the enactment of the Electronic Transactions Act, Sri Lanka became one of the first three countries in the Asian Region (and first country in South Asia) to sign the United Nations Convention on the Use of Electronic Communications in International Contracts. This was consequent to a Cabinet decision initiated by the Ministry of Science and Technology.</p>
<p>The Convention aims to enhance legal certainty and commercial predictability where electronic communications are used in relation to international contracts. It addresses the determination of a party’s location in an electronic environment; the time and place of dispatch and receipt of electronic communications; the use of automated message systems for contract formation; and the criteria to be used for establishing functional equivalence between electronic communications and paper documents – including “original” paper documents – as well as between electronic authentication methods and hand-written signatures.</p>

<h2>Data Protection.</h2>
<p>At present the Government is pursuing a policy based on the adoption of a Data Protection Code of Practice, encompassing the private sector, with the possibility of the code being placed on a statutory footing through regulations issued under the Information and Communication Technology Act of 2003. As such, this approach can be seen as self- or co-regulatory approach.</p>

<p>You will be notified of any Privacy Policy changes:</p>
<ul>
  <li>On our Privacy Policy Page</li>
</ul>
<p>Can change your personal information:</p>
<ul>
  <li>By visiting your profile page</li>
</ul>

<!-- How does our site handle Do Not Track signals?
We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.

Does our site allow third-party behavioral tracking?
It's also important to note that we allow third-party behavioral tracking -->

<!-- COPPA (Children Online Privacy Protection Act)

When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.
We do not specifically market to children under the age of 13 years old. -->
<h2>Children Online Privacy.</h2>
<p>When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection puts parents in control. Parents must monitor their children and protect children's privacy and safety online.</p>
<p>We do not specifically market to children under the age of 13 years old.</p>

<h2>Fair Information Practices.</h2>
<p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
<p>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:
We will notify you via email</p>
<ul>
  <li>Within 1 business day</li>
</ul>

<p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>

<h2>CAN SPAM Act.</h2>
<p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>
<p>We collect your email address in order to:</p>
<ul>
  <li>Send information, respond to inquiries, and/or other requests or questions</li>
</ul>
<p>To be in accordance with CANSPAM, we agree to the following:</p>
<ul>
  <li>Not use false or misleading subjects or email addresses.</li>
</ul>
<p>If at any time you would like to unsubscribe from receiving future emails, you can email us at and we will promptly remove you from ALL correspondence.</p>


<h2>Contacting Us.</h2>

<p>If there are any questions regarding this privacy policy, you may contact us using the information below.</p>
<p>Website : <a href="https://www.bestway.lk">Bestway.lk</a></p>
<p>Email : <a href="contact@bestway.lk">contact@bestway.lk</a></p>

<p>Last Edited on 2016-12-18</p>

</div>
</div>
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts.php" %}
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/sidebar.js"></script>
<script>
$(document).ready(function(){
    $("#shw-srch").click(function(){
        $("#mainSearchCon").slideToggle("slow");
    });
    //$('select').chosen({width: '100%'});
    $('#sh-cat').click(function(){
        $("#leftCol").slideToggle("slow");
      //  $('#contentArea').removeClass("col-md-7").addClass("col-md-9");
    });
    //$(this).removeClass("classname");
});
</script>
</body>
</html>
