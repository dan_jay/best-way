<!DOCTYPE html>
<html lang="en">
<head >
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BestWay.lk | Login </title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/login.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">


</head>
<body>

<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}

<!-- main start -->
{% include "front/includes/main_search.php" %}
<main id="main-wrap" class="container-fluid">
    
    <!-- promo bar goes here -->


    <div id="MainContentCol">
        <h1 id="slogan">The Biggest online market in Sri Lanka.</h1>
        <h2 id="miniSlogan">No more outside shopping. All in one. Buy, Sell and Earn!</h2>

        <div>
            <div class="globalCon">

                <div id="signup-panel" class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Sign up now for exclusive deals!</h3>
                  </div>
                  <div class="panel-body">

                    <form>
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="fname">First Name</label>
                                <input type="text" class="form-control" id="fname" placeholder="Jane Doe">
                            </div>
                            <div class="col-xs-6">
                                <label for="lname">Last Name</label>
                                <input type="text" class="form-control" id="lname" placeholder="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="regemail">Email</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control" id="regemail" placeholder="jane.doe@example.com">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label for="newpwd">Password</label>
                                <input type="password" class="form-control" id="newpwd" placeholder="">
                            </div>
                        </div>
                      
                    </form>

                  </div>
                  <div class="panel-footer">
                    <button type="submit" class="btn btn-default">Send invitation</button>
                  </form>
                  </div>
                </div>

            </div>

        </div>


    </div>
</main>

<div class="divisionBar">
    <div class="globalConIn">

    </div>
</div>

<!-- footer bar -->
{% include "front/includes/footer.php" %}

<!--<div id="toolWidget"></div>-->
{% include "front/includes/scripts-index-page.php" %}
</body>
</html>