<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway Employer Profile</title>
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
        
    <script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>    
    <!-- <script src="https://use.fontawesome.com/f7f916392e.js"></script> -->


    <style type="text/css">
    body{
    	background-color: #FCFCFC;
    }
    #main-con {
        border: 1px solid #ccc;
    }

    #post_cav fieldset {
    background: white;
    border: 0 none;
    border-radius: 3px;
    padding: 20px 30px;
    
    box-sizing: border-box;
    
    /*stacking fieldsets above each other*/
    position: absolute;
}

    #post_cav fieldset:not(:first-of-type) {
    display: none;
}
    
    </style>
</head>
<body>
	<div id="main-con">
		<!-- top bar navigation goes here -->
    	<?php $this->load->view('includes/top_nav'); ?>

        <div class="container">
            <div class="row">
                <div class="ui stackable menu">
                  <a class="item">Manage Vacancies</a>
                  <a class="item">Post a Vacancy</a>
                  <a class="item">Company Profile</a>
                  <div class="ui simple dropdown item">
                      More
                      <i class="dropdown icon"></i>
                      <div class="menu">
                        <a class="item"><i class="fa fa-pencil-square-o"></i> Edit Profile</a>
                        <a class="item"><i class="globe icon"></i> Choose Language</a>
                        <a class="item"><i class="settings icon"></i> Account Settings</a>
                      </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="post_step" class="ui steps">
                  <div class="active step">
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                    <div class="content">
                      <div class="title"> Vacancy Details</div>
                      <!-- <div class="description">Vacancy info</div> -->
                    </div>
                  </div>
                  <div class="disabled step">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <div class="content">
                      <div class="title"> Candidate</div>
                      <!-- <div class="description">Candidate info</div> -->
                    </div>
                  </div>
                  <div class="disabled step">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                    <div class="content">
                      <div class="title"> Confirm &amp; Post</div>
                    </div>
                  </div>
                </div>
                <div id="post_cav">
                    <!-- multistep form -->

                    <form id="example-advanced-form" action="#" class="ui form">
                        
                        <fieldset>

                            <h4 class="ui dividing header">About Vacancy</h4>
                              
                            <div class="required field">
                                  <label>Job Category</label>
                                  <div class="ui fluid selection dropdown">
                                    <input name="gender" type="hidden">
                                    <div class="default text">Specify job category</div>
                                    <i class="dropdown icon"></i>
                                    <div class="menu">
                                        <div class="item" data-value="SDQ">IT-Sware/DB/QA/Web/Graphics/GIS</div>
                                        <div class="item" data-value="HNS">IT-HWare/Networks/Systems</div>
                                        <div class="item" data-value="ACA">Accounting/Auditing/Finance</div>
                                        <div class="item" data-value="BAF">Banking/Insurance</div>
                                        <div class="item" data-value="SMM">Sales/Marketing/Merchandising</div>
                                        <div class="item" data-value="HAT">HR/Training</div>
                                        <div class="item" data-value="COM">Corporate Management/Analysts</div>
                                        <div class="item" data-value="OAS">Office Admin/Secretary/Receptionist</div>
                                        <div class="item" data-value="CCE">Civil Eng/Interior Design/Architecture</div>
                                        <div class="item" data-value="ITT">IT-Telecoms</div>
                                        <div class="item" data-value="CUR">Customer Relations/Public Relations</div>
                                        <div class="item" data-value="LWT">Logistics/Warehouse/Transport</div>
                                        <div class="item" data-value="MAE">Eng-Mech/Auto/Elec</div>
                                        <div class="item" data-value="POS">Manufacturing/Operations</div>
                                        <div class="item" data-value="MAC">Media/Advert/Communication</div>
                                        <div class="item" data-value="HRF">Hotels/Restaurants/Food</div>
                                        <div class="item" data-value="HOT">Hospitality/Tourism</div>
                                        <div class="item" data-value="SRF">Sports/Fitness/Recreation</div>
                                        <div class="item" data-value="MHN">Hospital/Nursing/Healthcare</div>
                                        <div class="item" data-value="LEL">Legal/Law</div>
                                        <div class="item" data-value="SQC">Supervision/Quality Control</div>
                                        <div class="item" data-value="APC">Apparel/Clothing</div>
                                        <div class="item" data-value="AIM">Ticketing/Airline/Marine</div>
                                        <div class="item" data-value="TAL">Teaching/Academic/Library</div>
                                        <div class="item" data-value="RLT">R&amp;D/Science/Research</div>
                                        <div class="item" data-value="AGD">Agriculture/Dairy/Environment</div>
                                        <div class="item" data-value="SEC">Security</div>
                                        <div class="item" data-value="BEC">Fashion/Design/Beauty</div>
                                        <div class="item" data-value="IDV">International Development</div>
                                        <div class="item" data-value="KPO">KPO/BPO</div>
                                        <div class="item" data-value="IME">Imports/Exports</div>
                                    </div>
                                  </div>
                            </div>
                            
                            <div class="required field">
                                <label>Vacancy Title</label>
                                <div class="fields">
                                    <input name="jobtitle" placeholder="Job Title (eg:- Web Designer)" type="text">
                                </div>
                            </div>   
                            
                            <div class="required field">
                                <label>Vacancy Description</label>
                                <textarea style="height: 59px;"></textarea>
                            </div>

                            <div class="fields">
                                <div class="required ten wide field">
                                    <label>Contract</label>
                                    <select multiple="" class="ui dropdown">
                                      <option value="">Select Country</option>
                                      <option value="full_time">Full time</option>
                                      <option value="part_time">Part time</option>
                                      <option value="contract">Contract</option>
                                      <option value="temporary">Temporary</option>
                                      <option value="internship">Internship</option>
                                      <option value="other">Other</option>
                                    </select>
                                </div>

                                <div class="required six wide field">
                                    <label>Salary</label>
                                    <div class="ui right labeled input">
                                      <div class="ui label">$</div>
                                      <input placeholder="Amount" type="text">
                                      <div class="ui basic label">.00</div>
                                    </div>
                                </div>
                            </div>

                            <div class="fields">
                                <div class="required five wide field">
                                    <label>Vacancy Start Date</label>
                                    <div class="ui left action input input-append date">
                                      <span class="ui teal label">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                      </span>
                                      <input id="v-s-d" placeholder="Vacancy Start Date" type="text">
                                    </div>
                                </div>
                                <div class="required five wide field">
                                    <label>Vacancy End Date</label>
                                    <div class="ui left action input">
                                      <button class="ui teal label">
                                        <i class="cart icon"></i>
                                      </button>
                                      <input id="v-e-d" placeholder="Vacancy End Date" type="text">
                                    </div>
                                </div>
                                <div class="five wide field">
                                    <label>Job Start Date</label>
                                    <div class="ui left action input">
                                      <button class="ui teal label">
                                        <i class="cart icon"></i>
                                      </button>
                                      <input id="j-s-d" placeholder="Job Start Date" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="ui button" tabindex="0">Submit Order</div>

                                                             
                            <input type="button" name="next" class="next action-button ui button" value="Next" />
                            <div class="ui error message"></div>
                        </fieldset>                     
                        
                        <fieldset>         
                        <h3>Profile</h3>            
                            <label for="name-2">First name *</label>
                            <input id="name-2" name="name" type="text" class="required"><br>
                            <label for="surname-2">Last name *</label>
                            <input id="surname-2" name="surname" type="text" class="required"><br>
                            <label for="email-2">Email *</label>
                            <input id="email-2" name="email" type="text" class="required email"><br>
                            <label for="address-2">Address</label>
                            <input id="address-2" name="address" type="text"><br>
                            <label for="age-2">Age (The warning step will show up if age is less than 18) *</label>
                            <input id="age-2" name="age" type="text" class="required number"><br>
                            <p>(*) Mandatory</p>
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="button" name="next" class="next action-button" value="Next" />
                        </fieldset>                     
                       
                        <fieldset>
                         <h3>Finish</h3>
                            <legend>Terms and Conditions</legend>
                     
                            <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                            <input type="submit" name="submit" class="submit action-button" value="Submit" />
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>

	</div>
    
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/semantic.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>
    <script>
        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $("input.next").click(function(){
                       
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            
            $("#post_step .step").eq($("fieldset").index(current_fs)).removeClass("active");
            
            //activate next step on progressbar using the index of next_fs
            $("#post_step .step").eq($("fieldset").index(next_fs)).removeClass("disabled").addClass("active");
            
            //show the next fieldset
            next_fs.show(); 
            current_fs.hide(); 
            //hide the current fieldset with style
            
        });

        $("input.previous").click(function(){
            /*if(animating) return false;
            animating = true;*/
            
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            
            //de-activate current step on progressbar
            $("#post_step .step").eq($("fieldset").index(current_fs)).removeClass("active");

            //activate previous step on progressbar
            $("#post_step .step").eq($("fieldset").index(previous_fs)).addClass("active");
            
            current_fs.hide();
            //show the previous fieldset
            previous_fs.show(); 
            //hide the current fieldset with style
            
        });

        /*$(".submit").click(function(){
            return false;
        });*/
$('.ui.dropdown')
  .dropdown()
;
$('.ui.form')
  .form({
    fields: {
      gender: {
        identifier  : 'gender',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter a gender'
          }
        ]
      },
      name: {
        identifier  : 'name',
        rules: [
          {
            type   : 'empty',
            prompt : 'Please enter your name'
          }
        ]
      }
    }
  });

  $(".delete.icon").addClass("fa fa-times");
  $(document).ready( function(){        
        var o = new Date,
            n = new Date;
        $("#v-s-d").datetimepicker({
            format: "dd-mm-yyyy"
        });
        $("#v-e-d").datetimepicker({
            format: "dd-mm-yyyy"
        });
        $("#j-s-d").datetimepicker({
            format: "dd-mm-yyyy"
        });
        /*$("#date_to").datetimepicker({
            format: "dd-mm-yyyy",
            autoclose: 1,
            pickTime: !1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            maxView: 2,
            defaultDate: n,
            minDate: o,
            forceParse: 0
        });*/
});
</script>
</body>
</html>