<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Post Your Ad</title>
    {% include "front/includes/head-assets.php" %}

    <link rel="stylesheet" href="css/imageup/jquery.fileupload.css"/>
    <link rel="stylesheet" href="css/imageup/jquery.fileupload-ui.css"/>
    <noscript><link rel="stylesheet" href="css/imageup/jquery.fileupload-noscript.css"/></noscript>
    <noscript><link rel="stylesheet" href="css/imageup/jquery.fileupload-ui-noscript.css"/></noscript>
    <style type="text/css">
        body{
            background-color: #E9EBEE;
        }
        #cover-con{
            min-height: 260px;
            background-image: url("https://www.bestway.lk/img/adpost/mban.jpg");
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        #adv-post{
            margin-top: 15px;
            min-height: 300px;
            background-color: #FFFFFF;
        }
        #adv-post h2{
            margin-top: 15px;
            padding-bottom: 10px;
            color: rgba(0,0,0,.6);
            text-align: center;
        }
        .t-select-active {
            background-color: #2BA8E3;
            color: #FFFFFF;
        }
        #its-free{
            text-align: center;
        }
        #free-content{
            margin:0px auto;
            width: 40%;
        }
        #shout-img{
            width: 70px;
            float: left;
        }
        #f-t-p{
            font-size: 22px;
            font-weight: bold;
            color:  #2980b9;
        }
        #i-w-e{
            font-size: 18px;
            font-weight: bold;
            color:  #e74c3c;
        }
        #looking{
            border: 1px solid #95CC6B;
        }
        #offer, #looking{
            min-height: 300px;
            border-radius: 3px;
        }
        /* temp */
        #offer-form{
            display: block !important;
        }
        /**/
        #offer-form, #looking-form{
            margin-top: 15px;
            padding: 0px 15px;
            display: none;
        }
        .t-content{
            margin-bottom: 5px;
            height: 55px;
        }
        .f-border{
            border: 1px solid;
            border-color: #e5e6e9 #dfe0e4 #d0d1d5;
            border-radius: 3px;
        }
        .t-select{
            padding: 15px;
            width: 100%;
            display: inline-block;
            background-color: #F6F7F9;
            font-size: 20px;
            font-weight: bolder;
            text-align: center;
            cursor: pointer;
        }
        .t-select-active{
            background-color: #2BA8E3;
            color: #FFFFFF!important;
            cursor: default!important;
        }
        /*.t-select:hover{*/
            /*background-color: #2BA8E3;*/
            /*color: #FFFFFF;*/
        /*}        */
        .colour_purple{
            color: #9E5BA1;
        }
        .colour_green{
            color: #95CC6B;
        }

        /*********************** over rides **********************/
        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
        .ui.icon.buttons > button {
            margin: 2px 1px;
            padding: 10px 22px !important;
        }
        #offer-form .ui.header {
            color:  #d35400;
            margin-bottom: 20px;
            border-bottom: 1px solid #e59866;
        }
        #offer-form .ui.form .field > label , #offer-form .ui.form .inline.fields > label {
            width: 130px;
            font-size: 0.9rem;
        }
        #offer-form .ui.selection.dropdown, #offer-form .ui.form input[type="text"], #offer-form .ui.form textarea {
            border: 1px solid #85c1e9;
        }
        #offer-form .ui.selection.dropdown:focus, #offer-form .ui.form input[type="text"]:focus, #offer-form .ui.form textarea:focus {
            border: 1px solid #28b463;
        }
        .ad_sel_bx{
            border: 1px solid #efefef;
            height: 100px;
            text-align: center;
        }
        .ad_sel_bx:hover{
            background-color: #f2f3f4;
            box-shadow: 0px 1px 1px #999;
        }
        .bx_sdow{
            box-shadow: 0px 4px 5px #999;
        }
        .bx_clicked{
            background-color: #f2f3f4;
            box-shadow: 0px 1px 1px #999;
        }
        .ad_sel_bx_con{
            padding: 0.2em;
        }
        .icon_con{
            margin: 5px auto;
            width: 68px;
            height: 65px;
        }
        img.image {
            height: 150px!important;
        }
    </style>
</head>
<body>
<main id="main-con">
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}
    <header id="cover-con" class="container-fluid">

    </header>

    <div id="adpost-con" class="container-fluid">
        <div id="adv-post" class="container f-border">
            <div class="row">
                <h2>Create your Advertisement</h2>
                <div class="ui divider"></div>
                <div class="t-content">
                    <div class="col-md-6">
                        <a href="{{app-context}}sell" >
                        <div id="offer-btn" class="t-select colour_purple f-border">
                            <span class="">Offer Something</span>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div id="looking-btn" class="t-select t-select-active colour_green f-border">
                            <span class="">Looking for Something</span>
                        </div>
                    </div>
                </div>

                <div id="ad_bx_con" class="col-md-12">
                    {%for x in categories %}
                    <div class="col-md-2 ad_sel_bx_con">
                        <a onclick="$(this).parents('#ad_bx_con:first').find('div.ad_sel_bx').removeClass('bx_clicked').addClass('bx_sdow'); $(this).find('div.ad_sel_bx').removeClass('bx_sdow').addClass('bx_clicked'); bestway.index.adpost.update_subcats('{{x.id}}'); $('#main_cat').val('{{x.id}}'); $('#cat-spec-sub').parent()[0].scrollIntoView({behavior: 'smooth'});" id="cat_{{forloop.counter}}"><div class="ad_sel_bx bx_sdow">
                            <div class="icon_con"><span class="{{x.css_class}}"></span></div>
                            {{x.label}}
                        </div>
                        </a>
                    </div>
                    {%endfor%}

                </div>


            </div>




            <div id="form-container" class="row">
                <div id="offer-form" class="col-md-8">
                    <form id="offer" class="ui form col-md-12" action="/post-your-ad"  method="post" enctype="multipart/form-data">
                        {%csrftok%}
                        <h4 class="ui dividing header">Details about your advertisement</h4>
                        <div class="inline fields hidden">
                            <label>Select the type</label>
                            <div class="field">
                                <div class="ui checkbox">
                                    <input id="for-sale" class="hidden" name="for-sale" type="checkbox"/>
                                    <label for="for-sale">For Sale</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui checkbox">
                                    <input id="for-rent" class="hidden" name="for-rent" type="checkbox"/>
                                    <label for="for-rent">For Rent</label>
                                </div>
                            </div>
                        </div>

                        <div class="hidden field">
                            <label>Select a Category</label>
                            <select class="ui fluid dropdown" id="cat-select">
                                <option value="">Category</option>
                                {% for y in catz %}
                                {{y|safe}}
                                {% endfor %}
                            </select>
                        </div>
                        <div id="cat-spec-sub" class="field">

                        </div>

                        <div class="field">
                            <label>Title of your Ad</label>
                            <div class="ui input">
                                <input name="ad-title" id="ad-title" placeholder="Ad Title" type="text"/>
                            </div>
                        </div>
                        <div class="field">
                            <label>Description</label>
                            <textarea name="txt-desc"></textarea>
                        </div>
                        <div class="inline field">
                            <label>Price</label>
                            <div class="ui Left labeled input">
                                <div class="ui teal label">Rs.</div>
                                <input name="txt-price" placeholder="Price" type="text"/>
                            </div>
                        </div>


                        <div id="cat-speci" class="">
                        </div>

                        <div class="" style="margin: 30px auto;" id="adpicuploadbox2">
                        </div>




                        <h4 class="ui dividing header">Ad Location</h4>

                        <div class="two fields">
                            <div class="field">
                                <label>District</label>
                                <select name="ad-district" class="" id="addistrict">
                                    <option value="any">Location</option>
                                    {% for y in locs.districts %}
                                    {{y|safe}}
                                    {% endfor %}
                                </select>
                            </div>
                            <div class="field">
                                <label>City</label>
                                <select name="ad-city" class="" id="adcity">
                                    <option value="">Select City</option>
                                    {% for y in locs.cities %}

                                    {% for z in y %}
                                    {{z|safe}}
                                    {% endfor %}

                                    {% endfor %}
                                </select>
                            </div>
                        </div>


                        <input type="hidden" name="main_cat" id="main_cat" value="" />
                        <input type="hidden" name="type" id="ad-type" value="buy" />
                        <button type="submit" class="ui button" tabindex="0">Post Ad</button>

                    </form>
                </div>

            </div>
            <div class="ui divider"></div>
            <div id="its-free">
                <div id="free-content" class="ui compact segment">
                    <img id="shout-img" src="img/adpost/shout.gif"/>
                    <p id="f-t-p">It's free to post ads on Bestway.lk</p>
                    <p id="i-w-e">It will ever!</p>
                </div>
            </div>

        </div>
    </div>
</main>
{% include "front/includes/footer.php" %}
<!--
<script data-src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"
 type="text/javascript"></script> -->
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/semantic.js" type="text/javascript"></script>
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script src="js/imgLiquid.js" type="text/javascript"></script>

<script>
    window.app_context = "{{app-context}}";
</script>

{% script "/js/web/main.js" %}
{% script "/js/bwinc.js" %}
{% script "js/adpost.js" %}

<script type="text/javascript">
    $(document).ready(function() {

       // $('#condition').dropdown();
        //$('#item-type').dropdown();
        $('#c-type').dropdown();
        //$('#ad-district').dropdown();
        $('#adcity').chosen();
        $('#addistrict').chosen();
        $('#addistrict').on('change',
            function(){
//                     console.log($(this));
                var chosenId =  $(this).find('option:selected').data('id');
                var sub = $(this).parent().parent().find('select#adcity');
                sub.find('option').not('option:first').hide();
                sub.find('option[data-parent-id='+chosenId+']').show();
                sub.trigger('chosen:updated');
            }
        );

        //$('.button').popup();

    });

    $(".img-btn").click(function(e){
        e.preventDefault();
    });

    $(".s-btn").click(function(){
        //var elid =
        $(this).siblings("input").click().next('button').removeClass('disabled').addClass('red');
        //alert(elid);
    });


    $(window).load(function()
        {
            $('#prof_pg').dropdown({
                on: 'hover'
            });
            //alert("{{servlet-context}}" );
            //bestway.index.upload.upload_ad_fns();
            bestway.index.adpost.set_cat_spec_markup();
        }

    );
</script>

</body>
</html>