<!DOCTYPE html>
<html lang="en">
<head >
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BestWay.lk | My Page</title>

	{% include "front/includes/head-assets.php" %}
	<link href="css/mypage.css" rel="stylesheet" type="text/css" />
            <style type="text/css">
    		body{
    			background-color: #F9F9F9;
    		}
    		#top-con, #main{
    			padding: 0px;
    		}
    		#pageCover{
    			position: relative;
    		}
    		#pageCover #cover-pic{
    			width: 100%;
    		}
    		#pro-pic{
    			position: absolute;
    			bottom: 16px;
    			left: 16px;
    			background: rgba(0, 0, 0, .3);
    			border-radius: 4px;
    			box-shadow: -2px 4px 2px rgba(0, 0, 0, .07);
    		}
    		#pro-img{
    			border: 2px solid #FFFFFF;
    			border-radius: 4px;
    			width: 150px;
    		}
    		#cover-func{
    			height: 50px;
    			background-color: #FFF;
    			border-radius: 0px 0px 5px 5px;
    			border: 1px solid #d3e0e9;
    		}
    		#page-links{
    			padding-left: 145px;
    		}
    		#page-links ul{
    			list-style: none;
    		}
    		#page-links ul li{
    			float: left;
    			padding: 1px 15px 5px;
    			border-right: 1px solid #d3e0e9;
    		}
    		#page-links ul li:last-child{
    			border-right: none;
    		}
    		#pro-name{
    			position: absolute;
    			color: #FFFFFF;
    			bottom: 50px;
    			left: 200px;
    			font-weight: bold;
    			text-shadow: -1px 2px 0px #000000;
    		}
    		#pro-name h1{
    			font-size: 26px;
    		}
    		#pro-name h2{
    			font-size: 18px;
    		}
    		#map {
    	        width: 100%;
    	        height: 300px;
          	}
          	#pageInfo{
          		margin-top: 15px;
          	}
          	#pageImgPan img{
          		margin: 2px 0px;
          		border: 2px solid #d3e0e9;
          		width: 128px;
          	}
          	#pageImgPan img:nth-child(odd){
          		margin-left: 4px;
          	}

    		/* ads style start */
    		#class-con{
    			border-radius: 4px;
    			background-color: #ffffff;
    			border: 1px solid #d3e0e9;
    		}
    		#ad-con ul {
    			list-style: none;
    			padding: 0px;
    		}
    		#ad-con .buttons {
    			margin-bottom: 20px;
    		}
    		#ad-con .list li {
    			width: 100%;
    			border: 1px solid #d3e0e9;
    			border-right: none;
    			border-left: none;
    			margin-bottom: 10px;
    			padding-bottom: 10px;
    		}
    		#ad-con .grid li {
    			float: left;
    			border: 1px solid #CCC;
    		}
    		#ad-list-con{
    			overflow: auto;
    			margin: 0px;
    		}
    		.grid .ad{
    			width: 280px;
    			height: auto;
    			margin: 0px 0px 5px 3px;
    		}
    		.ad .itm_img{
    			border: 1px solid #d3e0e9;
    		}
    		.grid .adprice, .list .adprice{
    			font-size: 1.7em;
    		    font-family: 'Roboto', sans-serif;
    		    font-weight: 600;
    		    color: #009432;
    		    display: inline;
    		}
    		.grid .itm_img_con{
    			margin-bottom: 5px;
    		}
    		.grid .ad_txt{
    			margin-top: 2px;
    			font-size: 12px;
    		}
    		.list .ad{
    			height: auto;
    		}
    		.list img{
    			width: 140px;
    		}
    		.list .itm_img_con{
    			margin-right: 25px;
    		}
    		.list .itm_img_con{
    			float: left;
    		}
    		.list .txt_con{
    		}
    		/* ads style end */
    		.bdr{
    			border: 1px solid #d3e0e9;
    			margin-bottom: 12px;
    		}
    		.panel-body{
    			padding: 0px;
    		}
    		.panel-heading {
    		    background-color: #363D40;
    		    color: #EEEEEE;
    		    text-align: center;
    		    border-radius: 0px;
    		}
    		.panel-body p{
    			padding-left: 10px;
    		}
    	</style>
</head>
<body>
    <div id="main-con">
<!-- top bar navigation goes here -->

		{% include "front/includes/top_nav.php" %}

    	<div id="top-con" class="container">
    		 <!-- promo bar goes here -->

			<!-- promo bar goes here -->
			{% include "front/includes/promo_bar.php" %}

			{% include "front/includes/main_search.php" %}
		</div>
    	</div>

    	<article id="main" class="container">
    		<div class="row">
    		<div id="page" class="col-md-9">
    				<div id="pageCoverCon">
    					<div id="pageCover">
							{{csrftok}}
    						<a href="#"><img id="cover-pic" src="img/nature-path-facebook-cover-photo.jpg"></a>
    						<div id="pro-pic">
    							<a href="#"><img id="pro-img" src="img/propic.png"></a>
    						</div>
    						<div id="pro-name">
    							<h1>Comworld Computer Zone</h1>
    							<h2>Branded Computers &amp; Laptops Accessories</h2>
    						</div>
    						<div id="cover-func">
    							<div id="page-links">
    								<ul>
    									<li><a href="#">Page</a></li>
    									<li><a href="#">Photos</a></li>
    									<li><a href="#">Events</a></li>
    									<li><a href="#">Offers</a></li>
    									<li><a href="#">Edit</a></li>
    								</ul>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div id="pageInfo" class="row">
    					<aside id="infoCon" class="col-md-4">
    						<div id="about-page" class="bdr">
    							<div class="panel-heading">
    	                            <h3 class="panel-title">About</h3>
    	                        </div>
    							<div class="panel-body">
    								<p>630/3/B Kandy Road, Bandarawaththa, Kadawata.</p>
    								<p>071 768 4399</p>
    								<p>www.comworld.lk</p>
    							</div>
    					    </div>

    					    <div class="bdr">
    							<div class="panel-heading">
    	                            <h3 class="panel-title">Open Hours</h3>
    	                        </div>
    							<div class="panel-body">
    								<p><span>Mon :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    								<p><span>Tue :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    								<p><span>Wed :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    								<p><span>Thu :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    								<p><span>Fri :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    								<p><span>Sat :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    								<p><span>Sun :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
    							</div>
    					    </div>

    					    <div id="locMap" class="bdr">
    							<div class="panel-heading">
    	                            <h3 class="panel-title">Location Map</h3>
    	                        </div>
    							<div class="panel-body">
    								<div id="map"></div>
    								<script>
    							      function initMap() {
    							        var mapDiv = document.getElementById('map');
    							        var map = new google.maps.Map(mapDiv, {
    							          center: {lat: 44.540, lng: -78.546},
    							          zoom: 8
    							        });
    							      }
    							    </script>
    							</div>
    					    </div>

    					    <div id="pageImgPan" class="bdr">
    							<div class="panel-heading">
    	                            <h3 class="panel-title">Photos</h3>
    	                        </div>
    							<div class="panel-body">
    								<img src="img/demo3/eiffel_thumb.jpg" alt="eiffel">
    								<img src="img/demo3/eiffel_thumb.jpg" alt="eiffel">
    								<img src="img/demo3/eiffel_thumb.jpg" alt="eiffel">
    								<img src="img/demo3/eiffel_thumb.jpg" alt="eiffel">
    								<img src="img/demo3/eiffel_thumb.jpg" alt="eiffel">
    								<img src="img/demo3/eiffel_thumb.jpg" alt="eiffel">
    							</div>
    					    </div>

    					</aside>
    					<article id="recAds" class="col-md-8">
    						<article id="class-con">
    		            		<div id="ad-con">
    							    <div class="buttons">
    							        <span class="btn btn-info grid">Grid View</span>
    							        <span class="btn btn-info list">List View</span>
    							    </div>
    							    <div id="ad-list-con" class="row">
    								    <ul class="grid">
    								        <li id="" class="ad col-md-6">
    								        	<div class="itm_wrapper">
    										        <h1><a href="#">Ad Name</a></h1>

    						                        <h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
    						                        <div class="itm_img_con">
    						                        	<img src="img/2.jpg" class="itm_img">
    						                        </div>
    						                        <div class="txt_con">
    							                        <p class="adprice">Rs. 12,000.00</p>
    							                        <p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
    						                        </div>
    					                        </div>
    								        </li>
    								        <li id="" class="ad col-md-6">
    									        <div class="itm_wrapper">
    										        <h1><a href="#">Ad Name</a></h1>

    						                        <h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
    						                        <div class="itm_img_con">
    						                        	<img src="img/2.jpg" class="itm_img">
    						                        </div>
    						                        <div class="txt_con">
    							                        <p class="adprice">Rs. 12,000.00</p>
    							                        <p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
    						                        </div>
    						                    </div>
    								        </li>
    								    </ul>
    							    </div>
    							</div>
    	            		</article>
    					</article>
    				</div>
    			</div>
    		</div>
    	</article>
    </div>
    <!-- page footer goes here -->




<!-- main start -->
<main id="main-wrap" class="container-fluid global-page">
    

    <section>


        <section id="profiler" class="col-md-3">
            <div>
                
            </div>
            
        </section>

        <section id="wall" class="col-md-6">
            
        </section>

        <section id="misc" class="col-md-3">
            
        </section>

    </section>

</main>

	<!-- page footer goes here -->
	<!-- footer bar -->
	{% include "front/includes/footer.php" %}

	{% include "front/includes/scripts-my-page.php" %}
</body>
</html>