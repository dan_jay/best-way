<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway View Hotel</title>
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
        
    <script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>    
    <!-- <script src="https://use.fontawesome.com/f7f916392e.js"></script> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demo.css" />

    <!-- desoSlide styles -->
    <link href="<?php echo base_url(); ?>assets/css/jquery.desoslide.css" rel="stylesheet">

    <!-- Animate, used for slides transition (if you set effect.provider to 'animate', it's the default value) -->
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

    <!-- Magic, used for slides transition (if you set effect.provider to 'magic') -->
    <link href="<?php echo base_url(); ?>assets/css/magic.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style type="text/css">
    body{
    	background-color: #FCFCFC;
    }
    #hotel-con{
        padding: 0px;
        margin: 25px 0px 0px;
    }
    #hotel-title{
        display: inline;
        margin-right: 10px;
        color: #636363;
        font-size: 32px;
    }
    #main-con {
        /*border: 1px solid #ccc;*/
    }

    #hot-main-img{
        height: 415px;
    }
    #get_promo .panel-body p{
        font-size: 13px;
        font-weight: bold;
        padding: 0.2em;
    }
    #hot_descript{
        margin-bottom: 25px;
    }
    #hot_feat_list ul{
        overflow: auto;
    }
    #hot_feat_list ul>li{
        list-style: none;
        float: left;
        margin-right: 30px;
        width: 250px;
        font-size: 13px;
        line-height: 24px;
    }
    .des_feat_label{
        font-weight: bolder;
        font-size: 15px;
    }
    .fa{
        margin-right: 10px;
    }
    .tbl_mid .fa{
        margin-right: 0px;
    }
    .feat_cat{
        margin-bottom: 30px;
    }
    .feat_cat ul li .fa{
        margin-right: 10px;
    }
    .feat_cat .feat_label{
        display: block;
        font-weight: bold;
        font-size: 14px;
    }
    #map {
        width: 100%;
        height: 250px;
        margin-top: 15px;
    }

    .collect_rew{
        color: #990066;
    }
    .unlock_rew{
        color: #4e4e4e;
    }
    .hot-img{
        margin-bottom: 15px;
        padding-left: 0px;
    }
    .sm-12n6{
        height: 200px;
    }
    .segments{
        font-weight: bolder;
    }
    .segments p i{
        margin-right: 5px;
    }
    .tbl_header{
        font-size: 14px;
        font-weight: bold;
        color: #005EB8;
    }
    .tbl_room_img{
        width: 150px;
    }
    .tbl_mid{
        vertical-align: middle !important;
        text-align: center !important;
    }
    .tbl_hot_opt{
        display: block;
        color: #458f00;
        font-weight: bold;
        font-size: 13px;
        line-height: 20px;
    }
    .tbl_hot_opt_sml{
        display: block;
        color: #7e7e7e;
        font-size: 10px;
    }
    .tbl_hot_opt_sml .tbl_label{
        font-weight: bold;
    }
    .tbl_price{
        /*color: #96BF49;*/
        font-weight: bold;
    }
    .tbl_price .tbl_prev_price{
        font-size: 12px;
        text-decoration: line-through;
    }
    .tbl_price.colr{
        color: #990066;
        font-size: 24px;
    }

    /* ************* Override **************** */
    .ui.segment {
        background-color: #F2EAF0 !important;
        color: #906 !important;
    }
    .ui.table thead th {
        background-color: #E5E3DB;
    }
    #ad_image{
        min-height: 490px;
    }
    #ad_image img.ad_l_image{
        width: 450px;
        border: 1px solid;
        border-color: #E5E6E9 #DFE0E4 #D0D1D5;
    }
    #ad_image #slider_images{
        padding-left: 13px;
    }
    #slider{
        padding: 0px;
    }
    #slider_thumb_container{
        float: left;
    }
    #slider_thumbs{
        margin: 5px 0px;
        list-style: none;
        text-align: left;
    }
    #slider_thumbs img{
        margin-bottom: 5px;
        width: 80px;
        border: 2px solid #22313f;
        border-radius: 3px;
        transition: border .25s linear,opacity .25s linear;
    }

    .list-inline > li {
        padding-left: 0px;
        padding-right: 0px;
    }
    .panel-heading{
        padding: 6px 10px;
        background: #990066;
        background: -moz-linear-gradient(top,#603 0%,#906 100%);
        background: -webkit-linear-gradient(top,#603 0%,#906 100%);
        background: linear-gradient(to bottom,#603 0%,#906 100%);
    }
    .panel-title{
        color: #ffffff;
        font-size: 15px;
    }
    .ui.dividing.header{
        margin-top: 30px;
    }
    .ui.comments{
        max-width: 100% !important;
    }
    .comment{
        min-height: 70px;
        margin-top: 15px !important;
        background-color: #FFFFFF !important;
        border: 1px solid #eeeeee !important;
        padding: 10px 20px 10px 10px !important;
        box-shadow: 2px 3px 6px rgba(0,0,0,0.25);
    }
    .comment h3{
        color: #026dc5;
        font-weight: bold;
        font-size: 15px;
    }
    .comment p{
        line-height: 22px !important;
        text-align: justify;
    }
    .ui.labeled.icon.buttons > .button > .fa, .ui.labeled.icon.button > .fa {
      position: absolute;
      height: 100%;
      line-height: 2.7;
      border-radius: 0px;
      border-top-left-radius: inherit;
      border-bottom-left-radius: inherit;
      text-align: center;
      margin: 0em;
      width: 2.57142857em;
      background-color: rgba(0, 0, 0, 0.05);
      color: '';
      box-shadow: -1px 0px 0px 0px transparent inset;
    }

    /* Left Labeled */

    .ui.labeled.icon.buttons > .button > .fa,
    .ui.labeled.icon.button > .fa {
      top: 0em;
      left: 0em;
    }
    /*==================================================
     * Effect 2
     * ===============================================*/
    .shadow
    {
      position: relative;
    }
    .shadow:before, .shadow:after
    {
      z-index: -1;
      position: absolute;
      content: "";
      bottom: 15px;
      left: 10px;
      width: 50%;
      top: 80%;
      max-width:300px;
      background: #777;
      -webkit-box-shadow: 0 15px 10px #777;
      -moz-box-shadow: 0 15px 10px #777;
      box-shadow: 0 15px 10px #777;
      -webkit-transform: rotate(-3deg);
      -moz-transform: rotate(-3deg);
      -o-transform: rotate(-3deg);
      -ms-transform: rotate(-3deg);
      transform: rotate(-3deg);
    }
    .shadow:after
    {
      -webkit-transform: rotate(3deg);
      -moz-transform: rotate(3deg);
      -o-transform: rotate(3deg);
      -ms-transform: rotate(3deg);
      transform: rotate(3deg);
      right: 10px;
      left: auto;
    }
    </style>
</head>
<body>
<script>
    $(window).load(function() {
        $('#slider').desoSlide({
            thumbs: $('#slider_thumbs li > a'),
            auto: {
                start: false
            },
            effect: {
                provider:       'null',        
                name:           'none'            
            },
            overlay:            'hover',   
            first: 0,
            interval: 3000
        });
    });
</script>
	<div id="main-con">
		<!-- top bar navigation goes here -->
    	<?php $this->load->view('includes/top_nav'); ?>

        <div class="container">
            <div id="hotel-con" class="row">
                <h1 id="hotel-title" class="ui header">Ella Eco Lodge</h1><div class="ui star rating" data-rating="3" data-max-rating="5"></div>
                <div class="ui divider"></div>
            </div>
            <div id="hotel-img-con" class="row">
                <article id="ad_image" class="col-md-8">
                    
                    <div id="slider" class="col-lg-12"></div>
                        
                    <div class="row">
                        <article id="slider_images" class="col-lg-12">
                            <ul id="slider_thumbs" class="desoslide-thumbs-horizontal list-inline text-center">
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/city.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/city_thumb.jpg" alt="city">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/building.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/building_thumb.jpg" alt="building">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/eiffel.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/bridge.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/bridge_thumb.jpg" alt="bridge">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/city.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/city_thumb.jpg" alt="city">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/city.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/city_thumb.jpg" alt="city">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/building.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/building_thumb.jpg" alt="building">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/eiffel.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/bridge.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/bridge_thumb.jpg" alt="bridge">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/city.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/city_thumb.jpg" alt="city">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/eiffel.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/bridge.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/bridge_thumb.jpg" alt="bridge">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>assets/img/demo3/city.jpg">
                                        <img src="<?php echo base_url(); ?>assets/img/demo3/city_thumb.jpg" alt="city">
                                    </a>
                                </li>
                            </ul>
                        </article>
                    </div>
                </article>
                <aside class="col-md-4">
                    <div id="get_promo">
                        <div class="panel panel-default shadow">
                          <div class="panel-heading">
                            <h3 class="panel-title">Bestway.lk Rewards</h3>
                          </div>
                          <div class="panel-body">
                            <p class="collect_rew">You’ll collect 1 night with this stay</p>
                            <p class="unlock_rew">Remember collect 10 nights, get 1 free*</p>
                          </div>
                        </div>
                    </div>
                    <!-- <article id="review-summery">
                        <div>
                            <h3>Newbie in Bestway</h3>
                        </div>
                        <i class="material-icons">sentiment_satisfied</i> 50%
                    </article> -->
                    <article id="share-box">
                        <div class="panel panel-default shadow">
                          <!-- List group -->
                          <ul class="list-group">
                            <li class="list-group-item">
                                <button class="ui facebook button">
                                  Facebook
                                </button>
                            </li>
                            <li class="list-group-item">
                                <button class="ui twitter button">
                                  Twitter
                                </button>
                            </li>
                            <li class="list-group-item">
                                <button class="ui google plus button">
                                  Google Plus
                                </button>
                            </li>
                            <li class="list-group-item">
                                <button class="ui vk button">
                                  VK
                                </button>
                            </li>
                            <li class="list-group-item">
                                <button class="ui linkedin button">
                                  LinkedIn
                                </button>
                            </li>
                            <li class="list-group-item">
                                <button class="ui instagram button">
                                  Instagram
                                </button>
                            </li>
                            <li class="list-group-item">
                                <button class="positive ui button"><i class="fa fa-envelope" aria-hidden="true"></i> Email to a friend</button>
                            </li>
                          </ul>
                        </div>
                    </article>
                </aside>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ui horizontal segments">
                        <div class="ui segment">
                            <p><i class="fa fa-bed" aria-hidden="true"></i> Number of rooms: 259</p>
                        </div>
                        <div class="ui segment">
                            <p><i class="fa fa-taxi" aria-hidden="true"></i> Airport Pickup</p>
                        </div>
                        <div class="ui segment">
                            <p><i class="fa fa-wifi" aria-hidden="true"></i> Free WiFi</p>
                        </div>
                        <div class="ui segment">
                            <p><i class="fa fa-television" aria-hidden="true"></i> Television</p>
                        </div>
                        <div class="ui segment">
                            <p><i class="fa fa-wheelchair" aria-hidden="true"></i> Extra care</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <table class="ui celled padded table">
                      <thead>
                        <tr>
                            <th>Room types</th>
                            <th>Max</th>
                            <th>Options</th>
                            <th>Price per night</th>
                            <th>Rooms</th>
                            <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <h2 class="tbl_header">Deluxe with Sea View</h2>
                            <img class="tbl_room_img" src="<?php echo base_url(); ?>assets/img/hotels/h_rooms (1).jpg">
                          </td>
                          <td class="tbl_mid">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i>
                          </td>
                          <td class="single line">
                                <span class="tbl_hot_opt">Breakfast Included</span>
                                <span class="tbl_hot_opt">Special conditions</span>

                                <span class="tbl_hot_opt_sml"><span class="tbl_label">Not included in room price:</span> Sale Tax 16.84%, Service Charge 10%
                                Room info</span>
                          </td>
                          <td class="tbl_mid">
                            <span class="tbl_price">USD <span class="tbl_prev_price">250.00</span></span><br>
                            <span class="tbl_price colr">147.44</span>
                          </td>
                          <td class="tbl_mid">
                                <div class="ui compact selection dropdown">
                                  <i class="dropdown icon"></i>
                                  <div class="text">0</div>
                                  <div class="menu">
                                    <div class="item">1</div>
                                    <div class="item">2</div>
                                    <div class="item">3</div>
                                    <div class="item">4</div>
                                    <div class="item">5</div>
                                    <div class="item">6</div>
                                    <div class="item">7</div>
                                    <div class="item">8</div>
                                    <div class="item">9</div>
                                  </div>
                                </div>
                          </td>
                          <td class="tbl_mid">Dis di di din 2</td> 
                        </tr>
                        <tr>
                          <td>
                            <h2 class="tbl_header">Deluxe Room with Sea View</h2>
                            <img class="tbl_room_img" src="<?php echo base_url(); ?>assets/img/hotels/h_rooms (2).jpg">
                          </td>
                          <td class="tbl_mid">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <i class="fa fa-user" aria-hidden="true"></i>
                          </td>
                          <td class="single line">
                            Weight
                          </td>
                          <td class="tbl_mid">
                            <span class="tbl_price">USD <span class="tbl_prev_price">250.00</span></span><br>
                            <span class="tbl_price colr">147.44</span>
                          </td>
                          <td class="tbl_mid">
                                <div class="ui compact selection dropdown">
                                  <i class="dropdown icon"></i>
                                  <div class="text">0</div>
                                  <div class="menu">
                                    <div class="item">1</div>
                                    <div class="item">2</div>
                                    <div class="item">3</div>
                                    <div class="item">4</div>
                                    <div class="item">5</div>
                                    <div class="item">6</div>
                                    <div class="item">7</div>
                                    <div class="item">8</div>
                                    <div class="item">9</div>
                                  </div>
                                </div>
                          </td>
                          <td class="tbl_mid">Dis di di din 2</td>
                        </tr>
                      </tbody>
                    </table>    
                </div>
            </div>
        </div>
	</div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article>
                    <h3 class="ui dividing header">About Ella Eco Lodge</h3>
                    <div id="hot_descript" class="row">
                        <div class="col-md-2">
                            <p class="des_feat_label">Description</p>
                        </div>
                        <div class="col-md-10">
                            <p>Ambassador Hotel Bangkok has two towers offering 755 rooms and suites furnished with comfortable amenities and facilities catering to business travelers, conference delegates, and leisure seeks. Guests have a wide variety of dining and entertainment options to choose from, including a reverd Chinese restaurant famed for its dim sum. The elegant...</p>
                        </div>
                    </div>
                    <div id="hot_feat_list" class="row">
                        <div class="col-md-2">
                            <p class="des_feat_label">Features</p>
                        </div>
                        <div class="col-md-10">
                            <div class="feat_cat"><span class="feat_label">Internet</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>free Wi-Fi in all rooms</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Wi-Fi in public areas</li>
                                </ul>
                             </div>
                                
                            <div class="feat_cat"><span class="feat_label">Transportation</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>valet parking</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>car park</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>airport transfer</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>shuttle service </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>car hire </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>bicycle rental</li>
                                 </ul>
                             </div> 
                                
                            <div class="feat_cat"><span class="feat_label">Fitness & recreation</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>yoga room </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>spa/sauna</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>gym/fitness</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>fitness center</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>sauna</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>spa</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>massage</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>solarium</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>steamroom</li>
                                 </ul>
                             </div>

                            <div class="feat_cat"><span class="feat_label">Swimming & soaking</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>swimming pool</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>hot tub</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>hot spring bath</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>outdoor pool</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>indoor pool</li>
                                </ul>
                             </div>

                            <div class="feat_cat"><span class="feat_label">Water sports</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>private beach</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>water sports (non-motorized)</li>
                                </ul>
                             </div>
                            <div class="feat_cat"><span class="feat_label">Land sports</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>squash courts</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>table tennis </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>tennis courts</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>garden</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>golf course (on site)</li>
                                </ul>
                             </div>
                            <div class="feat_cat"><span class="feat_label">For kids</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>kids club </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>pool (kids)</li>
                                </ul>
                             </div>
                            <div class="feat_cat"><span class="feat_label">Food & beverage</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>24-hour room service</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>room service</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>BBQ facilities</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>restaurant</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>coffee shop</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>poolside bar</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>bar</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>vending machine</li>
                                </ul>
                             </div>

                            <div class="feat_cat"><span class="feat_label">Pets</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>pets allowed</li>
                                </ul>
                             </div>

                            <div class="feat_cat"><span class="feat_label">Cleaning services</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>laundry service</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>dry cleaning</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>daily housekeeping </li>
                                </ul>
                             </div>
                                
                            <div class="feat_cat"><span class="feat_label">Guest services</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>express check-in/check-out</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>shrine</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>chapel </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>luggage storage</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>babysitting</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>newspapers</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>tours</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>concierge</li>
                                </ul>
                             </div>
                            <div class="feat_cat"><span class="feat_label">On-site facilities</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>casino</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>facilities for disabled guests</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>family room</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>elevator</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>executive floor</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>nightclub</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>salon</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>shops</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>smoking area</li>
                                </ul>
                             </div>
                            <div class="feat_cat"><span class="feat_label">Health and security</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>24-hour security</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>24-hour front desk</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>safety deposit boxes</li>
                                </ul>
                             </div>
                            <div class="feat_cat"><span class="feat_label">Business & money services</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>fax machine </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>meeting facilities</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>business center</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>ATM/cash machine on site</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>currency exchange</li>
                                </ul>
                             </div>

                            <div class="feat_cat"><span class="feat_label">Languages Spoken</span>
                                <ul>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>English</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Chinese (Cantonese)</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Chinese (Mandarin)</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Croatian</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Filipino</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>French</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>German </li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Italian</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Spanish</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Korean</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Portuguese</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Russian</li>
                                    <li><i class="fa fa-certificate" aria-hidden="true"></i>Thai</li>
                                </ul>
                             </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>

    <div class="container">   
        <div class="row">
            <div class="col-md-12">
                <h3 class="ui dividing header">Location Map</h3>
                <div id="map"></div>
                <script>
                  function initMap() {
                    var mapDiv = document.getElementById('map');
                    var map = new google.maps.Map(mapDiv, {
                      center: {lat: 44.540, lng: -78.546},
                      zoom: 8
                    });
                  }
                </script>
            </div>
        </div>
    </div> 

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article>
                    <h3 class="ui dividing header">Customer Reviews</h3>

                    <div class="ui comments">
                      <div class="comment">
                        <a class="avatar">
                          <img src="<?php echo base_url(); ?>assets/img/app/avatar-dhg.png">
                        </a>
                        <div class="content">
                          <a class="author">Matt</a>
                          <div class="metadata">
                            <div class="ui star rating" data-rating="3" data-max-rating="5"></div><span class="date">Today at 5:42PM</span>
                          </div>
                          <div class="text">
                            <h3>Scenic location</h3>
                            <p>Nice old style sprawling hotel, well located on rocky bluff overlooking Galle beach and fort (in distance), very large rooms and spacious bathrooms and sizeable balcony - all with ocean views. The reception area is large and leads to an open air seating area with 180 degree views to rocky bluff and ocean. The two pools are large but the tiny beach is not for swimming, too dangerous with big surf and rocks. The buffet breakfast is excellent and the food is good overall. The staff are attentive with reasonably quick service. Menu n food prices are competitive with other hotels of similar rating. Relatively quite except for the road noise at first daylight with the buses competing for passengers up the Galle-Colombo coast road. The complex is not overcrowded like some resorts and you can have a relaxing time with family without having to compete for lounges by the pool. Overall a great stay for a couple of days.</p>
                          </div>
                        </div>
                      </div>
                      <div class="comment">
                        <a class="avatar">
                          <img src="<?php echo base_url(); ?>assets/img/app/avatar-dhg-old.png">
                        </a>
                        <div class="content">
                          <a class="author">Elliot Fu</a>
                          <div class="metadata">
                            <div class="ui star rating" data-rating="3" data-max-rating="5"></div><span class="date">Yesterday at 12:30AM</span>
                          </div>
                          <div class="text">
                            <h3>Good view, relaxing and breakfast is nice</h3>
                            <p>Spent 2 nights. Great location to MRT, shops, and restaurants. Gym and pool are also nice and clean. Would recommend it! </p>
                          </div>
                        </div>
                      </div>
                      <div class="comment">
                        <a class="avatar">
                          <img src="<?php echo base_url(); ?>assets/img/app/avatar-fat.jpg">
                        </a>
                        <div class="content">
                          <a class="author">Joe Henderson</a>
                          <div class="metadata">
                            <div class="ui star rating" data-rating="3" data-max-rating="5"></div><span class="date">5 days ago</span>
                          </div>
                          <div class="text">
                            <h3>Fantastic hotel with great view and service</h3>
                            <p>Jetwing lighthouse was the best hotel we stayed in Srilanka. Although it is expensive for us, the quality, facilitates,service and view are very worth. We booked Delux room and the hotel upgrade it to honeymoon suite with congratulation cake and fruits inside. What superize us most is, the suite covers the whole corner of the floor we were and has another separated door. Our room is surrounded by the sea and swimming pool. The lighthouse is closed to Galle fort, train station and bus station. It serves best breakfast buffet and dinner. The only negative thing is, the sockets are not enough, particularly in the bathroom area.</p>
                          </div>
                        </div>
                      </div>
                      <form class="ui reply form">
                        <div class="field">
                          <textarea></textarea>
                        </div>
                        <div class="ui blue labeled submit icon button">
                          <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Reply
                        </div>
                      </form>
                    </div>

                </article>
            </div>
        </div>
    </div>
    
</div>
<?php $this->load->view('includes/footer'); ?>
    
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/semantic.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/imgLiquid.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>

    <!-- desoSlide core -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.desoslide.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $(".imgLiquidFill").imgLiquid(
            );
        });
            //jQuery time
            var current_fs, next_fs, previous_fs; //fieldsets
            var left, opacity, scale; //fieldset properties which we will animate
            var animating; //flag to prevent quick multi-click glitches

            $("input.next").click(function(){
                           
                current_fs = $(this).parent();
                next_fs = $(this).parent().next();
                
                $("#post_step .step").eq($("fieldset").index(current_fs)).removeClass("active");
                
                //activate next step on progressbar using the index of next_fs
                $("#post_step .step").eq($("fieldset").index(next_fs)).removeClass("disabled").addClass("active");
                
                //show the next fieldset
                next_fs.show(); 
                current_fs.hide(); 
                //hide the current fieldset with style
                
            });

            $("input.previous").click(function(){
                /*if(animating) return false;
                animating = true;*/
                
                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();
                
                //de-activate current step on progressbar
                $("#post_step .step").eq($("fieldset").index(current_fs)).removeClass("active");

                //activate previous step on progressbar
                $("#post_step .step").eq($("fieldset").index(previous_fs)).addClass("active");
                
                current_fs.hide();
                //show the previous fieldset
                previous_fs.show(); 
                //hide the current fieldset with style
                
            });

            /*$(".submit").click(function(){
                return false;
            });*/
            $('.ui.dropdown')
              .dropdown()
            ;
            $('.ui.form')
              .form({
                fields: {
                  gender: {
                    identifier  : 'gender',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Please enter a gender'
                      }
                    ]
                  },
                  name: {
                    identifier  : 'name',
                    rules: [
                      {
                        type   : 'empty',
                        prompt : 'Please enter your name'
                      }
                    ]
                  }
                }
              });

              $(".delete.icon").addClass("fa fa-times");
              $(document).ready( function(){        
                    var o = new Date,
                        n = new Date;
                    $("#v-s-d").datetimepicker({
                        format: "dd-mm-yyyy"
                    });
                    $("#v-e-d").datetimepicker({
                        format: "dd-mm-yyyy"
                    });
                    $("#j-s-d").datetimepicker({
                        format: "dd-mm-yyyy"
                    });
                    /*$("#date_to").datetimepicker({
                        format: "dd-mm-yyyy",
                        autoclose: 1,
                        pickTime: !1,
                        todayHighlight: 1,
                        startView: 2,
                        minView: 2,
                        maxView: 2,
                        defaultDate: n,
                        minDate: o,
                        forceParse: 0
                    });*/
            });
            $('.ui.rating')
              .rating('disable')
            ;
</script>
</body>
</html>