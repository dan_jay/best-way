<!DOCTYPE html>
<html lang="en">
<head >
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway.lk | Login</title>
    <!-- common assets  -->
    {% include "front/includes/head-assets.php" %}

    <link href="css/login.css" rel="stylesheet" type="text/css" />
<style>


</style>

</head>
<body>

<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}

<!-- main start -->
<main id="bg1" class="container-fluid">
  <div id="bg2" class="container-fluid">
    <!-- promo bar goes here -->
    <div class="container">
        <h1 id="slogan">The Biggest online market in Sri Lanka.</h1>
        <h2 id="miniSlogan">No more outside shopping. All in one. Buy, Sell and Earn!</h2>

        <div id="m-con" class="row">
          <div id="signin-pn" class="col-md-offset-3 col-md-6">
            <div class="si-header">
              <h3 class="s-panel-head-tx">Sign in to your account</h3>
            </div>
            <div class="si-body">
            <form action="{{app-context}}login" id="signinfrm" method="POST">
              <div class="col-md-offset-1 col-md-10">
                  <label for="username" class="frm-lable">Email</label>
                  <div class="input-group">
                      <span class="input-group-addon">@</span>
                      <input type="email" class="form-control" id="regemail" name="username" placeholder="Email" required >
                  </div>
                  <label for="password" class="frm-lable">Password</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                    <input type="password" class="form-control" id="newpwd" name="password" placeholder="Password" required>
                  </div>
                  <div class="ui toggle checkbox col-md-offset-0 col-md-6">
                    <input id="remme" name="remme" value="Remember Me" type="checkbox">
                    <label id="rem-me" for="remme" style="font-weight: bold;">Remember Me</label>
                  </div>
                  <div id="fgt-pwd" class="col-md-6">
                      <label class="frm-lable">Forgot password?</label>
                  </div>
              </div>
                <div class="col-md-12">
                    {%csrftok%}
                    <button type="submit" class="fluid ui orange button">Sign In</button>
                </div>
                </form>
              <div class="clearfix">
              </div>
              <div class="ui horizontal divider">
                OR
              </div>
              {% include "front/includes/custom_lock.php" %}
            </div>
            <div class="si-footer">
              <div id="ft-txt" class="col-sm-7 col-md-7"><h4>Don't have an account?</h4></div>
              <div class="col-md-1 col-sm-1 hidden-xs"></div>
              <div class="col-md-4 col-xm-12 col-sm-4"><button id="si-btn"class="fluid ui green button">Sign Up</button></div>
            </div>
          </div>

<!-- signup -->
<div id="signup-pn" class="col-md-offset-3 col-md-6">
  <div class="si-header">
    <h3 class="s-panel-head-tx">Sign up now for exclusive deals!</h3>
  </div>
  <div class="si-body">
      <form id="signupfrm" action="signin" method="post" >
    <div class="col-md-offset-1 col-md-10">
        <label for="fname" class="frm-lable">First Name</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
            <input type="text" class="form-control" id="fname"
                   name="fname" placeholder="First Name" required >
        </div>
        <label for="lname" class="frm-lable">Last Name</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required >
        </div>
        <label for="regemail" class="frm-lable">Email</label>
        <div class="input-group">
            <span class="input-group-addon">@</span>
            <input name="regemail" type="email" class="form-control" id="regemail" placeholder="Email" required >
        </div>
        <label for="newpwd" class="frm-lable">Password</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
          <input type="password" class="form-control" id="newpwd" placeholder="Password" required>
        </div>
        <div class="ui toggle checkbox col-xs-12">
          <input id="remme" name="accept-term" value="Accept terms" type="checkbox">
          <label id="rem-me" for="accept-term" style="font-weight: bold;">Accept terms and conditions</label>
        </div>
    </div>
    <div class="col-md-12">
      <button id="join" type="submit" class="fluid ui orange button">Sign Up</button>
    </div>
      </form>
    <div class="clearfix">
    </div>
    <div class="ui horizontal divider">
      OR
    </div>
    {% include "front/includes/custom_lock.php" %}
  </div>
  <div class="si-footer">
    <div id="ft-txt" class="col-sm-7 col-md-7"><h4>Already have an account?</h4></div>
    <div class="col-md-1 col-sm-1 hidden-xs"></div>
    <div class="col-md-4 col-xm-12 col-sm-4"><button id="su-btn"class="fluid ui green button">Sign In</button></div>
  </div>
</div>

        </div>
    </div>
  </div>
</main>

<!-- footer bar -->
{% include "front/includes/footer.php" %}

<!--<div id="toolWidget"></div>-->

{% include "front/includes/scripts-index-page.php" %}
<script>
$(document).ready(function(){
    $("#su-btn").click(function(){
        $("#signin-pn").show();
        $("#signup-pn").hide();
    });
    $("#si-btn").click(function(){
        $("#signin-pn").hide();
        $("#signup-pn").show();
    });
});
</script>
</body>
</html>
