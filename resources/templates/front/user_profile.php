<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <title>Bestway | {{profile.display-name}}</title>

    {% include "front/includes/head-assets.php" %}

  <link href="{{app-context}}css/app/toolkit.css" rel="stylesheet">
  <link href="{{app-context}}css/app/application.css" rel="stylesheet">

  <style>
  #topfix{
    margin-top: 55px;
  }
  #main-wrap{
    padding: 5px 0px;
  }
  #leftCon{
    padding-left: 0px;
  }
  #midCon{
    padding-left: 0px;
    padding-right: 0px;
  }
  #rightCon{
    padding-right: 0px;
  }
  #pro_tab{
    margin-top: 60px;
  }
  #pro_pic_con{
    position: relative !important;
  }
  #pro_pic{
    position: absolute;
    margin: -99px 16%;
    border: 2px solid #fff;
  }
  #pro_pic img{
    max-width: 160px !important;
  }
  #pro_pic_con:hover #pro_pic_changer{
    opacity: 1;
  }
  #pro_pic_con:hover #pro_pic_changer i{
    font-size: 15px;
    margin-right: 5px;
  }
  #upload-file {
    display: none;
  }
  #pro_name{
    margin-top: 70px;
  }
  #pro_border{
    margin-top: -76px;
  }

  .top_btn {
  display:inline-block;
  cursor:pointer;
  font-family:Arial;
  font-size:13px;
  font-weight:bold;
  padding:6px 12px;
  margin-bottom: 5px;
  text-decoration:none;
}
.top_btn:active {
  position:relative;
  top:1px;
}
.gr_btn{
  -moz-box-shadow:inset 0px 1px 0px 0px #9acc85;
  -webkit-box-shadow:inset 0px 1px 0px 0px #9acc85;
  box-shadow:inset 0px 1px 0px 0px #9acc85;
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #74ad5a), color-stop(1, #68a54b));
  background:-moz-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:-webkit-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:-o-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:-ms-linear-gradient(top, #74ad5a 5%, #68a54b 100%);
  background:linear-gradient(to bottom, #74ad5a 5%, #68a54b 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#74ad5a', endColorstr='#68a54b',GradientType=0);
  background-color:#74ad5a;
  border:1px solid #3b6e22;
  color:#ffffff;
}
.gr_btn:hover {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #68a54b), color-stop(1, #74ad5a));
  background:-moz-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:-webkit-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:-o-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:-ms-linear-gradient(top, #68a54b 5%, #74ad5a 100%);
  background:linear-gradient(to bottom, #68a54b 5%, #74ad5a 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#68a54b', endColorstr='#74ad5a',GradientType=0);
  background-color:#68a54b;
  color: #FFFFFF;
  text-decoration: none;
}
.gra_btn{
  -moz-box-shadow:inset 0px 1px 0px 0px #f9f9f9;
  -webkit-box-shadow:inset 0px 1px 0px 0px #f9f9f9;
  box-shadow:inset 0px 1px 0px 0px #f9f9f9;
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9));
  background:-moz-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:-webkit-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:-o-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:-ms-linear-gradient(top, #f9f9f9 5%, #e9e9e9 100%);
  background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9',GradientType=0);
  background-color:#f9f9f9;
  border:1px solid #D5D5D5;
  color:#666666;
}
.gra_btn:hover{
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9));
  background:-moz-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:-webkit-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:-o-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:-ms-linear-gradient(top, #e9e9e9 5%, #f9f9f9 100%);
  background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9',GradientType=0);
  background-color:#e9e9e9;
  color: #666666;
  text-decoration: none;
}
#msg_btn i{
  margin-right: 5px;
}

</style>
</head>
<body>


<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}

<!-- <div class="anq global-page" id="app-growl"></div>
 --><!-- search bar -->
<div id="topfix" class="global-page">

  <!-- promo bar goes here -->
  {% include "front/includes/promo_bar.php" %}
  {% include "front/includes/main_search.php" %}
</div>
<div class="cd fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="msgModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="d">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <button type="button" class="cg fx fp eg k js-newMsg">New message</button>
        <h4 class="modal-title">Messages</h4>
      </div>
    </div>
  </div>
</div>

<div id="main-wrap" class="container amt">
  <div>
    <div id="leftCon" class="gn">
      <div id="pro_tab" class="qv rc aog alu">

        <div class="qw dj">
          <div id="pro_pic_con">
            <svg id="pro_border" height="95" width="170">
              <path id="X1" d="M 0 60 0 0 0" stroke="#d3e0e9" stroke-width="1" fill="none" />
              <path id="X2" d="M 165 00 l 0 60" stroke="#d3e0e9" stroke-width="1" fill="none" />
              <path id="Y1" d="M 0 0 l 165 0" stroke="#d3e0e9" stroke-width="1" fill="none" />
            </svg>
           <div id="pro_pic">
              <a href="/my-profile">
                       <img src="{{ profile.profile-pic }}"  data-src="img/app/avatar-dhg-old.png" />
              </a>
            </div>
          </div>

          <h5 id="pro_name" class="qy">
            <a class="aku" href="profile/index.html">{{profile.display-name}}</a>
          </h5>

          <p class="alu">{{profile.subtitle}}</p>

          <ul class="aoi hidden">
            <li class="aoj">
              <a href="#userModal" class="aku" data-toggle="modal">
                Friends
                <h5 class="ali">12M</h5>
              </a>
            </li>

            <li class="aoj">
              <a href="#userModal" class="aku" data-toggle="modal">
                Enemies
                <h5 class="ali">1</h5>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="qv rc sm sp">
        <div class="qw">
          <h5 class="ald">About</h5>
          <ul class="eb tb">
            <li><span class="dp h xh all"></span>From <a href="#">Oh, Canada</a></li>
            <li><span class="dp h ajw all"></span>Member <a href="#">Junior</a></li>
          </ul>
        </div>
      </div>


    </div>

    <div id="midCon" class="gz">
    <div id="app"></div>
      <ul class="ca qo anx">



        {%for a in user-ads %}
        <li class="qf b aml">
          <div class="qg">
            <div class="qn">
              <small class="eg dp">{{a.publisheddate|date:mediumDateTime}}</small>
                <h5><a href="{{a.url}}" rel="{{a.description}}" title="{{a.title}}">{{a.title}}</a></h5>
            </div>

            <p>
              {{a.description|safe}}
            </p>

            <div class="any" data-grid="image-s" data-target-height="650">

              <div style="" >

                <img data-action="zoom" data-width="1200" data-height="900" src="{{a.imgs.0.url}}" />

              </div>
            </div>

          </div>
        </li>
        {% endfor %}

      </ul>
    </div>
    <div id="rightCon" class="hidden gn">
      <div class="alert pv alert-dismissible ss" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <a class="pr" href="profile/index.html">Visit your profile!</a> Check your self, you aren't looking too good.
      </div>

      <div class="hidden qv rc alu ss">
        <div class="qw">
          <h5 class="ald">Sponsored</h5>
          <div data-grid="images" data-target-height="150">
            <img class="qh" data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_2.jpg">
          </div>
          <p><strong>It might be time to visit Iceland.</strong> Iceland is so chill, and everything looks cool here. Also, we heard the people are pretty nice. What are you waiting for?</p>
          <button class="cg ts fx">Buy a ticket</button>
        </div>
      </div>

      <div class="hidden qv rc alu ss">
        <div class="qw">
          <h5 class="ald">Likes <small>· <a href="#">View All</a></small></h5>
          <ul class="qo anx">
            <li class="qf alm">
              <a class="qj" href="#">
                <img
                        class="qh cu"
                        src="img/app/avatar-fat.jpg">
              </a>
              <div class="qg">
                <strong>Jacob Thornton</strong> @fat
                <div class="aoa">
                  <button class="cg ts fx">
                    <span class="h vc"></span> Follow</button>
                </div>
              </div>
            </li>
            <li class="qf">
              <a class="qj" href="#">
                <img
                        class="qh cu"
                        src="img/app/avatar-mdo.png">
              </a>
              <div class="qg">
                <strong>Mark Otto</strong> @mdo
                <div class="aoa">
                  <button class="cg ts fx">
                    <span class="h vc"></span> Follow</button></button>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="qz">
          Dave really likes these nerds, no one knows why though.
        </div>
      </div>
   </div>
  </div>
</div>
<!-- footer bar -->
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts-my-profile.php" %}

  </body>
</html>