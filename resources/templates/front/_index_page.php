<!DOCTYPE html>
<html lang="en">
<head >
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway | The best online market in Sri Lanka</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/landing.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.animateSlider.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style type="text/css">

        #cover-con{
            min-height: 290px;
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#99d4e1+0,edf7ff+91,78b825+100 */
            background: #99d4e1; /* Old browsers */
            background: -moz-linear-gradient(top,  #99d4e1 0%, #edf7ff 91%, #78b825 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top,  #99d4e1 0%,#edf7ff 91%,#78b825 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom,  #99d4e1 0%,#edf7ff 91%,#78b825 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#99d4e1', endColorstr='#78b825',GradientType=0 ); /* IE6-9 */
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>
<body>

<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}

<!-- main start -->
<main id="main-wrap" class="container-fluid">
    

    <div id="MainContentCol" class="container">

        <h1 id="slogan" class="container">Everything nearby !!!</h1>
        <div class="ui shape">
          <div class="sides">
            <div class="active side">
                <h2 class="miniSlogan container">Don't waste time. All in one. Buy, Sell and Earn!</h2>
            </div>
            <div class="side">
                <h2 class="miniSlogan container">Find everything in Sri Lanka</h2>
            </div>
          </div>
        </div>
        <!-- <h2 class="miniSlogan" class="container">Don't waste time. All in one. Buy, Sell and Earn!</h2> -->


        <!-- search bar -->
        {% include "front/includes/main_search.php" %}
        
        <div id="mainBan" class="intro container">
            <div class="row">
                <div class="ban_imgs_con col-md-4 col-sm-4">
                    <div class="ban_imgs">
                        <img src="img/main_ban1.jpg" class="f_border_b">
                    </div>
                </div>
                <div class="ban_imgs_con col-md-4 col-sm-4">
                    <div class="ban_imgs">
                        <img src="img/main_ban3.jpg" class="f_border_b">                    
                    </div>
                </div>
                <div class="ban_imgs_con_hor col-md-4 col-sm-4">
                    <div class="ban_imgs">
                        <img src="img/main_ban_sm_1.jpg" class="f_border_b">                    
                    </div>
                    <div class="ban_imgs">
                        <img src="img/main_ban_sm_2.jpg" class="f_border_b">                    
                    </div>
                </div>
            </div>
            <!--<div id="cityBack"></div>-->
        </div>


    </div>
</main>
<!-- main end -->

<div id="testimo" class="divisionBar">
    <div class="ui olive inverted segment">
        <div id="feedcon" class="container" data-content="You can click to see other feeds">
            <div class="ui text shape">
              <div class="sides">
                <div class="active ui side">Did you know? This side starts visible.</div>
                <div class="ui side">Help, its another side!</div>
                <div class="ui side">This is the last side</div>
              </div>
            </div>
        </div>  
    </div>
</div>

<div id="contentArea">
    <div class="container">
        <div class="row">
            {%for x,y in categories %}
            <div class="pointText col-md-2">
                <div class="cat-con">
                    <h3><a href="#">{{x|name}}</a></h3>
                </div>
            </div>
            {% endfor %}
        </div>
    </div>
    <!-- Template page-->
</div>

<div class="divisionBar">
    <div class="globalConIn">

    </div>
</div>

<!-- footer bar -->
{% include "front/includes/footer.php" %}

<!--<div id="toolWidget"></div>-->
{% include "front/includes/scripts.php" %}

</body>
</html>