<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <!-- <base href="/front/" ></base> -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Home</title>


    <!-- common assets  -->
    {% include "front/includes/head-assets.php" %}
    <link rel="stylesheet" href="https://www.bestway.lk/css/AdminLTE.css" />

    <style type="text/css">
    body{
    	background-color: #F4F4F4;
    }
	#home-con{
		margin-top: 5px;
	}
    #gr_wh{
        padding: 6px 10px 6px 0px;
        margin-bottom: 10px;
        margin-right: 10px;
        background-color: #FFFFFF;
        border-radius: 4px;
        border: 1px solid #990066;
    }
    #gr_wh_nm{
        font-weight: bolder;
        color: #5c4c5c;
        font-size: 16px;
    }
    #mainAds h1{
      white-space: nowrap;
      width: 298px;
      overflow: hidden;
      text-overflow: ellipsis;
      color: #3B5998;
    }
    #gr_wh_pro_pic{
      height: 80px;
      margin-right: 10px;
      border-radius: 6px;
    }
    #welcome-con{
      padding: 10px;
      margin-bottom: 15px;
      clear: left;
      text-align: center;
      font-weight: bold;
      text-shadow: 0 1px 0 rgba(255, 255, 255, 1);
      background-color: #F2EAF0;
    }
    #welcome-txt{
      color: #990066;
      font-size: 24px;
      padding-bottom: 6px;
    }
    #intro-txt{
      color: #90949c;
      font-size: 20px;
      line-height: 30px;
    }
    #leftCol{
        background-color: #F2EAF0;
    }
    #news-btn-con{
        margin-top: 5px;
        margin-bottom: 5px;
    }
    #news-hl-con{
        padding: 5px;
    }
    #news-hl-con .news-hl{
        margin-bottom: 5px;
        display: inline-block;
        height: 60px;
        overflow: hidden;
    }
    #news-hl-con .news-hl-img{
        height: 100px;
        width: 100px;
        overflow: hidden;
        float: left;
        margin-right: 5px;
    }
    #news-hl-con .news-hl-content{
        display: flex;
    }
    #news-hl-con .news-hl-img img{
        width: 100%;
    }
    #news-hl-con .news-hl-content h4 a{
        line-height: 15px;
        font-size: 13px;
        text-align: justify;
        cursor: pointer;
    }
    .pg_pr_name{
      margin-top: -22px;
    }
    .pg_pr_name #home_img_container img{
      float: left;
      margin-left: 5px;
    }
    .ui.purple.label{
        background-color: #990066 !important;
        border-color: #330022 !important;
    }
    .ad_img_con{
      background-color: #f7f7f7;
      border: 1px solid #ccc;
      margin: 2px;
      display: inline-block;
      width: 310px;
    }

    @media screen and (max-width: 980px) {
      #mainSearchCon {
        background-color: #3863A0;
        display: none;
      }
    }
    .AnimalsPetSupplies, .OfficeSchoolSupplies, .Properties, .ReligiousCeremonial, .SecurityProtection,
    .Services, .Software, .SportingGoods, .ToysGames, .VehiclesParts,
    .WeddingsEvents, .ApparelAccessories, .Antiques, .BusinessIndustrial, .Finance,
    .FlowersGift, .FoodBeverages, .Furniture, .Hardware, .HealthBeauty,
    .HomeGarden, .Jobs, .ArtsHobbies, .LuggageBags, .Media,
    .MotherKids, .NoveltySpecialUse, .CamerasOptics, .CraftsPaintingsPottery, .Education,
    .Electronics, .Entertainment, .HotelsTravel
    {
      background-size: 550% 650%;
      width: 28px;
      height: 28px;
    }
    .AnimalsPetSupplies{ background-position: -1px 4px;  }
    .ApparelAccessories{ background-position: -30px -49px;  }
    .ArtsHobbies{ background-position: -59px -101px;  }
    .CamerasOptics{ background-position: -59px -127px;  }
    .CraftsPaintingsPottery{ background-position: -89px -128px;  }
    .Education{ background-position: -117px -128px;  }
    .Electronics{ background-position: -1px -153px;  }
    .Entertainment{ background-position: -30px -154px;  }
    .HotelsTravel{ background-position: -57px -154px;  }
    .OfficeSchoolSupplies{ background-position: -30px 4px;  }
    .Properties{ background-position: -59px 5px;  }
    .ReligiousCeremonial{ background-position: -88px 3px;  }
    .SecurityProtection{ background-position: -116px 3px;  }
    .Services{ background-position: -1px -23px;  }
    .Software{ background-position: -30px -25px;  }
    .SportingGoods{ background-position: -59px -23px;  }
    .ToysGames{ background-position: -87px -24.19px;  }
    .VehiclesParts{ background-position: -116px -24px;  }
    .WeddingsEvents{ background-position: -1px -50px;  }
    .Antiques{ background-position: -59px -51px;  }
    .BusinessIndustrial{ background-position: -88px -50px;  }
    .Finance{ background-position: -117px -50px;  }
    .FlowersGift{ background-position: -1px -75px;  }
    .FoodBeverages{ background-position: -30px -76px;  }
    .Furniture{ background-position: -60px -77px;  }
    .Hardware{ background-position: -88px -77px;  }
    .HealthBeauty{ background-position: -116px -76px;  }
    .HomeGarden{ background-position: -1px -103px;  }
    .Jobs{ background-position: -30px -102px;  }
    .LuggageBags{ background-position: -87px -101.7px;  }
    .Media{ background-position: -117px -102px;  }
    .MotherKids{ background-position: -1px -126px;  }
    .NoveltySpecialUse{ background-position: -30px -127px;  }
    a .cat-text{
      vertical-align: middle;
      margin-top: -15px;
      display: inline-block;
    }
    #leftCol{
        padding-left: 0px;
        padding-right: 0px;
    }
    @media (max-width: 980px) {
      #leftCol{
          display: none;
          margin-bottom: 10px;
      }
      #rightCol{
        display: none;
      }
    }
    .news-lang-select{
        box-shadow: 0px 0px 8px #555 !important;
        border: 1px solid white !important;
    }

    .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('https://www.bestway.lk/imgs/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('https://www.bestway.lk/imgs/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
        .jssora22l.jssora22lds { background-position: -10px -31px; opacity: .3; pointer-events: none; }
        .jssora22r.jssora22rds { background-position: -70px -31px; opacity: .3; pointer-events: none; }
        .news-con .news-txt{
            font-size: 1.31rem;
        }
        .news-head-con{
            font-size: 13px;
            font-weight: bold;
            color: #990066;
            cursor: pointer;
        }
        .news-date-time i{
            margin-right: 4px;
        }
        .hm-news-time{
            float: right;
        }
        .ui.modal > .header {
          color: #990066;
        }
        .ui.modal > .content {
          width: auto !important;
        }
        .ui.modal > .content > .image {
          float: left;
          margin-bottom: 15px;
          margin-right: 15px;
        }
        .ui.modal > .content > .description > p {
          font-size: 14px;
          line-height: 23px;
        }
</style>
</head>
<body>
<div id="main-con">
<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}


<!-- promo bar goes here -->
    {% include "front/includes/promo_bar.php" %}

    {% include "front/includes/top-hor-menu.php" %}
     <!-- <div class="global-page"> -->
        {% include "front/includes/main_search.php" %}
    <!-- </div> -->

<div id="main-wrap" class="container">


	<div id="home-con" class="row">

        <div id="leftCol" class="col-md-2">
            <!-- main_cat.php -->
            {% include "front/includes/sidebar.php" %}
        </div>
        <div class="col-md-7">
<!-- /Special AdBox -->
            <div id="contentArea" class="row">
                <!-- Ad layout in home page-->
                <div id="gr_wh" class="col-md-12" >
                  <span class="ui purple right ribbon label">Welcome !</span>
                  <div class="pg_pr_name" >
                    <div id="home_img_container" class="visual-area">
                        <!--<img id="gr_wh_pro_pic"  height="80px" src="{{ profile-pic }}" style="height: 80px!important;">-->
                    </div>
                    <p id="gr_wh_nm">
                     Hi, {{fnamelname}}
                    </p>
                  </div>
                </div>



                <div id="mainAds_container" >

                <div id="welcome-con" class="wb">
                  <div id="welcome-txt">Welcome to Bestway.lk</div>
                  <div id="intro-txt">Now You can post your advertisement free, you can use search to find anything you want.<br>Let your friends know that you are here...</div>
                </div>

                <div id="slider-con">
                    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 600px; height: 280px; overflow: hidden; visibility: hidden;">
                        <!-- Loading Screen -->
                        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                            <div style="position:absolute;display:block;background:url('https://www.bestway.lk/imgs/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                        </div>
                        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 600px; height: 280px; overflow: hidden;">
                            <div data-b="0" data-p="170.00" data-po="80% 55%">
                                <img data-u="image" src="https://www.bestway.lk/img/banner1.png" />
                            </div>
                            <div data-b="1" data-p="170.00" style="display:none;">
                                <a href="{{app-context}}sell"><img data-u="image" src="https://www.bestway.lk/img/banner2.png" /></a>
                            </div>
                        </div>
                        <!-- Bullet Navigator -->
                        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                            <!-- bullet navigator item prototype -->
                            <div data-u="prototype" style="width:16px;height:16px;"></div>
                        </div>
                        <!-- Arrow Navigator -->
                        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:10px;width:40px;height:58px;" data-autocenter="2"></span>
                        <span data-u="arrowright" class="jssora22r" style="top:0px;right:10px;width:40px;height:58px;" data-autocenter="2"></span>
                    </div>
                </div>

                <!-- tabs container start -->
                    <div id="listing_container" ><!-- Listings populate here --></div>
                <!-- tabs container end -->

                </div>
                <div>

                </div>

            </div>
        </div>

        <div id="rightCol" class="col-md-3">
            <div id="news-btn-con">
                <button class="ui primary right floated button">
                    News
                </button>
                <div class="ui small buttons">
                    <button class="ui teal button">E</button>
                    <button class="ui yellow button news-lang-select">සි</button>
                    <button class="ui red button">த</button>
                </div>
            </div>
            <div id="news-hl-container" class="">
                <div class="news-hl">
                    <div class="news-hl-img" >
                        <img src="https://www.bestway.lk/img/s_logo.png" width="50px" height="50px" />
                    </div>
                    <div class="news-hl-content">
                        <h4>
                            <a href="">Welcome to Bestway.lk</a></h4>
                    </div>
                </div>


            </div>

            {%comment%}
            {% include "front/includes/todo.php" %}
            {%endcomment%}

            <div class="fb-page" data-href="https://www.facebook.com/Bestwaylanka/" data-tabs="timeline" data-small-header="false"
                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div
                    class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Bestwaylanka/"><a
                    href="https://www.facebook.com/Bestwaylanka/">Bestway.lk</a></blockquote></div></div>
            {%comment%}
            <p id="sp">sponsored</p>
<!-- Special AdBox -->
            <div id="sp_ad_x1" class="sp_ad">
                <img src="img/s1.png" />

                <h1><a href="#">Ad Name</a></h1>

                <p class="ad_desc">Ad description</p>
            </div>
            <div id="sp_ad_x2" class="sp_ad">
                <img src="img/s2.png" />

                <h1><a href="#">Ad Name</a></h1>

                <p class="ad_desc">Ad description</p>
            </div>
            {%endcomment%}
        </div>
    </div>
</div>
</div>
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts.php" %}

<script src="https://www.bestway.lk/js/jssor.slider-22.0.15.min.js" type="text/javascript" ></script>
<!-- Latest compiled and minified JavaScript -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/sidebar.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $("#shw-srch").click(function(){
        $("#mainSearchCon").slideToggle("slow");
    });
    //$('select').chosen({width: '100%'});
    $('#sh-cat').click(function(){
        var contentArea = $('#contentArea');
        $("#leftCol").slideToggle( "slow", function () {

            contentArea.removeClass("col-md-7").addClass("col-md-12");
            contentArea.parent().removeClass("col-md-7").addClass("col-md-9");
        }

        );
    });
    //$(this).removeClass("classname");
});
</script>
<script type="text/javascript">
        jQuery(document).ready(function () {

        var _SlideshowTransitions = [
            {$Duration:1000,$Delay:80,$Cols:8,$Rows:4,$Opacity:2},
            {$Duration:1200,$Opacity:2}
        ];
        var options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: _SlideshowTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/

            //$('#news-modal').modal({inverted: true, blurring: true});

            //$('.ui.modal').modal({inverted: true, blurring: true});
            //$('.modal').css({"bottom": "auto", "top": "30%"});
        });
    </script>
<div id="news-modal"></div>

</body>
</html>
