<!DOCTYPE html>
<html lang="en">
<head >
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Bestway | The best online market in Sri Lanka</title>
    <link href="css/landing.css" rel="stylesheet" type="text/css" />

    <!-- common assets  -->
    {% include "front/includes/head-assets.php" %}

    <link href="css/range.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.animateSlider.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <style type="text/css">

    .ad_sel_bx{
        background-color: #FFFFFF;
      border: 1px solid #efefef;
      height: 100px;
      text-align: center;
    }
    .ad_sel_bx:hover{
      background-color: #f2f3f4;
      box-shadow: 0px 1px 1px #999;
    }
    .bx_sdow{
      box-shadow: 0px 4px 5px #999;
    }
    .bx_clicked{
      background-color: #f2f3f4;
     box-shadow: 0px 1px 1px #999;
    }
    .ad_sel_bx_con{
      padding: 0.2em;
    }
    .icon_con{
      margin: 5px auto;
      width: 64px;
      height: 64px;
    }
    .ui.range .inner .track-fill {
        background-color : #96BF49 !important;
    }
    </style>
</head>
<body>

<main>
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}

    <!-- main start -->
    <main class="container-fluid main-wrap">


        <div id="MainContentCol" class="container-fluid">

            <header id="cover-con" class="container-fluid" >
                <div class="visible-xs-block">
                  <h1 id="site_url">BestWay.lk</h1>
                  <h2 id="main_slogan">Everything nearby !!!</h2>
                  <h3 id="sub_slogan">Find anything in Sri Lanka</h3>
                  <h4 id="mini_slogan">You can Sell, Buy &amp; Earn</h4>

                  <div class="row">
                    <div class="col-xs-6">
                      <a class="col-xs-12 mb-btn ui green button" id="hssignin"  href="/signin">Login</a>
                    </div>
                    <div class="col-xs-6">
                      <a class="col-xs-12 mb-btn ui green button" id="hssignin"  href="/signin">Sign Up</a>
                    </div>
                  </div>
                </div>

                <ul class="anim-slider hidden-xs">

                    <!-- Slide No1 -->
                    <li class="anim-slide">
                        <h1 id="site_url">BestWay.lk</h1>
                        <h2 id="main_slogan">Everything nearby !!!</h2>
                        <h3 id="sub_slogan">Find anything in Sri Lanka</h3>
                        <h4 id="mini_slogan">You can Sell, Buy &amp; Earn</h4>
                        <img src="img/slider/bwindevices.png" alt="Bestway.lk in Devices" id="bwindev">
                        <!-- images start -->
                        <img src="img/slider/football.png" alt="football" id="football" class="hidden-sm">
                        <img src="img/slider/iphone.png" alt="iphone" id="iphone" class="hidden-sm">
                        <img src="img/slider/bag.png" alt="bag" id="bag" class="hidden-sm">
                        <img src="img/slider/car.png" alt="car" id="car" class="hidden-sm">
                        <img src="img/slider/iron.png" alt="iron" id="iron" class="hidden-sm">
                        <img src="img/slider/tv.png" alt="tv" id="tv" class="hidden-sm">
                        <img src="img/slider/watch.png" alt="watch" id="watch" class="hidden-sm">
                        <img src="img/slider/house.png" alt="house" id="house" class="hidden-sm">
                        <img src="img/slider/antenna.png" alt="antenna" id="antenna" class="hidden-sm">
                        <img src="img/slider/baby.png" alt="baby" id="baby" class="hidden-sm">
                        <img src="img/slider/bed.png" alt="bed" id="bed" class="hidden-sm">
                        <img src="img/slider/camera.png" alt="camera" id="camera" class="hidden-sm">
                        <img src="img/slider/cart.png" alt="cart" id="cart" class="hidden-sm">
                        <img src="img/slider/champagne.png" alt="champagne" id="champagne" class="hidden-sm">
                        <img src="img/slider/desktop.png" alt="desktop" id="desktop" class="hidden-sm">
                        <img src="img/slider/diamond.png" alt="diamond" id="diamond" class="hidden-sm">
                        <img src="img/slider/dinner.png" alt="dinner" id="dinner" class="hidden-sm">
                        <img src="img/slider/dress.png" alt="dress" id="dress" class="hidden-sm">
                        <img src="img/slider/drink.png" alt="drink" id="drink" class="hidden-sm">
                        <img src="img/slider/drum-set.png" alt="drum-set" id="drum-set" class="hidden-sm">
                        <img src="img/slider/electric-guitar.png" alt="electric-guitar" id="electric-guitar" class="hidden-sm">
                        <img src="img/slider/flowers.png" alt="flowers" id="flowers" class="hidden-sm">
                        <img src="img/slider/gamepad.png" alt="gamepad" id="gamepad" class="hidden-sm">
                        <img src="img/slider/gift.png" alt="gift" id="gift" class="hidden-sm">
                        <img src="img/slider/handshake.png" alt="handshake" id="handshake" class="hidden-sm">
                        <img src="img/slider/hdd.png" alt="hdd" id="hdd" class="hidden-sm">
                        <img src="img/slider/headphone.png" alt="headphone" id="headphone" class="hidden-sm">
                        <img src="img/slider/laptop.png" alt="laptop" id="laptop" class="hidden-sm">
                        <img src="img/slider/light.png" alt="light" id="light" class="hidden-sm">
                        <img src="img/slider/mortarboard.png" alt="mortarboard" id="mortarboard" class="hidden-sm">
                        <img src="img/slider/pendrive.png" alt="pendrive" id="pendrive" class="hidden-sm">
                        <img src="img/slider/phone-book.png" alt="phone-book" id="phone-book" class="hidden-sm">
                        <img src="img/slider/pizza.png" alt="pizza" id="pizza" class="hidden-sm">
                        <img src="img/slider/placeholder.png" alt="placeholder" id="placeholder" class="hidden-sm">
                        <img src="img/slider/ram.png" alt="ram" id="ram" class="hidden-sm">
                        <img src="img/slider/report.png" alt="report" id="report" class="hidden-sm">
                        <img src="img/slider/shirt.png" alt="shirt" id="shirt" class="hidden-sm">
                        <img src="img/slider/shoes.png" alt="shoes" id="shoes" class="hidden-sm">
                        <img src="img/slider/washing.png" alt="washing" id="washing" class="hidden-sm">
                        <!-- images end -->
                    </li>


                    <!-- Slide No2 -->
                    <li class="anim-slide">
                        <img src="img/slider/lookfuture.png" alt="We always look for future trends" id="luk_future">
                        <img src="img/slider/meeting.png" alt="Bestway.lk in Devices" id="meeting_img">
                        <img src="img/slider/barchart.png" alt="Bestway.lk in Devices" id="barchart">
                        <h3 id="todo" class="hide"></h3>
                        <h3 id="bounceUp">You can search the market</h3>

                        <h3 id="bounce">Analyze results</h3>
                        <h4 id="bounceUp" class="hide"></h4>
                        <h4 id="bounceRight" class="hide"></h4>

                        <h3 id="fade">Grow your income</h3>
                        <h4 id="fadeUp" class="hide"></h4>
                        <h4 id="fadeDown" class="hide"></h4>

                        <h3 id="rotate" class="hide">Bestway is for Everyone</h3>
                        <h4 id="rotateRight" class="hide"></h4>
                        <h4 id="rotateLeft" class="hide"></h4>
                    </li>

                    <!-- Slide No3 -->
                    <li class="anim-slide">
                        <img src="img/slider/climb.png" alt="CSS3" id="css3"/>
                        <img src="img/slider/jetpack.png" alt="HTML5" id="html5"/>
                        <h2 id="animatecss" class="hide">Animate.css</h2>
                        <h3 id="ubw">Use Bestway.lk</h3>
                        <h3 id="boosty">And boost yourself up in the business</h3>
                    </li>

                    <!-- Arrows -->
                    <nav class="anim-arrows">
                        <span class="anim-arrows-prev"></span>
                        <span class="anim-arrows-next"></span>
                    </nav>
                    <!-- Dynamically created dots -->

                </ul>
            </header>
            <div id="search-con" class="container-fluid">
                <div class="container">
                    <div class="row ">
                        <div id="horvac">
                            <div class="foh">
                                <div class="foh-con">
                                    <!-- search bar -->
                                    {% include "front/includes/main_search.php" %}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- main end -->
    </main>

    <div id="testimo" class="divisionBar" style="visibility: hidden;">
        <div class="ui olive inverted segment">
            <div id="feedcon" class="container" data-content="You can click to see other feeds">
                <div class="ui text shape">
                    <div class="sides">
                        <div class="active ui side">Did you know? This side starts visible.</div>
                        <div class="ui side">Help, its another side!</div>
                        <div class="ui side">This is the last side</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="contentArea">
        <div class="container">
            <div class="row">
                {%for x in categories %}
                <div class="col-xs-6 col-sm-4 col-md-2 ad_sel_bx_con">
                    <a href="/search-results?main-cat={{x.id}}"><div class="ad_sel_bx bx_sdow">
                        <div class="icon_con"><span class="{{x.css_class}}"></span></div>
                        {{x.label}}
                    </div></a>
                </div>
                {% endfor %}
            </div>
        </div>
        <!-- Template page-->
    </div>




    <!-- footer bar -->
    {% include "front/includes/footer.php" %}

    {% include "front/includes/scripts-index-page.php" %}

<script>
$('form.ui.form')
    .form({
        inline: true,
        fields: {
            name: {
                identifier: 'fname',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your first name'
                    }
                ]
            },
            skills: {
                identifier: 'lname',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your last name'
                    }
                ]
            },
            username: {
                identifier: 'regemail',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a valid email address'
                    }
                ]
            },
            password: {
                identifier: 'nwpwd',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a password'
                    },
                    {
                        type   : 'minLength[6]',
                        prompt : 'Your password must be at least {ruleValue} characters'
                    }
                ]
            },
            terms: {
                identifier: 'acc_term',
                rules: [
                    {
                        type   : 'checked',
                        prompt : 'You must agree to the terms and conditions'
                    }
                ]
            }
        },
        onFailure: function(formErrors, fields){
            $("form.form").find("div.message.error").text(formErrors[0]);
        }
    })
;

</script>


</body>
</html>
