<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <base href="/front/" ></base> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway | FAQ</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway|Crimson+Text|Open+Sans+Condensed:300|Peddana" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/AdminLTE.css">
    <link rel="stylesheet" href="css/home.css">
    <!-- <link href="css/chosen.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <style type="text/css">
    h1{
      font-family: 'Raleway', sans-serif;
      font-size: 35px;
      line-height: 40px;
      color: #990066;
    }
    h2{
      font-family: 'Open Sans Condensed', sans-serif;
      font-size: 30px;
      color: #FF4500;
    }
    p{
      font-family: 'Crimson Text', serif;
      font-size: 16px;
      line-height: 24px;
    }
    p ul li{
      font-family: 'Peddana', serif;
      font-size: 18px;
      line-height: 22px;
    }
    p, li{
      color: #AAAAAA;
    }
    #faqimg{
      float: right;
      width: 300px;
      margin: 15px;
    }
    li.ma{
      color: #990066;
    }
    </style>
    </head>
    <body>
    <div id="main-con">
    <!-- top bar navigation goes here -->
    {% include "front/includes/top_nav.php" %}


    <!-- promo bar goes here -->
        {% include "front/includes/promo_bar.php" %}

        {% include "front/includes/top-hor-menu.php" %}
         <!-- <div class="global-page"> -->
            {% include "front/includes/main_search.php" %}
        <!-- </div> -->

    <div id="main-wrap" class="container">
<h1>Bestway.lk FAQ</h1><img id="faqimg" src="https://www.bestway.lk/img/faq.png" alt="faq" >

<p>Welcome and thank you for visiting <a href="https://www.bestway.lk">Bestway.lk</a>. Please read and follow our house rules before accessing our services.</p>

<h2>Starting With BestWay</h2>

<ol>
<li class="ma">What is BestWay?</li>
  <p>BestWay is Srilanka's one of the largest Online Shopping Community where buyers and sellers meet to make awesome deals. At BestWay, we pride ourselves in offering the best prices online in Srilanka for nearly all items sold ranging from fashion and electronics to jewelry and books. BestWay is the "one stop shop" for buyers looking for top deals and sellers looking to rock their sales. </p>

<li class="ma">Why should I choose BestWay?</li>
  <p>BestWay Online Shopping Community comprises of millions of genuine buyers and sellers completing thousands of transactions every day. Sellers on BestWay are based across the country bringing in wide variety of products ranging from fashion and electronics to Jewelries and books offering the best price in the market.</p>
  <p>As a buyer, whether you are looking for an inexpensive used phone or a brand new laptop, a dress for your wife or an awesome tie for your boyfriend, you can find a selection that works for you with convenient payment methods and reliable product delivery.</p>

<li class="ma">How does it work?</li>
  <p>Buying or selling on BestWay is a simple process. All that you need to do is to register for an account to get started. Simply follow the steps below to complete your registration today!</p>
    <ol style="list-style-type:upper-roman;">
      <li>Click Login/Join button on the right top corner of the homepage.</li>
      <li>Click Create a new account (in desktop) or I am a new customer (on your mobile device) under the new member section.</li>
      <li>Fill up the form to complete the registration.</li>
      <li>Do I need a separate account to buy or sell?</li>
    </ol>
  <li>No. You only need one account in order to buy and sell.</li>
</ol>

<h2>Account Troubleshooting </h2>
<ol>
<li class="ma">I forgot my password? Can I reset it?</li>
<p>To retrieve your password, go to the Login page and click on Forgot Password?. You will be asked to fill in and submit your email address. After doing so, you will receive an email which enables you to reset your password.</p>
<li class="ma">I can't register on the website</li>
<ol style="list-style-type:upper-roman;">
<li>Make sure that you don't already hold a BestWay account.</li>
<li>Make sure you have filled out all required fields on the registration page.</li>
<li>Click on Register and take a look at the information highlighted in red to be changed or filled.</li>
<li>Once the registration form is filled out and submitted, an email notification will be sent to your email account with a link to validate your account. Please access your email to validate your account.</li>
<li>If you continue to experience problems, please send an email including your phone number to xxxxxxxx@bestway.lk . We will call you back ASAP and walk you through the registration process.</li>
</ol>
</ol>

<h2>Buying</h2>

<ol>
<li class="ma">Is it free?</li>
  <p>BestWay is 100% free for buyers. We are here to open you up to a whole new world of possibilities.</p>
<li class="ma">Who do I buy my products from?</li>

<p>All items sold on our website are being sold by third parties. Merchants and private individuals who post their listings and are responsible for pictures and descriptions. However, we are screening sellers and listings constantly in an effort to ensure only serious sellers use our website.</p>

<p>To make sure your purchase is a great success; look out for sellers with:</p>

***(our further to decide rating system)***
</ol>

<p>Last Edited on 2016-12-18</p>

</div>
</div>
{% include "front/includes/footer.php" %}

{% include "front/includes/scripts.php" %}
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
<!-- SlimScroll -->
<script src="js/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/sidebar.js"></script>
<script>
$(document).ready(function(){
    $("#shw-srch").click(function(){
        $("#mainSearchCon").slideToggle("slow");
    });
    //$('select').chosen({width: '100%'});
    $('#sh-cat').click(function(){
        $("#leftCol").slideToggle("slow");
      //  $('#contentArea').removeClass("col-md-7").addClass("col-md-9");
    });
    //$(this).removeClass("classname");
});
</script>
</body>
</html>
