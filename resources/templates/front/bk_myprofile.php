<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>Bestway | My Page</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'/>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="css/app/toolkit.css" rel="stylesheet" />
    <link href="css/app/application.css" rel="stylesheet" />
    <style>
  #topfix{
    margin-top: 55px;
  }
  #main-wrap{
    padding: 5px 0px;
  }
  #leftCon{
    padding-left: 0px;
  }
  #midCon{
    padding-left: 0px;
    padding-right: 0px;
  }
  #rightCon{
    padding-right: 0px;
  }
  #pro_pic_con{
    position: relative;
  }
  #pro_pic_con:hover #pro_pic_changer{
    opacity: 1;
  }
  #pro_pic_changer{
opacity: 0;
z-index: 5;
transition: opacity .13s ease-out;
background-color: rgba(0,0,0,0.8);
color: #FFFFFF;
padding: 20px 20px;
position: absolute;
top: -8px;
text-align: center;
overflow: hidden;
width: 98px;
margin: 0 0 0 74px;
  }
input[type='file'] {
  opacity:0
}
</style>
</head>

<body>

<!-- top bar navigation goes here -->
{% include "front/includes/top_nav.php" %}


<!-- <div class="anq global-page" id="app-growl"></div>
 --><!-- search bar -->
<div id="topfix" class="global-page">
 <!-- promo bar goes here -->
        {% include "front/includes/promo_bar.php" %}
        {% include "front/includes/main_search.php" %}
</div>
<div class="cd fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="msgModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="d">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <button type="button" class="cg fx fp eg k js-newMsg">New message</button>
        <h4 class="modal-title">Messages</h4>
      </div>
    </div>
  </div>
</div>

<div id="main-wrap" class="container amt">
  <div>
    <div id="leftCon" class="gn">
      <div class="qv rc aog alu">
        <!--<div class="qx" style="background-image: url(img/app/iceland.jpg);"></div>-->
        <div class="qw dj">
            <div id="pro_pic_con">
                <a href="/my-profile">
                    <img class="aoh" src="img/app/avatar-dhg-old.png">
                </a>
                <div id="pro_pic_changer">
                    <i class="fa fa-camera"></i> Change Image
                </div>
            </div>

          <h5 class="qy">
            <a class="aku" href="profile/index.html">{{fnamelname}}</a>
          </h5>



          <p class="alu">I wish i was a little bit taller, wish i was a baller, wish i had a girl… also.</p>
          <ul class="aoi">
            <li class="aoj">
              <a href="#userModal" class="aku" data-toggle="modal">
                Friends
                <h5 class="ali">12M</h5>
              </a>
            </li>

            <li class="aoj">
              <a href="#userModal" class="aku" data-toggle="modal">
                Enemies
                <h5 class="ali">1</h5>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="qv rc sm sp">
        <div class="qw">
          <h5 class="ald">About <small>· <a href="#">Edit</a></small></h5>
          <ul class="eb tb">
            <li><span class="dp h xh all"></span>Went to <a href="#">Oh, Canada</a></li>
            <li><span class="dp h ajw all"></span>Became friends with <a href="#">Obama</a></li>
            <li><span class="dp h abu all"></span>Worked at <a href="#">Github</a></li>
            <li><span class="dp h ack all"></span>Lives in <a href="#">San Francisco, CA</a></li>
            <li><span class="dp h adt all"></span>From <a href="#">Seattle, WA</a></li>
          </ul>
        </div>
      </div>

      <div class="qv rc sm sp">
        <div class="qw">
          <h5 class="ald">Photos <small>· <a href="#">Edit</a></small></h5>
          <div data-grid="images" data-target-height="150">
            <div>
              <img data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_5.jpg">
            </div>

            <div>
              <img data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_6.jpg">
            </div>

            <div>
              <img data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_7.jpg">
            </div>

            <div>
              <img data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_8.jpg">
            </div>

            <div>
              <img data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_9.jpg">
            </div>

            <div>
              <img data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_10.jpg">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="midCon" class="gz">
      <ul class="ca qo anx">

        <li class="qf b aml">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Message">
            <div class="fj">
              <button type="button" class="cg fm">
                <span class="h xi"></span>
              </button>
            </div>
          </div>
        </li>

        <li class="qf b aml">
          <a class="qj" href="#">
            <img
              class="qh cu"
              src="img/app/avatar-dhg.png">
          </a>
          <div class="qg">
            <div class="qn">
              <small class="eg dp">4 min</small>
              <h5>Kevin Rathna</h5>
            </div>

            <p>
              Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>

            <div class="any" data-grid="images">
              <div style="display: none">
                <img data-action="zoom" data-width="1050" data-height="700" src="img/app/unsplash_1.jpg">
              </div>

              <div style="display: none">
                <img data-action="zoom" data-width="640" data-height="640" src="img/app/instagram_1.jpg">
              </div>

              <div style="display: none">
                <img data-action="zoom" data-width="640" data-height="640" src="img/app/instagram_13.jpg">
              </div>

              <div style="display: none">
                <img data-action="zoom" data-width="1048" data-height="700" src="img/app/unsplash_2.jpg">
              </div>
            </div>

          </div>
        </li>

        
      </ul>
    </div>
    <div id="rightCon" class="gn">
      <div class="alert pv alert-dismissible ss" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <a class="pr" href="profile/index.html">Visit your profile!</a> Check your self, you aren't looking too good.
      </div>

      <div class="qv rc alu ss">
        <div class="qw">
          <h5 class="ald">Sponsored</h5>
          <div data-grid="images" data-target-height="150">
            <img class="qh" data-width="640" data-height="640" data-action="zoom" src="img/app/instagram_2.jpg">
          </div>
          <p><strong>It might be time to visit Iceland.</strong> Iceland is so chill, and everything looks cool here. Also, we heard the people are pretty nice. What are you waiting for?</p>
          <button class="cg ts fx">Buy a ticket</button>
        </div>
      </div>

        <div id="app" name="apt" style="min-width:15px; min-height: 15px; background: #123ABC">
            Loading javascript....
        </div>

      <div class="qv rc alu ss">
        <div class="qw">
        <h5 class="ald">Likes <small>· <a href="#">View All</a></small></h5>
        <ul class="qo anx">
          <li class="qf alm">
            <a class="qj" href="#">
              <img
                class="qh cu"
                src="img/app/avatar-fat.jpg">
            </a>
            <div class="qg">
              <strong>Jacob Thornton</strong> @fat
              <div class="aoa">
                <button class="cg ts fx">
                  <span class="h vc"></span> Follow</button>
              </div>
            </div>
          </li>
           <li class="qf">
            <a class="qj" href="#">
              <img
                class="qh cu"
                src="img/app/avatar-mdo.png">
            </a>
            <div class="qg">
              <strong>Mark Otto</strong> @mdo
              <div class="aoa">
                <button class="cg ts fx">
                  <span class="h vc"></span> Follow</button></button>
              </div>
            </div>
          </li>
        </ul>
        </div>
        <div class="qz">
          Dave really likes these nerds, no one knows why though.
        </div>
      </div>

      <div class="qv rc aok">
        <div class="qw">
          &copy; 2015 Bootstrap

          <a href="#">About</a>
          <a href="#">Help</a>
          <a href="#">Terms</a>
          <a href="#">Privacy</a>
          <a href="#">Cookies</a>
          <a href="#">Ads </a>

          <a href="#">info</a>
          <a href="#">Brand</a>
          <a href="#">Blog</a>
          <a href="#">Status</a>
          <a href="#">Apps</a>
          <a href="#">Jobs</a>
          <a href="#">Advertise</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- footer bar -->

{% include "front/includes/footer.php" %}

{% include "front/includes/scripts-my-profile.php" %}

  </body>
</html>