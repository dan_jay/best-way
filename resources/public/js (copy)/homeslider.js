$(".anim-slider").animateSlider(
            {
                autoplay    :true,
                interval    :10000,
                animations  : 
                {
                    0   :   //Slide No1
                    {
                        h1  : 
                        {
                            show      : "bounceIn",
                            hide      : "fadeOutDownBig",
                            delayShow : "delay0-5s"
                        },
                        h2:
                        {
                            show      : "fadeInUpBig",
                            hide      : "fadeOutLeft",
                            delayShow : "delay1-5s"
                        },
                        h3  :
                        {
                            show      : "fadeInUpBig",
                            hide      : "fadeOutRight",
                            delayShow : "delay2-5s"
                        },
                        h4:
                        {
                            show      : "fadeInUpBig",
                            hide      : "zoomOut",
                            delayShow : "delay4-5s"
                        },
                        "img#bwindev":
                        {
                            show      : "flipInX",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay3-5s"
                        },

                        "img#football":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5s"
                        },
                        "img#iphone":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-2s"
                        },
                        "img#bag":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-4s"
                        },
                        "img#car":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-6s"
                        },
                        "img#iron":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-8s"
                        },
                        "img#tv":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-10s"
                        },
                        "img#watch":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-12s"
                        },
                        "img#house":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-14s"
                        },
                        "img#antenna":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-16s"
                        },
                        "img#baby":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-18s"
                        },
                        "img#bed":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-20s"
                        },
                        "img#camera":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-22s"
                        },
                        "img#cart":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-24s"
                        },
                        "img#champagne":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-26s"
                        },
                        "img#desktop":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-28s"
                        },
                        "img#diamond":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-30s"
                        },
                        "img#dinner":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-32s"
                        },
                        "img#dress":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-34s"
                        },
                        "img#drink":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-36s"
                        },
                        "img#drum-set":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-38s"
                        },
                        "img#electric-guitar":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-40s"
                        },
                        "img#flowers":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-42s"
                        },
                        "img#gamepad":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-44s"
                        },
                        "img#gift":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-46s"
                        },
                        "img#handshake":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-48s"
                        },
                        "img#hdd":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-50s"
                        },
                        "img#headphone":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-52s"
                        },
                        "img#laptop":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-54s"
                        },
                        "img#light":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-56s"
                        },
                        "img#mortarboard":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay5-58s"
                        },
                        "img#pendrive":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6s"
                        },
                        "img#phone-book":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-2s"
                        },
                        "img#pizza":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-4s"
                        },
                        "img#placeholder":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-6s"
                        },
                        "img#ram":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-8s"
                        },
                        "img#report":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-10s"
                        },
                        "img#shirt":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-12s"
                        },
                        "img#shoes":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-14s"
                        },
                        "img#washing":{
                            show      : "fadeIn",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay6-16s"
                        }
                    },
                    1   : //Slide No2
                    {  
                        "img#luk_future"  :
                        {
                            show      : "fadeInUp",
                            hide      : "fadeOutDownBig",
                            delayShow : "delay0-2s"
                        },
                        "img#meeting_img"  :
                        {
                            show      : "fadeInUp",
                            hide      : "fadeOutDownBig",
                            delayShow : "delay3s"
                        },
                        "img#barchart"  :
                        {
                            show      : "fadeInUp",
                            hide      : "fadeOutDownBig",
                            delayShow : "delay5-5s"
                        },
                        "#todo":
                        {
                            show        : "fadeIn",
                            hide        : "fadeOut",
                            delayShow   : "delay1-5s"
                        },
                        "#bounce"   :
                        {
                            show        : "bounceIn",
                            hide        : "bounceOut",
                            delayShow   : "delay2s"
                        },
                        "#bounceUp":
                        {
                            show        : "bounceInDown",
                            hide        : "bounceOutLeft",
                            delayShow   : "delay2-5s"
                        },
                        "#bounceRight":
                        {
                            show        : "bounceInRight",
                            hide        : "bounceOutRight",
                            delayShow   : "delay3s"
                        },
                        "#fade" :
                        {
                            show        : "fadeInLeft",
                            hide        : "fadeOutLeft",
                            delayShow   : "delay3-5s"
                        },
                        "#fadeUp":
                        {
                            show        : "fadeInUpBig",
                            hide        : "fadeOutUpBig",
                            delayShow   : "delay4s"
                        },
                        "#fadeDown":
                        {
                            show        : "fadeInDownBig",
                            hide        : "fadeOutDownBig",
                            delayShow   : "delay4-5s"   
                        },
                        "#rotate" :
                        {
                            show        : "rotateIn",
                            hide        : "rotateOut",
                            delayShow   : "delay5-5s"
                        },
                        "#rotateRight" :
                        {
                            show        : "rotateInUpRight",
                            hide        : "rotateOutDownRight",
                            delayShow   : "delay6s"
                        },
                        "#rotateLeft" :
                        {
                            show        : "rotateInUpLeft",
                            hide        : "rotateOutDownLeft",
                            delayShow   : "delay6-5s"
                        }
                    },
                    2   : //Slide No3
                    {
                        "img#css3"  :
                        {
                            show      : "fadeInUp",
                            hide      : "fadeOutDownBig",
                            delayShow : "delay2s"
                        },
                        "img#html5" :
                        {
                            show      : "fadeInUpBig",
                            hide      : "fadeOutUpBig",
                            delayShow : "delay4-5s"
                        },
                        "img#jquery"  :
                        {
                            show      : "bounceIn",
                            hide      : "flipOutY",
                            delayShow : "delay1-5s"
                        },
                        "img#modernizr" :
                        {
                            show      : "rollIn",
                            hide      : "flipOutY",
                            delayShow : "delay2s"
                        },
                        "#animatecss"   : 
                        {
                            show      : "lightSpeedIn",
                            hide      : "flipOutY",
                            delayShow : "delay2-5s"
                        }
                    }
                }
            });