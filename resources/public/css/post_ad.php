<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Post Your Ad</title>
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/imageup/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/imageup/jquery.fileupload-ui.css">
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/imageup/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/imageup/jquery.fileupload-ui-noscript.css"></noscript>

<style type="text/css">
    body{
    	background-color: #E9EBEE;
    }
    #cover-con{
        min-height: 260px;
        background-image: url("<?php echo base_url(); ?>assets/img/adpost/mban.jpg");
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    #adv-post{
        margin-top: 15px;
        min-height: 300px;
        background-color: #FFFFFF;
    }
    #adv-post h2{
        margin-top: 15px;
        padding-bottom: 10px;
        color: rgba(0,0,0,.6);
        text-align: center;
    }
    #its-free{
        text-align: center;
    }
    #free-content{
        margin:0px auto;
        width: 40%;        
    }
    #shout-img{
        width: 70px;
        float: left;
    }
    #f-t-p{
        font-size: 22px;
        font-weight: bold;
        color:  #2980b9;
    }
    #i-w-e{
        font-size: 18px;
        font-weight: bold;
        color:  #e74c3c;
    }
    #looking{
        border: 1px solid #95CC6B;
    }
    #offer, #looking{
        min-height: 300px;
        border-radius: 3px;
    }
    /* temp */
    #offer-form{
        display: block !important;
    }
    /**/
    #offer-form, #looking-form{
        margin-top: 15px;
        padding: 0px 15px;
        display: none;
    }
    .t-content{
      margin-bottom: 5px;
      height: 55px;
    }
    .f-border{        
        border: 1px solid;
        border-color: #e5e6e9 #dfe0e4 #d0d1d5;
        border-radius: 3px;
    }
    .t-select{
        padding: 15px;
        width: 100%;
        display: inline-block;
        background-color: #F6F7F9;
        font-size: 20px;
        font-weight: bolder;
        text-align: center;
        cursor: pointer;
    }
    .t-select:hover{
        background-color: #2BA8E3;
        color: #FFFFFF;
    }
    .colour_purple{
        color: #9E5BA1;
    }
    .colour_green{
        color: #95CC6B;
    }

    /*********************** over rides **********************/
    .inputfile {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
    }
    .ui.icon.buttons > button {
      margin: 2px 1px;
      padding: 10px 22px !important;
    }
    #offer-form .ui.header {
      color:  #d35400;
      margin-bottom: 20px;
      border-bottom: 1px solid #e59866;
    }
    #offer-form .ui.form .field > label , #offer-form .ui.form .inline.fields > label {
      width: 130px;
      font-size: 0.9rem;
    }
    #offer-form .ui.selection.dropdown, #offer-form .ui.form input[type="text"], #offer-form .ui.form textarea {
      border: 1px solid #85c1e9;
    }
    #offer-form .ui.selection.dropdown:focus, #offer-form .ui.form input[type="text"]:focus, #offer-form .ui.form textarea:focus {
      border: 1px solid #28b463;
    }
    .ad_sel_bx{
      border: 1px solid #efefef;
      height: 100px;
      text-align: center;
    }
    .ad_sel_bx:hover{
      background-color: #f2f3f4;
      box-shadow: 0px 1px 1px #999;
    }
    .bx_sdow{
      box-shadow: 0px 4px 5px #999;      
    }
    .bx_clicked{
      background-color: #f2f3f4;
     box-shadow: 0px 1px 1px #999;
    }
    .ad_sel_bx_con{
      padding: 0.2em;
    }
    .icon_con{
      margin: 5px auto;
      width: 68px;
      height: 65px;
    }
</style>
</head>
<body>
<main id="main-con">
    <!-- top bar navigation goes here -->
    <?php $this->load->view('includes/top_nav'); ?>
    <header id="cover-con" class="container-fluid">
        
    </header>
    <div id="adpost-con" class="container-fluid">
        <div id="adv-post" class="container f-border">
            <h2>Create your Advertisement</h2>
                <div class="ui divider"></div>
            <div class="row">
                <div class="t-content">
                  <div class="col-md-6">
                      <div id="offer-btn" class="t-select colour_purple f-border">
                          <span class="">Offer Something</span>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div id="looking-btn" class="t-select colour_green f-border">
                          <span class="">Looking for Something</span>
                      </div>
                  </div>
                </div>
                <div id="ad_bx_con" class="col-md-12">
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_1"></span></div>
                      Animals &amp; Pet Supplies
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_2"></span></div>
                      Antiques
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_3"></span></div>
                      Apparel &amp; Accessories
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_4"></span></div>
                      Arts &amp; Hobbies
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_clicked">
                      <div class="icon_con"><span class="itm_5"></span></div>
                      Business &amp; Industrial
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_6"></span></div>
                      Cameras &amp; Optics
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_7"></span></div>
                      Crafts, Paintings &amp; Pottery
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_8"></span></div>
                      Education
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_9"></span></div>
                      Electronics
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_10"></span></div>
                      Entertainment
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_11"></span></div>
                      Finance
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_12"></span></div>
                      Flowers &amp; Gift
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_13"></span></div>
                      Food &amp; Beverages
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_14"></span></div>
                      Furniture
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_15"></span></div>
                      Hardware
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_16"></span></div>
                      Health &amp; Beauty
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_17"></span></div>
                      Home &amp; Garden
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_18"></span></div>
                      Hotels &amp; Travel
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_19"></span></div>
                      Jobs
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_20"></span></div>
                      Luggage &amp; Bags
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_21"></span></div>
                      Media
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_22"></span></div>
                      Mother &amp; Kids
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_23"></span></div>
                      Novelty &amp; Special Use
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_24"></span></div>
                      Office &amp; School Supplies
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_25"></span></div>
                      Properties
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_26"></span></div>
                      Religious &amp; Ceremonial
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_27"></span></div>
                      Security &amp; Protection
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_28"></span></div>
                      Services
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_29"></span></div>
                      Software
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_30"></span></div>
                      Sporting Goods
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_31"></span></div>
                      Toys &amp; Games
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_32"></span></div>
                      Vehicles &amp; Parts
                    </div></a>
                  </div>
                  <div class="col-md-2 ad_sel_bx_con">
                    <a href=""><div class="ad_sel_bx bx_sdow">
                      <div class="icon_con"><span class="itm_33"></span></div>
                      Weddings &amp; Events
                    </div></a>
                  </div>
                      
                </div>
            </div>
            <div id="form-container" class="row">
                <div id="offer-form" class="col-md-8">
                    <form id="offer" class="ui form col-md-12">
                        <h4 class="ui dividing header">Details about your advertisement</h4>
                          <div class="inline fields">
                            <label>Select the type</label>
                            <div class="field">
                              <div class="ui checkbox">
                                <input id="for-sale" class="hidden" ame="for-sale" type="checkbox">
                                <label for="for-sale">For Sale</label>
                              </div>
                            </div>
                            <div class="field">
                              <div class="ui checkbox">
                                <input id="for-rent" class="hidden" name="for-rent" type="checkbox">
                                <label for="for-rent">For Rent</label>
                              </div>
                            </div>
                          </div>

                          <div class="field">
                            <label>Select a Category</label>
                                <select class="ui fluid dropdown" id="cat-select">
                                  <option value="">Category</option>
                                  <option value="1">Cat 1</option>
                                  <option value="2">Cat 2</option>
                                  <option value="3">Cat 3</option>
                                  <option value="4">Cat 4</option>
                                </select>
                          </div>
                          <div class="field">
                            <label>Select Sub Category</label>
                            <select class="ui fluid dropdown" id="sub-cat-select">
                              <option value="">Sub Category</option>
                              <option value="1">Sub Cat 1</option>
                              <option value="2">Sub Cat 2</option>
                              <option value="3">Sub Cat 3</option>
                              <option value="4">Sub Cat 4</option>
                            </select>
                          </div>

                          <div class="inline fields">
                            <label>Condition</label>
                            <select class="ui fluid dropdown" id="condition">
                              <option value="">Condition</option>
                              <option value="1">Used</option>
                              <option value="2">New</option>
                            </select>
                          </div>

                          <div class="field">
                            <label>Item Type</label>
                            <select class="ui fluid dropdown" id="item-type">
                              <option value="">Item Type</option>
                              <option value="1">Type 1</option>
                              <option value="2">Type 2</option>
                            </select>
                          </div>
                          <div class="field">
                            <label>Title of your Ad</label>
                            <div class="ui input">
                              <input name="ad-title" id="ad-title" placeholder="Ad Title" type="text">
                            </div>
                          </div>
                          <div class="field">
                            <label>Description</label>
                            <textarea></textarea>
                          </div>
                          <div class="inline field">
                            <label>Price</label>
                            <div class="ui Left labeled input">                                 
                              <div class="ui teal label">Rs.</div>
                              <input placeholder="Price" type="text">
                            </div>
                           </div>

                           <div class="ui horizontal segments">
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-1" id="file-1" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-2" id="file-2" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-3" id="file-3" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-4" id="file-4" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-5" id="file-5" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                          </div>
                          <div class="ui horizontal segments">
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-1" id="file-1" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-2" id="file-2" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-3" id="file-3" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-4" id="file-4" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="ui segment">
                              <img class="ui small image" src="<?php echo base_url(); ?>assets/img/image.png">
                              <div class="ui  icon buttons">
                                <button class="ui green button img-btn s-btn"><i class="fa fa-picture-o"></i></button>
                                <input name="file-5" id="file-5" type="file" class="inputfile">
                                <button class="ui disabled button img-btn r-btn"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                          </div>
                          <FIELDSET>
                            <LEGEND><b>Vehicles</b></LEGEND>
                            <div class="fields">
                            </div>
                            <div class="two fields">
                            <div class="field">
                                <label>Brand</label>
                                <select class="ui fluid dropdown" id="vehi-brand">
                                  <option value="">Brand *</option>
                                  <option value="alfa-romeo">Alfa Romeo</option>
                                  <option value="aston-martin">Aston Martin</option>
                                  <option value="audi">Audi</option>
                                  <option value="austin">Austin</option>
                                  <option value="bmw">BMW</option>
                                  <option value="buick">Buick</option>
                                  <option value="cadillac">Cadillac</option>
                                  <option value="changan">Changan</option>
                                  <option value="chery">Chery</option>
                                  <option value="chevrolet">Chevrolet</option>
                                  <option value="chrysler">Chrysler</option>
                                  <option value="citroen">Citroën</option>
                                  <option value="daewoo">Daewoo</option>
                                  <option value="daihatsu">Daihatsu</option>
                                  <option value="datsun">Datsun</option>
                                  <option value="dodge">Dodge</option>
                                  <option value="ferrari">Ferrari</option>
                                  <option value="fiat">Fiat</option>
                                  <option value="ford">Ford</option>
                                  <option value="geely">Geely</option>
                                  <option value="gmc">GMC</option>
                                  <option value="hino">Hino</option>
                                  <option value="honda">Honda</option>
                                  <option value="hummer">Hummer</option>
                                  <option value="hyundai">Hyundai</option>
                                  <option value="isuzu">Isuzu</option>
                                  <option value="jaguar">Jaguar</option>
                                  <option value="jeep">Jeep</option>
                                  <option value="kia">Kia</option>
                                  <option value="lamborghini">Lamborghini</option>
                                  <option value="land-rover">Land Rover</option>
                                  <option value="lexus">Lexus</option>
                                  <option value="lincoln">Lincoln</option>
                                  <option value="mahindra">Mahindra</option>
                                  <option value="maruti">Maruti</option>
                                  <option value="mazda">Mazda</option>
                                  <option value="mercedes-benz">Mercedes-Benz</option>
                                  <option value="mg">MG</option>
                                  <option value="micro">Micro</option>
                                  <option value="mini">Mini</option>
                                  <option value="mitsubishi">Mitsubishi</option>
                                  <option value="morris">Morris</option>
                                  <option value="moto-guzzi">Moto Guzzi</option>
                                  <option value="nissan">Nissan</option>
                                  <option value="oldsmobile">Oldsmobile</option>
                                  <option value="opel">Opel</option>
                                  <option value="perodua">Perodua</option>
                                  <option value="peugeot">Peugeot</option>
                                  <option value="plymoth">Plymouth</option>
                                  <option value="pontiac">Pontiac</option>
                                  <option value="porsche">Porsche</option>
                                  <option value="proton">Proton</option>
                                  <option value="renault">Renault</option>
                                  <option value="rover">Rover</option>
                                  <option value="royal-enfield">Royal Enfield</option>
                                  <option value="saab">SAAB</option>
                                  <option value="scion">Scion</option>
                                  <option value="seat">SEAT</option>
                                  <option value="skoda">Skoda</option>
                                  <option value="smart">Smart</option>
                                  <option value="ssang-yong">Ssang Yong</option>
                                  <option value="subaru">Subaru</option>
                                  <option value="suzuki">Suzuki</option>
                                  <option value="tata">Tata</option>
                                  <option value="toyota">Toyota</option>
                                  <option value="vauxhall">Vauxhall</option>
                                  <option value="volkswagen">Volkswagen</option>
                                  <option value="volvo">Volvo</option>
                                  <option value="zoyte">Zotye</option>
                                  <option value="other">Other brand</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Model</label>
                                <select class="ui fluid dropdown" id="vehi-model">
                                  <option value="">Select Model</option>
                                  <option value="1">Kadawatha</option>
                                  <option value="2">Mahara</option>
                                  <option value="3">Kiribathgoda</option>
                                  <option value="4">Ganemulla</option>
                                </select>
                            </div>
                          </div>
                          </FIELDSET>

                          <h4 class="ui dividing header">Ad Location</h4>
                          <div class="two fields">
                            <div class="field">
                                <label>District</label>
                                <select class="ui fluid dropdown" id="ad-district">
                                  <option value="">Select District</option>
                                  <option value="1">Colombo</option>
                                  <option value="2">Kaluthara</option>
                                  <option value="3">Gampaha</option>
                                  <option value="4">Galle</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>City</label>
                                <select class="ui fluid dropdown" id="ad-city">
                                  <option value="">Select City</option>
                                  <option value="1">Kadawatha</option>
                                  <option value="2">Mahara</option>
                                  <option value="3">Kiribathgoda</option>
                                  <option value="4">Ganemulla</option>
                                </select>
                            </div>
                          </div>
                          
                          <div class="ui button" tabindex="0">Post Ad</div>

                    </form>
                </div>
                <div id="looking-form" class="col-md-12">
                    <form id="looking" class="col-md-12">
                        
                    </form>
                </div>
            </div>
            <div class="ui divider"></div>
            <div id="its-free">
                <div id="free-content" class="ui compact segment">
                  <img id="shout-img" src="<?php echo base_url(); ?>assets/img/adpost/shout.gif">
                  <p id="f-t-p">It's free to post ads on Bestway.lk</p>
                  <p id="i-w-e">It will ever!</p>
                </div>
            </div>
        </div>
    </div>
    
</main>
<?php $this->load->view('includes/footer'); ?>

    <script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/semantic.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.js" type="text/javascript"></script>

        
    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
        })
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            $('.fancybox').fancybox();

                /*
                 *  Different effects
                 */

            $('#offer-btn').click(function(){
                $( "#offer-form" ).slideDown( "slow" );
                $( "#looking-form" ).slideUp( "slow" );
            });
            $('#looking-btn').click(function(){
                $( "#looking-form" ).slideDown( "slow" );
                $( "#offer-form" ).slideUp( "slow" );
            });

            $('#cat-select').dropdown();
            $('#sub-cat-select').dropdown();
            $('#condition').dropdown();
            $('#item-type').dropdown();
            $('#c-type').dropdown();
            $('#ad-district').dropdown();
            $('#ad-city').dropdown();

            //$('.button').popup();

        });

        $(".img-btn").click(function(e){
          e.preventDefault();
        });

        $(".s-btn").click(function(){
          //var elid = 
              $(this).siblings("input").click().next('button').removeClass('disabled').addClass('red');
          //alert(elid);
        });

        // function uploadImage($img){
        //   var formdata = new FormData($img);
        //   var request = new XMLHttpRequest();
        //   request.open('post', '<?php echo base_url(); ?>imageupload');
        //   request.send(formdata);
        // }
    </script>

</body>
</html>