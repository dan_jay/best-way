<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bestway | My Store</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/semantic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body{
			background-color: #F9F9F9;
		}
		#top-con, #main{
			padding: 0px;
		}
		#pageCover{
			position: relative;
		}
		#pageCover #cover-pic{
			width: 100%;
		}
		#pro-pic{
			position: absolute;
			bottom: 16px;
			left: 16px;
			background: rgba(0, 0, 0, .3);
			border-radius: 4px;
			box-shadow: -2px 4px 2px rgba(0, 0, 0, .07);
		}
		#pro-img{
			border: 2px solid #FFFFFF;
			border-radius: 4px;
			width: 150px;
		}
		 /*#pro-pic{
		    position: relative;
		  }*/
		  #pro-pic:hover #pro_pic_changer{
		    opacity: 1;
		  }
		  #pro_pic_changer{
			opacity: 0;
			z-index: 35;
			transition: opacity .13s ease-out;
			background-color: rgba(0,0,0,0.8);
			color: #FFFFFF;
			padding: 20px 20px;
			position: absolute;
			top: 90px;
			text-align: center;
			overflow: hidden;
			width: 100%;
		  }
		#cover-func{
			height: 50px;
			background-color: #FFF;
			border-radius: 0px 0px 5px 5px;
			border: 1px solid #d3e0e9;
		}
		#page-links{
			padding-left: 145px;
		}
		#page-links ul{
			list-style: none;
		}
		#page-links ul li{
			float: left;
			padding: 1px 15px 5px;
			border-right: 1px solid #d3e0e9;
		}
		#page-links ul li:last-child{
			border-right: none;
		}
		#pro-name{
			position: absolute;
			color: #FFFFFF;
			bottom: 60px;
			left: 200px;
			font-weight: bold;
			text-shadow: -1px 2px 0px #000000;
		}
		#pro-name h1{
			font-size: 26px;
		}
		#pro-name h2{
			font-size: 18px;
		}
		#map {
	        width: 100%;
	        height: 300px;
      	}
      	#pageInfo{
      		margin-top: 15px;
      	}
      	#pageImgPan img{
      		margin: 2px 0px;
      		border: 2px solid #d3e0e9;
      		width: 128px;
      	}
      	#pageImgPan img:nth-child(odd){
      		margin-left: 4px;
      	}

		/* ads style start */
		#class-con{
			border-radius: 4px; 
			background-color: #ffffff;
			border: 1px solid #d3e0e9;
		}
		#ad-con ul { 
			list-style: none;
			padding: 0px;
		}
		#ad-con .buttons { 
			margin-bottom: 20px; 
		}
		#ad-con .list li { 
			width: 100%; 
			border: 1px solid #d3e0e9;
			border-right: none;
			border-left: none; 
			margin-bottom: 10px; 
			padding-bottom: 10px; 
		}
		#ad-con .grid li { 
			float: left;
			border: 1px solid #CCC;
		}
		#ad-list-con{
			overflow: auto;
			margin: 0px;
		}

		#points{
			background-color: #EC6F5A;
		}
		#badges{
			background-color: #55BADF;
		}
		.wd_box{
			width: 100%;
			height: 75px;
			padding: 10px;
			margin-bottom: 15px;
			color: #FFFFFF;
		}
		.grid .ad{
			width: 280px;
			height: auto;
			margin: 0px 0px 5px 3px;
		}
		.ad .itm_img{
			border: 1px solid #d3e0e9;
		}
		.grid .adprice, .list .adprice{
			font-size: 1.7em;
		    font-family: 'Roboto', sans-serif;
		    font-weight: 600;
		    color: #009432;
		    display: inline;
		}
		.grid .itm_img_con{
			margin-bottom: 5px;
		}
		.grid .ad_txt{
			margin-top: 2px;
			font-size: 12px;
		}
		.list .ad{
			height: auto;
		}
		.list img{
			width: 140px;
		}
		.list .itm_img_con{
			margin-right: 25px;
		}
		.list .itm_img_con{
			float: left;
		}
		.list .txt_con{
		}
		/* ads style end */
		.bdr{			
			border: 1px solid #d3e0e9;
			margin-bottom: 12px;
		}
		.panel-body{
			padding: 0px;
		}
		.panel-heading {
		    background-color: #363D40;
		    color: #EEEEEE;
		    text-align: center;
		    border-radius: 0px;
		}
		.panel-body p{
			padding-left: 10px;
		}
	</style>
</head>
<body>
<div id="main-con">
	<!-- top bar navigation goes here -->

	<?php $this->load->view('includes/top_nav'); ?>

	<div id="top-con" class="container">
		<?php $this->load->view('includes/promo_bar'); ?>

	    <!-- <div class="global-page">
	        <?php $this->load->view('includes/main_search'); ?>
		</div> -->
	</div>

	<article id="main" class="container">
		<div class="row">
			<div id="page" class="col-md-9">
				<div id="pageCoverCon">
					<div id="pageCover">
						<a href="#"><img id="cover-pic" src="<?php echo base_url(); ?>assets/img/nature-path-facebook-cover-photo.jpg"></a>
						<div id="pro-pic">
							<a href="#"><img id="pro-img" src="<?php echo base_url(); ?>assets/img/propic.png"></a>
							<div id="pro_pic_changer">
				              <i class="fa fa-camera"></i> Change Image
				            </div>
						</div>
						<div id="pro-name">
							<h1>Comworld Computer Zone</h1>
							<h2>Branded Computers & Laptops Accessories</h2>
						</div>
						<div id="cover-func">
							<div id="page-links">
								<ul>
									<li><a href="#">Page</a></li>
									<li><a href="#">Photos</a></li>
									<li><a href="#">Events</a></li>
									<li><a href="#">Offers</a></li>
									<li><a href="#">Edit</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div id="pageInfo" class="row">
					<aside id="infoCon" class="col-md-4">
						<div id="about-page" class="bdr">
							<div class="panel-heading">
	                            <h3 class="panel-title">About</h3>
	                        </div>
							<div class="panel-body">
								<p>630/3/B Kandy Road, Bandarawaththa, Kadawata.</p>
								<p>071 768 4399</p>
								<p>www.comworld.lk</p>
							</div>
					    </div>

					    <div class="bdr">
							<div class="panel-heading">
	                            <h3 class="panel-title">Open Hours</h3>
	                        </div>
							<div class="panel-body">
								<p><span>Mon :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
								<p><span>Tue :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
								<p><span>Wed :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
								<p><span>Thu :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
								<p><span>Fri :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
								<p><span>Sat :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
								<p><span>Sun :</span> <span>09:00 AM</span> <span>10:30 PM</span></p>
							</div>
					    </div>

					    <div id="locMap" class="bdr">
							<div class="panel-heading">
	                            <h3 class="panel-title">Location Map</h3>
	                        </div>
							<div class="panel-body">
								<div id="map"></div>
								<script>
							      function initMap() {
							        var mapDiv = document.getElementById('map');
							        var map = new google.maps.Map(mapDiv, {
							          center: {lat: 44.540, lng: -78.546},
							          zoom: 8
							        });
							      }
							    </script>
							</div>
					    </div>

					    <div id="pageImgPan" class="bdr">
							<div class="panel-heading">
	                            <h3 class="panel-title">Photos</h3>
	                        </div>
							<div class="panel-body">
								<img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
								<img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
								<img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
								<img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
								<img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
								<img src="<?php echo base_url(); ?>assets/img/demo3/eiffel_thumb.jpg" alt="eiffel">
							</div>
					    </div>

					</aside>
					<article id="recAds" class="col-md-8">
						<article id="class-con">
		            		<div id="ad-con">
							    <div class="buttons">
							        <span class="btn btn-info grid">Grid View</span>
							        <span class="btn btn-info list">List View</span>
							    </div>
							    <div id="ad-list-con" class="row">
								    <ul class="grid">
								        <li id="" class="ad col-md-6">
								        	<div class="itm_wrapper">
										        <h1><a href="#">Ad Name</a></h1>

						                        <h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
						                        <div class="itm_img_con">
						                        	<img src="<?php echo base_url(); ?>assets/img/2.jpg" class="itm_img">
						                        </div>
						                        <div class="txt_con">
							                        <p class="adprice">Rs. 12,000.00</p>
							                        <p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
						                        </div>
					                        </div>
								        </li>
								        <li id="" class="ad col-md-6">
									        <div class="itm_wrapper">
										        <h1><a href="#">Ad Name</a></h1>

						                        <h2><a href="#">Publisher Name. </a><span>Time (xx mins ago)</span></h2>
						                        <div class="itm_img_con">
						                        	<img src="<?php echo base_url(); ?>assets/img/2.jpg" class="itm_img">
						                        </div>
						                        <div class="txt_con">
							                        <p class="adprice">Rs. 12,000.00</p>
							                        <p class="ad_txt">New House Gardens is a secure gated community of 5 villas just 250 metres from Galle's best beach</p>
						                        </div>
						                    </div>
								        </li>
								    </ul>
							    </div>
							</div>  
	            		</article>
					</article>
				</div>
			</div>
			<div id="widgets" class="col-md-3">
				<div class="wd_box" id="points">
					<h3>Points</h3>
				</div>
				<div class="wd_box" id="badges">
					<h3>Badges</h3>
				</div>
			</div>
		</div>
	</article>
</div>
<!-- page footer goes here -->
<?php $this->load->view('includes/footer'); ?>

	<script src="<?php echo base_url(); ?>assets/js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/semantic.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/app/chart.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9UzumFWNCH2xhZZFqwTNJee0MgnXZzMI&callback=initMap"
  type="text/javascript"></script>

    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
          // execute/clear BS loaders for docs
        $(function(){
            if (window.BS&&window.BS.loader&&window.BS.loader.length) {
              while(BS.loader.length){(BS.loader.pop())()}
            }
	        $('#prof_pg').dropdown({
	            on: 'hover'
	        });
        });
        $('span').on('click',function(e) {
		    if ($(this).hasClass('grid')) {
		        $('#ad-list-con ul').removeClass('list').addClass('grid');
		    }
		    else if($(this).hasClass('list')) {
		        $('#ad-list-con ul').removeClass('grid').addClass('list');
		    }
		});
    </script>
</body>
</html>