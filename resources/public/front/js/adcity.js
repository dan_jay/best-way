/**
 * Created by Kavi on 4/30/2016.
 */
(function ($) {
    'use strict';
    $('.item').on("click", function () {
        $(this).next().slideToggle(100);
        $('p').not($(this).next()).slideUp('fast');
    });
}(jQuery));