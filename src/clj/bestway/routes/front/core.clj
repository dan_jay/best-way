(ns bestway.routes.front.core
  (:require [bestway.layout :refer [*identity* *app-context* *listing-pic-upload-path* *profile-pic-upload-path*]]
            [compojure.core :refer [defroutes routes GET POST context]]
            [ring.util.http-response :refer [ok]]
            [ring.util.response :refer [redirect response]]
            [ring.util.request]
            [clojure.data.json :as json]
            [ring.middleware.session :as session]
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [buddy.auth.accessrules :refer [restrict]]
            [cognitect.transit :as t]
            [postal.core :as mail]
            [clojure.java.io :as io]
            [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit :refer (get-sch-adapter)]
            [org.httpkit.server :refer [with-channel on-receive on-close send!]]
            [cognitect.transit :as t]
            [clojure.core.async :refer [<! >! put! close! go] :as a]
            [clojure.tools.logging :as log]
            [ring.util.response :as response]
            [cognitect.transit :as transit]
            [bestway.layout :as layout])
  (:import (org.bson.types ObjectId)
           (org.httpkit BytesInputStream)
           (java.io ByteArrayInputStream)))


(use 'ring.middleware.anti-forgery)
(use 'ring.util.anti-forgery)
(use 'selmer.parser)
(use 'bestway.util)
(use 'bestway.db.core)
(use 'bestway.db.get)
(use 'bestway.db.todo)
(use 'bestway.db.set)
(use 'ring.util.response)
(use 'bestway.app.front.core)
(use 'bestway.app.front.chat)


;;=============================================================================;;
;;========================Main Routes==========================================;;
;;=============================================================================;;

(defroutes front-routes

           ;(GET "/post-ad/sub-categories/:cat" [cat] (fn [request]
           ;
           ;                                            (compojure.response/render
           ;                                              {
           ;                                               :status  200
           ;                                               :headers {"Content-Type" "application/transit+json"}
           ;                                               :body    (tr-write (:children (subcat-list cat)))
           ;                                               }
           ;                                              request)
           ;                                            ))
           ;
           ;(GET "/post-ad/category-input-details/:subcat" [subcat] (fn [request]
           ;                                                          (compojure.response/render
           ;                                                            {
           ;                                                             :status 200
           ;                                                             ;:headers { "Location" (str layout/*app-context* "my-profile") }
           ;                                                             :body   (subcat-list cat)
           ;                                                             }
           ;                                                            request)
           ;                                                          ))
           (GET "/buy" [] (fn [request]
                            (dashboard request "front/post_ad_buy.php" {
                                                                        :path-uri   (find-key request :uri)
                                                                        :identity   (find-key request :identity)
                                                                        :categories (category-list)
                                                                        :ads        (listing-ads)
                                                                        :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                                        :subcatz    (for [s (seq (catz-list))]
                                                                                      ;(if (= (:_id s)  cat_id)
                                                                                      (map #(-> (str "<option style='display: none;' data-parent-id='" (:id s) "'  value='" (:_id %1) "'>" (:label %1) "</option>")) (:children s)))
                                                                                      ;)
                                                                        :ad-type "buy"
                                                                        :locations  (venues-list)


                                                                        :fnamelname (find-key request :fnamelname)
                                                                        :user       (find-key request :user)})))

           (GET "/sell" [] (fn [request]
                             (dashboard request "front/post_ad_sell.php" {
                                                                          :path-uri   (find-key request :uri)
                                                                          :identity   (find-key request :identity)
                                                                          :categories (category-list)
                                                                          :ads        (listing-ads)
                                                                          :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                                          :subcatz    (for [s (seq (catz-list))]
                                                                                        ;(if (= (:_id s)  cat_id)
                                                                                        (map #(-> (str "<option style='display: none;' data-parent-id='" (:id s) "'  value='" (:_id %1) "'>" (:label %1) "</option>")) (:children s)))
                                                                                        ;)

                                                                          :locations  (venue-list)
                                                                          :locs       (location-dropdown)
                                                                          :ad-type "sell"

                                                                          :fnamelname (find-key request :fnamelname)
                                                                          :user       (find-key request :user)})))


           (POST "/post/remove/:id" [id] (fn [request]
                                           (if-not (nil? layout/*identity*)
                                             (do
                                                    (delete-ad id)
                                                    (compojure.response/render
                                                      (make-json-response 200 {:type "success" :msg "Item Deleted"})
                                                      request)
                                             ))))

           (POST "/post/publish/:id" [id] (fn [request]
                                           (if-not (nil? layout/*identity*)
                                             (let [publish? (find-key request :publish)
                                                   msg (if (= publish? "on") "Your post submitted for review!" "Item is un published")
                                                   ]
                                               (publish-ad id publish?)
                                               (compojure.response/render
                                                  (make-json-response 200 {:publish (= publish? "on") :msg msg})
                                                 request)
                                               )

                                             )))

           (GET "/post/edit/:id" [id] (fn [request]
                                        (def item (listings-item-details id))
                             (dashboard request "front/post_ad_edit.php" {
                                                                          :item      item
                                                                          :path-uri   (find-key request :uri)
                                                                          :identity   (find-key request :identity)
                                                                          :categories (category-list)
                                                                          :ads        (listing-ads)
                                                                          :catz       (map #(-> (str "<option  value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                                          :subcatz    (for [s (seq (catz-list))]
                                                                                        ;(if (= (:_id s)  cat_id)
                                                                                        (map #(-> (str "<option style='display: none;' data-parent-id='" (:id s) "'  value='" (:_id %1) "'>" (:label %1) "</option>")) (:children s)))
                                                                          ;)

                                                                          :locations  (venue-list)
                                                                          :locs       (location-dropdown item)

                                                                          :fnamelname (find-key request :fnamelname)
                                                                          :user       (find-key request :user)})))


           (POST "/post/send-message/:item" [item] (fn [request]
                                                (send-msg-to-publisher item request)
                                               ; (send-email-to-publisher item request)
                                                (compojure.response/render (make-json-response 200 {:msg "Your message is delivered." :type "success"}) request)

                                                ))


           (POST "/post-your-ad" [] (fn [request]
                                      ;(println request)

                                      (if (= (find-key request :ad-title) "")
                                        (redirect (:referer redirect))


                                        (let [item    (save-ad request)
                                              item-id (:_id item)
                                              ]
                                          (if item-id
                                            ;(redirect  (str *app-context* "item/" item-id))
                                            (redirect (:url item))
                                            (str *app-context* "/sell")))

                                        )
                                      )
            )




           (GET "/home" [] (fn [request]
                             ;(println request)
                             (dashboard request "front/home.php" {
                                                                  :home true
                                                                  :identity   (find-key request :identity)
                                                                  :categories (catz-list)
                                                                  :ads        (listing-ads)
                                                                  :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                                  :locations  (venues-list)
                                                                  :locs       (location-dropdown)
                                                                  :fnamelname (find-key request :fnamelname)
                                                                  :user       (find-key request :user)})))






           ;(GET "/item/:id" [id] (fn [request] (dashboard request "front/item.php"
           ;                                               {;:categories  ( category-list  )
           ;                                                :item      (listings-item-details id)
           ;                                                :catz      (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))
           ;
           ;                                                :locations (venues-list)
           ;                                                :locs       (location-dropdown)
           ;                                                }
           ;                                               )))

           (GET "/category" [] (fn [request] (dashboard request "front/category.php" {})))


           (GET "/my-profile" [] (fn [request]
                                   ;(println request)
                                   (my-profile request)))


           ;(GET "/user/:id" [id] (fn [request]
           ;                        (log/debug (str (find-key request :identity)))
           ;                        (if (= (str (find-key request :identity)) id)
           ;                          (compojure.response/render
           ;                            {
           ;                             :status  301
           ;                             :headers {"Location" (str layout/*app-context* "my-profile")}
           ;                             }
           ;                            request)
           ;                          (user-profile request id)
           ;                          )
           ;
           ;                        ))
           ;(GET "/ws" []
           ;     #(log/info "Submission!")
           ;  )
           (GET "/account-settings" [] (fn [request] (dashboard request "front/settings.php" {
                                                                                              :identity   (find-key request :identity)
                                                                                              ;:categories  (first ( category-list ) )
                                                                                              ;:ads  ( listing-ads )
                                                                                              ;:locations  (first ( venues-list ) )
                                                                                              :userdata   (find-key request :user)
                                                                                              :fname      (find-key request :fname)
                                                                                              :lname      (find-key request :lname)
                                                                                              :fnamelname (find-key request :fnamelname)
                                                                                              :user       (find-key request :user)})))

           (POST "/account-settings" []

                                       (fn [request]

                                         (compojure.response/render
                                           {

                                            :status  200
                                            :headers {"Content-Type" "application/transit+json"}
                                            :body    (tr-write (dissoc (get-user-settings (str *identity*)) :_id ))
                                            }
                                             request
                                           )
                                         )
                                     )

           (POST "/save-settings" [] (fn [request]
                                       ; (println request)
                                       (make-json-response 200 (save-user-settings request)

                                                      )))

           (POST "/listing-comments/:id" [listing-id]

             (fn [request]
               (compojure.response/render
                 {

                  :status  200
                  :headers {"Content-Type" "application/transit+json"}
                  :body    (tr-write (dissoc (get-listing-comments listing-id) :_id :id))
                  }

                 request))
             )

           (POST "/add-listing-comments" [] (fn [request]
                                       ; (println request)
                                       (make-response 200 (save-listing-comments request)

                                                      )))



           ;;;; Profile pic upload
           ;(GET "/my-profile" [] (fn [request]  (dashboard request "front/myprofile.php" {
           ;                                                                               :identity (find-key request :identity )
           ;                                                                               :categories  (first ( category-list ) )
           ;                                                                               :ads  ( listing-ads )
           ;                                                                               :locations  (first ( venues-list ) )
           ;                                                                               :fnamelname (find-key request :fnamelname)
           ;                                                                               }
           ;                                                ) ) )




           (GET "/my-page" [] (fn [request]

                                (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                                (dashboard request "front/mystore.php"
                                           {
                                            :identity   (find-key request :identity)
                                            :categories (category-list)
                                            :ads        (listing-ads)
                                            :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                            :locations  (venues-list)
                                            :locs       (location-dropdown)
                                            :fnamelname (find-key request :fnamelname)
                                            :user       (find-key request :user)})))



           ;(GET "/search-results" [] (fn [request]
           ;
           ;                            (add-tag! :csrftok  (fn [_ _]  (anti-forgery-field) ))
           ;                            (dashboard request "front/adsearch.php"
           ;                                       {
           ;                                        :identity (find-key request :identity )
           ;                                        :categories   ( category-list )
           ;                                        :ads  ( listing-ads )
           ;                                        :locations  ( venues-list )
           ;                                        :locs       (location-dropdown)
           ;                                        :fnamelname (find-key request :fnamelname)
           ;                                        }
           ;                                       ) ) )
           (POST "/upload-profile-pics" [] (fn [request]

                                             (let
                                               [
                                               ; result (handle-upload request)
                                                result (handle-upload (find-key request :upload-file) (find-key request :_id))
                                                status (:status result)
                                                uploaded? (= (:status result) "OK")
                                                full-path-filename-ext (:filename result)
                                                file-name (file-name full-path-filename-ext)
                                                file-path (if uploaded?

                                                            (str "user/" file-name)


                                                            "user/default-pic.png")


                                                ;moved? (move-file full-path-filename-ext (str *profile-pic-upload-path* file-name))
                                                ]

                                               (try
                                                 (img-to-jpeg full-path-filename-ext (str *profile-pic-upload-path* (find-key request :_id) ".jpg") )
                                                 (catch Exception e (log/error "file hasn't moved")))

                                               ;(update-user-prof-pic
                                               ;  (ObjectId. (str (find-key request :_id))) file-path)
                                               ;
                                               ;(update-user-session-data :profile-pic file-path request)

                                               ;(make-response 200 result)
                                               (make-response 200 {:new-prof-pic (str *app-context* "user/" (find-key request :_id) ".jpg?n=" (rand-int 1) ) :status uploaded?}))))




           ;(POST "/upload-ad-pics-wm" [] (fn [request]
           ;                                ;(log/debug (second (first (find-key request :params))))
           ;                                (let
           ;                                  [
           ;
           ;                                   file-param (find-key request :params)
           ;                                   idx (find-key request :index)
           ;                                   upload-results
           ;                                   (handle-upload
           ;                                     (conj
           ;                                       ;;file-param
           ;                                       {:upload-file (second (first (find-key request :params)))}
           ;                                       request)
           ;                                     ;"ad-pics"
           ;                                     )
           ;                                   uploaded? (= (:status upload-results) "OK")
           ;                                   uploaded-file (:filename upload-results)
           ;                                   ]
           ;                                  ;(log/info upload-results)
           ;                                  (add-watermark! (str layout/*app-base-location* "resources/public/ad/") (str layout/*app-base-location* uploaded-file))
           ;                                  ;(log/info (first file-param))
           ;                                  (if uploaded? uploaded-file nil)
           ;                                  ;(log/error upload-results)
           ;
           ;                                  (make-response 200 {:ad-pic uploaded-file :status uploaded?})
           ;
           ;                                  )
           ;                                )
           ;                              )

  )








;;=============================================================================;;
;;========================Login Routes=========================================;;
;;=============================================================================;;

(defroutes front-login-route
           ;(GET "/" [] (fn [request] (dashboard request "front/index_page.php" {} )  )  )

           (GET "/" [] (fn [request]
                         (if (authorized? request) (redirect "/home")
                                                   (dashboard request "front/index_page.php"
                                                              {
                                                               :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))
                                                               :categories (category-list)
                                                               :locations  (venues-list)
                                                               :locs       (location-dropdown)}))))
                                                               ;:locations  (venues-list  )
                                                               ;:is-logged  (session )






           (GET "/search-results" [] (fn [request]
                                       ;(log/info request)
                                       ; (if (authorized? request) (redirect "/home")
                                       (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                                       (dashboard request "front/adsearch.php"
                                                  {
                                                   ; :categories  (category-list)
                                                   :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                   :subcatz    (for [s (seq (catz-list))]
                                                                 ;(if (= (:_id s)  cat_id)
                                                                 (map #(-> (str "<option style='display: none;' data-parent-id='" (:id s) "'  value='" (:_id %1) "'>" (:label %1) "</option>")) (:children s)))
                                                                 ;)

                                                   :locations  (venues-list)
                                                   :locs       (location-dropdown)
                                                   ;:locations  (venues-list  )
                                                   ;:is-logged  (session )
                                                   :user       (find-key request :user)
                                                   :fnamelname (find-key request :fnamelname)})))

                                       ;)



           (GET "/privacy-policy" [] (fn [request]
                                     ;  (log/info request)
                                       ; (if (authorized? request) (redirect "/home")
                                       (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                                       (dashboard request "front/privacy.php"
                                                  {})))




           (GET "/terms-service" [] (fn [request]
                                    ;  (log/info request)
                                      ; (if (authorized? request) (redirect "/home")
                                      (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                                      (dashboard request "front/terms.php"
                                                 {})))

           (GET "/contact-us" [] (fn [request]
                                      ;  (log/info request)
                                      ; (if (authorized? request) (redirect "/home")
                                      (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                                      (dashboard request "front/contact.php"
                                                 {})))


           (GET "/faq" [] (fn [request]
                              (println request)
                                      ; (if (authorized? request) (redirect "/home")
                              (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                              (dashboard request "front/faq.php"
                                         {})))





           (POST "/search-results" [] (fn [request]

                                        ; (if (authorized? request) (redirect "/home")
                                        (dashboard request "front/adsearch.php"
                                                   {
                                                    :categories (category-list)
                                                    :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))
                                                    :locations  (venues-list)
                                                    :locs       (location-dropdown)
                                                    ;:is-logged  (session )
                                                    :user       (find-key request :user)})))


                                        ;)


           (POST "/auth0" [] (fn [req]


                              (let
                                 [user-info (user-login-get-atoken (find-key req "code"))
                                  user-record (conj (get-user-by-username (:email user-info)) (get-user-by-username (:email user-info)) )
                                  registered? (not (empty? user-record))
                                  auth-token     (if (not registered?) (user-login-generate-auth-token (user-login-social-new user-info)) (user-login-generate-auth-token (:_id user-record)))
                                  ]
                                (make-json-response 200 {:authToken auth-token})
                                )

                     )
           )

           (GET "/auth/:token" [token] (fn [request]
                                                 (user-login-validate-auth-token token request)
                                                 ))

           (GET "/logout" [] (fn [request]

                               (logout request)
                              ))

           (POST "/login" [] (fn [request]
                               ; (log/info " out: " request )
                               (login request)))

           (GET "/signin" [] (fn [request]
                               ; (log/info " out: " request )
                               (show-login)))

           (GET "/login" [] (fn [request]
                              (show-login)))

           (POST "/" [] (fn [request]
                          ; (log/info " out: " request )
                          (login request)))

           (GET "/post-your-ad" [] (fn [request]
                                     (show-login)
                                      )

                                    )


           (GET "/about-us" [] (fn [request]
                            ;(println request)
                            ; (if (authorized? request) (redirect "/home")
                            ;(add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
                            (dashboard request "front/about-us.php"
                                       {})))

           (GET "/signup" [] (fn [request]
                               (show-login)))


           (POST "/join" [] (fn [request]
                              (compojure.response/render (make-response 200 (userjoin request))
                                request)))

           ;(POST "/send-message/:item" [item] (fn [request]
           ;                           (send-email-to-publisher item request)
           ;                           (compojure.response/render (make-json-response 200 {:msg "Your message is delivered." :type "success"}) request)
           ;
           ;                   ))

           (POST "/post/report/:id" [id] (fn [request]

                                             (report-ad id request)
                                             ))

           (GET "/profile/:id" [id] (fn [request]
                                   ;(log/debug (str (find-key request :identity)))
                                   (if (= (find-key request :identity) id)
                                     ;(compojure.response/render
                                     ;  {
                                     ;   :status  301
                                     ;   :headers {"Location : " (str layout/*app-context* "my-profile")}}
                                     ;
                                     ;  request)
                                     (redirect "/my-profile")
                                     (user-profile request id))))




           (GET "/item/:id" [id] (fn [request] (dashboard request "front/item.php"
                                                          {;:categories  ( category-list  )

                                                           ;:item      (listings-item-details id)
                                                           :item      (if (.contains id "-")
                                                                        (listing-item-details-by-url (str layout/*app-context* "item/" id))
                                                                        (listings-item-details id))

                                                           :catz      (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                           :locations (venues-list)
                                                           :locs      (location-dropdown)})))



           (GET "/post-ad/sub-categories/:cat" [cat] (fn [request]

                                                       (compojure.response/render
                                                         {
                                                          :status  200
                                                          :headers {"Content-Type" "application/transit+json"}
                                                          :body    (tr-write (:children (subcat-list cat)))
                                                          }

                                                         request)))


           (GET "/post-ad/category-input-details/:subcat" [subcat] (fn [request]
                                                                     (compojure.response/render
                                                                       {
                                                                        :status 200
                                                                        ;:headers { "Location" (str layout/*app-context* "my-profile") }
                                                                        :body   (subcat-list cat)}

                                                                       request)))
  )






;;===================================WS==================================;;
;;=======================================================================;;
(defroutes asyn-chat-routes
           ;(GET  "/chat-message" req (ring-ajax-get-or-ws-handshake req))
           ;(GET  "/threads" req (ring-ajax-get-or-ws-handshake req))
           ;(GET  "/thread/:id" req (ring-ajax-get-or-ws-handshake req))
           ;(POST "/chat-message" req (ring-ajax-post                req))
)


(defroutes ws-routes

           (GET  "/ws-search" req (ring-ajax-get-or-ws-handshake req))

           (GET "/search" request

             (fn [request]
               (with-channel request channel
                             (ws-connect! channel)
                             (on-close channel (partial ws-disconnect! channel))
                             (on-receive channel #'ws-search-handler))))


           (GET "/news-listing" request

             (fn [request]
               (with-channel request channel
                             (ws-news-connect! channel)
                             (on-close channel (partial ws-news-disconnect! channel))
                             (on-receive channel #'ws-news-listing-handler)))
             )


           (GET "/chat-message" request

             (fn [request]
                 ;(println request)
               (with-channel request ch-chsk
                             (ws-chat-connect! ch-chsk request)
                             (on-close ch-chsk  (partial ws-chat-disconnect! ch-chsk request))
                             (on-receive ch-chsk #'ws-chat-message-handler)))
             )



           (not-found "Not Found")

)







;;=======================================================================;;



;;=======================================================================;;
;;========================MODULE ROUTES==================================;;
;;=======================================================================;;
(use 'bestway.app.front.news)

(defroutes news-routes
           (GET "/news/:lang/:src" [lang source]
             (fn [request]
               (for [news (get-rss-entries "http://www.hirunews.lk/rss/sinhala.xml")]
                     (str "<a href='http://localhost:8088/news/sinhala/hiru/" (last (clojure.string/split (:link news) #"/")) "' >" "<h3>" (:content news) "</h3>")

                 )
               )
           )


  )

;;=======================================================================;;



;;=======================================================================;;
;;========================MANAGEMENT ROUTES==================================;;
;;=======================================================================;;

