(ns bestway.db.core
  (:refer-clojure :exclude [sort find])
  (:require [monger.core :as mg]
            [mount.core :refer [defstate]]

            [clojure.tools.logging :as log]

            [monger.collection :as mc]
            [monger.query :refer [with-collection read-preference paginate] :as mq]
    ;            [monger.query :as mq]
            [monger.operators :refer :all]

            [buddy.hashers :as hashers]
            [bestway.config :refer [env]]
            )
  (:import (org.bson.types ObjectId)))

(use 'bestway.util)

(defonce db (atom nil))

(defn connect! []
  ;; Tries to get the Mongo URI from the environment variable
  ;(reset! db (-> (:database-url env) mg/connect-via-uri :db))
  (reset! db (-> "mongodb://127.0.0.1/zda_dev" mg/connect-via-uri :db))
  ;(reset! db (-> (:database-url env) mg/connect-via-uri :db))

  )

(defn disconnect! []
  (when-let [conn @db]
    (mg/disconnect conn)
    (reset! db nil)))

(defstate conn  :start (connect!)  :stop (disconnect!))

(defn create-user [user]
  (let [u (mc/insert-and-return @db "user-prime" user)
        s (mc/insert-and-return @db "user-settings" {:me (dissoc user :password ) :_id (:_id u)})]
  u)
)

(defn update-user [id first-name last-name email]
  (mc/update @db "user-prime" {:_id id}
             {$set {:first-name first-name
                    :last-name last-name
                    :email email}})

  )




(defn update-user-prof-pic [id file-name]
  (mc/update @db "user-prime" {:_id id}
             {$set {
                    :profile-pic file-name
                    }})
  )


(defn get-user-by-id
  [id]  (mc/find-one-as-map @db "user-prime" {:_id (ObjectId. (str id) )} )
  )

(defn get-user-by-username
  [username]
  (mc/find-one-as-map @db "user-prime" {:username username} )
  )

(defn get-user-by-email
  [email]
  (mc/find-one-as-map @db "user-prime" {:email email} )
  )

(defn user-exists-id?
  "arg0 : ObjectId
  "
  [id] (not (empty? (get-user-by-id id)))
  )

(defn user-exists-username?
  "arg0 :  username
  "
  [username] (not (empty? (get-user-by-username username)) )
  )

(defn user-exists-email?
  " arg0 : email
  "
  [email] (not (empty? (get-user-by-email email)))
  )


(defn create-user! [user]
  (if-not (user-exists-username? (:username user) )
    (let
      [
       password (hashers/encrypt (:password user))
       new-user (assoc user :password password)
       ] (create-user new-user))
    (log/error (Exception. "user-exists") "HERE" )

    ) )

(defn get-user-by-username-and-password [username password]
  ; (prn username password)
  (let [
        user (mc/find-one-as-map @db "user-prime" {:username username } )
        password-matched? ( hashers/check password (:password user) )
        ]
    (if password-matched?
      true
      false
      )))

(defn get-user [username password]
  ; (prn username password)
  (let [
        user (mc/find-one-as-map @db "user-prime" {:username username } )
        password-matched? ( hashers/check password (:password user) )
        ]
    (if password-matched?
      true
      false
      )))

(defn get-user-settings
  "get user settings from user-settings collection"
  [id]
  (let [
        settings (mc/find-one-as-map @db "user-settings" {:_id (ObjectId. id) } )
        data      (if (not-empty settings)
                    (dissoc settings :security)
                    {}
                    )

        ]
(log/debug settings) (println  settings)
    data)
  )



(defn get-user-data [username]
  ; (prn username password)
  (let [
        user (mc/find-one-as-map @db "user-prime" {:username username } )
        data      (if (not-empty user)
                    user
                    false
                    )
        ]

    data)
  )

(defn get-staff-user [app-code div-code username password]
  ; (prn username password)
  (log/info "password-matched =" app-code)
  (let [
        ; appcode (mc/find-one-as-map @db "apps" {:app-code app-code } )
        ; div-code (mc/find-one-as-map @db "division" {:div-code div-code } )
        app (mc/find-one-as-map @db "app" {:appManagement.users {:$elemMatch { :username username }}} )
        staff (get-in app [:appManagement :users])
        user (for [smem staff :when (=(get-in smem [:username] ) username ) ] smem)
        password-matched? ( hashers/check password (:password (first user)) )
        ]
    ;(log/info "password-matched =" password-matched)
    (if password-matched? (dissoc (first user) :password)  nil)
    ))

(defn delete-user! [id]
  (let [oid (ObjectId. (str id)) ]
    (mc/remove-by-id db "ser-prime" oid)
    )
  )
