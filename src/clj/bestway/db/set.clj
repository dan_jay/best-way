(ns bestway.db.set
  (:require [monger.core :as mg]
            [mount.core :refer [defstate]]
            [monger.collection :as mc]
            [clojure.tools.logging :as log]
            [monger.operators :refer :all]
            [buddy.hashers :as hashers]
            [bestway.config :refer [env]]
            [clojure.pprint :refer [pprint]]
            )
  (:import (org.bson.types ObjectId)
           (org.joda.time DateTime)))
(use 'bestway.db.core)
(use 'bestway.util)

(defn insert-ad [data]
    (mc/insert-and-return @db "listing_items" data)
  )

(defn delete-ad[id]
  (mc/update @db "listing_items" {:_id id} {"$set" {"status.deleted" true}})
  )

(defn publish-ad[id publish?]
    (mc/update @db "listing_items" {:_id id} {"$set" {"status.submitted" true "status.pending" true "status.approved" false}})
  )

(defn change-passwd
  "checks and change user password if matched"
  [id password]
  (if (password-eligible? password)
    (mc/update @db "user-prime" {:_id (ObjectId. id)}
               {$set {:password (hashers/encrypt password) }  })
    false)
)


(defn set-user-settings [id user-settings]

  (let [new-pwd (find-key user-settings :new-password)
        user (get-user-by-id id)
        password-confirmed? (= new-pwd (find-key user-settings :new-password-clone))
        password-matched? ( hashers/check (find-key user-settings :password) (:password user) )
        ]
        (if (and password-confirmed? password-matched?)
          (change-passwd id new-pwd)
          false)

    )
  (mc/upsert @db "user-settings"
             {:_id (ObjectId. id)}
             {$set (dissoc user-settings :security)}
             )
)


;(mc/upsert @db "user-settings" {:_id (ObjectId. "587d0dba8ce1001764979031")} {:first-name "Dan"
;                                                                              :last-name                                     "Jay"
;                                                                              :display-name                                  "dankay"
;                                                                              :birth-day                                     {:year 1990 :month 03 :day 25}
;                                                                              :email                                         "foo@bar.baz"
;                                                                              :contact-num                                   ""
;                                                                              :address                                       {
;                                                                                                                              :line1 ""
;                                                                                                                              :line2 ""
;                                                                                                                              :city ""
;                                                                                                                              :district ""
;                                                                                                                              }
;                                                                              })
;


(defn add-comment [userid comment & subjectid]
  ;(prn id )
  ;(prn user-settings)
  (mc/upsert @db "listing-comments"
             {:id userid}
             {$set comment}
             )

  )

(defn save-chat-msg
  ""
  [id data]
    (mc/update @db "user-messages" {:id id} {$set data} {:upsert true})
 )

(defn retrieve-latest-news
  "Get news from Temporary store"
  [current-id lng]
  (let [has-newer? (if (empty? (mc/find-one @db "recent_news"
             {:_id current-id :lng lng})) true false)

        ]

    (if has-newer?
      (mc/find-one @db "recent_news" {})
      {}
      )
   )
  )


(defn store-news
  "Temporary store news"
  [id news]
  (mc/upsert @db "recent_news"
             {:_id id}
             {$set news}
             )
  )


(defn set-bw-auth-token[id token]
     (mc/upsert @db "user-prime"
                {:_id id}
                {$set {:bw-auth-token token :bw-auth-token-created-at (DateTime.)}}
                )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;===============================================================================;;;
;;;============CAUTION=====CAUTION======CAUTION=======CAUTION=====================;;;
;;;===============================================================================;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn edit-categories [id cat-data]
  ;(prn id )

  (mc/upsert @db "categories"
             {:_id (ObjectId. id)}
             {$set cat-data}
             )

)