(ns bestway.db.get
  (:refer-clojure :exclude [sort find])
  (:require [monger.core :as mg]
            [mount.core :refer [defstate]]
            [monger.collection :as mc]
            [monger.query :refer [with-collection read-preference paginate fields find]]
            [monger.query :as mq]
            [monger.operators :refer :all]
            [monger.conversion :refer [from-db-object]]
            [clojure.tools.logging :as log]
            [monger.cursor :as cur]

            [buddy.hashers :as hashers]
            [bestway.config :refer [env]])

  (:import org.bson.types.ObjectId)
  (:import com.mongodb.ReadPreference))

(require '[monger.cursor :refer [make-db-cursor add-options format-as]])

(use 'bestway.db.core)
(use 'bestway.util)

;(make-db-cursor @db "user-prime" ref fields)

(defn webapp-left-sidebar-items
  []
  (mc/find-one-as-map @db "webapp-left-sidebar-items" {}))


;(with-collection @db "user-prime"
;                 (mc/find-one-as-map @db "user-prime" [{}])
;                 (println "user data changed!!! ")
;                 )

(defn venue-list
  []
  (mc/find-maps @db "location"))


(defn get-main-category
  "main category id string"
  [cat-id]
  (let [
        mcat (mc/find-one-as-map @db "categories" {:id cat-id})

        ]
      (if (nil? mcat) {} mcat)
    )
)


(defn get-listing-comments
  "get user settings from user-settings collection"
  [listing-id]
  (let [
        comments (mc/find-one-as-map @db "listing_comments" {:listing-id listing-id } )
        data      (if (not-empty comments)
                    comments
                    {}
                    )
        ]

    data)
  )


(defn venue-list-district
  []
  (mc/find-maps @db "location"))


(defn venue-list-province
  []
  (mc/find-maps @db "location" {} [{:_id 0}]))


(defn venues-list
  []
  ;mc/find-maps @db "loc") ;edited by kev
  ;(log/info (mc/find-maps @db "location" ))
  (mc/find-maps @db "locz"))


(defn category-list []
  ;(mc/find-maps @db "categories"   )
 ; (mc/find-one-as-map @db "categories" { :_id (ObjectId. "577ac6ba4a26958d218b4567") })
  ;(mc/find-maps @db "catz"   )
  (mc/find-maps @db "categories"))


(defn subcat-list [cat]
  (mc/find-one-as-map @db "categories" { :id cat}))
  ;(mc/find-one-as-map @db "categories" { :_id (ObjectId. "577ac6ba4a26958d218b4567") })


(defn catz-list []
  (mc/find-maps @db "categories"))



(defn search-listing [txt-query]
  (with-collection @db "categories"
                   ; (mg/find { :year { $lt 2010 $gte 2000 } :revenue { $gt 20000000 } })
                   ;(mq/find {:label {:$eq txt-query}})
                   ; (fields [ :year :title :producer :cast :budget :revenue ])
                   (paginate :page 1 :per-page 3))
  ;(sort (sorted-map :title 1))
  ;(read-preference {} ReadPreference/PRIMARY)



  (let [txt-q (clojure.string/trim (clojure.string/replace txt-query #"\+" " "))]
    (log/info txt-q
              ;(mc/find-maps @db "listing_items" {:label txt-q})
              (mc/aggregate @db "listing_items" [{:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}} {:$match {:label txt-q}}]))
    )

  )
;(defn search-listing-by-txt-cat-loc-price [txt cat loc price]
;  (let [txt-q (clojure.string/lower-case (clojure.string/trim (clojure.string/replace txt #"\+" " ")) )
;        txt-cat (clojure.string/trim (clojure.string/replace cat #"\W" ""))
;        txt-loc (clojure.string/trim (clojure.string/replace loc #"\W" ""))
;        min-price (:min price)
;        max-price (:max price)
;        ]
;    ;(log/info min-price)
;    (cond
;          (= txt-cat "any")
;          (mc/aggregate @db "listing_items" [{:$match { :$text  { :$search txt-q }   }} {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}}])
;
;          (= txt-loc "any")
;          (mc/aggregate @db "listing_items"  [{ :$match { :$text  { :$search txt-q }  :category.main txt-cat  } } {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}} ])
;
;          (= txt-q "")
;          (mc/aggregate @db "listing_items" [{:$match {:category.main txt-cat}} {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}}])
;
;          (= min-price "")
;          (mc/aggregate @db "listing_items" [{:$match {:category.main txt-cat}} {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}}])
;
;          (= max-price "")
;          (mc/aggregate @db "listing_items" [{:$match {:category.main txt-cat}} {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}}])
;
;          :else
          ;(mc/aggregate @db "listing_items" [ { :$match { :$text  { :$search txt-q }  :category.main txt-cat  } } {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}} ] )
;    )
;    )
;  )


(defn search-listing-by-txt-cat-loc-price [txt cat loc price]
  (let [txt-q (clojure.string/lower-case (clojure.string/trim (clojure.string/replace txt #"\+" " ")))
        txt-cat (clojure.string/trim (clojure.string/replace cat #"\W" ""))
        txt-loc (clojure.string/lower-case (clojure.string/trim (clojure.string/replace txt #"\+" " ")))
        min-price  (clojure.string/trim (get :min price ""))
        max-price (clojure.string/trim (get :max price ""))
        query-items (atom {:status.active true})]

    ;(log/info min-price)

    (if-not (= txt-q "")
      (swap! query-items conj  {:$text {:$search (str "\"" txt-q "\"")}}))


    (if-not (= txt-cat "any")
      (swap! query-items conj {:category.main  txt-cat}))


    ;(if-not (= txt-loc "any")
    ;  (swap! query-items conj {:location (str "\"" txt-loc "\"" )} )
    ;
    ;  )


    (if-not (or (= min-price "") (nil? min-price))
      (swap! query-items conj {:price { :$gt min-price}}))


    (if-not (or (= max-price "") (nil? max-price))
      (swap! query-items conj {:price { :$lte max-price}}))


    ;
    (log/info  @query-items)

    ;(update-in {:$match  "" } [:$match] @query-items)
    (let [ q1 {:$project {:label 1 :title 1 :publisheddate 1 :publisher 1 :price 1}}
          q2 (assoc-in {:$match  "" } [:$match] @query-items)
          q  [ q2 {:$project {:label 1 :title 1 :releasedate 1 :publisher 1 :price 1}}]]

      ;(log/info "q2 :" q2)
      ;(log/info "q :" q)
      ; (mc/aggregate @db "listing_items" [{:$match {:$text {:$search "blizzard entertainment"}} }, {:$project {:label 1, :title 1, :releasedate 1, :publisher 1, :price 1}} ])
      ;(mc/aggregate @db "listing_items" q )
      (map #(->(update % :publisheddate get-time-gap)) (mc/aggregate @db "listing_items" q)))))






(defn search-listing-by-txt-cat-subcat-loc-price [txt cat sub-cat loc minprice maxprice page limit]
  (let [
        txt-q (if (or (nil? txt) (= txt "")) "" (clojure.string/lower-case (clojure.string/trim (clojure.string/replace txt #"\+" " "))))
        txt-cat (if (nil? cat) "any" cat)
        txt-subcat sub-cat
        ;txt-subcat (clojure.string/lower-case (clojure.string/trim (clojure.string/replace sub-cat #"\+" " ")))
        txt-loc (clojure.string/lower-case (clojure.string/trim loc))

        ;min (if (not (clojure.string/blank? minprice)) (read-string minprice) nil)
        ;max (if (not (clojure.string/blank? maxprice)) (read-string maxprice) nil)



        min-price (if (number? min) min 0)
        max-price (if (number? max) max 100000)

        page_ (read-string page)
        limit_ (read-string limit)
        page (if (number? page_) (Integer/parseInt page) 1)
        limit (if (number? limit_) (Integer/parseInt limit) 10)

        ; query-vars (atom {:text "" :cat-main "" :cat-sub "" :loc "" :min-price "" :max-price "" :page page :limit limit} )

        query-items (atom {})]


    ;(log/info subcat)
    (if (and (not=  minprice "any") (not=  maxprice "any"))
      (try
        (if-not (or (= minprice "") (= maxprice "") (nil? minprice) (nil? maxprice))
          (swap! query-items conj {:price {:$gte (Double/parseDouble minprice ) :$lte (Double/parseDouble maxprice)}}))
        (catch NumberFormatException e
          ))

      (do
        (try
         (if-not (or (= minprice "any") (nil? minprice))
           (swap! query-items conj {:price {:$gte  (Double/parseDouble minprice)}}))
         (catch NumberFormatException e
           ))

        (try
          (if-not (or (= maxprice "any") (nil? maxprice))
            (swap! query-items conj {:price {:$lte  (Double/parseDouble maxprice)}}))
          (catch Exception e
            ))
        )
      )

    (if (not= txt-q "")

        (swap! query-items conj {:$text {:$search (str "\"" txt-q "\"")}})

      )




    (if (not= txt-cat "any")
      (do
        (swap! query-items conj {:category.main txt-cat})))
    ; (reset! query-vars (assoc @query-vars :cat-main (str "\"" txt-cat "\"")))




    (if (not= txt-subcat "any")
      (do
        (swap! query-items conj {:category.sub txt-subcat})))
    ; (reset! query-vars (assoc @query-vars :cat-sub (str "\"" txt-subcat "\"")))



    (swap! query-items conj {:status.active true})  ;;;;;;;;;;;;;;; published and approved posts
    ;
    ;  (swap! query-items conj {:studio { :category.main (str txt-loc) } } )
    ;)
;    (log/info @query-items)



    ;
    (println @query-items)
              (log/info @query-items)

    ;(update-in {:$match  "" } [:$match] @query-items)
    (let [
          ;q1 {:$project {:label 1 :title 1 :publisheddate 1 :publisher 1 :price 1}              }
          q2 (assoc-in {:$match ""} [:$match] @query-items)
          q [q2
             {:$lookup {:from "user-settings" :localField "publisher" :foreignField "_id" :as "publisher_data"}}

             {:$project {:label 1 :imgs 1 :title 1 :description 1 :publisheddate "$publisheddate" :publisher_data._id 1 :publisher_data.me.display-name 1 :price 1 :url 1}}
             {:$skip (* (dec page) limit)}
             {:$limit limit}]]


      {

       :total    (mc/count @db "listing_items" @query-items)
       :per-page limit
       :page     page
       :results  (map #(-> (update % :publisheddate get-time-gap)) (mc/aggregate @db "listing_items" q))}))

  ;(with-collection @db "listing_items"
  ;               ;(find {:$text {:$search "encore software"}})
  ;               (find @query-items)
  ;               (find [{:$match {}} {:$lookup { :from "user-settings" :localField "publisher" :foreignField "_id" :as "publisher-data"  }  } {:$project {:label 1, :title 1, :releasedate 1, :publisher-data 1, :price 1}}])
  ;               (fields [:_id :images :label :title :releasedate :publisher :price ])
  ;                ;(sort {})
  ;               (paginate :page page :per-page limit) )

  )


(defn listing-ads
  []
    (mc/find-maps @db "listing_items" { :type.list_as  "ad"}))

(defn get-publisher-data-by-id
  "Ad Listing publisher data by String id"
  ;[^String st]
  ;(:me (mc/find-one-as-map @db "user-settings" {:_id (ObjectId. st ) }   ))
  [id]
  "Ad Listing publisher data by ObjectId"
  (:me (mc/find-one-as-map @db "user-settings" {:_id (ObjectId. (str id)) }  ))
  )

(defn listings-item-details
  [item-id]
  (let
    [
      item-data (mc/find-one-as-map @db "listing_items" { :_id (ObjectId. (str item-id))})
      publisher-data (assoc (get-publisher-data-by-id (ObjectId. (str (:publisher item-data)))) :id item-id)
      publisher-data-with-id (assoc publisher-data :id (str (:publisher item-data)))
     ]

    (assoc item-data :publisher publisher-data-with-id))
  )

(defn listing-item-details-by-url
  "Match URL with an item"
  [^String item]
  (let
    [
     item-data (mc/find-one-as-map @db "listing_items" { :url item})
     publisher-data (get-publisher-data-by-id (:publisher item-data))
     publisher-data-with-id (assoc publisher-data :id (str (:publisher item-data)))
     ]

    (assoc item-data :publisher publisher-data-with-id))
  )

(defn user-listings
    [user-id]
    (if-not (nil? user-id)
      (do

          (mc/find-maps @db "listing_items" { :publisher (ObjectId. (str user-id)) })
        )
        nil
    )
)

(defn user-listings-active
  [user-id]
  (if-not (nil? user-id)
    (do

      (mc/find-maps @db "listing_items" { :publisher (ObjectId. (str user-id))  :status.active true } )
      )
    nil
    )
)

(defn user-listings-pending
  [user-id]
  (if-not (nil? user-id)
    (do

      (mc/find-maps @db "listing_items" { :publisher (ObjectId. (str user-id)) :status.active false } )
      )
    nil
    )
  )

(defn get-user-by-auth-token
  ""
  [bw-auth-token]
    (mc/find-one-as-map @db "user-prime" {"bw-auth-token" bw-auth-token} )
)

;;
;;;;;;;;;;   Chat Messaging
;;

(defn get-thread-by-author-id
  ""
  [id]
  (sort-by :threadID  (mc/find-maps @db "user-messages" {"author"  (ObjectId. (str id)) } ))
)


(defn get-threads-by-sender-receiver
  ""
  [id]
  (sort-by :threadID  (mc/find-maps @db "user-messages"  { "$or" [ {"author"  (ObjectId. (str id)) } {"to"  (ObjectId. (str id)) } ] } ))
  )

(defn get-thread-by-thread-id
  ""
  [id]
  (mc/find-one-as-map @db "user-messages" {:_id (ObjectId. (str id))} )
  )

(defn user-messages-cursor
   "get tailable cursor to user-messages collection"
  []
      (let [db-cur (make-db-cursor @db "user-messages") ]
        (-> db-cur
            (add-options :tailable)
            (add-options :awaitdata)
            (format-as :map)
            )
        db-cur)
   )


