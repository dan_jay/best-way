(ns bestway.db.todo
  (:refer-clojure :exclude [sort find])
  (:require [monger.core :as mg]
            [mount.core :refer [defstate]]
            [monger.collection :as mc]
            [monger.query :refer [with-collection read-preference paginate fields find]]
            [monger.query :as mq]
            [monger.operators :refer :all]

            [clojure.tools.logging :as log]

            [buddy.hashers :as hashers]
            [bestway.config :refer [env]]
            )
  (:import org.bson.types.ObjectId)
  (:import com.mongodb.ReadPreference))


(use 'bestway.db.core)
(use 'bestway.util)

(defn todos-list
  [user-id]
  (mc/find @db "user-prime" {:_id user-id} { :id 1, :title 1, :done 1} )
  )

(defn add-todo!
  [user-id todo-item]
    (mc/update @db "user-prime" {:_id user-id } {"$push" { "todo.tasks" { :title todo-item :done false } } })
  )

(defn delete-todo!
  [user-id id]
  (mc/update @db "user-prime" {:_id user-id } {"$pull" { "todo.tasks" {:_id id } }})
  )

(defn update-todo!
  [user-id id title]
  (mc/update @db "user-prime" {:_id user-id "todo.tasks._id" id } { "$set" { "todo.tasks.$.title" title } })
  )

(defn toggle-todo!
  [user-id id]
    (mc/update @db "user-prime" {:_id user-id "todo.tasks._id" id } { "todo.tasks.$.done" (mc/find @db "user-prime" {:_id user-id "todo.tasks._id" id }) })
 )

(defn mark-all!
  [done?]
;;
  )

(defn delete-all-done!
  []
;;
  )

