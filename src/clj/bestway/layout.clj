(ns bestway.layout
  (:require [selmer.parser :as parser]
            [selmer.filters :as filters]
            [markdown.core :refer [md-to-html-string]]
            [ring.util.http-response :refer [content-type ok]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            )
  )

(declare ^:dynamic *identity*)
(declare ^:dynamic *app-context*)
(declare ^:dynamic *websock-context*)
(declare ^:dynamic *app-base-location*)

(declare ^:dynamic *tmp-path*)
(declare ^:dynamic *profile-pic-upload-path*)
(declare ^:dynamic *listing-pic-upload-path*)

(declare ^:dynamic *visitor-allowed-urls*)

(parser/set-resource-path!  (clojure.java.io/resource "templates"))

;(parser/cache-off!)

(parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))
(parser/add-tag! :csrftok (fn [_ _] (anti-forgery-field)))

(filters/add-filter! :markdown (fn [content] [:safe (md-to-html-string content)]))
(filters/add-filter! :ad-property #(clojure.string/replace % "_" " ") )
(filters/add-filter! :ad-price #(let [price (double %)
                                       formatted (format "%,.2f" price )]
                                   formatted))

 (filters/add-filter! :key key)
 (filters/add-filter! :val val)
 (parser/render  "{% for item in m %} KEY is {{item|key}} VALUES is {{item|val}}\n{% endfor %}"  {:m  {:a 1 :b 1}})

(defn render
  "renders the HTML template located relative to resources/templates"
  [template & [params]]
  (content-type
    (ok
      (parser/render-file
        template
        (assoc params
          :page template
          :csrf-token *anti-forgery-token*
          :ws-context *websock-context*
          :app-context *app-context*)))
    "text/html; charset=utf-8"))

(defn error-page
  "error-details should be a map containing the following keys:
   :status - error status
   :title - error title (optional)
   :message - detailed error message (optional)

   returns a response map with the error page as the body
   and the status specified by the status key"
  [error-details]

  {:status  (:status error-details)
   :headers {"Content-Type" "text/html; charset=utf-8"}
   ;:body    (parser/render-file "error.html" error-details)
   :body    (parser/render-file "error.php" {})

   })
