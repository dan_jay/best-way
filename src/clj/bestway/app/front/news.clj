(ns bestway.app.front.news
  (:require
    [org.httpkit.client :as http]
    [clojure.core.async
     :as a
     :refer [>! <! >!! <!! go chan buffer close! thread
             alts! alts!! timeout]]
    [clojure.string :as string]))
(require 'feedme)
(require '[reaver :refer [parse extract extract-from edn data text attr]])


(defn get-rss-entries
  "Get news from RSS feed"
  [rss-url & filter]

  (let [
        feed (feedme/parse rss-url)
        entries (:entries feed)
        ]

    entries))

;(defn extract-from-website
;  "Extract html from site
;  lbl-keys vector of keys for labeling output (allowed to be nil or [])
;  selectors-types :selection path followed by extraction :type
;  "
;  [url ^vec lbl-keys imgs texts lnks edns ]
;  (extract-from (parse (:body @(http/get url {:insecure? true})))
;           lbl-keys
;                (for [i imgs]
;                     [ i (attr :src)]
;                  )
;           "#newsItemComplete img.img-responsive" (attr :src)
;           "#newsItemComplete .newsContent" text
;
;           )
;  )

;(defn sync-news
;  [filename last-news-time]
;  (let [c (chan)]
;    (go (while true ( filename (<! c))))
;    (dotimes [n num-quotes] (go (>! c (random-quote))))))


(defn extract-from-news-site
  "Extract news from news site"
    [url feature-img-selector body-selector]
    (def news (:body @(http/get url {:insecure? true})))

    (extract (parse news)
                  [:img :body]
                  feature-img-selector (attr :src)
                  body-selector text
                )
)



;(defn get-news-articles
;  "docstring"
;  [url]
;  ;(extract-from-news-site url  ".latest-box .lts-txt2")
;  (get-rss-entries url)
;  (def txt-container (extract-from-news-site url  ".latest-box .lts-txt2") )
;  ;(extract-from-news-site "http://www.hirunews.lk/sinhala/158734" ".latest-box .lts-txt2")
;  ;(print txt-container)
;  (into [] (concat [] (apply concat (for [a txt-container]
;                                      [(let [news-body (get-in (:content a) [:content])
;                                             news-str (apply
;                                                        (fn [& vals]
;                                                          (filter string? vals)) news-body)
;                                             ]
;                                        ; (println news-str)
;
;                                         news-str)]
;                                      )
;                             )
;    ) )
;  )

(defn get-article-contents
  "docstring"
  [url img-sel body-sel & options ]

  (let [
        feed (take 4 (get-rss-entries url ))
        container (for [entry feed]
                    (assoc
                      (extract-from-news-site (:link entry) img-sel body-sel)
                      :id (str (:published entry)) :published (if-let [date-time (:published entry)] (let [jdt (org.joda.time.DateTime. date-time)] {:year (str (.getYear jdt)) :month (str (.getAsShortText (.monthOfYear jdt))) :day (str (.getAsText (.dayOfWeek jdt))) :date (str (.getDayOfMonth jdt)) :time (let [t (.toLocalTime jdt) h (.getHourOfDay (.toLocalTime jdt)) m (.getMinuteOfHour (.toLocalTime jdt))] (str h ":" (format "%02d" m))) }) ) :categories (get entry :categories "Latest") :title (:title entry) :summary (:content entry)
                      )
                    )
     ]

  container
  )
)
;


(defn get-eng-news
  "Get English news"
  []

      (let [news1
            (try
              (let [
                    news1-pre1 (get-article-contents "http://www.hirunews.lk/rss/english.xml" ".latest-box .latest-pic img" ".latest-box .lts-txt2")
                    news1-pre2 (for [n news1-pre1] (assoc-in n [:id] (string/replace (:id n) #"[^0-9]+" "")))
                    news (for [n news1-pre2] (update-in n [:summary] #(str (first (string/split % #"\.\.\.")) "...")))
                    ]
                news)

              (catch Exception e []
                ))

            news2 (try
              (let [
                    news2-pre1 (get-article-contents "http://www.adaderana.lk/rss.php" "#newsItemComplete p img.img-responsive"
                                                     "#newsItemComplete .newsContent")
                    news2-pre2 (for [n news2-pre1] (assoc-in n [:id] (string/replace (:id n) #"[^0-9]+" "")))
                    news (for [n news2-pre2] (update-in n [:summary] #(str (string/replace % #"<img.+\/>" "") "...")))
                    ]
                news)

              (catch Exception e []
                                 ))




            all   (concat news1 news2)]

        (shuffle all))

  )


(defn get-sin-news
  "Get Sinhala news"
  []

       (let [news1
            (try
              (let [
                    news1-pre1 (get-article-contents "http://www.hirunews.lk/rss/sinhala.xml" ".latest-box .latest-pic img" ".latest-box .lts-txt2")
                    news1-pre2 (for [n news1-pre1] (assoc-in n [:id] (string/replace (:id n) #"[^0-9]+" "")))
                    news (for [n news1-pre2] (update-in n [:summary] #(str (first (string/split % #"\.\.\.")) "...")))
                    ]
                news)

              (catch Exception e []
                ))

            news2 (try
              (let [
                    news2-pre1 (get-article-contents "http://sinhala.adaderana.lk/rsshotnews.php" "#newsItemComplete p img.img-responsive"
                                                     "#newsItemComplete .newsContent")
                    news2-pre2 (for [n news2-pre1] (assoc-in n [:id] (string/replace (:id n) #"[^0-9]+" "")))
                    news (for [n news2-pre2] (update-in n [:summary] #(str (string/replace % #"<img.+\/>" "") "...")))
                    ]
                news)

              (catch Exception e []
                                 ))




            all   (concat news1 news2)]

        (shuffle all))

  )



(defn get-tam-news
  "Get Tamil news"
  []

        (let [news1
            (try
              (let [
                    news1-pre1 (get-article-contents "http://www.hirunews.lk/rss/tamil.xml" ".latest-box .latest-pic img" ".latest-box .lts-txt2")
                    news1-pre2 (for [n news1-pre1] (assoc-in n [:id] (string/replace (:id n) #"[^0-9]+" "")))
                    news (for [n news1-pre2] (update-in n [:summary] #(str (first (string/split % #"\.\.\.")) "...")))
                    ]
                news)

              (catch Exception e []
                ))

            news2 (try
              (let [
                    news2-pre1 (get-article-contents "http://tamil.adaderana.lk/rss.php" "#newsItemComplete p img.img-responsive"
                                                     "#newsItemComplete .newsContent")
                    news2-pre2 (for [n news2-pre1] (assoc-in n [:id] (string/replace (:id n) #"[^0-9]+" "")))
                    news (for [n news2-pre2] (update-in n [:summary] #(str (string/replace % #"<img.+\/>" "") "...")))
                    ]
                news)

              (catch Exception e []
                                 ))




            all   (concat news1 news2)]

        (shuffle all))

  )



;(let [
;      news (get-rss-entries "http://www.hirunews.lk/rss/sinhala.xml")
;      id-updated  (for [n news] (update-in n [:id] #(last (string/split  % #"\/")) ) )
;      summary-updated  (for [n id-updated] (update-in n [:content] (string/split  % #"\.\.\.") ) )
;      ]
;  summary-updated
;  )
;(let [
;      news (get-rss-entries "http://www.hirunews.lk/rss/sinhala.xml")
;      img-updated (for [n news] (assoc-in n [:img] (news-feature-img (:id n)) ))
;      body-updated  (for [n img-updated] (assoc-in n [:body] (news-body (:id n)) ))
;      id-updated  (for [n body-updated] (update-in n [:id] #(string/split  % #"\/")) )

;     summary-updated  (for [n id-updated] (update-in n [:content] #(first (string/split  % #"\.\.\."))  ))
;      ]
;  summary-updated
;  )




;;;;
;http://www.hirunews.lk/rss/english.xml => English
;http://www.hirunews.lk/rss/tamil.xml

;http://www.adaderana.lk/rss.php => english
;http://tamil.adaderana.lk/rss.php => tamil

