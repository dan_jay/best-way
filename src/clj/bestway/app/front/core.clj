(ns bestway.app.front.core
  (:require [bestway.layout :as layout]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.http-response :refer [ok]]
            [ring.util.response :refer [redirect response]]
            [ring.util.request]
            [selmer.parser :as parser]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [ring.middleware.session :as session]
            [clojure.tools.logging :as log]

            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [buddy.auth.accessrules :refer [restrict]]

            [postal.core :as mail]
            [clojure.java.io :as io]
            [org.httpkit.server :refer [websocket? with-channel open? on-receive on-close send!]]
            [org.httpkit.client :as http-client]
            [cognitect.transit :as t]
            [clojure.core.async :refer [<! >! put! close! go] :as a]
    ;[clojure.core.async :as a]

            [taoensso.truss :as truss :refer (have have! have?)] ;; Assertion comes to save

            [cognitect.transit :as transit]
            [ring.util.response :as response]
            [crypto.random]
            [taoensso.sente :as sente] ; <--- websocket

    ;; web-server adapter --->
           [taoensso.sente.server-adapters.http-kit      :refer (get-sch-adapter)]




            )

  (:import (org.bson.types ObjectId)
           (org.joda.time DateTime)
           (java.io ByteArrayInputStream)
           (com.mongodb WriteResult)
           ))


(def mail-script-path "/bestway/public/includes/")

(use 'ring.middleware.anti-forgery)
(use 'ring.util.anti-forgery)
(use 'selmer.parser)
(use 'bestway.util)
(use 'bestway.db.core)
(use 'bestway.db.get)
(use 'bestway.db.todo)
(use 'bestway.db.set)
(use 'ring.util.response)
(use 'bestway.app.front.news)
(use '[clojure.java.shell :only [sh]])

(def upload-path "/tmp/")

;(defn authorized? [request]
;  (if (and
;        (= (get-in request [:session :role]) "user")
;        (get-in request [:session :identity])
;        (authenticated? request))
;
;    true false))


;;=============================================================================;;
;;========================Handling Functions===================================;;
;;=============================================================================;;
(defn upload-ad-pics-wm
  "Upload and add watermark to uploading pics"
  [request]
  (into [] (apply concat
            (for [f (filter #(-> (not= 0 (:size %) ) ) (first (find-key request :upload-file)))]
              [
    ; (println f)
               (when-let [upload-results (handle-upload f (find-key request :_id))]
                 (when-let [uploaded? (= (:status upload-results) "OK")]
                     (let
                                                  [full-path-filename-ext (:filename upload-results)
                                                   file-name (file-name full-path-filename-ext)
                                                   png-file-name (clojure.string/replace file-name #"\..+{3,4}" ".png")
                                                   ]
                       ;(println full-path-filename-ext)

                       (if
                        (add-watermark!  layout/*listing-pic-upload-path*
                          full-path-filename-ext )
                         {:url (str layout/*app-context* "ad/" (str png-file-name)) :dimensions (getImageDimensions (str
                                                                                                                       layout/*listing-pic-upload-path* png-file-name))}
                         {:url (str layout/*app-context* "ad/image.png") :dimensions {:width 960, :height 796}}
                         ))))]))))







(defn save-ad [req]
  (println req)
  (let [
        form-params (:params req)
        ad-type (:type form-params)
        ad-title (find-key req :ad-title)
        ad-district (:ad-district form-params)
        ad-city (:ad-city form-params)
        main-cat (find-key req :main_cat)
        sub-cat (find-key req :sub_cat)
        txt-desc (find-key req :txt-desc)
        price (double (Double/parseDouble (find-key req :txt-price)))
        images (upload-ad-pics-wm req)
        status  {:active false :approved false :submitted false :deleted false} ;"status" : {"active":false, "approved":false, "submitted" : false, "deleted": false}
        publisher (ObjectId. (str layout/*identity*))
        currency "LKR"
        submitteddate (DateTime.) ; date time this ad is submitted for approval
        props (dissoc form-params :type :__anti-forgery-token :upload-file :index :ad-title :main_cat :sub_cat :txt-desc :txt-price :ad-city :ad-district)
       ; props (into [] (filter #(not (nil? %)) (for [i (range 1 8)] (let [val (find-key req (keyword (str "param" i)))]  (if val (hash-map :value val))))))
        ]
        ;publisheddate (DateTime.)

        

         (insert-ad {
                     :id (rand-int 2)
                     :type ad-type
                :title  ad-title
                     :url (str layout/*app-context* "item/" (make-ad-item-url ad-title))
                :category {:sub sub-cat :main main-cat}
                :description  txt-desc
                :publisher publisher
                :imgs images
                :status status
                :price price

                     :location {:district ad-district :city ad-city}

                :properties props
                :currency currency
                :submitteddate submitteddate}))
  )



(defn save-user-settings
  "save-user-settings"
  [req]
  ;(println req)
  (let [
        bis (:body req)
        in (ByteArrayInputStream. (.bytes bis))
        ;w (.read bis)
        reader (transit/reader in :json)
        ]

      ;(println req)
      (if (.isInstance WriteResult (set-user-settings (str layout/*identity*) (transit/read reader))) {:msg "Settings Saved" :type "success"} {:msg "Settings not Saved, Please try again later" :type "warning"} )
    )

  )

(defn save-listing-comments
  "save listing comments"
  [req]
  ;(println req)
  (let [
        bis (:body req)
        in (ByteArrayInputStream. (.bytes bis))
        ;w (.read bis)
        reader (transit/reader in :json)
        ]

    ;(println req)
    (if (.isInstance WriteResult (add-comment (ObjectId. (str layout/*identity*)) (transit/read reader))) true false)
    )

  )



(defn show-login []
  ; (create-user! {:username "dan1" :password "123456"})
  (add-tag! :csrftok (fn [_ _] (anti-forgery-field)))
  (layout/render "front/login.php"))


(defn show-index []
  (add-tag! :csrftok  (fn [_ _]  (anti-forgery-field)))
  (layout/render "front/index_page.php" {
                                         :catz       (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))
                                         :locations  (venues-list)
                                         :categories (category-list)}))
                                         ;;:categories (map #(-> (str "" (:label %1) "")) (catz-list))




(defn profile [request template-page & kv]
  (if (authorized? request)
    (let [
          left-sidebar-items (dissoc (webapp-left-sidebar-items) :_id)]


      (layout/render template-page (conj (first kv) {:left-sidebar-items left-sidebar-items :user (get-in request [:session :user]) :page-link (:uri request)})))

    (show-login)))

(defn user-login-generate-auth-token
  "generate one time authentication code"
  [user-id]
  (let [token (str (crypto.random/url-part 64))]
      (set-bw-auth-token user-id token)
    token)
  )


(defn user-login-social-new [userinfo]
  (let [
        last-name (find-key userinfo :last-name)
        first-name (find-key userinfo :first-name)
        fnamelname (find-key userinfo :name)
        email (find-key userinfo :email)
        ;session (:session request)
        userdata (create-user {:first-name first-name :last-name last-name :username email :email email})
        ]
    ;      (log/info " out " request )
    (if userdata
      ;(let [updated-session (assoc session :identity (keyword username) :role "staff" :user userdata )]
      ; (assoc (redirect "/") :session updated-session)
      (println (mail/send-message {:from    "reg@bestway.lk"
                                   :to      [email]
                                   :subject "Hi!"
                                   :body    "User registration"})))

    ;{:code 0, :error :SUCCESS, :message "message sent"}

    (:_id userdata))
  )

(defn user-login-validate-auth-token
  "validate one time authentication code"
  [token request]

  (let [
        userdata (get-user-by-auth-token (str token))
        user (dissoc userdata :password)
        profile-pic (if-let [pic (:profile-pic userdata)] pic "user/default-pic.png")
        session (:session request)
        updated-session
        (assoc session
          ;  :identity (keyword username)
          :identity (str (:_id userdata))
          :fnamelname (str (:first-name userdata) " " (:last-name userdata))
          :role "user"
          :email (:email userdata)
          :user user
          :id (:_id userdata)
          :slogan (:subtitle userdata)
          :profile-pic (get-user-profile-img! (str (:_id userdata)))
          :cookies {"bw-user" ""})

        id (str (:_id userdata))]

    (binding [
              layout/*identity*
              id])

    (assoc (redirect "/home") :session updated-session)
    )

  )



(defn user-login-join [request]
  (let [
        ;remember (find-key request :remme)
        lastname (find-key request :lname)
        email (find-key request :regemail)
        firstname (find-key request :fname)
        password (find-key request :newpwd)
        ;session (:session request)
        userdata (create-user! {:first-name firstname :last-name lastname :username email :password password})]
    ;      (log/info " out " request )
    (if userdata
      ;(let [updated-session (assoc session :identity (keyword username) :role "staff" :user userdata )]
      ; (assoc (redirect "/") :session updated-session)
      (println (mail/send-message {:from    "reg@bestway.lk"
                                   :to      [email]
                                   :subject "Hi!"
                                   :body    "User registration"})))

    ;{:code 0, :error :SUCCESS, :message "message sent"}

    (show-login)))

(defn user-login-get-atoken
  "Confirm user login status with Auth0 server"
  [auth-token]
   (def prom-atoken-res
     (http-client/get
    "https://bestwaylk.eu.auth0.com/userinfo"
    {
     ;:insecure? true
     ;;:content-type "application/json"
    ; :oauth-token "SGLCtENuv53WgZKS"
     :headers
               {
               "authorization", (str "Bearer "  auth-token )
                "cache-control", "no-cache"}
     ;:body "{ \"include_email\": true\"}"
     ;:body "{ \"flags\": { \"enable_dynamic_client_registration\": true } }"
     ;:body (json/write-str {"grant_type" "authorization_code" "client_id" "SUTx9p7MI0gycUaivjSG8Pcv5Z364qC2" "client_secret" "3prl1Hg4khZYQWuM7vb-LNnGVNFDlq7XBZnX8Nv_Rq73pQ8kKUKkPxMOGTLbqFbl" "code" (str auth-code) "redirect_uri" "http://luminus.bestway.lk/auth0"})
     }
    )
     )
  (let [user-info (json/read-json (:body @prom-atoken-res))
        fnamelname (:name user-info)
        email      (:email user-info)
        profile-pic (get user-info :picture_large (:picture user-info))
        family_name (:family_name user-info)
        given-name (:given_name user-info)

        ] {:fnamelname fnamelname :first-name given-name :last-name family_name :email email :profile-pic profile-pic})
  )



(defn location-dropdown
  "Returns the html markup for cities and districts"
  [& item]
  ;(println (get-in (first item) [:location :district] ))
  (let
    [loc (venue-list)]
    {
     :districts    (map #(-> (str "<option data-id='" (:_id %) "' value='" (name (second (keys %1))) "'>" (name (second (keys %1))) "</option>")) loc)

     :cities (map #( let [cities (:city ((second (keys %1)) %1))]

                     (for [city cities] (str "<option "  " data-parent-id='" (:_id %1) "' value='" city "'>" city "</option>") ) )  (venue-list))

     }
    ))






(defn my-profile [request template-page & kv]
  (if (authorized? request)
    (let [
          left-sidebar-items (dissoc (webapp-left-sidebar-items) :_id)]


      (layout/render template-page (conj (first kv) {:left-sidebar-items left-sidebar-items :user (get-in request [:session :user]) :page-link (:uri request)})))

    (show-login)))


(defn dashboard [request template-page kv]
  ;(log/error "Test0#############################################\n" kv )
  ;(log/info request)
  (let
      [
        current-uri (:uri request)
        session (get request :session {:role "visitor"})    ;;; TODO check this line !!!
        allowed (visitor-allowed? request)
        authorized (authorized? request)]



    (if (or allowed authorized)
      (layout/render template-page (merge kv {:pathname current-uri} session))
      (if (= current-uri "/")
        (show-index)
        (redirect "/")))))





(defn user-profile
  "user profile page"
  [request id]
  (dashboard request "front/user_profile.php" {
                                               :user       (find-key request :user)
                                               :identity   (find-key request :identity)
                                               :catz       (map #(-> (str "<option value='" (:_id %1) "'>" (:label %1) "</option>")) (catz-list))
                                               :locations  (venues-list)
                                               ;:user-ads   (user-listings (find-key request :id))
                                               :user-ads   (user-listings id)
                                               :profile    (assoc  (get-publisher-data-by-id id) :profile-pic (get-user-profile-img! id))
                                               :fnamelname (find-key request :fnamelname)}))





(defn my-profile
  "my profile page"
  [request]
  (if (nil? layout/*identity*) (redirect "/")
                               (dashboard request "front/myprofile.php" {
                                                                         :user             (find-key request :user)
                                                                         :identity         (str layout/*identity*)
                                                                         :catz             (map #(-> (str "<option value='" (:id %1) "'>" (:label %1) "</option>")) (catz-list))

                                                                         :locations        (venues-list)
                                                                         :user-ads         (user-listings (find-key request :id))
                                                                         :user-ads-active  (user-listings-active (str layout/*identity*))
                                                                         :user-ads-pending (user-listings-pending (str layout/*identity*))
                                                                         :dispaly-name       (find-key request :display-name)
                                                                         :fnamelname       (find-key request :fnamelname)
                                                                         }))

  )




(defn login [request]
  (let [
        remember (find-key request :remme)
        username (find-key request :username)
        password (find-key request :password)
        session (:session request)
        stat (get-user username password)
        userdata (get-user-data username)
        ]
    ;(log/info " out " (:profile-pic userdata) )
    (if-not (false? stat)
      (let [
            user (dissoc userdata :password)
            profile-pic (if-let [pic (:profile-pic userdata)] pic "user/default-pic.png")
            uid (:_id userdata)
            usettings (get-user-settings (str uid))
            updated-session

            (assoc session
              ;  :identity (keyword username)
              :identity (str (:_id userdata))
              :fnamelname (str (:first-name userdata) " " (:last-name userdata))
              :role "user"
              :email username
              :user user
              :id uid
              :display-name (find-key usettings :display-name)
              :slogan (:subtitle userdata)
              :profile-pic (get-user-profile-img! (:_id userdata))
              :cookies {"bw-user" ""})

            id (str (:_id userdata))]

        (binding [
                  layout/*identity*
                    id])



        (assoc (redirect (str layout/*app-context* "home")) :session updated-session))
        ;         (dashboard)

      (show-login))))




(defn update-user-session-data [property-kw new-value current-request]
  (let
    [property (if (keyword? property-kw) property-kw (keyword property-kw))]
    (assoc (:session current-request) property new-value)))
      ;(assoc (:session (vary-meta (:session current-request) assoc :recreate true)) property new-value)



(defn report-ad
  "Save advertisement reports petitioned against"
  [id request]
  ;;; :status {:reported true } {:reported [{:by-role "visitor" :by "annonymous" :at (DateTime. ) :reason []}]}
  )


(defn send-msg-to-publisher
  "send chat msg to ad publisher"
  [item-id request]

  (let [
        identity layout/*identity*
        subject (find-key request :subject)
        message (find-key request :message)
        user_data (find-key request :send-to)
        ;password (find-key request :newpwd)
        ;session (:session request)
        item (listings-item-details item-id)
        userdata (read-string user_data)
        user_id (:id userdata)

        ]
    (println request)
    (if (and (not (empty? message)) (not (empty?  identity  )))
      ;(let [updated-session (assoc session :identity (keyword username) :role "staff" :user userdata )]
      (let [
            author-id (str identity)
            threadID  (str author-id "_" user_id)
            participants (.split threadID "_")
            msgID (str "m_" (clj-time.core/now))
            authorName (get-in request [:session :display-name])
            to  user_id
            threadName (:title item)
            message-text message
            timestamp (clj-time.core/now)
            data {:threadID threadID
                  :threadName threadName
                  :to (ObjectId. to)
                  :author  (ObjectId. author-id)
                  :authorName authorName
                  :id msgID
                  :text message-text
                  :timestamp timestamp
                  }

            ]
        (save-chat-msg msgID data)

        ;(send! (first @chat-channels) (json/write-str (send-thread)))


        )
      )
    ;{:code 0, :error :SUCCESS, :message "message sent"}
    )
  )

(defn send-email-to-publisher
  "send email to ad publisher"
  [item-id request]

  (let [
        ;remember (find-key request :remme)
        subject (find-key request :subject)
        message (find-key request :message)
        user_id (find-key request :send-to)
        ;password (find-key request :newpwd)
        ;session (:session request)
        item (listings-item-details item-id)
        userdata (get-publisher-data-by-id (ObjectId. user_id))
        email (:email userdata)
        ]

    (if (and (not (empty? message)) (not (empty?  email  )))
      ;(let [updated-session (assoc session :identity (keyword username) :role "staff" :user userdata )]
      (println
        (mail/send-message {:user    "contact" :pass "c87vubVArrJ8kHsWKX" :sender "info@bestway.lk" ;ssl
                            :host    "localhost"
                            }{
                            :from    "info@bestway.lk"
                            :to      [email ]
                            :subject (str "New message - " (:title item) subject)
                            :body [:alternative {:type "text/plain"
                                                 :content (str message " " (:url item))}
                                   ;{:type "text/html" :content (parser/render-file "email-templates/signup.tpl.php" {}) }
                                   ]
                            })
        )
      )
    ;{:code 0, :error :SUCCESS, :message "message sent"}
    )
  )

(defn userjoin [request]

  (let [
        ;remember (find-key request :remme)
        lastname (find-key request :lname)
        email (find-key request :regemail)
        firstname (find-key request :fname)
        password (find-key request :newpwd)
        display-name (str firstname " " lastname)
        userdata (cond
                   (or (= firstname "") (= lastname "")) "Please type in your firstname & lastname"
                   (= email "") "Please enter your email address"
                   (false? (email-valid? email) ) "Please enter a valid email address"
                   (= password "") "Password cannot be blank. Please enter minimum of 6 characters."
                   (true? (or (true? (user-exists-email? email)) (true? (user-exists-username? email)))) "Your email address already registered. Please login."
                   :else (create-user! {:first-name firstname :last-name lastname :display-name display-name  :username email :password password}))
        ]
    ;      (log/info " out " request )
    (if (string? userdata)
      {:type "error" :msg userdata}
      (do
        ;(mail/send-message
        ;  {:user "receptionist" :pass "owflsYBIERjEEByLF6" :sender "receptionist@bestway.lk" ;ssl
        ;   :host "localhost"
        ;   }
        ;  {:from    "receptionist@bestway.lk"
        ;   :to      [email]
        ;   :subject "Ayubowan! Wannakkam! Welcome!"
        ;   :body    [:alternative {:type    "text/plain"
        ;                           :content "Welcome to bestway.lk"}
        ;             {:type "text/html" :content (parser/render-file "email-templates/signup.tpl.php"
        ;                                                             {
        ;                                                              :auth-url layout/*app-context* "auth/" (user-login-generate-auth-token (:_id userdata))
        ;                                                              :base-url layout/*app-context*
        ;                                                              }
        ;                                                             )}]
        ;   })
        (sh "php" (str mail-script-path "sendmail-registration.php") (str firstname) (str email) (str layout/*app-context* "auth/" (user-login-generate-auth-token (:_id userdata))))
        {:type "success" :msg "User successfully registered."}
        )

      )



    ;(show-login)
    ))



 (defn logout [req]
    (assoc (response/redirect "/") :session nil))





;;=============================================================================;;
;;========================Ad Search============================================;;
;;=============================================================================;;

(defn search-results [request]
  (try
    (let [
          txt-query (get request :query "")
          id-m-cat (get-main-category (find-key request :main-cat ))
          main-cat (if (false? id-m-cat) "any" (:id id-m-cat))
          id-s-cat (get request :sub-cat "any")
          sub-cat (if (some (partial = id-s-cat) (map :_id (:children id-m-cat) )) id-s-cat "any")
          id-loc-dist (get request :loc-district "any")
          max-price (get request :price-max 100000.0)
          min-price (get request :price-min 0.0)
          page (get request :page 1)
          limit (get request :perpage 10)]
      ;(search-listing-by-txt-cat-subcat-loc-price "" "any" "any" "any" "0.0" "10000.0" "1" "10")
      (search-listing-by-txt-cat-subcat-loc-price txt-query main-cat sub-cat id-loc-dist min-price max-price page limit)
     )
    (catch NullPointerException e
      (.printStackTrace e)
      (search-listing-by-txt-cat-subcat-loc-price "" "any" "any" "any" "0.0" "10000.0" "1" "10")
      ))

  )






;(defn home-page-listings [request]
;  (let [
;        txt-query (find-key request :query)
;        id-m-cat (find-key request :main-cat)
;        id-loc-dist (find-key request :loc-district)
;        max-price (find-key request :price-max)
;        min-price (find-key request :price-min)
;        page (find-key request :page)
;        limit (find-key request :perpage)]
;
;
;
;    (search-listing-by-txt-cat-subcat-loc-price txt-query id-m-cat "" id-loc-dist min-price max-price page limit))
;  )



(defn home-news-listings [
                          request
                          ]

   (let [
         lng (find-key request :lng)
      ]

 (case lng
    "sin" (get-sin-news)
    "eng" (get-eng-news)
    "tam" (get-tam-news)
    (get-sin-news)
   )
 )

)

(defn chat-messaging  [
                 request
                 ]

  (println "here" request)
  ["message" "Here we go"])

;;=============================================================================;;
;;========================WebSocket Functions==================================;;
;;=============================================================================;;

;; NEW SENTE Funcs
;(let [{:keys [chat-messaging send-fn connected-uids
;              ajax-post-fn ajax-get-or-ws-handshake-fn]}
;      (sente/make-channel-socket! (get-sch-adapter) {})]
;
;  (def ring-ajax-post                ajax-post-fn)
;  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
;  (def ch-chsk                       chat-messaging) ; ChannelSocket's receive channel
;  (def chsk-send!                    send-fn) ; ChannelSocket's send API fn
;  (def connected-uids                connected-uids) ; Watchable, read-only atom
;  )


;; Previous working functions
(def chat-channels (atom {}))

(defonce news-channels (atom #{}))

(defonce channels (atom #{}))

(defn build-annotations [coll]
  (reduce (fn [m [k & vs]]
            (assoc m k (conj vs)))
          {} coll))

(defn ws-chat-connect! [channel request]
  (println "channel open" {(get-in request [:params :uid])  channel})
  (let [kw-uid (keyword (get-in request [:params :uid]))]
        (if-not (nil? kw-uid)
          (if (nil? (get @chat-channels kw-uid nil))
              (swap! chat-channels assoc kw-uid channel )
            )
          )
  )
  )



(defn ws-chat-disconnect! [channel request status]
  (log/info "channel closed:" status)
  (swap! chat-channels dissoc (keyword (get-in request [:params :uid]))))

(defn ws-news-connect! [channel]
  (log/info "channel open")
  (swap! news-channels conj channel))

(defn ws-connect! [channel]
  (log/info "channel open")
  (swap! channels conj channel))

(defn ws-news-disconnect! [channel status]
  (log/info "channel closed:" status)
  (swap! channels #(remove #{channel} %)))

(defn ws-disconnect! [channel status]
  (log/info "channel closed:" status)
  (swap! channels #(remove #{channel} %)))

(defn ws-search-handler [msg]
  (println "got message for search page:" msg)
  ;(println (type (clojure.walk/keywordize-keys (clojure.edn/read-string msg))))
  (let [

        json-msg-map (clojure.edn/read-string  msg)

        msg-map-post (clojure.walk/keywordize-keys json-msg-map)

        search-results (search-results msg-map-post)
        ;tester2 (println search-results)
        listing-results (json/write-str search-results)]


    ;(log/info listing-results)
    ; (log/info " prep " msg-map-prep)
    ;(log/info " post " msg-map-post)
    ;(log/info "sending message:" listing-results)
    (doseq [channel @channels]
      (send! channel listing-results))))


(def raw-data
         [
          {
           :id "m_1"
           :threadID "t_1"
           :threadName "Jing and Bill"
           :authorName "Bill"
           :text "Hey Jing want to give a Flux talk at ForwardJS?"
           :timestamp (- 111231 99999)
           }
          {
           :id "m_2"
           :threadID "t_1"
           :threadName "Jing and Bill"
           :authorName "Bill"
           :text "Seems like a pretty cool conference."
           :timestamp (- 111231 89999)
           }
          {
           :id "m_3"
           :threadID "t_1"
           :threadName "Jing and Bill"
           :authorName "Jing"
           :text "Sounds good.  Will they be serving dessert?"
           :timestamp (- 111231 79999)
           }
          {
           :id "m_4"
           :threadID "t_2"
           :threadName "Dave and Bill"
           :authorName "Bill"
           :text "Hey Dave want to get a beer after the conference?"
           :timestamp (- 111231 69999)
           }
          {
           :id "m_5"
           :threadID "t_2"
           :threadName "Dave and Bill"
           :authorName "Dave"
           :text "Totally!  Meet you at the hotel bar."
           :timestamp (- 111231 59999)
           }
          {
           :id "m_6"
           :threadID "t_3"
           :threadName "Functional Heads"
           :authorName "Bill"
           :text "Hey Brian are you going to be talking about functional stuff?"
           :timestamp (- 111231 49999)
           }
          {
           :id "m_7"
           :threadID "t_3"
           :threadName "Bill and Brian"
           :authorName "Brian"
           :text "At ForwardJS?  Yeah of course.  See you there!"
           :timestamp (- 111231 39999)
           }] )

(defn send-thread
  [id]
    (into [] conj (get-threads-by-sender-receiver id) )

    ;(get-thread-by-author-id id)
    ;raw-data
  )

(defn ws-chat-message-handler [msg]
  (def input (clojure.walk/keywordize-keys (json/read-str (clojure.string/replace msg "#inst" ""))))
  ;(send! ( @chat-channels (keyword (str (:uid input) )) ) (json/write-str {"check" "Out"}))

  (println "got message for chat:" input)
  ;(println " : " (json/write-str (send-thread (:uid input))))
      (println "chat channels:" @chat-channels)

(if-not (= (:cmd input) "KA")
(do
  (if (= (:cmd input) "GET")
    (send! (get @chat-channels (keyword (:uid input) )) (json/write-str (send-thread (:uid input))))
  (let [
        ;json-msg (json/read-str msg)

        ;msg-prep (clojure.string/replace msg "#inst" "")
        msg-map-post input

        ;msg-map (clojure.walk/keywordize-keys (json/read-str  msg))
        ;msg-map (clojure.walk/keywordize-keys (json/read-str  msg))
        author-id (:author-id (:message msg-map-post))
    ;    listing-results (json/write-str (chat-messaging msg-map-post))]
         listing-results (json/write-str (send-thread author-id))
        message (first msg-map-post)
        threadID  (:threadID msg-map-post)
        participants (.split threadID "_")
        msgID (:id (:message msg-map-post))
        authorName (:authorName (:message msg-map-post))
        to  (nth participants (- 1 (.indexOf (vec participants) author-id)))
        threadName (:threadName msg-map-post)
        message-text (:text (:message msg-map-post))
        timestamp (:date (:message msg-map-post))
        data {:threadID threadID
              :threadName threadName
              :to (ObjectId. to)
              :author  (ObjectId. author-id)
              :authorName authorName
              :id msgID
              :text message-text
              :timestamp timestamp
              }

        ]
    (save-chat-msg msgID data)

    ;(send! (first @chat-channels) (json/write-str (send-thread)))


    )
  ) )  )    ) 
  

;(defn ws-home-listing-handler [msg]
;  (log/info "got message for home page:" msg)
;  (let [
;        json-msg (json/read-str msg)
;        msg-map-prep (clojure.walk/postwalk #(if (= "" %) (str " ") %) json-msg)
;        msg-map-post (clojure.walk/keywordize-keys msg-map-prep)
;
;        ;msg-map (clojure.walk/keywordize-keys (json/read-str  msg))
;        ;msg-map (clojure.walk/keywordize-keys (json/read-str  msg))
;
;        listing-results (json/write-str (home-page-listings msg-map-post))]
;
;    ;(log/info json-msg)
;    ; (log/info " prep " msg-map-prep)
;    ;(log/info " post " msg-map-post)
;    (log/info "sending message:" listing-results)
;    (doseq [channel @channels]
;      (send! channel listing-results))))



(defn ws-news-listing-handler [msg]
  (log/info "got message for home page:" msg)
  (let [
        json-msg-map (clojure.edn/read-string  msg)

        msg-map-post (clojure.walk/keywordize-keys json-msg-map)

        ;search-results (home-news-listings msg-map-post)

        ;msg-map (clojure.walk/keywordize-keys (json/read-str  msg))
        ;msg-map (clojure.walk/keywordize-keys (json/read-str  msg))

        listing-results (json/write-str (home-news-listings msg-map-post))
        ]

    ;(log/info json-msg)
    ; (log/info " prep " msg-map-prep)
    ;(log/info " post " msg-map-post)
    (log/info "sending message:" listing-results)
    (doseq [channel @news-channels]
      (send! channel listing-results))))


;(defn ws-handler [request]
;  (with-channel request channel
;                (ws-connect! channel)
;                (on-close channel (partial ws-disconnect! channel))
;                (on-receive channel #'ws-search-handler ) ))


(defn check-user-messages
  ""
  []
  (def dcur  (user-messages-cursor))
          (while (.hasNext dcur)
         (println "HERE-0")
          (let
              [ msg-item    (.next dcur)
               author-id (str (get msg-item "author"))
               threadID  (str (get msg-item "threadID"))
               participants (.split threadID "_")
               to (nth participants (- 1 (.indexOf (vec participants) author-id)))
               ]
            (if-not (nil? (find-key  @chat-channels (keyword (str to) )))
              (send! (get @chat-channels (keyword (str to) ) ) (json/write-str (send-thread to)))
              )
            )
        )
    (.join (Thread/currentThread)         )
  )

;;=======================================================================;;

;(doto
;  (Thread. check-user-messages "CheckUserMessages")
;  (.setDaemon true)
;  (.start))

;;=============================================================================;;
;;========================Todos Functions==================================;;
;;=============================================================================;;



