(ns bestway.misc.watermark
  "This is an app that takes images from a folder and applies a watermark"
  (:require [clojure.java.io :as io])
  (:require [clojure.java.shell :as shell])
  (:require [clojure.string :as string]
            [clojure.tools.logging :as log])
  ;(:gen-class)
  (:import (javax.imageio ImageIO)
           (java.awt.image BufferedImage)))

; GLOBALS
(def base-dir "uploads/"
  ;"The location of the watermark file"
  )

(def watermark-file
  "The watermark image itself"
  (io/file (str base-dir "watermark.png")))

(def unprocessed-folder
  "The location of the unprocessed images to be watermarked"
  (str base-dir "preprocess"))

; FUNCTIONS
(defn parse-dimensions
  "Parse '123x2345' type of strings into a pair of ints"
  [point]
  (map read-string (string/split point #"x")))

(defn filename-from-path
  "Get the file name out of a path"
  [path]
  (.getName (io/file path) )
  ;(last (string/split path #"/"))
  )

(defn file-ext
  "Get the extension of a file"
  [file]
  (string/lower-case (last (string/split file #"\."))))

(defn is-img
  "Check if the extension of the file matches that of an image"
  [ext]
  (not= -1 (.indexOf ["jpg" "jpeg" "png" "gif"] ext)))

(defn list-files-from-folder
  "List all files recursively in a folder"
  [folder]
  (file-seq (io/file folder)))

;
(defn calculate-watermark
  "Calculates the position and size of the watermark in relation to the image"
  [image watermark watermark-ratio]
  (let [image-path   (get image :path)
        image-width  (get image :width)
        image-height (get image :height)
        width        (* image-width watermark-ratio)
        height       (* image-height watermark-ratio)
        left         (- image-width (/ width 1.5))
        top          (- image-height (- image-height height))]
    {:watermark (get watermark :path)
     :image image-path
     :width width
     :height height
     :left left
     :top top}))

;(defn identify-width-and-height
;  "Get the width and height of an image"
;  [file-path]
;
;  (let [params ["identify" "-format" "%[fx:w]x%[fx:h]" file-path]]
;    (parse-dimensions (:out (apply shell/sh params)))))

(defn identify-width-and-height
  "Get the width and height of an image"
  [file-path]
  (let[
       f (clojure.java.io/file file-path)
       i (ImageIO/read f)
       ]
    ;{:width (.getWidth i) :height (.getHeight i)}
    [ (.getWidth i) (.getHeight i) ]
    )
  )

(defn composite-watermark
  "Put a watermark on the bottom right of the image"
  ;[watermark-path image-path & [outfile]]
  [watermark-path img-path out-dir]
  (log/debug img-path)
  (let[
       fo (clojure.java.io/file img-path)
       fw (clojure.java.io/file watermark-file)
       io (ImageIO/read fo)
       iw (ImageIO/read fw)
       wo   (.getWidth io)
       ho   (.getHeight io)
       combined (BufferedImage. wo ho BufferedImage/TYPE_BYTE_INDEXED)
       g (.getGraphics combined)
       file-name (filename-from-path img-path)
       out-file (clojure.string/replace file-name #"\..+{3,4}" ".png")
       ]
    (.drawImage g io 0 0 nil)
    (.drawImage g iw 0 0 wo ho nil)
    (ImageIO/write combined "PNG" (clojure.java.io/file (str out-dir out-file ) ))
    ;(println g)
    )

  )

(defn get-image-info [filename]
  "Get the details of an image like width and height"
  (let [dimensions (identify-width-and-height filename)]
    {:path filename
     :width (first dimensions)
     :height (second dimensions)}))

(defn list-images-from-folder
  "Returns a list of files and dimensions ((filepath width height) &)"
  [folder]
  (->> (list-files-from-folder folder)
       ; Make sure all items are files and the extension are of images
       (filter #(and (.isFile %) (is-img (file-ext (.getName %)))))
       ; Change output to (filepath:string width:int height:int)
       (map #(get-image-info (.getAbsolutePath %)))))

; ENTRY
(defn wm-init
  "Main entrance"
  [out-dir args]
  (let [watermark-path (.getAbsolutePath watermark-file)
        watermark-info (get-image-info watermark-path)
        ;image-list (list-images-from-folder unprocessed-folder)
        image-list args
        images (map #(calculate-watermark % watermark-info 1/2) image-list)]
    ;(doall   (map #(apply composite-watermark (vals %)) images)    )
    (composite-watermark images image-list out-dir)
    )
  ;(shutdown-agents)
  )

