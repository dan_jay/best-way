(ns bestway.handler
  (:require [compojure.core :refer [defroutes routes wrap-routes]]
            [bestway.layout :refer [error-page *identity*]]
            [bestway.app.front.core :refer [check-user-messages]] ;;CAUTION ************************************* !!!
    ;[bestway.routes.cms.admin :refer [admin-routes admin-login-route]]
            [bestway.routes.front.core :refer [front-routes front-login-route ws-routes news-routes asyn-chat-routes]]
    ;;  [adwiz.routes.cms.core :refer [resta-routes resta-login-route]]
    ;; [adwiz.routes.front.core :refer [front-routes front-login-route]]
    ;[bestway.routes.front.core :refer [front-routes]]
            [chord.http-kit :refer [wrap-websocket-handler]]
            [org.httpkit.server :refer [with-channel run-server on-close send!]]
            [clojure.core.async :as a]
            [compojure.route :as route]
            [clojure.tools.logging :as log]
    ; [luminus.logger :as logger]
    ;[taoensso.timbre.appenders.rotor :as rotor]
            [selmer.parser :as parser]
            [bestway.env :refer [defaults]]
            [mount.core :as mount]

            [bestway.middleware :as middleware]
            [environ.core :refer [env]]
            [bestway.config :as conf]

            [clojure.tools.nrepl.server :as nrepl]
            [bestway.layout :as layout]))


(defonce nrepl-server (atom nil))



(mount/defstate init-app
                :start ((or (:init defaults) identity))
                :stop  ((or (:stop defaults) identity)))

(defn start-nrepl
  "Start a network repl for debugging when the :repl-port is set in the environment."
  []
  (when-let [port (conf/bwconf :repl-port)]
    (try
      (reset! nrepl-server (nrepl/start-server :port port))
      (log/info "nREPL server started on port" port)
      (catch Throwable t
        (log/error "failed to start nREPL" t)))))

(defn stop-nrepl []
  (when-let [server @nrepl-server]
    (nrepl/stop-server server)))

(defn start-news-watcher
  "periodical update catcher for news from RSS feeds"
  []


  )

(defn start-messages-listener []
(doto
  (Thread. check-user-messages)
  (.setDaemon true)
  (.start))
  (def message-queue (future check-user-messages))
)

(defn init
  "init will be called once when
   app is deployed as a servlet on
   an app server such as Tomcat
   put any initialization code here"
  []


  (if (conf/bwconf :dev) (parser/cache-off!))
  (start-nrepl)
  ;;start the expired session cleanup job
  ;(session/start-cleanup-job!)
  (log/info "\n-=[ Bestway started successfully"
               (when (conf/bwconf :dev) "using the development profile") "]=-")


  (start-messages-listener)
  )


(defn destroy
  "destroy will be called when your application
   shuts down, put any clean up code here"
  []
  (log/info "bestway is shutting down...")
  (stop-nrepl)
  (log/info "shutdown complete!"))




(def app-routes
  (routes
    ws-routes
    asyn-chat-routes
    (wrap-routes #'front-routes middleware/wrap-csrf )
    (wrap-routes #'front-routes middleware/wrap-auth )
    (wrap-routes #'front-routes middleware/wrap-restricted)
    ;(wrap-routes #'ws-handler wrap-websocket-handler)
    (wrap-routes #'front-login-route middleware/wrap-csrf)
    (wrap-routes #'front-login-route middleware/wrap-auth-control)
    ;(middleware/wrap-restricted  #'front-routes)
    ;(middleware/wrap-auth #'front-routes)


    (wrap-routes #'news-routes middleware/wrap-csrf)

    (route/not-found
      (:body
        (error-page {:status 404
                     :title "page not found"})))))


;(defn app [& req]  (middleware/wrap-base #'app-routes))
(def app (middleware/wrap-base #'app-routes))
  

