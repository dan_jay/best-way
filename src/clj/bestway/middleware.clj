(ns bestway.middleware
  (:require [bestway.env :refer [defaults]]
            [clojure.tools.logging :as log]
            [bestway.layout :refer [*app-context* *app-base-location* *websock-context* *tmp-path* *profile-pic-upload-path*
                                    *listing-pic-upload-path*
                                    error-page
                                    *identity*
                                    *visitor-allowed-urls*]]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [ring.middleware.webjars :refer [wrap-webjars]]
            [ring.middleware.format :refer [wrap-restful-format]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.session.store]

            [monger.ring.session-store :refer [monger-store]]

            [ring.middleware.reload :refer [wrap-reload]]

            [bestway.config :as conf]
    ;[environ.core :refer [env]]
            [chord.http-kit :refer [wrap-websocket-handler]]

            [ring-ttl-session.core :refer [ttl-memory-store]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth :refer [authenticated?]]
            [monger.core :as mg])
  (:import [javax.servlet ServletContext]))

(use 'bestway.util)

(defn wrap-context [handler]
  (fn [request]
    (binding [*app-context* ;(find-key request :host )
              ;"http://localhost:8088/"
              ;(:app-context conf/bwconf)
              (:http-url conf/bwconf)
                ;(if-let [context (:servlet-context request)]
                ;; If we're not inside a servlet environment
                ;; (for example when using mock requests), then
                ;; .getContextPath might not exist
                ;(try (.getContextPath ^ServletContext context)
                ;     (catch IllegalArgumentException _ context))
                ;; if the context is not specified in the request
                ;; we check if one has been specified in the environment
                ;; instead
                ;(:app-context conf/env))
              *websock-context*   (:ws-url conf/bwconf)
              *tmp-path*   (:tmp conf/bwconf)
              *profile-pic-upload-path*   (:profile-pic-uploads conf/bwconf)
              *listing-pic-upload-path*   (:listing-pic-uploads conf/bwconf)
              *app-base-location* (:app-base-location conf/bwconf)
              *visitor-allowed-urls* (:public-front-urls conf/bwconf)

              ]


      (handler request))))
    ;(log/info request)


(defn wrap-internal-error [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable t
        (log/error t)
        (error-page {:status 500
                     :title "Something very bad has happened!"
                     :message "We've dispatched a team of highly trained gnomes to take care of the problem."})))))

(defn wrap-csrf [handler]
  (wrap-anti-forgery
    handler
    {:error-response
     (error-page
       {:status 403
        :title "Invalid anti-forgery token"})}))

(defn wrap-auth-control [handler]
  (fn [request]
     ;(restrict handler {:handler authenticated?})
    (handler request)))




(defn wrap-formats [handler]
  (let [wrapped (wrap-restful-format
                  handler
                  {:formats [:json-kw :transit-json :transit-msgpack]})]
    (fn [request]
      ;; disable wrap-formats for websockets
      ;; since they're not compatible with this middleware
      ((if (:websocket? request) handler wrapped) request))))

(defn on-error [request response]
  (error-page
    {:status 403
     :title (str "Access to " (:uri request) " is not authorized")}))

(defn wrap-restricted [handler]
  (restrict handler {:handler authenticated?
                     :on-error on-error})

 (defn wrap-restricted-admin [handler]
   (restrict handler {:handler {:and [authenticated? is-admin?]}  :on-error on-error})))


(defn wrap-restricted-index [handler]
  (restrict handler {:handler {:and [authenticated? is-admin?]}  :on-error on-error}))

(defn wrap-restricted [handler]
  (restrict handler {:handler authenticated?  :on-error on-error}))

(defn wrap-identity [handler]
  (fn [request]
    (binding [*identity* (get-in request [:session :identity])]
      (handler request))))

(defn wrap-auth [handler]
  (let [backend (session-backend)]
    (-> handler
        wrap-identity
        (wrap-authentication backend)
        (wrap-authorization backend))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-auth
      wrap-webjars

      (wrap-defaults
        (-> site-defaults
            (assoc-in [:security :anti-forgery] false)

            (assoc :session {:store (let [conn  (mg/connect) db  (-> "mongodb://127.0.0.1/zda_dev" mg/connect-via-uri :db)]
                                                                        (monger-store db "bw_sessions"))})
            (assoc-in [:session :cookie-name] "bestway-lk-web")
            (assoc-in [:session :store] (ttl-memory-store 13600))
            ))

      wrap-context
      wrap-reload
      wrap-internal-error
      ))
