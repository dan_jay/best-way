(ns bestway.util
  (:require [clojure.tools.logging :as log]
            [cheshire.core :refer [generate-string]]
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.hashers :as hashers]
            [buddy.core.hash :as hash :refer [digest]]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [bestway.layout :refer [*tmp-path*]]
            [bestway.misc.watermark :as wm]
            [pantomime.mime :refer [mime-type-of]]
            [clojure.data.codec.base64 :as b64]
    ;[base64-clj.core :as base64]
            [monger.joda-time :as mgjoda]
            [clj-time.core :as t]
            [clj-time.format :as fmt]
            [clojure.java.io :as io]
            [cognitect.transit :as transit]
            [pandect.algo.sha256 :as pansha1 :refer :all]
            [bestway.layout :as layout]
            [clojure.spec.alpha :as s]
            [clout.core :as clout] ;;; match URLs with config.edn
            [clojure.string :as string])
  (:import [java.io File FileInputStream FileOutputStream PrintWriter ByteArrayOutputStream]
           (org.bson.types ObjectId)
           (java.util Date)
           (java.net URLDecoder)
           (org.joda.time DateTime)
           (javax.imageio ImageIO)
           (java.awt.image BufferedImage))
  )




;(def uploadpath *tmp-path* )
; (defn is-admin? [req]
;   (log/error (Exception. "user-exists") (str req) )
;   (contains? (apply hash-set (:role req)) "admin")
;  )

(extend-protocol json/JSONWriter
  ObjectId
  (-write [^ObjectId object out]
    (json/write (.toString object) out)))

(extend-protocol json/JSONWriter
  Date
  (-write [^Date object out]
    (json/write (.toString object) out)))

(def regex-url #"https?:\/\/www\.?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&//=]*")

(defn tr-write [x]
  (try
    (let [baos (ByteArrayOutputStream.)
          w    (transit/writer baos :json)
          _    (transit/write w x)
          ret  (.toString baos)]
      (.reset baos)
      ret)
    (catch Exception e
      ;(.printStackTrace e)
      ))
)


(defn find-key [m k]
  (loop [m' m]
    (when (seq m')
      (if-let [v (get m' k)]
        v
        (recur (reduce merge
                       (map (fn [[_ v]]
                              (when (map? v) v))
                            m')))))))

(defn is-admin? [req]
  (= (find-key req :role) "admin")
 )

(defn authorized? [request]
  (if (and
        (= (get-in request [:session :role]) "user")
        (get-in request [:session :identity])
        (authenticated? request))

    true false))

(defn item-url[url]
  (let [path (.getPath (java.net.URL. url))
        ]
    (if (string? (re-matches #"^/item/[0-9a-zA-Z]+" path)) true false)
    )
  )


(def visitor-routes (for [var layout/*visitor-allowed-urls*]
                      (clout/route-compile var)
                      ))

(defn visitor-allowed?
  "Using Clout library"
  [request]
  (if (empty? (filter #(clout/route-matches % request)  layout/*visitor-allowed-urls*))
    false
    true))

(defn -visitor-allowed? [request]
  (if (and (not (nil? layout/*visitor-allowed-urls*)) (.contains layout/*visitor-allowed-urls* (str "/" (second (.split (:uri request) "/")))))
    true
    false))



(defn make-response [status value-map]
  {:status status
   :headers {"Content-Type" "application/json"}
   :body (generate-string value-map)})

(defn make-json-response [status value-map]
  {:status status
   :headers {"Content-Type" "application/json"}
   :body (generate-string value-map)})

(defn make-html-response [status body]
  {:status status
   :headers {"Content-Type" "text/html"}
   :body body})

(defn file-name [full-path-to-file]
  ;(URLDecoder/decode
  ;  (str path File/separator filename)
  ;  "utf-8")
    (.getName (io/file full-path-to-file) )
  )

(defn file-path [path & [filename]]
  (URLDecoder/decode
    (str path File/separator filename)
    "utf-8"))

(defn getImageDimensions [file-path]
  (let[
       f (clojure.java.io/file file-path)
       i (javax.imageio.ImageIO/read f)
       ]
    {:width (.getWidth i) :height (.getHeight i)}
    )
  )

(defn password-eligible?
  "check whether the given password meets the requirements"
  [password]
  (> (.length password) 5)
  )

(defn copy-file [source-path dest-path]
  (io/copy (io/file source-path) (io/file dest-path)))

(defn move-file [source-path dest-path]
  (io/copy (io/file source-path) (io/file dest-path) )
  (io/delete-file source-path true)
  )

(defn get-user-profile-img!
  "Returns the img URL of the image. Generates a default image in *profile-pic-upload-path* if an image not exists for the user id"
  [id]
  (let [f (io/file (str layout/*profile-pic-upload-path* id ".jpg"))]
    (if-not (.exists f) (copy-file (str layout/*profile-pic-upload-path* "default.jpg") (str layout/*profile-pic-upload-path* id ".jpg")))
    (str layout/*app-context* "user/" id ".jpg?n=" (rand-int 4))
    )
  )
(defn file-mime [path-file & req-type]
  (let
        [
         mime (mime-type-of (clojure.java.io/file (file-path path-file)))
         ]
  (if (and (= (first req-type) :image)
  (case mime
      "image/jpg" true
      "image/jpeg" true
      "image/png" true
      "image/gif" true
      "image/bmp" true
          false) )
    {:result true :extension (str/replace mime "image/" "")  }
     {:result false :extension nil}
    )
  ;(log/info (mime-type-of (File. (file-path path-file))))

  ) )

(defn present?
  "Returns false if x is nil or blank, true otherwise.(weavejester/valip)"
  [x]
  (not (string/blank? x)))

(defn email-valid?
  "Returns true if the email address is valid, based on RFC 2822. Email
  addresses containing quotation marks or square brackets are considered
  invalid, as this syntax is not commonly supported in practise. The domain of
  the email address is not checked for validity.
  (weavejester/valip)
  "
  [email]
  {:pre [(present? email)]}
  (let [re (str "(?i)[a-z0-9!#$%&'*+/=?^_`{|}~-]+"
                "(?:\\.[a-z0-9!#$%&'*+/=?" "^_`{|}~-]+)*"
                "@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+"
                "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
    (boolean (re-matches (re-pattern re) email))))

(defn img-to-jpeg
  "convert image to jpeg"
    [img-path out-file]
  ;(log/debug img-path)
  (let[
       fo (clojure.java.io/file img-path)

       io (ImageIO/read fo)

       wo   (.getWidth io)
       ho   (.getHeight io)

       combined (BufferedImage. wo ho BufferedImage/TYPE_BYTE_INDEXED)

       g (.getGraphics combined)

       file-name (file-name img-path)
       ]
    (.drawImage g io 0 0 wo ho nil)
    (ImageIO/write combined "JPEG" (clojure.java.io/file out-file ))
    ;(println g)
    )
)

;(defn handle-upload [path {:keys [upload-file size tempfile] :as params}]
(defn handle-upload [upload-file filename-prefix]
  ;; lets slow things down so that we can see an upload indicator
  ;[path {:keys [tempfile size filename]}]
  ;upload-file size tempfile
  (println upload-file)
  ;(log/info (find-key upload-file :filename) )

  (let [
   ;upload-file (find-key request :upload-file)
   size (find-key upload-file :size)
   tempfile (find-key upload-file :tempfile)
   ext-check (file-mime tempfile :image)
   ext-passed? (:result ext-check)
   ext-type (:extension ext-check)
   ;filename (find-key upload-file :filename)
   ;generated-name (str (quot (System/currentTimeMillis) 1000) "-" (str (pansha1/sha1  (File. tempfile))) "." ext-type)
    generated-name (str filename-prefix "-" (str (pansha1/sha256  (clojure.java.io/file tempfile))) "." ext-type)

        ]
 ;(log/info (file-mime tempfile :image))
  ;(Thread/sleep 5000)

  (cond
    ;; Seems ajax-cljs sets filename to null when no file selected prior to
    ;; clicking upload button, but IframeIo sends ""
    (not ext-passed?)
    (make-response 400 {:status "ERROR"
                        :message (str "Invalid File type"
                                      " - Not apply here")})
    (or (not upload-file)
        (= "" upload-file))
    (make-response 400 {:status "ERROR"
                        :message "No file parameter sent"})
    (< size 100)
    (make-response 400 {:status "ERROR"
                        :message (str "File less than 100 bytes"
                                      " - Can't be bothered")})
    (>= size 100)
    (try
      (with-open [in (new FileInputStream (file-path tempfile))
                  out (new FileOutputStream (str *tmp-path* "" generated-name))]
        (let [source (.getChannel in)
              dest   (.getChannel out)]
          (.transferFrom dest source 0 (.size source))
          (.flush out))
    ;(make-response 200 {:status "OK"
    ;                    :filename (str "resources\\uploads\\" media-type "\\" generated-name)
    ;                    :size (or size 0)
    ;                    ;:tempfile (str tempfile)
    ;                    }
    ;                    )

         {                  :status "OK"
                            :filename (str *tmp-path* "" generated-name)
                            :size (or size 0)
                            :tempfile (str tempfile)
          }


        )
      (catch Exception e
        (log/error e)
        (make-response 400 {:status "ERROR"
                              :message "Unexpected Error"}) )
      ))


    ) )

(defn string-to-base64-string
   "s -string to turn into base64"
   [s]
   ;(String.
   (b64/encode
     (.getBytes s)
     )
   ;(base64/encode  s )
   ;"UTF-8"
   ;"UTF-8"
   ;)
   )

(defn byte-array-to-base64-string
  "b - byte array to turn into base64"
  [b]
  (String.
  (b64/encode
    b
    )
  ;(base64/encode  s )
  ;"UTF-8"
  "UTF-8"
  )
  )

(defn slurp-into-bytes
  "Slurp the bytes from a a file
  f - path + file-name.ext
  "
  [f]
  (with-open [out (ByteArrayOutputStream.)]
    (clojure.java.io/copy (clojure.java.io/input-stream f) out)
    (.toByteArray out)))


(defn image-encode-base64 [original-image]
  (str "data:image/"
       (:extension (file-mime original-image :image)) ";base64,"
       (byte-array-to-base64-string
         (slurp-into-bytes original-image))
       )
  )

(defn make-ad-item-url
  "create the URL for an ad listing page"
  [title]
  (-> title
      (string/lower-case)
      (string/trim)
      (string/replace #"[\s]" "-")
      (str "-" (rand-int 3))
      )
  )



(defn add-watermark! [ out-dir img-files]
  (wm/wm-init out-dir img-files)
  )

(defn get-time-gap [time]
     (let [units [{:name "second" :limit 60 :in-second 1}
                {:name "minute" :limit 3600 :in-second 60}
                {:name "hour" :limit 86400 :in-second 3600}
                {:name "day" :limit 604800 :in-second 86400}
                {:name "week" :limit 2629743 :in-second 604800}
                {:name "month" :limit 31556926 :in-second 2629743}
                {:name "year" :limit nil :in-second 31556926}]
         diff
           ;(if (and (= (count time) 1) (not (zero? (count time))) (<= (count time) 2))
                (t/in-seconds (t/interval (DateTime. time) (DateTime. (t/now))))
                ;(t/in-seconds (t/interval (DateTime. (first time)) (DateTime. (second time)))))
           ]
     (if (< diff 5)
       "just now"
       (let [unit (first (drop-while #(or (>= diff (:limit %))
                                          (not (:limit %)))
                                     units))]
         (-> (/ diff (:in-second unit))
             Math/floor
             int
             (#(str % " " (:name unit) (when (> % 1) "s") " ago")))))))