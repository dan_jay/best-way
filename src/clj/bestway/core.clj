(ns bestway.core
  (:require [bestway.handler :as handler]
            [luminus.repl-server :as repl]
            [luminus.http-server :as http]
            [org.httpkit.server :as http-kit]
            [ring.middleware.reload :as reload]
            [environ.core :refer [env]]
            [bestway.config :as conf ]
            [mount.core :as mount]
            [clojure.tools.logging :as log]
            ;[luminus.logger :as logger]
            [bestway.db.core :as database]
            )
  (:gen-class)
  )

;contains function that can be used to stop http-kit server
;(defonce server-ws (atom nil))
;
;(defonce server (atom nil))


;
;(defn start-server-ws [port]
;  ;(handler/init)
;  (reset! server-ws
;          (http-kit/run-server
;            ;(if (conf/bwconf :dev) (reload/wrap-reload #'handler/app) (handler/app) )
;            (handler/app)
;            {:port port}))
;)

(mount/defstate ^{:on-reload :noop}
                server-ws
                :start
                (http/start
                  { :handler handler/app
                   :port 8091
                   })
                :stop
                (http/stop server-ws))

;
;(defn start-server [port]
;  ;(handler/init)
;  (reset! server
;          ;(http-kit/run-server
;          (http/start {:handler  (handler/app) :host "http://localhost" :port port})
;            ;(if (conf/bwconf :dev) (reload/wrap-reload #'handler/app) (handler/app) )
;            ;(handler/app)
;            ;{:port port})
;             )
;)


(mount/defstate ^{:on-reload :noop}
                http-server
                :start
                (http/start
                  { :handler handler/app
                    :port 8089
                  })
                :stop
                (http/stop http-server))

;(defn stop-server []
;  (when @server
;    (handler/destroy)
;    (@server-ws :timeout 100)
;;    (@server :timeout 100)
;    (reset! server nil)))


(mount/defstate ^{:on-reload :noop}
                repl-server
                :start
                (when-let [nrepl-port (conf/bwconf :nrepl-port)]
                  (repl/start {:port nrepl-port}))
                :stop
                (when repl-server
                  (repl/stop repl-server)))

;(mount/defstate log
;                :start (logger/init (:log-config conf/env)))


(defn -main [& args]

  (let [
        port 8089
        port-ws 8091
  ]
    (mount/start #'http-server )
    (mount/start #'server-ws )
    (mount/start #'conf/bwconf)
    ;(.addShutdownHook (Runtime/getRuntime) (Thread. stop-server))
;    (start-server port)
;    (start-server-ws port-ws)
    (mount/start #'database/conn)
    (handler/init)
    ;(handler/start-nrepl)
    (binding [log/*logger-factory* #{:info :warn}])
    (log/info "server started on port:" port)
    (log/info "ws-server started on port:" port-ws)
    ))
