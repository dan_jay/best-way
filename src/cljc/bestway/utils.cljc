(ns bestway.utils
  #?(:cljs (:require
             [cljs.reader]

             [goog.i18n.NumberFormat :as nf]
             [goog.i18n.NumberFormatSymbols :as symbols]
             ) )
  )



  #?(:cljs (def app-state-block (.getElementById js/document "app_state"))  )

;#?(:cljs (set! goog.i18n.NumberFormatSymbols goog.i18n.NumberFormatSymbols_si)  )

#?(:cljs (set! goog.i18n.NumberFormatSymbols (clj->js (assoc (js->clj goog.i18n.NumberFormatSymbols_si) :CURRENCY_PATTERN "¤ #,##0.00" ) ) ) )


  #?(:cljs
     (defn get-app-state
       [var]
     (let
       [
          node (.getElementsByClassName app-state-block (str var))
          val-node (aget node 0)
        ]
       (if (object? node)
         (str "")
         (cljs.reader/read-string (.-innerHTML val-node))
        )
       )
     )
  )

#?(:cljs (defonce  app-context (get-app-state "app_context" ) ) )