(ns bestway.bw-chat.app-state
  (:require [cljs-time.core :as dt]
            [cljs-time.coerce :as dt2]
            [clojure.walk]
            [goog.dom :as gdom]
            [bestway.index.websocket :as bws]
            )
  )
(require 'goog.object)
;; -------------------------
;; -------------------------

(defonce raw-data
            [
               {
                :id "m_1"
                :threadID "t_1"
                :threadName "Jing and Bill"
                :authorName "Bill"
                :text "Hey Jing want to give a Flux talk at ForwardJS?"
                :timestamp (- (dt/now) 99999)
                }
               {
                :id "m_2"
                :threadID "t_1"
                :threadName "Jing and Bill"
                :authorName "Bill"
                :text "Seems like a pretty cool conference."
                :timestamp (- (dt/now) 89999)
                }
               {
                :id "m_3"
                :threadID "t_1"
                :threadName "Jing and Bill"
                :authorName "Jing"
                :text "Sounds good.  Will they be serving dessert?"
                :timestamp (- (dt/now) 79999)
                }
               {
                :id "m_4"
                :threadID "t_2"
                :threadName "Dave and Bill"
                :authorName "Bill"
                :text "Hey Dave want to get a beer after the conference?"
                :timestamp (- (dt/now) 69999)
                }
               {
                :id "m_5"
                :threadID "t_2"
                :threadName "Dave and Bill"
                :authorName "Dave"
                :text "Totally!  Meet you at the hotel bar."
                :timestamp (- (dt/now) 59999)
                }
               {
                :id "m_6"
                :threadID "t_3"
                :threadName "Functional Heads"
                :authorName "Bill"
                :text "Hey Brian are you going to be talking about functional stuff?"
                :timestamp (- (dt/now) 49999)
                }
               {
                :id "m_7"
                :threadID "t_3"
                :threadName "Bill and Brian"
                :authorName "Brian"
                :text "At ForwardJS?  Yeah of course.  See you there!"
                :timestamp (- (dt/now) 39999)
                }] )


(defn convert-message [{:keys [id authorName text timestamp] :as msg}]
  {:message/id id
   :message/author-name authorName
   :message/author-id (.-textContent (gdom/getElement "user_id"))
   :message/text text
   :message/date (dt2/to-date timestamp)
   }
  )

(defn threads-by-id [coll msg]
  (let [{:keys [id threadID threadName timestamp]} msg]
    (if (contains? coll threadID)
      (assoc-in coll [threadID :thread/messages id]
                (convert-message msg))
      (assoc coll threadID
             {:thread/id threadID
              :thread/name threadName
              :thread/messages {id (convert-message msg)}
              })
      ))
  )

(defn threads [coll msg]
  (let [{:keys [threadID threadName]} msg
        {:keys [thread/id]} (last coll)
        last-msg-idx (dec (count coll))]
    (if (= id threadID)
      (update-in coll [last-msg-idx :thread/messages] conj (convert-message msg))
      (conj coll
             {:thread/id threadID
              :thread/name threadName
              :thread/messages [(convert-message msg)]
              })
      ))
 )

(def ws-base (.text (.find (js/jQuery "#app_state") "#ws_context")))
(def state (atom {}))
(def chat-state (atom {}))

(def url-base (.text (.find (js/jQuery "#app_state") "#app_context")))

(def ws-chat-url (str ws-base "chat-message?uid=" (.-textContent (gdom/getElement "user_id"))))
(def ws-chat-id "ws_chat")
(defonce chat-vars (atom {:cmd "GET" :uid (.-textContent (gdom/getElement "user_id"))}))

(defn update-chat! [results]

  (println (reduce threads [] (js->clj results :keywordize-keys true) ))
  (reset! state {:threads (reduce threads [] (js->clj results :keywordize-keys true) )})
;;      (reset! state {:threads  (reduce threads [] raw-data)    })
)

(add-watch chat-state :watcher
           (fn [key atom old-state new-state]
               ;(prn "-- Atom Changed --")
               ;(prn "key" key)
               ;(prn "atom" atom)
               ;(bws/send-msg! ws-chat-id ;ws-chat-url
               ;                     (clojure.walk/stringify-keys new-state)
               ;
               ;                     (fn [response] )
               ;                     )
               ;(print "new-state" new-state)
               )
           )

(defn keep-chat-alive![res-fn]
  (js/setInterval (fn []

                    (bws/send-msg! "ws_chat" {"cmd" "KA" "uid" (.-textContent (gdom/getElement "user_id"))}

                                   res-fn
                                   )


                    ) 6000)
  )

;(set! (.-onload (gdom/getWindow)) (fn []
                                                 (if (gdom/getElement "chat-seller")

                                                   (let
                                                     [
                                                      title (.-value (gdom/getElement "title"))
                                                      identity (.-value (gdom/getElement "id"))
                                                      item (.-value (gdom/getElement "item-id"))
                                                      seller (.-value (gdom/getElement "publisher-id"))
                                                      seller-name (.-value (gdom/getElement "publisher-name"))
                                                      new-thread {
                                                                  :id "m_1"
                                                                  :threadID (str identity "_" seller)
                                                                  :item item
                                                                  :threadName (str title )
                                                                  :authorName ""
                                                                  :text ""
                                                                  :timestamp (- (dt/now) 99999)
                                                                  }
                                                      ]
                                                     (reset! state {:threads  (reduce threads [] [new-thread])    })

                                                         (bws/make-websocket! ws-chat-id ws-chat-url
                                                                              (clojure.walk/stringify-keys @chat-vars)
                                                                             #(update-chat! %)
                                                          )
                                                     )
                                                   (do

                                                     (bws/make-websocket! ws-chat-id ws-chat-url
                                                                          (clojure.walk/stringify-keys @chat-vars)

                                                                          #(update-chat! %)
                                                                          )
                                                     )

                         )

     ;) )


(keep-chat-alive! #'update-chat!)
