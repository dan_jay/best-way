(ns bestway.bw-chat.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [goog.dom :as gdom]
            [om.next :as om]
            [cljs.core.async :refer [chan <!]]
            [bestway.bw-chat.parser :as p]
            [bestway.bw-chat.components.root :as root]
            [bestway.bw-chat.web-api :as api]
            [bestway.bw-chat.app-state :as state]
            [bestway.index.websocket :as bws]))

(enable-console-print!)


(if (gdom/getElement "chat-seller")
  (let [out     (chan)
        ;state (<! (api/get-initial-data out))
        state   state/state
        item-id (.-value (gdom/getElement "item-id"))
        ]

    (def reconciler
      (om/reconciler {:state  state
                      :remote #(print %1)
                      :parser (om/parser {:read p/read :mutate p/mutate})}))

    (om/add-root! reconciler
                  root/ChatApp
                  (gdom/getElement "bw-chat"))

     (om/transact! reconciler `[(thread/select {:thread/id "t_1"}) :threads])




    (om/transact! reconciler `[(thread/select {:thread/id ~item-id}) :threads])


    )





  ;(om/transact! reconciler `[(thread/select {:thread/id "5986020f0a975a370fbb7b68"}) :threads])
  (add-watch state/state :watcher
             (fn [key atom old-state new-state]
               (let [out   (chan)
                     ;state (<! (api/get-initial-data out))
                     state new-state
                     ]

                 (def reconciler
                   (om/reconciler {:state  state
                                   ;:remote #(print %1)
                                   :parser (om/parser {:read p/read :mutate p/mutate})}))

                 (om/add-root! reconciler
                               root/ChatApp
                               (gdom/getElement "bw-chat"))

                 (om/transact! reconciler `[(thread/select {:thread/id "t_1"}) :threads])
                 ;)



                 (if (gdom/getElement "chat-seller")

                   (let [
                         item-id (.-value (gdom/getElement "item-id"))
                         seller (.-value (gdom/getElement "publisher-id"))
                         identity (.-value (gdom/getElement "id"))
                         seller-name (.-value (gdom/getElement "publisher-name"))
                         ]
                     (om/transact! reconciler `[(thread/select {:thread/id ~(str identity "_" seller)}) :threads])
                     )

                   )

                  (om/update-state!  (om/app-root reconciler)  {:state new-state
                                                                :parser (om/parser {:read p/read :mutate p/mutate})
                                                                })
                 )

               )
             )

  )








