(ns bestway.bw-chat.parser
  (:require [om.next :as om]
            [cljs-time.core :as dt]
            [goog.dom :as gdom]
            [cljs-time.coerce :as dt2]
            [bestway.bw-chat.app-state :as state]
            [bestway.index.websocket :as bws]))

(defmulti read om/dispatch)

(defn thread-with-derived
  [{:keys [thread/id] :as thread} st]
  (let [selected-id (:selected/thread st)]
    (if (= selected-id id)
            (assoc thread :thread/selected true)
            thread))
  )

(defmethod read :default
  [{:keys [state] :as env} k _]
  (let [st @state] ;; CACHING!!!
;;    (println "READ :default" k env)
    (if (contains? st k)
      {:value (get st k)}
      {:remote true})))

(defmethod read :threads
  [{:keys [state query]} k _]
  (let [st   @state
        tree (om/db->tree query (get st k) st)]
;;    (println tree)
    {:value (into [] (map #(thread-with-derived % st) tree))}))

(defmulti mutate om/dispatch)

(defmethod mutate 'thread/select
  [{:keys [state]} _ {:keys [thread/id]}]
;;  (println "MUTATE thread/select: " @state)
  (let [swapper (fn [st]
                  (assoc-in
                   (assoc st :selected/thread id)
                   [:threads/by-id id :thread/read] true))]
    {:action #(swap! state swapper)}))

(defmethod mutate 'message/new
  [{:keys [state]} _ {:keys [thread/id message/text]}]
  (let [now     (dt/now)
        msg-id  (str "m_" now)
        new-msg {:message/id msg-id
                 :message/author-name (.-textContent (gdom/getElement "user_display-name"))
                 :message/author-id (.-textContent (gdom/getElement "user_id"))
                 :message/authorName (.-textContent (gdom/getElement "user_display-name"))
                 :message/date (dt2/to-date now)
                 :message/text text
                 :message/read false}
        threadName      (if (gdom/getElement "chat-seller")
                          (str (.-value (gdom/getElement "title")) " - " (.-value (gdom/getElement "publisher-name")) )
                          "_"
                         )

        swapper (fn [st]
                  (update-in
                   (assoc-in st [:messages/by-id msg-id] new-msg)
                   [:threads/by-id id :thread/messages]
                   conj [:messages/by-id msg-id]))]

    ;(print (get-in @state [:threads/by-id id :thread/messages]))
    ;(reset! state/chat-state {"threadID" id "message" new-msg})
    {:action (fn []
               (swap! state swapper)
               (bws/send-msg! state/ws-chat-id ;ws-chat-url
                              (clojure.walk/stringify-keys {
                              "threadID" id
                              "threadName" threadName
                              "message" new-msg }
                                                           )

                              (fn [response]
                                  (state/update-chat! response)
                                )
                              )


               )}
    )
  )
