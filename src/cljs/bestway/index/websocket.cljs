(ns bestway.index.websocket
  (:require [goog.dom :as gdom])
  )

(defonce ws-channels (atom {
                      ;:url chan
                      }))

(defonce ws-chan (atom nil))
;(def json-reader (t/reader :json))
;(def json-writer (t/writer :json))

(defn receive-msg
  [update-fn]
  (fn [msg]
    (update-fn ;msg
     ;(->> msg .-data (t/read json-reader))
     ;(.parse js/JSON msg)
     ;;(.log js/console msg)

      )))

(defn send-msg!
  [id msg receive-handler]
  ;;(.log js/console id)
  ;;(.log js/console (aget (clj->js @ws-channels) id) )
  (def ws (aget (clj->js @ws-channels) id ))
  (if  ws
    (do
    (set! (.-onmessage ws)
          (fn [e]
            ;(receive-handler (.parse js/JSON (.-data e)))
            ;;(.log js/console (.parse js/JSON (.-data e) ))
            (receive-handler (.parse js/JSON (.-data e) ));(.parse js/JSON e))
     ))
    (.send ws msg)
     ;(.send @ws-chan (.stringify js/JSON msg))
    )
    ;(throw (js/Error. "Websocket is not available!"))
    )
)




(defn search-keep-alive![]
      (js/setInterval (fn []

                         ; (send-msg! "ws_listing" "" (fn []))
                          (send-msg! "ws_search" "" (fn []))
                        ;  (send-msg! "ws_chat" {"cmd" "KA" "uid" (.-textContent (gdom/getElement "user_id"))} (fn []))


                        ) 6000)
  )

(defn news-keep-alive![]
  (js/setInterval (fn []

                     (send-msg! "ws_listing" "" (fn []))
                    ;(send-msg! "ws_search" "" (fn []))
                    ;  (send-msg! "ws_chat" {"cmd" "KA" "uid" (.-textContent (gdom/getElement "user_id"))} (fn []))


                    ) 6000)
  )


(defn make-websocket! [id url data receive-handler]
  ;(.log js/console "attempting to connect websocket" url)
  (if-let [chan (js/WebSocket. url)]
    (do
      ;(set! (.-onmessage chan) (receive-transit-msg! receive-handler))
      ;;(.log js/console "receiving...")

      (set! (.-onmessage chan)
            (fn [e]
              ; ;(.log js/console (.parse js/JSON (.-data e)))
              ;;(.log js/console (str "3" e))
              ;;(.log js/console e)
                (receive-handler (.parse js/JSON(.-data e)) )
              ))

      (set! (.-onopen chan)
            (fn [e]
              ( .send chan
                     data
                     )

              ))

      (set! (.-onclose chan)
            (fn [e]
              (.close chan)
              (make-websocket! id url data
                      receive-handler
                      )

              )

            )

      ;(reset! ws-chan chan)

      (swap! ws-channels assoc id chan )

;      ;(.log js/console @ws-channels)
      ;(keep-alive!)
      ;(.log js/console "Websocket connection established with: " url)
      )
    (throw (js/Error. "Websocket connection failed!"))))
