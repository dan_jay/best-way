(ns bestway.index.news
  (:require [reagent.core :as reagent]
            [reagent.session :as session]
            [reagent.format :as reformat]
            [reagent-forms.core :as form]
            [goog.crypt :as gcrypt]
            [goog.uri.utils :as guri]
            ;[cognitect.transit :as t]
            [cemerick.url :as curl]
            [clojure.string :as string]
            [bestway.utils :as util]
            [bestway.index.websocket :as bws]
            ;[reagent-modals.modals :as reagent-modals]
            [taoensso.truss :as truss :refer-macros (have have! have?)]
            [bestway.index.common :as common]
            [soda-ash.core :as sa]
            )
  )

(def url-base (.text (.find (js/jQuery "#app_state") "#app_context")))
;(js/alert (util/get-app-state "app_context") )
(def ws-base (.text (.find (js/jQuery "#app_state") "#ws_context")))
(def ws-listing-url (str ws-base "news-listing"))
(def ws-listing-id "ws_listing")



(defn home-empty []
  (fn []
    [:div {:id "listing"} [:div {:class "ad col-md-6"}
                           [:div
                            [:div {:class "abc"} "Sorry! No news available at the moment"]]]
     ]
    ))

(defn home-sidebar-listing-empty []
  (fn []
    [:div {:class "news-hl"}
     [:div {:class "news-hl-content"}
      [:img {:src "https://www.bestway.lk/img/icons/loadin_ico_news.gif"}]
      ]]
    )
    )




; ********************************** Home results ***************************************/

(defn listing-item [list-of-items]
  [:div
  (into [] (concat [:div
                    {:id "mainAds" :class "listing"} ]
         (apply concat
          ; (for [publisher list-of-items]

               (let [news list-of-items]
                 ;(js/console.log news)
                 (for [one-map news]

                    [[:div {:class "col-md-6 news-con" }

                     [:div {:class "news-wrap wb"  }
                      [:div {:class "news-cat"}
                       [:h4 (if (zero?  (count (aget one-map "categories"))) "Latest news" (first (aget one-map "categories") ) ) ]
                       ] ; div.news-cat closing

                      [:div {:class "news-img-con"}
                       [:img {:src (.-img one-map

                                   )
                              }]
                       ]  ; div.news-img-con closing

                      [:div {:class "news-head-con"}

                                                                       [sa/Modal {:trigger (reagent/as-element [:sa/Header (.-title one-map)])
                                                                                  :closeIcon (reagent/as-element [:i {:aria-hidden true :class "close fa fa-times-circle"}])
                                                                                  }
                                                                        [sa/ModalHeader  (.-title one-map)]
                                                                        [sa/ModalContent
                                                                         {:image true}
                                                                         [sa/Image {;:wrapped true
                                                                                    :size    "large"
                                                                                    :src     (.-img one-map)}]
                                                                         [sa/ModalDescription

                                                                          [:p (.-body one-map)]
                                                                          ]]
                                                                        ]


                       ] ; div.news-head-con closing


                        (if-let [published (aget one-map "published") ]
                          [:div {:class "news-date-time"}
                          [:i {:class "fa fa-calendar", :aria-hidden "true"}]
                          [:span {:class "news-date"}
                          (str (aget published "day") ", " (aget published "date") (aget published "month") (aget  published "year")
                               )]
                           [:div {:class "hm-news-time"}
                          [:i {:class "fa fa-clock-o", :aria-hidden "true"}]
                          [:span (str  (aget published "time"))]
                           ]
                          ]
                          )
                         ; div.news-date-time closing
                      [:div {:class "news-txt"}
                       [:p {:class  "news-summary" }
                        (.-summary one-map)
                        ]
                       ]   ; div.news-txt closing

                      ]

                     ]]

                   )
               )

                                                                           )
                                                                  )
        )
      ;)
      ]
  )

(defn listing-sidebar-item [list-of-items]
  [:div
   (into [] (concat [:div {:id "news-hl-con" :class "wb"} ]
                    (apply concat


                           (let [news list-of-items]
                             ;(js/console.log news)
                             (for [one-map news]


[ [:div {:class "news-hl"}                                                            [:div {:class "news-hl-img" }

                                                             [:img {:src (.-img one-map
                                                                                )
                                                                    }]]
                                                            [:div {:class "news-hl-content"}
                                                             [sa/Modal {:trigger (reagent/as-element [:h4 [:a (.-title one-map)]])
                                                                        :closeIcon (reagent/as-element [:i {:aria-hidden true :class "close fa fa-times-circle"}])
                                                                        }
                                                              [sa/ModalHeader (.-title one-map)]
                                                              [sa/ModalContent
                                                               {:image true}
                                                               [sa/Image {;:wrapped true
                                                                          :size    "large"
                                                                          :src     (.-img one-map)}]
                                                               [sa/ModalDescription
                                                                [sa/Header (:title (.-title one-map))]
                                                                [:p (.-body one-map)]
                                                                ]]
                                                              ]


                                                             ]
]

]
                               )
                             )

                           )
                    )
         )
   ;)
   ]
  )




(defonce home-listing (reagent/atom [(home-empty)]))
(defonce home-side-listing (reagent/atom [(home-sidebar-listing-empty)]))



(defonce home-vars
  (reagent/atom
    {
     :query        ""
     :lng     "sin"
     :sub-cat      ""
     :perpage      "10"
     :page         "1"
     :loc-district "any"
     :price-min    "any"
     :price-max    "any"
     }
    )
  )



(defn show-loader []
  (bestway.index.common/show-loader)
  ;(fn []
  ;  [:div {:class "ui segment"}
  ;   [:div {:class "ui active dimmer inverted"}
  ;    [:div {:class "ui mini text loader"} "Loading"]
  ;    ]
  ;   ]
  ; )
)






(defn update-listing! [results]
  (if-not (empty? results)
    (do
      (reset! home-listing (listing-item results))
      (reset! home-side-listing (listing-sidebar-item results))
      )
    (do
    (reset! home-listing [(home-empty)])
    (reset! home-side-listing [(home-sidebar-listing-empty)])
      )
    )
)


(defonce news-toggle (reagent/atom :hidden))

(defn home-listing-component
  []
  (let [
        ;results @search-results
        ]                                                   ;; <-- closed over by lifecycle fns

    (reagent/create-class                                   ;; <-- expects a map of functions
      {
       :component-did-mount
                     (fn []
                       (reset! news-toggle :shown)
                       (.scrollIntoView (js/document.getElementById "listing_container"))
                       )
       :component-will-mount                                ;; the name of a lifecycle function
                     (fn []
                       ;;(.log js/console "component-will-mount") ;; remember to repeat parameters

                       )
       ; #(;;(.log js/console "component-will-mount"))  ;; your implementation

       ;; other lifecycle funcs can go in here

       :display-name "bestway-home-listing-component"       ;; for more helpful warnings & errors

       :reagent-render                                      ;; Note:  is not :render
                     (fn []
                       ;;(.log js/console "component-is-rendering") ;; remember to repeat parameters


                       @home-listing

                       )
       :component-will-unmount
                     (fn []
                       (reset! news-toggle :hidden)
                       )

       }

      )
    )
  )                                                         ;; end of  home-listing-component

(defmacro _id [id]
  (.getElementById js/document (str id))
)

(defn home-headline-listing-component
  "News Language and Visibility controller"
  []
  (reagent/create-class                                   ;; <-- expects a map of functions
    {
     ;:component-did-mount                (show-pagination @search-pagination)

     :component-will-mount                                ;; the name of a lifecycle function
                   (fn []
                     ;;(.log js/console "component-will-mount") ;; remember to repeat parameters

                     )
     ; #(;;(.log js/console "component-will-mount"))  ;; your implementation

     ;; other lifecycle funcs can go in here

     :display-name "bestway-sidebar-headlines-listing-component"       ;; for more helpful warnings & errors

     :reagent-render                                      ;; Note:  is not :render
                   (fn []
                     ;;(.log js/console "component-is-rendering") ;; remember to repeat parameters

                     @home-side-listing


                     )

     }

    )

)


(defn news-btn-container
   "News btn container"
   []
  [:div
  [:button {:id :sh-news :class "ui primary right floated button "
                :on-click (fn [e]
                            (if (= @news-toggle :hidden)
                              (reagent/render-component [(home-listing-component)] (.getElementById js/document "listing_container"))
                              (reagent/unmount-component-at-node (.getElementById js/document "listing_container"))
                              )

                            )
            } "News" ]
  [:div {:class "ui small buttons"}
   [:button {:class  (if (= (get @home-vars :lng) "eng") "ui teal button lng news-lang-select" "ui teal button lng")
             :on-click (fn [e]
                         (let [lng "eng"]
                           (reset! home-side-listing      [:div {:class "news-hl-content"}
                                                           [:img {:src "https://www.bestway.lk/img/icons/loadin_ico_news.gif"}]
                                                           ])

                           (swap! common/prefs assoc :lng)
                           (swap! home-vars update-in [:lng] #(str lng))

                           (bws/send-msg!
                             ws-listing-id
                             (clojure.walk/stringify-keys @home-vars)

                             #(update-listing! %)
                             )


                           )
                         )

             } "E"]
   [:button {:class  (if (= (get @home-vars :lng) "sin") "ui yellow button lng news-lang-select" "ui yellow button lng")
             :on-click (fn [e]
                         (let [lng "sin"]
                           (reset! home-side-listing      [:div {:class "news-hl-content"}
                                                           [:img {:src "https://www.bestway.lk/img/icons/loadin_ico_news.gif"}]
                                                           ])
                           (swap! common/prefs assoc :lng)
                           (swap! home-vars update-in [:lng] #(str lng))

                           (bws/send-msg!
                             ws-listing-id
                             (clojure.walk/stringify-keys @home-vars)

                             #(update-listing! %)
                             )
                           )
                         )

             } "සි"]
   [:button {:class  (if (= (get @home-vars :lng) "tam") "ui red button lng news-lang-select" "ui red button lng")
             :on-click (fn [e]
                         (let [lng "tam"]
                           (reset! home-side-listing      [:div {:class "news-hl-content"}
                                                           [:img {:src "https://www.bestway.lk/img/icons/loadin_ico_news.gif"}]
                                                           ])
                           (swap! common/prefs assoc :lng)
                           (swap! home-vars update-in [:lng] #(str lng))

                         (bws/send-msg!
                           ws-listing-id
                           (clojure.walk/stringify-keys @home-vars)

                           #(update-listing! %)
                           )
                         )
                                                                   )

             } "த"]
   ]
   ]
   )

(defn news-control
   "News Language and Visibility controller"
   []
  (reagent/create-class                                   ;; <-- expects a map of functions
    {
     ;:component-did-mount                (show-pagination @search-pagination)

     :component-will-mount                                ;; the name of a lifecycle function
                   (fn []
                     ;;(.log js/console "component-will-mount") ;; remember to repeat parameters

                     )
     ; #(;;(.log js/console "component-will-mount"))  ;; your implementation

     ;; other lifecycle funcs can go in here

     :display-name "bestway-home-listing-component"       ;; for more helpful warnings & errors

     :reagent-render                                      ;; Note:  is not :render
                   (fn []
                     ;;(.log js/console "component-is-rendering") ;; remember to repeat parameters


                     news-btn-container

                     )

     }

    )

   )


(.ready (js/jQuery js/document) (fn []





                                  (def listing_container (.getElementById js/document "news-hl-container"))

                                  (if-let [news-btns (.getElementById js/document "news-btn-con")]

                                    (do

                                      (bws/make-websocket! ws-listing-id ws-listing-url

                                                           (clojure.walk/stringify-keys @home-vars)

                                                           #(update-listing! %)

                                                           )
                                      (reagent/render-component [news-control] news-btns)
                                      (reagent/render-component [home-headline-listing-component] listing_container) ;; display on page
                                      (reagent/render-component [(home-listing-component)] (.getElementById js/document "listing_container"))

                                      )

                                    )




                                  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;###############################;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                                               ;;;;;;;;;;;                                  ;;;;;;



                                  ;(.on (js/jQuery "btn#lng") "click"
                                  ;        (fn [e]
                                  ;          (let [data-value (.attr (js/jQuery (.-target e)) "id")]
                                  ;            ;(js/alert data-value)
                                  ;            (swap! common/prefs assoc :lng )
                                  ;            )
                                  ;
                                  ;          (bws/send-msg!
                                  ;            ws-listing-id
                                  ;            (clojure.walk/stringify-keys @home-vars)
                                  ;
                                  ;            #(update-listing! %)
                                  ;            )
                                  ;          )
                                  ;        )
                                  ;;;;;;;;;;

                                  )

        )




