(ns bestway.index.settings
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent-forms.core :refer [bind-fields init-field value-of]]
            [reagent.session :as session]
            [reagent.validation :as validate]
            [ajax.core :refer [POST]]
            [cognitect.transit :as t]
            [goog.events :as events]
            [bestway.utils :as utils :refer [app-context]]
            [bestway.index.common :as common]
            )
   )

;; ------------------------
(def tr (t/reader :json))
;; ------------------------

;(enable-console-print!)

(defonce  settings-state
                (reagent/atom {:me {:display-name ""
                                    :first-name "Dan"
                                    :last-name  "Jay"

                                    :birth-day {:year 1990 :month 03 :day 24}
                                    :email "foo@bar.baz"
                                    :contact-num ""
                                    :address {
                                            :line1 ""
                                            :line2 ""
                                            :city ""
                                            :district ""
                                    }
                                 :preferences {
                                               :notification {


                                                              }
                                               :privacy {


                                                        }


                                              }
                            }
                       :weight 100
                       :height 200
                       :comments "some interesting comments on this subject"
                       :radioselection :b
                       :position [:left :right]

                       :pick-one :bar
                       :unique {:position :middle}
                       :pick-a-few [:bar :baz]
                        :nav {:general true
                              :profile false
                              :security false
                              :privacy false
                              :notification false
                              }
                       :many {:options :bar}}))

(defonce settings-me (reagent/cursor settings-state [:me] ))

(defonce settings-nav (reagent/cursor settings-state [:nav] ))

(defn send-settings-post
  "Ajax post request"
  []
  (POST (str app-context "save-settings")
        {:headers
                  {"Accept"       "application/transit+json"
                   "x-csrf-token" (.-value (.getElementById js/document "__anti-forgery-token"))
                   }
         ;:format :json
         :params  (dissoc @settings-state :nav)
         ;:response-format :edn
         :handler (fn [m] (reset! common/alert-msg (clojure.walk/keywordize-keys m)) (common/show-alert))
         }

        )
)



(defn get-settings-post
  "Ajax post request"
  []
  (POST (str app-context "account-settings")
        {:headers
                  {"Accept" "application/transit+json"
                   "x-csrf-token" (.-value (.getElementById js/document "__anti-forgery-token"))
                   }
         ;:response-format :transit
         ;:params (dissoc @settings-state :nav)
         ;:response-format :edn
         :handler         (fn [data]
                            ;(cljs.reader/read-string (:markup (filter #(= subcat (get % :_id )) data )))
                            ;(print data)
                            ;(reset! subcats data)
                            ;;(.log js/console data)
                            (swap! settings-state merge data)
                            ;;(.log js/console (clj->js @subcats))

                            )
         }

        )
  )



;(def settings-nav (atom {  :general "hidden"
;                         :profile ""
;                         :security "hidden"
;                         :privacy "hidden"
;                         :notification "hidden"
;                         } ))

(defn settings-navigate
  ""
  [k]
  (reset! settings-nav {
                        :general false
                        :profile false
                        :security false
                        :privacy false
                        :notification false
                        } )
  (swap! settings-nav assoc (keyword k) true)
)

;(def doc (atom {:me {:first-name "John"
;                    :age 35
;                    :email "foo@bar.baz"}
;           :weight 100
;           :height 200
;           :bmi 0.5
;           :comments "some interesting comments\non this subject"
;           :radioselection :b
;           :position [:left :right]
;           :pick-one :bar
;           :unique {:position :middle}
;           :pick-a-few [:bar :baz]
;           :many {:options :bar}}))

(defn district-source [text]
  (filter
    #(-> % (name ) (.toLowerCase %) (.indexOf text) (> -1))
    [
     :Ampara :Anuradhapura :Badulla :Batticaloa :Colombo :Galle :Gampaha :Hambantota :Jaffna :Kalutara :Kandy :Kegalle :Kilinochchi
     :Kurunegala :Mannar :Matale :Matara :Monaragala :Mullaitivu :Nuwara-Eliya :Polonnaruwa :Puttalam :Ratnapura :Trincomalee :Vavuniya
     ]))

(defn row [label input]
  [:div {:class "inline field"}
   [:label label]
    input])

(defn radio [label name value]
  [:div.radio
   [:label
    [:input {:field :radio :name name :value value}]
    label]])

(defn input [label type id]
  (row label [:input {:field type :id id}]))

(defn input-row [label placeholder type id]
   [:label label]
   [:input {:field type :id id :placeholder placeholder}]
 )
(defn row-input-text [label placeholder type id]
  [:div.row.field.inline
   [:div.col-md-4
    [:label label]
    ]
   [:div.col-md-8
    [:input {:field type :id id :placeholder placeholder}]
    ]
   [:div.col-md-4
    [:div.ui.left.pointing.red.basic.label {:field :alert :id :me.first-name :event empty?} "First name empty!"]
    ]
   ]
  )

(def form-template

[:div

   [:hr]
   (input "kg" :numeric :weight-kg)
   (input "lb" :numeric :weight-lb)

   [:hr]
   [:h3 "BMI Calculator"]
   (input "height" :numeric :height)
   (input "weight" :numeric :weight)
   (row "BMI"
        [:input.form-control
         {:field :numeric :fmt "%.2f" :id :bmi :disabled true}])
   [:hr]

   (row "District"
        [:span {:field           :typeahead
               :id              :ta
               :data-source     district-source
               :input-placeholder "Where do you live ?"
               :input-class     "typeahead-input"
               :list-class      "typeahead-list"
               :item-class      "typeahead-list-item"
               :highlight-class ""}])


   (row "isn't data binding lovely?"
        [:input {:field :checkbox :id :databinding.lovely}])
   [:label
    {:field :label
     :preamble "The level of awesome: "
     :placeholder "N/A"
     :id :awesomeness}]

   [:input {:field :range :min 1 :max 10 :id :awesomeness}]

   [:h3 "option list"]
   [:div.form-group
    [:label "pick an option"]
    [:select.form-control {:field :list :id :many.options}
     [:option {:key :foo} "foo"]
     [:option {:key :bar} "bar"]
     [:option {:key :baz} "baz"]]]

   (radio
     "Option one is this and that—be sure to include why it's great"
     :foo :a)
   (radio
     "Option two can be something else and selecting it will deselect option one"
     :foo :b)

   [:h3 "multi-select buttons"]
   [:div.btn-group {:field :multi-select :id :every.position}
    [:button.btn.btn-default {:key :left} "Left"]
    [:button.btn.btn-default {:key :middle} "Middle"]
    [:button.btn.btn-default {:key :right} "Right"]]

   [:h3 "single-select buttons"]
   [:div.btn-group {:field :single-select :id :unique.position}
    [:button.btn.btn-default {:key :left} "Left"]
    [:button.btn.btn-default {:key :middle} "Middle"]
    [:button.btn.btn-default {:key :right} "Right"]]

   [:h3 "single-select list"]
   [:div.list-group {:field :single-select :id :pick-one}
    [:div.list-group-item {:key :foo} "foo"]
    [:div.list-group-item {:key :bar} "bar"]
    [:div.list-group-item {:key :baz} "baz"]]

   [:h3 "multi-select list"]
   [:ul.list-group {:field :multi-select :id :pick-a-few}
    [:li.list-group-item {:key :foo} "foo"]
    [:li.list-group-item {:key :bar} "bar"]
    [:li.list-group-item {:key :baz} "baz"]]])


(def settings-general-section-component
  [:div  {:field :container
          :visible? #(-> %1 :nav :general)
          ;:visible? #(:show-name? %)
          :id :nav.general
          :class " ui form settings-form" }
   [:h3
    [:i {:class "fa fa-cog"}] " Account Settings"]
   [:div {:class "ui divider"}]


   [:div.row.field.inline
    [:div.col-md-3
     [:label "Name to Display"]
     ]
    [:div.col-md-8
     [:input {:field :text :id :me.display-name :placeholder "The name you like to be shown to public"}]

     [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.display-name :event empty?} "Display name empty!"]
     ]
    ]

   [:div.row.field.inline
    [:div.col-md-3
     [:label "First Name"]
     ]
    [:div.col-md-8
     [:input {:field :text :id :me.first-name :placeholder "First Name"}]

     [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.first-name :event empty?} "First name empty!"]
     ]
    ]


   [:div.row.field.inline
    [:div.col-md-3
     [:label "Last Name"]
     ]
    [:div.col-md-8
     [:input {:field :text :id :me.last-name :placeholder "Last Name"}]
     [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.last-name :event empty?} "Last name empty!"]
     ]
    ]


   [:div.row.field.inline
    [:div.col-md-3
     [:label "Birthdate"]
     ]
    [:div.col-md-8
     [:input {:field :datepicker :inline true :id :me.birth-day :date-format "yyyy/mm/dd" :read-only true }]

     [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.birth-day :event empty?} "Birthdate empty!"]
     ]
    ]


   [:div.row.field.inline
    [:div.col-md-3
     [:label "Email"]
     ]
    [:div.col-md-8
     [:input {:field :email :id :me.email :placeholder "foo@bar.lk"}]

     [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.email :event empty?} "Email empty!"]
     [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.email
                                             :event #(and (not (validate/is-email? %)) (not (empty? %)))
                                             }
      "Email is not valid!"
      ]
     ]
    ]

   [:div.row.field.inline
    [:div.col-md-3
     [:label "Contact number"]
     ]
    [:div.col-md-8
       [:input { :field :text :id :me.contact-num :placeholder "+94 xx xx xx xxx" } ]
       [:div.ui.top.pointing.red.basic.label.set-err {:field :alert :id :me.contact-num :event empty?} "Contact number empty!"]
     ]
    ]
  ]
)

(def settings-profile-section-component

  [:div  {:field :container :visible?  #(-> %1 :nav :profile) :id :nav.profile :class " ui form settings-form" }
   [:h3
    [:i {:class "fa fa-user"}] " Profile Settings"]
   [:div {:class "ui divider"}]

  [:div {:class "field"}
   [:h4 {:class "ui header"} "Your permanent Address"]
   [:div {:class "inline field"}
    [:div {:class "ui fluid icon input"}
     [:input {:field :text :id :me.address.line1 :name "address
            [addressl1]", :placeholder "15A, Street line 1", :type "text"}]
     ]
    ]
   [:div {:class "inline field"}
    [:div {:class "ui fluid icon input"}
     [:input {:field :text :id :me.address.line2 :name "address
            [addressl2]", :placeholder "Street line 2", :type "text"}]]]
   [:div {:class "inline field"}
    [:div {:class "ui fluid icon input"}
     [:input {:field :text :id :me.address.city :name "address
            [City]", :placeholder "City", :type "text"
              }]]]
  [:label "Select Your District"]
   [:div {:class "inline field"}
    [:div {:class "ui fluid icon input"}

      [:span {:field :typeahead :id :me.address.district :data-source district-source
              :input-class     "typeahead-input"
              :list-class      "typeahead-list"
              :item-class      "typeahead-list-item"
              :name "address [District]", :placeholder "District"
              :type "text"}]

      ]
    ]

   ;(row "District"
   ;     [:span {:field           :typeahead
   ;             :id              :ta
   ;             :data-source     district-source
   ;             :input-placeholder "Where do you live ?"
   ;             :input-class     "typeahead-input"
   ;             :list-class      "typeahead-list"
   ;             :item-class      "typeahead-list-item"
   ;             :highlight-class ""}])
   ]
   [:div {:class "u-map"}
    [:img {:src "http://bestway.lk/img/map.png", :alt "User location map"}]]
   ]
  )


(def settings-security-section-component
  [:div  {:field :container :visible? #(-> %1 :nav :security) :id :nav.security :class " ui form settings-form" }
   [:h3
    [:i {:class "fa fa-lock"}] " Security Settings"]
   [:div {:class "ui divider"}]

   [:div {:class "inline field "}
    [:div.col-md-4
      [:label "Current Password"]
    ]
    [:div.col-md-6
      [:input {:field :password :id :security.password :title "Change Password", :placeholder "Current Password", :name "password"}]
    ]
   ]
   [:div {:class "inline field "}
    [:div.col-md-4
      [:label "New Password"]
    ]
    [:div.col-md-6
      [:input {:field :password :id :security.new-password :title "Change Password", :placeholder "New Password", :name "new-password"}]
      ]
    ]
   [:div {:class "inline field "}
    [:div.col-md-4
      [:label "Confirm New Password"]
    ]
    [:div.col-md-6
      [:input {:field :password :id :security.new-password-clone :title "Change Password", :placeholder "New Password (repeat)", :name "new-password-clone"}]
      ]
    ]

    [:div {:class "u-map"}
     [:img {:src "http://bestway.lk/img/pas-protect.png", :alt "Protection Shield"}]]
    ]

  )



(def settings-privacy-section-component
  "docstring"

  [:div  {:field :container :visible? #(-> %1 :nav :privacy) :id :nav.privacy :class " ui form settings-form" }
   [:h3
    [:i {:class "fa fa-universal-access"}] " Privacy Settings"]
   [:div {:class "ui divider"}]
   [:div {:class "inline fields"}
    [:label {:for "addr-visi"} "My Address Visibility:"]
    [:div {:class "field"}
     [:div.btn-group {:field :single-select :id :preferences.privacy.my-address-visibility }
      [:button.btn.btn-default {:key "private"} "Only me"]
      [:button.btn.btn-default {:key "followers"} "Followers Only"]
      [:button.btn.btn-default {:key "public"} "Public"]]
      ]]
   [:div {:class "inline fields"}
    [:label {:for "mem-visi"} "My Membership details Visibility:"]
    [:div {:class "field"}
     [:div.btn-group {:field :single-select :id :preferences.privacy.my-membership-details-visibility }
      [:button.btn.btn-default {:key "private"} "Only me"]
      [:button.btn.btn-default {:key "followers"} "Followers Only"]
      [:button.btn.btn-default {:key "public"} "Public"]
      ]
     ]
    ]

  ]

)


(def settings-notification-section-component


  [:div  {:field :container :visible? #(-> %1 :nav :notification) :id :nav.notification :class " ui form settings-form" }
   [:h3
    [:i {:class "fa fa-bell"}] " Notification Settings"]
   [:div {:class "ui divider"}]

  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.i-received-a-message :name "i-received-a-message" }]
    [:label "I received a Message"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.someone-followed-me :name "someone-followed-me" }]
    [:label "Someone follow me"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.commented-on-my-listing :name "commented-on-my-listing" }]
    [:label "Comments on your listing"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.someone-liked-your-listing :name "someone-liked-my-listing" }]
    [:label "Someone liked my listing"]]]
   [:div {:class "inline field"}
    [:div {:class "ui  checkbox toggle"}
     [:input {:field :checkbox :id  :preferences.notification.someone-shared-my-listing :name "someone-shared-my-listing" }]
     [:label "Someone Shared my listing"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.follow-suggestions :name "follow-suggestions" }]
    [:label "Follow suggestions"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.answers-to-my-help-center-questions :name
                    "answers-to-your-help-center-questions" }]
    [:label "Answers to My Help Center questions"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.new-bestway-features :name "new-bestway-features" }]
    [:label "New Bestway features"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.invitations-to-bestway-survey :name "invitations-to-bestway-survey" }]
    [:label "Invitations to Bestway Survey"]]]
  [:div {:class "inline field"}
   [:div {:class "ui  checkbox toggle"}
    [:input {:field :checkbox :id :preferences.notification.people-i-referred-joined-bestway :name "people-i-referred-joined-bestway" }]
    [:label "People I referred joined Bestway"]]]


]
  )

(def settings-menu-component

  [:div {:class "ui vertical menu"}
     [:div {:class "item"}
      [:div {:class "header"}
       [:a {:class "set"  :on-click #( settings-navigate "general" ) }
          [:i {:class "fa fa-cog"   }] "General"
        ]
       ]
      ]
     [:div {:class "item"}
      [:div {:class "header"}
       [:a {:class "set" :on-click #( settings-navigate "profile" ) }
        [:i {:class "fa fa-user" }] "Profile"]]]
     [:div {:class "item"}
      [:div {:class "header"}
       [:a {:class "set" :on-click #( settings-navigate "security" ) }
        [:i {:class "fa fa-lock"}] "Security"]]]
     [:div {:class "item"}
      [:div {:class "header"}
       [:a {:class "set" :on-click #( settings-navigate "privacy" ) }
        [:i {:class "fa fa-eye-slash"}] "Privacy"]]]
     [:div {:class "item"}
      [:div {:class "header"}
       [:a {:class "set" :on-click #( settings-navigate "notification" ) }
        [:i {:class "fa fa-bell"}] "Notifications"]]]]

  )


(defn settings-menu
  ""
  []
  (reagent/create-class
    {
     :display-name "Settings Menu"
     ;:get-initial-state (fn [this])
     ;:component-will-receive-props (fn [this new-argv]  (.checkbox (js/jQuery ".ui.radio.checkbox"))                                     )
     ;:should-component-update (fn [this old-argv new-argv])
     ;:component-will-mount (fn [this])
     ;:component-did-mount (fn [this]                       )
     ;:component-will-update (fn [this new-argv])
     ;:component-did-update (fn [this old-argv])
     ;:component-will-unmount (fn [this])
     :reagent-render (fn []
                      ;settings-menu-component
                       [bind-fields settings-menu-component settings-nav ]
                       )}
    ;; :render (fn [this])
    )
  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn settings []
  (reagent/create-class
    {
     :display-name "Bestway Settings"
     ;:get-initial-state (fn [this])
     ;:component-will-receive-props (fn [this new-argv])
     ;:should-component-update (fn [this old-argv new-argv])
     ;:component-will-mount (fn [this])
     :component-did-mount (fn [this]
                            (.checkbox (js/jQuery ".ui.radio.checkbox")
                            )
     ;:component-will-update (fn [this new-argv])
     :component-did-update (fn [this old-argv]
                           ;  (if (:notification settings-nav)
                                (.checkbox (js/jQuery ".ui.radio.checkbox"))
                            ;   )
                             )
                             )
     ;:component-will-unmount (fn [this])
     :reagent-render (fn []

                       [:div {:class "ui padded segment"}


                        ;  [bind-fields form-template settings-state ]

                       ; [bind-fields settings-menu-component settings-state ]

                        [bind-fields settings-general-section-component settings-state]

                        [bind-fields settings-profile-section-component settings-state]
                        ;reagenr
                        [bind-fields settings-security-section-component settings-state]
                        ;
                        [bind-fields settings-privacy-section-component settings-state]
                        ;
                        [bind-fields settings-notification-section-component settings-state]


                        [:div {:class "ui form "}
                         [:button {:class "settings positive ui button" :on-click send-settings-post}
                          "Save Settings"
                          ]
                         ]
                        ]
                       )}

    )
 )


(.ready (js/jQuery "document")
        (fn []
          (if (not= (.getElementById js/document "settings_con") nil)
            (do
                (get-settings-post)

                (reagent/render-component [settings-menu] (.getElementById js/document "settings_menu"))

                (reagent/render-component [settings] (.getElementById js/document "settings_con"))
            ))




          )
  )
