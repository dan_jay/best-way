(ns bestway.index.user
  (:require
    [reagent.core :as reagent ]
    [reagent.session :as session]
    [ajax.core :refer [POST]]
    [goog.events :as events]
    [cljsjs.cropper]
    [bestway.index.common :refer [app-state show-loader hide-loader] ]
))



;; ------------------------
;; Views

(defn into-list [items]
  (into [:ul]
        (for [i items]
          [:li i])))

(defn set-status [class title items]
  [:div {:class class}
   [:h4 title]
   (into-list items)
   ]
  )

(defn status-component []
  (if-let [status (session/get :upload-status)]
    [:div
     [:h3 "Status"]
     status]))

(defn show-upload-indicator []
  (let [class "fa fa-spinner fa-spin fa-pulse"]
    (session/put! :upload-status [:div
                                  [:p "Uploading file... "
                                   [:span {:class class}]]])))

(defn set-upload-indicator []
  (let [class "fa fa-spinner fa-spin fa-pulse"]
    (session/put! :upload-status [:div
                                  [:p "Uploading file... "
                                   [:span {:class class}]]])))

(defn handle-response-error [ctx]
  (let [rsp (js->clj (:response ctx) :keywordize-keys true)
        status (set-status "alert alert-danger"
                           "Upload Failure"
                           [(str "Status: " (:status ctx) " "
                                 (:status-text ctx))
                            (str (:message rsp))])]
    ;(.log js/console (str "cljs-ajax error: " status))
    (session/put! :upload-status status)))

;;; cljs-ajax upload routines
(defn handle-response-ok [resp]
  (let [rsp (js->clj resp :keywordize-keys true)
        profile-pic  (:new-prof-pic rsp)
        upload-status  (:status rsp)
        status (set-status "alert alert-success"
                           "Upload Successful"
                           [(str "Filename: " (:filename rsp))
                            (str "Size: " (:size rsp))
                            ;(str "Tempfile: " (:tempfile rsp))
                            ]
                           )]
    (session/put! :upload-status status)
    (swap! app-state assoc :profile-pic profile-pic)

        (hide-loader)
        (.reload js/window.location)


     )
  )

(def profpic-crop-img (reagent/atom ""))
(def profpic-crop-obj (reagent/atom nil ) )

(defn cropper-js [id]
  (let
    [ cropper
     (js/Cropper. (.getElementById js/document id)
                  (js-obj
                    "strict" true
                    "aspectRatio" 1
                    "crop" (fn [e]
                             ;;(.log js/console (.-x (.-detail e)))
                             )
                    "zoom" (fn [e]
                            ; ;(.log js/console e)

                             ; ;(.log js/console (.-cropBoxData (.-cropper (.-target e))))
                             ;;(.log js/console (.-clientWidth (.-target e)))
                             ;;(.log js/console (.-ratio (.-detail e)))

                             )
                    "minCropBoxWidth" 180
                    "minCropBoxHeight" 180
                    "background" false
                    "autoCropArea" 0.6
                    "movable" true
                    )
                  )


     ]



    cropper
    )
  )

(defn cljs-ajax-upload-profile-pic [blob]
  (let [
        ;name (.-name element)
        ;blob  URL.createObjectURL($("input#"element-id).get(0).files[0]);
        ;blob  (.createObjectURL js/URL  (aget (.-files element  ) 0) )
        ;file (aget (.-files element) 0)
        form-data (doto
                    (js/FormData.)
                    (.append "upload-file" blob))
        ]
    (.close js/jQuery.fancybox)
    (show-loader)
    (POST "/upload-profile-pics" {:headers {"x-csrf-token" (.-value (.getElementById js/document "token"))}
                                  :body form-data
                                  :response-format :json
                                  :keywords? true
                                  :handler handle-response-ok
                                  ;  (session/put! :upload-status status)
                                  :error-handler handle-response-error})


    ))


(defn crop-component [propic-src]
  [:div

   [:div {:class "img-container"}
    [:img {:src propic-src :id "crp-img" } ]
    ]

   [:form {:id "upload-form"
           :enc-type "multipart/form-data"
           :method "POST"}
    [:input {:type "file"
             :on-change (fn [e]
                          ;;(.log js/console (.createObjectURL js/URL  (aget (.-files (-> e .-target)  ) 0) ))
                          ;(cljs-ajax-upload-profile-pic (-> e .-target))

                          (reset! profpic-crop-img (.createObjectURL js/URL  (aget (.-files (-> e .-target)  ) 0) ))

                          (if-not (= "" @profpic-crop-img)
                            (let []
                              (show-loader)

                              ;(js/setTimeout (fn []
                              ;                 (hide-loader)
                              ;                 )
                              ;               1900)


                              (js/setTimeout (fn []
                                               ;(.show (js/jQuery "#crop-popup,#prof-overlay") )

                                               (if (not (= nil @profpic-crop-obj))
                                                 (.destroy @profpic-crop-obj)
                                                 )
                                               (reset! profpic-crop-obj (cropper-js "crp-img"))

                                               (js/jQuery.fancybox (js-obj "href" "#crop-popup"
                                                                           "onStart" (hide-loader)
                                                                           ))

                                               )
                                             2000)



                              )

                            )

                          )
             :name "upload-file"
             :id "upload-file"}]
    ]
   ])


(defn cljs-ajax-upload-file [element-id]
  (let [el (.getElementById js/document element-id)
        name (.-name el)
        file (aget (.-files el) 0)
        form-data (doto
                    (js/FormData.)
                    (.append name file))]
    (POST "/upload-profile-pics" {
                                  :headers {"x-csrf-token" (.-value (.getElementById js/document "token"))}
                                  :body form-data
                                  :response-format :json
                                  :keywords? true
                                  :handler handle-response-ok
                                  :error-handler handle-response-error})
    (set-upload-indicator)))


(defn profile-pic-upload-component []
  (reagent/create-class                 ;; <-- expects a map of functions
    {

     ;:component-did-mount

     :component-will-mount              ;; the name of a lifecycle function
                   (fn []
                     ;(.log js/console "component-will-mount") ;; remember to repeat parameters

                     )

     :display-name "bestway-profile-pic-upload-component"  ;; for more helpful warnings & errors

     :reagent-render        ;; Note:  is not :render
                   (fn []
                     ;(.log js/console "component-is-rendering") ;; remember to repeat parameters

                     (crop-component @profpic-crop-img )

                     )

     }

    )
  )

(defn upload-component [text]
  [:div
   [:form {:id "upload-form"
           :enc-type "multipart/form-data"
           :method "POST"}
    [:label text]
    [:input {:type "file"
             :on-change (fn []
                          (cljs-ajax-upload-file "upload-file")"/user/"
                          )
             :name "upload-file"
             :id "upload-file"}]]])



(defn cljs-ajax-upload-button []
  [:div
   [:hr]
   [:button {:class "btn btn-primary" :type "button"
             :on-click #(cljs-ajax-upload-file "upload-file")}
    "Upload image" [:span {:class "fa fa-upload"}]]])

(defn profile-pic-upload-button []
  [:div
   [:hr]
   [:button {:class "btn btn-primary" :type "button" :id "ppupbtn"
             :on-click #(cljs-ajax-upload-profile-pic "upload-file")

             }
    "Upload image" [:span {:class "fa fa-upload"}]]])


(defn home-page []
  (fn []
    [:div [:h2 "file-upload"]
     [:p "File upload subtitle"]
     [status-component]
     ;[upload-component]
     ;[profile-pic-upload-component]
     ;[crop-component]
     ;[cljs-ajax-upload-button]
     ;[profile-pic-upload-button]
     [:hr]
     ;[:div [:a {:href "#/about"} "go to about page"]]
     ]))


(defn current-page []
  [:div [(home-page)]]
  )



(defn init! []
  ;(reagent/render [current-page] (.getElementById js/document "app"))
  )

(defn ^:export upload-fns []

  (reagent/render-component [profile-pic-upload-component]
                            (.getElementById js/document "app-crop-upload-box")

                            )

  (.addEventListener
    (.getElementById js/document "pro_pic_changer")
    "click"
    (fn [e]
      ;(js/alert "here @ prof pic upload")
      (.val (js/jQuery "#upload-file") "")
      (init!)
      (.trigger (js/jQuery "#upload-file") "click")

      )
    )

  (.addEventListener
    (.getElementById js/document "upload-profic")
    "click"
    (fn [e]
      (.toBlob  (.getCroppedCanvas @profpic-crop-obj) (fn [blob]
                                                        ;;(.log js/console blob)
                                                        (cljs-ajax-upload-profile-pic blob

                                                                                      )
                                                        ) )


      )
    )


  )







