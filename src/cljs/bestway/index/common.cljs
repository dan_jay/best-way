(ns bestway.index.common
  (:require [reagent.core :as reagent]
            [reagent.session :as session]
            [reagent.cookies :as cookies]
            [alandipert.storage-atom :refer [local-storage]]
            )
  )

(defmacro elem-by-id
  "usage nil nil id"
  [id]
  `( .getElementById js/document ~id )
  )

(defonce alert-msg (reagent/atom
                 ;(cookies/get "bw-user")
                 {
                  :msg "This is a message"
                  :type :info
                  }

    )
  )

(def prefstore        (local-storage (atom nil) ::lng) )
(def prefs (reagent/atom (if (nil? @prefstore) {:lng :si} @prefstore)))


;(defn alert [msg alert-type]
;    (swap! alert-msg assoc :msg msg :type alert-type)
;  )

(def app_state (js/jQuery "#app_state"))


;(def  app-state (reagent/atom {
;                           :context (.text (.find app_state "#app_context") )
;                           :path     (.text (.find app_state "#app_pathname") )
;                           :profile-pic  (.text (.find app_state "#user_profile_pic")):fnamelname  (.text (.find app_state "#user_fnamelname") )
;                           })
;)

(if-not (and  (cookies/contains-key? "bw-user") (= (cookies/get "bw-user") "") )
  (do
    (def app-state (reagent/atom {
                                  :context     (.text (.find app_state "#app_context"))
                                  :path        (.text (.find app_state "#app_pathname"))
                                  :user-id     (.text (.find app_state "#user_id"))
                                  :profile-pic (.text (.find app_state "#user_profile_pic"))
                                  :fnamelname (.text (.find app_state "#user_fnamelname"))
                                  })
      )
    (cookies/set! "bw-user" @app-state)
    )

  (def app-state (reagent/atom (cookies/get "bw-user"))

    )
  )

(add-watch app-state :watcher
           (fn [key atom old-state new-state]
             ;(prn "-- Atom Changed --")
             ;(prn "key" key)
             ;(prn "atom" atom)
             ;(prn "old-state" old-state)
             ;(prn "new-state" new-state)

             (cookies/set! "bw-user" new-state)
             )
           )

(def state (reagent/atom {
                          :loader "hidden"
                          }
                         )
  )

(defn into-list [items]
  (into [:ul]
        (for [i items]
          [:li i])))

(defn set-status [class title items]
  [:div {:class class}
   [:h4 title]
   (into-list items)])


(defn alert-popup-component
   ".bw-alert.bw-sucess\n.bw-alert.bw-normal\n.bw-alert.bw-warning\n.bw-alert.bw-error"
   []
  [:div {:class (str "alert bw-alert alert-dismissible ss bw-" (:type @alert-msg) ) :role "alert"}
   [:button {:type "button", :class "close", :data-dismiss "alert", :aria-label "Close"}
    [:span {:aria-hidden "true"} [:i {:class "fa fa-times"} "" ]]
    ]
   [:div
      (:msg @alert-msg)
    ]
   ]
)

(defn show-alert
  "showing the alert on pages combined with alert-msg atom"
  []
  (reagent/render-component
    (alert-popup-component)

    (.getElementById js/document "alert_container")

    )
)

(defn ^:export alert
  "showing the alert on pages combined with alert-msg atom"
  [msg & msg-type]

  (reset! alert-msg {:msg (str msg) :type (if (some #{"info" "normal" "success" "warning" "error"} msg-type) (first msg-type) "normal")})
  (reagent/render-component
    (alert-popup-component)
    (.getElementById js/document "alert_container")
    )
  )

(defn status-component []
  (if-let [status (session/get :upload-status)]
    [:div
     [:h3 "Status"]
     status]))


(defn loader-markup []

  [:div {:id "overlay-wrapper" }
   [:div {:id "overlay-content-box"}
    [:div {:class "loader", :title "Preparing the Bestway for You"}
     [:svg {:width "140px", :height "120px", :xmlns "http://www.w3.org/2000/svg", :viewBox "0 0 100 100", :preserveAspectRatio "xMidYMid", :class "uil-cube"}
      [:rect {:x "0", :y "0", :width "100", :height "100", :fill "none", :class "bk"}]
      [:g {:transform "translate(25 25)"}
       [:rect {:x "-20", :y "-20", :rx "60" :ry "60" :width "40", :height "40", :fill "#F28A3E", :opacity "0.9", :class "cube"}
        [:animateTransform {:keySplines "0.2 0.8 0.2 0.8", :calcMode "spline", :keyTimes "0;1", :type "scale", :repeatCount "indefinite", :begin "0s", :attributeName "transform", :from "1.25", :dur "1s", :to "1"}]]]
      [:g {:transform "translate(75 25)"}
       [:rect {:x "-20", :y "-20", :rx "60" :ry "60" :width "40", :height "40", :fill "#46c40f", :opacity "0.8", :class "cube"}
        [:animateTransform {:keySplines "0.2 0.8 0.2 0.8", :calcMode "spline", :keyTimes "0;1", :type "scale", :repeatCount "indefinite", :begin "0.1s", :attributeName "transform", :from "1.25", :dur "1s", :to "1"}]]]
      [:g {:transform "translate(25 75)"}
       [:rect {:x "-20", :y "-20", :rx "60" :ry "60" :width "40", :height "40", :fill "#3D7DBC", :opacity "0.7", :class "cube"}
        [:animateTransform {:keySplines "0.2 0.8 0.2 0.8", :calcMode "spline", :keyTimes "0;1", :type "scale", :repeatCount "indefinite", :begin "0.3s", :attributeName "transform", :from "1.25", :dur "1s", :to "1"}]]]
      [:g {:transform "translate(75 75)"}
       [:rect {:x "-20", :y "-20", :rx "60" :ry "60"  :width "40", :height "40", :fill "#FFFFFF", :opacity "0.6", :class "cube"}
        [:animateTransform {:keySplines "0.2 0.8 0.2 0.8", :calcMode "spline", :keyTimes "0;1", :type "scale", :repeatCount "indefinite", :begin "0.2s", :attributeName "transform", :from "1.25", :dur "1s", :to "1"}]]]]]]]

  )


(defn loader-component []

  (reagent/create-class
    {
     :component-did-mount  (fn []

                             )
     :component-did-update (fn []

                             )
     :reagent-render       (fn []

                             (loader-markup)


                             )
     }
    )

  )


(defn show-loader
  "show the loading effect with overlay"
  []
  ;(swap! state assoc :loader "")
  (reagent/render-component
    [(loader-component)]

    (.getElementById js/document "loader_container")

    )
  nil
  )

(defn hide-loader
  "hide the loading effect with overlay"
  []
  ;  (swap! state assoc :loader "hidden")
  (reagent/unmount-component-at-node
    (.getElementById js/document "loader_container")
    )

  )

(defn topbar-profile-pic
  "top profile pic markup"
  []

  [:a {:href (str (:context @app-state) "my-profile" ) :on-click #(->(set! (.. js/window -location -href) (str (:context @app-state)
                                                                                                           "my-profile"
                                                                                                       )))}
   [:img {:class "tp_prf_img" :src (:profile-pic @app-state) }]
   [:p {:class "tp_prf_nm"} (:fnamelname @app-state) ]]

  )

(defn main-profile-pic
  []
  [:img {:src (:profile-pic @app-state) }]
  )

(defn home-profile-pic
  []
  [:img {:id "gr_wh_pro_pic" :height "80px" :src (:profile-pic @app-state) :style { :height "80px!important"} }]
  )



(defn top-prof-pic-component []

  (reagent/create-class
    {
     :component-did-mount  (fn []

                             )
     :component-did-update (fn []

                             )
     :reagent-render       (fn []
                             (topbar-profile-pic)
                             )
     }
    )

  )


(defn main-prof-pic-component []

  (reagent/create-class
    {
     :component-did-mount  (fn []

                             )
     :component-did-update (fn []

                             )
     :reagent-render       (fn []
                             (main-profile-pic)
                             )
     }
    )

  )


(defn home-prof-pic-component []

  (reagent/create-class
    {
     :component-did-mount  (fn []

                             )
     :component-did-update (fn []

                             )
     :reagent-render       (fn []
                             (home-profile-pic)
                             )
     }
    )

  )


(.ready (js/jQuery js/document)
        ;(js/console.log @app-state)

        (def home-prof-pic-container (.getElementById js/document "home_img_container"))
        (if (not (nil? home-prof-pic-container))

          (reagent/render-component
            [(home-prof-pic-component)]

            home-prof-pic-container

            )

          )

        ;(def tp_prf_img_con (.getElementById js/document "tp_prf_img_con") )
        ;
        ;(if (not (nil? tp_prf_img_con))
        ;  (reagent/render-component
        ;    [(top-prof-pic-component)]
        ;
        ;    tp_prf_img_con
        ;
        ;    ))

        (def pro_pic_a (.getElementById js/document "pro_pic_a"))

        (if (not (nil? pro_pic_a))
          (reagent/render-component
            [(main-prof-pic-component)]

            pro_pic_a

            ))

        )

        ;(reagent/render-component
        ;  [(alert-popup-component)]
        ;
        ;      (.getElementById js/document "alert_container")
        ;
        ;  )



        ;(->> (js/jQuery ".img-liquid") (.css "height" "243px") (.imgLiquid )  )
        ;(.imgLiquid (js/jQuery ".img-liquid") (js-obj "fill" true) )

