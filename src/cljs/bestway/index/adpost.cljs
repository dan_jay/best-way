(ns bestway.index.adpost
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [cognitect.transit :as t]
            [ajax.core :refer [GET POST]]
            [bestway.utils :as util]
            [bestway.index.common :as common]
            [goog.events :as events]
            [clojure.string :as string])
  )

(def tr (t/reader :json))
;; ------------------------
;; Views
(def entry-data (reagent/atom {:meta-data [] :entry-listing {:category ""}}))
(def cat-spec-markup (reagent/atom [:div ""]))
(def subcats (reagent/atom ""))                             ;; Subcategories of the currently ticked Category


(defn update-cat-spec-markup!
  ""
  [subcat]
  (reset! cat-spec-markup
          ;(cljs.reader/read-string (:markup (filter #(= subcat (get % :_id)) @subcats )))
          ;           (:markup (filter #(= "aps_sub1" (get % :_id)) @subcats ))

         ; ;(.log js/console @subcats)
          (cljs.reader/read-string (:markup (first (filterv #(= subcat (get % :_id)) @subcats))))
          )
  ;;(.log js/console (type (first (filterv #(= "aps_sub1" (get % :_id)) @subcats ))))
  ;;(.log js/console @subcats)

  true)

(defn ^:export update-subcats
  "Refresh and update subcategories dropdown for the given main category Id"
  [cat]
  (reset! subcats  [:option {:value "not-specified"} "Sub Categories"] )
  (reset! cat-spec-markup [:div " "])
      (if cat
      (GET (str util/app-context "post-ad/sub-categories/" cat) {;:headers {"x-csrf-token" (.-value (.getElementById js/document "token"))     }
                                                                 :response-format :transit
                                                                 ;:params [cat]
                                                                 ;:handler #(reset! subcats (cljs.reader/read-string %)) }
                                                                 :handler         (fn [data]
                                                                                    ;(cljs.reader/read-string (:markup (filter #(= subcat (get % :_id )) data )))
                                                                                    ;(print data)
                                                                                    (reset! subcats data)
                                                                                    ;;(.log js/console  @subcats)

                                                                                    ;;(.log js/console (clj->js @subcats))

                                                                                    )
                                                                 }
           )
      )
  )

(defn subcat-component
  ""
  []
  (reagent/create-class {
                         :component-will-mount              ;; the name of a lifecycle function
                                          (fn []
                                            (reset! cat-spec-markup [:div " "])
                                            )
                         :display-name   "bestway-subcat-component" ;; for more helpful warnings & errors

                         :reagent-render (fn []
                                           [:div
                                            [:label "Select Sub Category"]

                                             (into [] (concat [:select {:class "ui fluid dropdown", :id "sub-cat-select" :name "sub_cat" ,
                                                                        :on-change (fn [e]
;                                                                                     (reset! cat-spec-markup [:div ""])
                                                                                     (update-cat-spec-markup! (.-value (.-target e)))
                                                                             )
                                                                        }
                                                               [:option {:value "default"} "Sub Category"]
                                                       ;(map #(-> [:option {:value (:_id %)} (:label %)])  @subcats)
                                                       (apply concat  (for  [sc @subcats] [ [:option {:value (:_id sc)} (:label sc) ] ] ) )

                                                               ]
                                                   )
                                             )

                                             ]


                                           )

                         :component-did-update (fn []
                                                ; (.dropdown (js/jQuery ".dropdown") "restore defaults")
                                                 )
                         :component-did-mount (fn []
                                                (aset (.getElementById js/document "sub-cat-select") "selectedIndex" 0)
                                                )

                         }
                        )
  )

(defn cat-spec-markup-component
  ""
  []
  (reagent/create-class {
                         ;:component-will-mount (fn [])             ;; the name of a lifecycle function


                         :display-name         "bestway-cat-speci-markup-component" ;; for more helpful warnings & errors

                         :reagent-render       (fn []
                                                 @cat-spec-markup
                                                 )
                         :component-did-update (fn []
                                                 ;(.dropdown (js/jQuery ".dropdown") "restore defaults")

                                                 )
                         :component-did-mount (fn []
                                                ;(common/hide-loader)

                                                )
                         }
                        )
  )

(def state_ (reagent/atom {:imgs (into [] (take 8 (repeat "https://www.bestway.lk/img/image.png"))) } ))
(def state-cursor (reagent/cursor state_ [:imgs] ))



(defn on-file-input-change [ev idn]
  (let [el (-> ev .-target)
        name (.-name el)
        file-idx idn

        blob  (.createObjectURL js/URL  (aget (.-files el  ) 0) )

        file  (-> el .-files (aget 0))
        form-data (doto
                    (js/FormData.)
                    (.append name file )
                    (.append "index" file-idx))]





    ;(set-upload-indicator)

    )

  )



(defn ad-pic-upload-box [idn]
  (let [
        ad-box
        [:div {:class "col-md-3 ui segment "}
         [:div {:class "ad-img-wrap"}
          [:img {:class "ui small image" :id (str "ad-img-" idn) :src (nth @state-cursor idn) } ]
          ]

         [:div {:class "ui icon buttons"}
          [:button {:class "ui green button img-btn s-btn" }
           [:i {:class "fa fa-picture-o"} ]
           ]
          [:input {:type "hidden" :name "index" :value idn } ]
          [:input {:class "inputfile" :type "file" :id (str "ad-pic-" idn) :name "upload-file[]"
                   :on-change
                          (fn [ev]
                            (let [
                                  el (-> ev .-target)
                                  name (.-name el)
                                  file-idx idn

                                  blob (.createObjectURL js/URL (aget (.-files el) 0))

                                  ; file (-> el .-files (aget 0))
                                  ; form-data  ; (doto (js/FormData.) (.append name file) (.append "index" file-idx))
                                  ]

                              (reset! state-cursor (assoc @state-cursor file-idx blob) )

                            ;  (set-upload-indicator)

                              ;file
                              ) )
                   } ]

          [:button {:class "ui disabled button img-btn r-btn" :id (str "pic-disc" idn)
                    :on-click (fn [e]
                                (reset! state-cursor (assoc @state-cursor idn "https://www.bestway.lk/img/image.png"))
                                (.removeClass (.addClass (js/jQuery (-> e .-target) ) "disabled") "red")
                                )
                    }
           [:i {:class "fa fa-times"} ]
           ]
          ]
         ]
        ]
    ;      ;(.log js/console ad-box)
    ad-box

    ))


(defn ad-pic-upload-boxes [list-of-items]
  (into [] (concat [:div {:class "row ui segments"}] (apply concat
                                                            (for [one-map list-of-items]
                                                              [
                                                               ;[:button {:class "ui disabled button img-btn r-btn" }]
                                                               (ad-pic-upload-box one-map)
                                                               ]
                                                              ))))
  )


(defn ad-pic-upload-component
  [n]
  (let [

        ]     ;; <-- closed over by lifecycle fns

    (reagent/create-class                 ;; <-- expects a map of functions
      {
       ;:component-did-mount                    (search-resutls)

       :component-will-mount              ;; the name of a lifecycle function
                             (fn []
                               ;(.log js/console "component-will-mount") ;; remember to repeat parameters

                               )
       ; #(;(.log js/console "component-will-mount"))  ;; your implementation

       ;; other lifecycle funcs can go in here
       :component-did-update (fn []
                               ;(js/alert 1)
                               (.imgLiquid (.css  (js/jQuery ".ad-img-wrap")  {:height "150px"}) {
                                                                                                  :fill true

                                                                                                  } )
                               )

       :display-name "bestway-ad-pic-upload-component"  ;; for more helpful warnings & errors

       :reagent-render        ;; Note:  is not :render
                             (fn []
                               ;(.log js/console "component-is-rendering") ;; remember to repeat parameters

                               (ad-pic-upload-boxes (range 0 n) )


                               )
        :component-did-mount (fn []

                               )
       }

      )
    )
  )


(defn ^:export set-cat-spec-markup []
  ;$(".ui.dropdown").dropdown();

  (reagent/render-component [cat-spec-markup-component] (.getElementById js/document "cat-speci"))
  (reagent/render-component [subcat-component] (.getElementById js/document "cat-spec-sub"))
  ;(.dropdown (js/jQuery ".ui.dropdown") )
  (.each (js/jQuery "input.ad_img_url")
         (fn [idx $this]

           (reset! state-cursor (assoc @state-cursor idx (aget $this "value")))

           )

         )

    ;(if-let
    ;        [main_cat (aget (.getElementById js/document "main_cat") "value" false)]
    ;        (.trigger (js/jQuery (str "#" main_cat)) "click")
    ;  )

  )

(if-let [ab (.getElementById js/document "adpicuploadbox")]
  (reagent/render-component
    [(ad-pic-upload-component 8)]
    ab
    )
)

(if-let [ab2 (.getElementById js/document "adpicuploadbox2")]
  (reagent/render-component
    [(ad-pic-upload-component 1)]
      ab2
    )
)


