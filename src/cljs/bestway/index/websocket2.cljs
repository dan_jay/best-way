(ns bestway.index.websocket2
  (:require-macros
    [cljs.core.async.macros :as asyncm :refer (go go-loop)])
  (:require
    ;; <other stuff>
    [cljs.core.async :as async :refer (<! >! put! chan)]
    [taoensso.sente :as sente :refer (cb-success?)] ; <--- Add this
    )
  )




(defonce ws-channels (atom {
                      ;:url chan
                      }))

(defonce ws-chan (atom nil))
;(def json-reader (t/reader :json))
;(def json-writer (t/writer :json))

(defn receive-msg
  [update-fn]
  (fn [msg]
    (update-fn ;msg
     ;(->> msg .-data (t/read json-reader))
     ;(.parse js/JSON msg)
     ;;(.log js/console msg)

      )))

(defn send-msg!
  [id msg receive-handler]
  ;;(.log js/console id)
  ;;(.log js/console (aget (clj->js @ws-channels) id) )
  (def ws (aget (clj->js @ws-channels) id))
  (if  ws
    (do
    (set! (.-onmessage ws)
          (fn [e]
            ;(receive-handler (.parse js/JSON (.-data e)))
            ;;(.log js/console (.parse js/JSON (.-data e) ))
            (receive-handler (.parse js/JSON (.-data e) ));(.parse js/JSON e))
     ))
    (.send ws msg)
     ;(.send @ws-chan (.stringify js/JSON msg))
    )
    (throw (js/Error. "Websocket is not available!")))
)


(defn make-websocket! [id url data receive-handler]
  ;(.log js/console "attempting to connect websocket" url)
  (let [{:keys [chsk ch-recv send-fn ro-watch-state]}
        (sente/make-channel-socket! "/search" ; Note the same path as before
                                    {:host "localhost:8090"
                                     :type :auto ; e/o #{:auto :ajax :ws}
                                     :params {}
                                     })]
    (def chsk       chsk)
    (def ch-chsk    ch-recv) ; ChannelSocket's receive channel
    (def chsk-send! send-fn) ; ChannelSocket's send API fn
    (def chsk-state ro-watch-state)   ; Watchable, read-only atom
    )

  )