(ns bestway.index.login
  (:require [ajax.core :as ajax]
            [ajax.core :refer [GET POST]]
            [goog.events :as events]
    ; [goog.dom :as dom]
            [bestway.index.common :as common])
  )

(defn default-headers [request]
  (-> request
      (update :uri #(str js/context %))
      (update
        :headers
        #(merge
           %
           {"Accept" "application/transit+json"
            "x-csrf-token" js/csrfToken}))))

(defn load-interceptors! []
  (swap! ajax/default-interceptors
         conj
         (ajax/to-interceptor {:name "default headers" }
                              )
         )
  )

(defn ^:export login-and-register []

  ;(.submit (js/jQuery "form#signinfrm") (fn [e]
  ;                                    (let [
  ;                                          form (.getElementById js/document "signinfrm")
  ;                                          url "/"
  ;                                          csrf-token (.-value (.getElementById js/document "token"))
  ;                                          ]
  ;                                      (.preventDefault e)
  ;                                      (POST "/"
  ;                                            ;{:handler      #(js/alert "Submission!")
  ;                                            {:handler (fn [data] (aset js/location "href" "/home"))
  ;                                             ;:content-type "application/json"
  ;                                             ;          :body         (gforms/getFormDataString form )
  ;                                             :body    (.serialize (js/jQuery form))
  ;                                             :headers {:x-csrf-token csrf-token}}
  ;
  ;                                            )
  ;                                      )))

  (.click (js/jQuery "#join") (fn [e]
                                (let [form (.getElementById js/document "signupfrm")
                                      url "/join"
                                      csrf-token (.-value (.getElementById js/document "__anti-forgery-token"))]
                                  ;;(.log js/console url)
                                  (.html (js/jQuery (.-target e)) "<div class='ui active inline loader'></div>")
                                  (.preventDefault e)
                                  (POST "/join"

                                        {;:response-format :json,
                                         :handler (fn [data]
                                                    (if (= (get data "type") "success")
                                                      (do

                                                        (.hide (js/jQuery "#signup-panel") 500)
                                                        (.removeClass (js/jQuery ".success.message") "hidden")
                                                        )
                                                      (do
                                                        (.html (js/jQuery "div.error")
                                                               (str "<p>" (get data "msg") "</p>"));
                                                        (.show (js/jQuery "div.error"))
                                                        )

                                                      )
                                                    )
                                         ;:content-type "application/json"
                                         ;          :body         (gforms/getFormDataString form )
                                         :body    (.serialize (js/jQuery "#signupfrm"))
                                         :headers {:x-csrf-token csrf-token}}

                                        )
                                  )))

  )


(defn send-ajax-post
  "Ajax post request"
  [dest-url {:headers []} {:params []}]
  (POST (str js/context "/save") {:headers {"x-csrf-token" (.-value (.getElementById js/document "token"))}
                                  ;:format :edn
                                  :params []
                                  ;:response-format :edn
                                  ; :handler #(reset! doc %)
                                  }

        )
  )


(.addEventListener
  js/window
  "DOMContentLoaded"
  (fn [] ;(.log js/console "DOM ContentLoaded callback")
    (login-and-register)

    )
  )
