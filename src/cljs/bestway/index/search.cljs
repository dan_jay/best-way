(ns bestway.index.search
  (:require [reagent.core :as reagent]
            [reagent.session :as session]
            [reagent.format :as reformat ]
            [reagent-forms.core :as form]
            [goog.crypt :as gcrypt]
            [goog.uri.utils :as guri]
            [cemerick.url :as curl]
            [clojure.string :as string]
            [bestway.utils :as util]
            [bestway.index.websocket :as bws]
           ; [bestway.index.websocket2 :as bws2]
            [bestway.index.common]
            [taoensso.truss :as truss :refer-macros (have have! have?)]
            )
  )

(def url-base (.text (.find (js/jQuery "#app_state") "#app_context")))
;(js/alert (util/get-app-state "app_context") )
(def ws-base (.text (.find (js/jQuery "#app_state") "#ws_context")))
(def ws-search-url (str ws-base "search"))
(def ws-search-id "ws_search")


(defn returned-empty []
  (fn []
    [:div {:id "no-search-itm"}
       [:div {:class "ui segment"}
        [:p {:id "sr-txt"} "Sorry...!"]
        [:img {:src "https://www.bestway.lk/img/search_err.png"}]
        [:p {:id "no-res-txt"} "No results available for your search"]]]
    ))


;************************* Search page results **************************
(defonce search-pagination (reagent/atom {

                                          :view-type "list"

                                          }))

(def search-results (reagent/atom [(returned-empty)]))

(enable-console-print!)

(defn result-item [list-of-items]
    (into [] (concat [:ul {:id "result-list" :class (:view-type @search-pagination)}] (apply concat (for [one-map list-of-items]
                                                                           [
                                                                            [:li {:class "ad col-md-12"}

                                                                             (if (= (:view-type @search-pagination) "list")

                                                                               [:div {:class "itm_img_con"}
                                                                                [:img {:src (if-not (empty? (aget one-map "imgs"))
                                                                                              (aget (aget (aget one-map "imgs") 0) "url")
                                                                                                                 "https://www.bestway.lk/img/bestway-no-image-s.png"
                                                                                                                 )
                                                                                       :class "itm_img"}
                                                                                 ]
                                                                                ]



                                                                                 )


                                                                             [:div {:class "itm_wrapper"}
                                                                              [:h1 [:a {:href (str (aget one-map "url"))
                                                                                        :title (.-title one-map)
                                                                                        } (.-title one-map)]]
                                                                              (let
                                                                                                     [
                                                                                                      publisher  (aget one-map "publisher_data")

                                                                                                      ]
                                                                                (print one-map )
                                                                                (.log js/console (aget publisher 0))
                                                                                                     (if-not (zero? (count publisher))
                                                                                                       [:a {:href
                                                                                                            (str url-base "profile/"
                                                                                                                 (aget (aget publisher 0) "_id")
                                                                                                                 )
                                                                                                            }

                                                                               (aget  (aget (aget publisher 0) "me") "display-name")" "] "Unknown")
                                                                               )
                                                                              [:span {:class "adtime"} "  published "(aget one-map "publisheddate") ]
                                                                                    ]

                                                                               (if (= (:view-type @search-pagination) "grid")

                                                                                   [:div {:class "itm_img_con"}
                                                                                    [:img {:src (if-not (empty? (aget one-map "imgs"))
                                                                                                  (aget (aget (aget one-map "imgs") 0) "url")
                                                                                                                     "https://www.bestway.lk/img/bestway-no-image-m.png"
                                                                                                                     )
                                                                                           :class "itm_img"}
                                                                                     ]
                                                                                    ]


                                                                                 )



                                                                             [:div {:class "txt_con"}
                                                                               [:p {:class "adprice"} (reformat/currency-format   (aget one-map "price")) ]
                                                                               [:p {:class "ad_txt"} (.-description one-map)]
                                                                               ]
                                                                              ]
                                                                            ]

                                                                           ))))
  )


(def search-vars
  (reagent/atom
    {
     :main-cat     (let [
                         cat-val (str (guri/getParamValue (.-href (.-location js/window)) "main-cat"))
                         ]
                     (if (nil? cat-val) "any" cat-val)
                     )
     :sub-cat      ""
     :query        (str (guri/getParamValue (.-href (.-location js/window)) "query"))
     :perpage      "10"
     :page         "1"
     :loc-district "any"
     :price-min    "any" ; "0.00"
     :price-max    "any" ; "1000000"
     }
    ))




(declare update-results!)


(add-watch search-vars :key
           (fn [k r os ns]
             (.removeClass (js/jQuery "#filter-reset") "disabled")

             (bws/send-msg!
               ws-search-id

               (clojure.walk/stringify-keys @search-vars)

               #(update-results! %)

               )
           )
)

(defn search-pre-loader []
 ; (bestway.index.common/show-loader)
  (fn []
    [:div {:class "ui segment"}
     [:div {:class "ui active dimmer inverted"}
      [:div {:class "ui mini text loader"} "Loading"]
      ]
     ]
   )
)

(defn show-loader[]

  (reagent/render-component (search-pre-loader) (.getElementById js/document "ad-list-con") )
  ;(reagent/unmount-component-at-node (.getElementById js/document "ad-list-con") )

)




(defn show-pagination []
  ; (reagent/render-component
  (if-not (and (empty? @search-pagination) (<= (:total @search-pagination) (:per-page @search-pagination)))
    (let [
          paging @search-pagination
          total (:total paging)
          per-page (:per-page paging)
          pages (Math/ceil (/ total per-page))
          page (:page paging)
          ]
      ; (js/console.log pages)
      ;(js/console.log total)

      (into [] (concat [:div {:class "ui pagination menu"}]
                       (apply concat
                              (for [one-map (range 1 (inc pages))]

                                (if (identical? one-map page)
                                  [
                                   [:div {:class "item active"}
                                    one-map
                                    ]
                                   ]

                                  [
                                   [:button {:class "item" :on-click (fn [e]
                                                                       (swap! search-vars assoc :page (str one-map))
                                                                       (bws/send-msg!
                                                                         ws-search-id
                                                                         (clojure.walk/stringify-keys @search-vars)

                                                                         #(update-results! %)

                                                                         )
                                                                       )}
                                    one-map
                                    ]
                                   ]
                                  )

                                )
                              )
                       )
            )
      )


    [:div ""]
    )
  ; (.getElementById js/document "pagi-con-top") )
)
(declare update-search)

(defn search-grid-list-component []
  (if (= (:view-type @search-pagination) "grid")
    (do
      [:div {:class " ui right buttons "}
       [:span {:id "grid-viewer", :class "btn btn-info grid" :on-click (fn [e]

                                                                         (swap! search-pagination assoc :view-type "grid")
                                                                         (update-search)
                                                                         )}
        [:i {:class "fa fa-th-large"}]]
       [:span {:id "list-viewer", :class "btn btn-info list" :on-click (fn [e]

                                                                         (swap! search-pagination assoc :view-type "list")
                                                                         (update-search)
                                                                         )
               }
        [:i {:class "fa fa-th-list"}] ]
       ]
      )
    (do
      [:div {:class " ui right buttons "}
       [:span {:id "list-viewer", :class "btn btn-info list" :on-click (fn [e]

                                                                         (swap! search-pagination assoc :view-type "grid")
                                                                         (update-search)

                                                                         )}
        [:i {:class "fa fa-th-large"}]]
       [:span {:id "list-viewer", :class "btn btn-info grid" :on-click (fn [e]

                                                                         (swap! search-pagination assoc :view-type "list")
                                                                         (update-search)

                                                                         )}
        [:i {:class "fa fa-th-list"}]]
       ]
      )
    )

  )

(defn update-results! [results]
  ;(.log js/console results)
  (def items (aget results "results"))
  (if-not (empty? items)
    (do
      (reset! search-results (result-item items ) )
      (reset! search-pagination {:total (.-total results) :per-page (aget results "per-page") :page (.-page results) :view-type "list" })
      (reagent/render-component (search-grid-list-component) (.getElementById js/document "grid-list-wrapper"))
      (reagent/render-component (show-pagination) (.getElementById js/document "pagi-con-top"))
      (reagent/render-component (show-pagination) (.getElementById js/document "pagi-con-bot"))
      )
    (reset! search-results (returned-empty))
  )


  ;(show-pagination @search-pagination)

  ;(show-pagination)


  ;;(.log js/console search-pagination)
)

(defn update-search
  "js search result list update with websock"
  []
  (bws/send-msg!
    ws-search-id
    (clojure.walk/stringify-keys @search-vars)

    #(update-results! %)

    )
  )




(defn search-result-component
  []
  (let [local "ul.list"
        shared "state"
        results @search-results
        ]                                                   ;; <-- closed over by lifecycle fns

    (reagent/create-class                                   ;; <-- expects a map of functions
      {
       ;:component-did-mount                (show-pagination @search-pagination)

       :component-will-mount                                ;; the name of a lifecycle function
                     (fn []
                       ;(.log js/console "component-will-mount") ;; remember to repeat parameters
                       (reagent/render-component search-grid-list-component (.getElementById js/document "grid-list-wrapper"))
                       )
       ; #(;(.log js/console "component-will-mount"))  ;; your implementation

       ;; other lifecycle funcs can go in here

       :display-name "bestway-search-results-component"     ;; for more helpful warnings & errors

       :reagent-render                                      ;; Note:  is not :render
                     (fn []
                       ;(.log js/console "component-is-rendering") ;; remember to repeat parameters


                       @search-results

                       )

       }

      )
    )
  )                                                         ;; end of  search-result-component


                                                      ;; end of  home-listing-component

;; (defmacro _id [id]
;;   (.getElementById js/document (str id))
;; )



(defn price-valid?
   "Validate price value"
   [price]
    (not= (re-matches #"^[0-9]{0,8}[0-9,]*[0-9]\.?[0-9]{0,2}$" (str price)) nil)
   )


(defn pg-on-load []



                                  (def ad-list-con (.getElementById js/document "ad-list-con"))
                                  (if (not (nil? ad-list-con))
                                    (do

                                      (bws/make-websocket! ws-search-id ws-search-url
                                                           (clojure.walk/stringify-keys @search-vars)

                                                           #(update-results! %)
                                                           )

                                      (reagent/render-component [(search-result-component)] ad-list-con)




                                      ;(reagent/render (show-pagination)
                                      ;                (.getElementById js/document "pagi-con-top"))
                                      ;
                                      ;   (reagent/render (show-pagination)
                                      ;                (.getElementById js/document "pagi-con-bot"))

                                      )

                                    )


                                  (let [
                                        this-url (.-href (.-location js/window))
                                        q (guri/getParamValue this-url "query")
                                        main-cat (guri/getParamValue this-url "main-cat")
                                        loc (guri/getParamValue this-url "loc-district")
                                        ]
                                    (.val (js/jQuery "input#txt-query") #(str q))
                                    (.val (js/jQuery "select#cat-select") #(str main-cat))
                                    (.val (js/jQuery "select#loc-select") #(str loc))

                                    (doto (js/jQuery "select") (.trigger "chosen:updated") (.trigger "change") )

                                  ;  (swap! search-vars update-in "query" #(str q))
                                    )


                                  ;;;;;;
                                  (.click (js/jQuery "button#filter-reset")
                                          (fn [e]
                                            (reset! search-vars
                                                    {
                                                     :main-cat     "any"
                                                     :sub-cat      "any"
                                                     :query        "" ;(guri/getParamValue (.-href (.-location js/window)) "query")
                                                     :perpage      "10"
                                                     :page         "1"
                                                     :loc-district "any"
                                                     :price-min    "0.00"
                                                     :price-max    "1000000.00"
                                                     }
                                                    )


                                           ; (reagent/force-update-all)
                                            (doto (js/jQuery "#cat-select,#sub-cat-select,#loc-select") (.trigger "chosen:updated") (.trigger "change") )
                                            (.addClass (js/jQuery "button#filter-reset") "disabled")
                                            ))

                                  ;;;;;;
                                  (.change (js/jQuery "#loc-select")
                                           (fn [e]


                                               (let [data-value (.val (js/jQuery "#loc-select"))]
                                                ; (js/alert data-value)
                                                 (swap! search-vars update-in [:loc-district] #(str data-value))
                                                 )

                                             ;(.log js/console @search-vars )
                                             (bws/send-msg!
                                               ws-search-id
                                               (clojure.walk/stringify-keys @search-vars)

                                               #(update-results! %)

                                               )
                                             ))

                                  ;;;;;;
                                  (.change (js/jQuery "#cat-select")
                                           (fn [e]
                                             (this-as this
                                               (let [data-value (.val (js/jQuery this))]
                                                 ;(js/alert data-value)
                                                 (swap! search-vars update-in [:main-cat] #(str data-value))
                                                 )
                                               )

                                             (bws/send-msg!
                                               ws-search-id
                                               (clojure.walk/stringify-keys @search-vars)

                                               #(update-results! %)

                                               )
                                             ))


                                  ;;;;;;
                                  (.change (js/jQuery "#res-p-page")
                                           (fn [e]

                                               (let [data-value (.val (js/jQuery "#res-p-page"))]
                                                 ; (js/alert data-value)
                                                 (swap! search-vars update-in [:perpage] (str data-value))
                                                 )


                                             (bws/send-msg!
                                               ws-search-id
                                               (clojure.walk/stringify-keys @search-vars)

                                               #(update-results! %)

                                               )
                                             ))

                                  ;;;;;;
                                  (.on (js/jQuery "#sub-cat-select" ) "change"
                                           (fn [e]

                                             ;;(.log js/console @search-vars )


                                               (let [data-value (.val (js/jQuery "select#sub-cat-select"))]
                                                 ;(js/console.log data-value )
                                                 (swap! search-vars update-in [:sub-cat] #(str data-value))
                                                 )


                                             (bws/send-msg!
                                               ws-search-id
                                               (clojure.walk/stringify-keys @search-vars)

                                               #(update-results! %)

                                               )
                                             ))

                                  ;;;;;;
                                  (.click (js/jQuery "button#price-refine")
                                          (fn [e]
                                            (.preventDefault e)
                                            ;;(.log js/console @search-vars )

                                            (let [
                                                  min-value (.val (js/jQuery "input#min-price"))
                                                  max-value (.val (js/jQuery "input#max-price"))
                                                  ]
                                              ;(js/alert data-attr))
                                              (if (and (price-valid? min-value) (price-valid? max-value) (> max-value min-value))
                                                (swap! search-vars assoc
                                                       :price-min min-value
                                                       :price-max max-value
                                                       )
                                                )


                                              )


                                            (bws/send-msg!
                                              ws-search-id
                                              (clojure.walk/stringify-keys @search-vars)

                                              #(update-results! %)

                                              )
                                            ))

                                  ;;;;;;
                                  (.click (js/jQuery "button#btn-query")
                                          (fn [e]
                                            (.preventDefault e)
                                            ;;(.log js/console @search-vars )

                                            (let [data-value (.val (js/jQuery "input#txt-query"))]
                                              ;(js/alert data-attr))
                                              (swap! search-vars update-in [:query] #(str data-value))
                                              )


                                            (bws/send-msg!
                                              ws-search-id
                                              (clojure.walk/stringify-keys @search-vars)

                                              #(update-results! %)

                                              )
                                            ))

                                  ;;;;;;


                                  (.blur (js/jQuery "input#min-price")
                                         (fn [e]

                                           ;;(.log js/console @search-vars )
                                           (this-as this
                                             (let [data-value (.val (js/jQuery this))]
                                               ;(js/alert data-attr))
                                               (if (price-valid? data-value)
                                                 (swap! search-vars update-in [:price-min] #(str data-value))
                                                 )
                                               )
                                             )

                                           (bws/send-msg!
                                             ws-search-id
                                             (clojure.walk/stringify-keys @search-vars)

                                             #(update-results! %)

                                             )
                                           ))

                                  ;;;;;;
                                  (.blur (js/jQuery "input#max-price")
                                         (fn [e]

                                           ;;(.log js/console @search-vars )

                                             (let [data-value (.val (js/jQuery "input#max-price"))]
                                               ;(js/alert data-attr))
                                               (if (price-valid? data-value)
                                                 (swap! search-vars update-in [:price-max] #(str data-value))
                                                 )
                                               )


                                           (bws/send-msg!
                                             ws-search-id
                                             (clojure.walk/stringify-keys @search-vars)

                                             #(update-results! %)

                                             )
                                           ))


                                  )



(.ready (js/jQuery "document") (pg-on-load))

