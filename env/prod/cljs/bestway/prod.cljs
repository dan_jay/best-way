(ns bestway.prod
  (:require
    [bestway.index.settings :as settings]
    ))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

;(core/init!)
