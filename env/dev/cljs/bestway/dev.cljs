(ns ^:figwheel-no-load bestway.dev
  (:require [bestway.index.common :as common]
            [devtools.core :as devtools]
            [figwheel.client :as figwheel :include-macros true]))

(enable-console-print!)

(figwheel/watch-and-reload
  :websocket-url "ws://localhost:3449/figwheel-ws"
  :build- ["app"]
 ; :on-jsload (search/update-search)
  )

(devtools/install!)

;(core/init!)
