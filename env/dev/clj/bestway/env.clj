(ns bestway.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [bestway.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[bestway started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[bestway has shut down successfully]=-"))
   :middleware wrap-dev
   })
