(ns user
  (:require
            [mount.core :as mount]
            [bestway.figwheel :refer [start-fw stop-fw cljs]]
            [bestway.core :as core]

            )
  )

(defn start []
  (mount/start-without #'core/http-server
                       #'core/server-ws
                       #'core/repl-server

                       ))

(defn stop []
  (mount/stop-except ;#'bestway.core/stop-server
    #'core/repl-server))

(defn restart []
  (stop)
  (start))
