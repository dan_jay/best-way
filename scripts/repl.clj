(use 'figwheel-sidecar.repl-api)
(start-figwheel! {:figwheel-options {
                                     :build-ids ["app"]
                                     :all-builds true
                                     }
                  }) ;; <-- fetches configuration
(cljs-repl)