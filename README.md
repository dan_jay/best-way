# bestway

generated using Luminus version "2.9.10.71"

Advertisement Platform with live local news feed.

## TODO
Improve chat service

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run


To compile clojurescript, run:

    lein cljsbuild auto


